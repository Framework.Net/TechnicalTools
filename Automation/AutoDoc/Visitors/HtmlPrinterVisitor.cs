﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;


namespace TechnicalTools.Automation.AutoDoc.Visitors
{
    public class HtmlPrinterVisitor : IDocVisitor<object>
    {
        public virtual string Print(IReadOnlyCollection<DocBlock> blocks, string title)
        {
            _sbs = new Stack<StringBuilder>();
            _sbs.Push(new StringBuilder());
            _sb.AppendLine("<!doctype html>");
            _sb.AppendLine("<html lang=\"fr\">");
            _sb.AppendLine("<head>");
            WriteHeaderContent(title);
            _sb.AppendLine("</head>");
            _sb.AppendLine("<body>");
            WriteBodyContent(blocks);
            _sb.AppendLine("</body>");
            _sb.AppendLine("</html>");
            return _sb.ToString();
        }
        protected const string BR = "<br />"; // space for old netscape
        protected StringBuilder _sb { get { return _sbs.Peek(); } }
        protected Stack<StringBuilder> _sbs = new Stack<StringBuilder>();

        protected virtual void WriteHeaderContent(string title)
        {
            _sb.AppendLine("  <meta charset=\"utf-8\">");
            _sb.AppendLine("  <title>" + title + "</title>");
            _sb.AppendLine("  <style type=\"text/css\">");
            WriteStyleContent();
            _sb.AppendLine("  </style>");
        }
        protected virtual void WriteStyleContent()
        {
            _sb.AppendLine("ol");
            _sb.AppendLine("{");
            _sb.AppendLine("  list-style-type: square;");
            _sb.AppendLine("  list-style-position: outside;");
            _sb.AppendLine("}");
            _sb.AppendLine("ol." + SubBulletListHtmlClass);
            _sb.AppendLine("{");
            _sb.AppendLine("  list-style-type: circle;");
            _sb.AppendLine("  list-style-position: outside;");
            _sb.AppendLine("}");
        }
        protected virtual void WriteBodyContent(IReadOnlyCollection<DocBlock> blocks)
        {
            foreach (var b in blocks)
                b.Accept(this);
        }

        object IDocVisitor<object>.Visit(DocText b)
        {
            var trimmed = b.NoTrim ? b.Text : b.Text?.Trim(' ');
            if (trimmed?.Length > 0)
            {
                if (_sb.Length > 0 &&
                    !char.IsWhiteSpace(_sb[_sb.Length - 1]))
                    _sb.Append(' ');

                _sb.Append(trimmed.Replace(Environment.NewLine, BR + Environment.NewLine));
            }
            return null;
        }

        object IDocVisitor<object>.Visit(DocContainer  b) { return VisitContainer(b, "", ""); }
        object IDocVisitor<object>.Visit(DocBold       b) { return VisitContainer(b, "<b>", "</b>"); }
        object IDocVisitor<object>.Visit(DocItalic     b) { return VisitContainer(b, "<i>", "</i>"); }
        object IDocVisitor<object>.Visit(DocUnderlined b) { return VisitContainer(b, "<u>", "</u>"); }
        object IDocVisitor<object>.Visit(DocColor      b) { return VisitContainer(b, $"<span style=\"color: {ColorTranslator.ToHtml(b.Color)};\">", "</span>"); }
        object IDocVisitor<object>.Visit(DocParagraph  b) { return VisitContainer(b, "<p>", "</p>"); }
        protected virtual string VisitContainer(DocContainer b, string htmlStartTag, string htmlEndTag)
        {
            _sbs.Push(new StringBuilder());

            foreach (var sb in b.Blocks)
                sb.Accept(this);

            var peak = _sbs.Pop();
            if (peak.Length > 0)
            {
                _sb.Append(htmlStartTag);
                _sb.Append(peak.ToString());
                _sb.Append(htmlEndTag);
            }
            return null;
        }

        object IDocVisitor<object>.Visit(DocBulletList b)
        {
            if (bulletListDepth > 0)
                _sb.Append("<ol class=\"" + SubBulletListHtmlClass + "\">");
            else
                _sb.Append("<ol>");
            ++bulletListDepth;
            foreach (var bullet in b.Bullets)
                VisitBullet(bullet);
            --bulletListDepth;

            _sb.AppendLine("</ol>");

            return null;
        }
        int bulletListDepth = 0;

        protected virtual void VisitBullet(DocBlock bullet)
        {
            _sb.Append("<li>");
            bullet.Accept(this);
            _sb.AppendLine("</li>");
        }
        public static readonly string SubBulletListHtmlClass = "subList";

        object IDocVisitor<object>.Visit(DocDocumentTitle b)
        {
            _sb.AppendLine("<h1>" + b.Text + "</h1>");
            return null;
        }
        object IDocVisitor<object>.Visit(DocMainTitle b)
        {
            _sb.AppendLine("<h2>" + b.Text + "</h2>");
            return null;
        }
        object IDocVisitor<object>.Visit(DocTinyTitle b)
        {
            _sb.AppendLine("<h3>" + b.Text + "</h3>");
            return null;
        }

        protected string CompleteLastBlankLineUpTo(int nbNewLineWished)
        {
            var nl = BR + Environment.NewLine;
            var nbNewLineCurrent = CountLastExistingEmptyLines();
            var spaces = Enumerable.Range(0, nbNewLineWished).Select(_ => nl).Join(string.Empty);
            var nbSpaces = Math.Max(nbNewLineWished - nbNewLineCurrent, 0);
            return spaces.Truncate(nbSpaces * nl.Length);
        }

        int CountLastExistingEmptyLines()
        {
            return CountLastExistingEmptyLines(new Stack<StringBuilder>(new Stack<StringBuilder>(_sbs)));
        }
        static int CountLastExistingEmptyLines(Stack<StringBuilder> sbs)
        {
            int count = 0;
            var nl = "<br/>"; // no space like BR here

            while (sbs.Count > 0)
            {
                var sb = sbs.Pop();
                int i = sb.Length - 1;
                while (i >= 0)
                {
                    int n = nl.Length - 1;
                    while (n >= 0 && i >= 0)
                    {
                        // Eat closing tags
                        if (sb[i] == '>' &&
                            n == nl.Length - 1) // not beginning to eat nl
                        {
                            var k = i - 1;
                            while (k >= 0 && char.IsWhiteSpace(sb[k]))
                                --k;
                            while (k >= 0 && char.IsLetterOrDigit(sb[k]))
                                --k;
                            while (k >= 0 && char.IsWhiteSpace(sb[k]))
                                --k;
                            if (k >= 0 && sb[k] == '/')
                                --k;
                            if (k >= 0 && sb[k] == '<')
                            {
                                i = k - 1;
                                continue;
                            }
                        }
                        if (sb[i] == nl[n])
                        {
                            --i;
                            --n;
                        }
                        else if (char.IsWhiteSpace(sb[i])) // html does not care about spaces
                            --i;
                        else
                            break;
                    }
                    if (n >= 0)
                        return count - 1; // -1 // We don't count the end of line after any text
                    ++count;
                }
            }
            return count;
        }
    }
}
