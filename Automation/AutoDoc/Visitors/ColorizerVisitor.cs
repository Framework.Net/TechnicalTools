﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;


namespace TechnicalTools.Automation.AutoDoc.Visitors
{
    public class ColorizerVisitor : IDocVisitor<DocBlock>
    {
        public class Colors
        {
            public Color DocumentTitleColor { get; set; } = Color.FromArgb(82, 67, 170);
            public Color MainTitleColor { get; set; } = Color.FromArgb(55, 65, 183);
            public Color TinyTitleColor { get; set; } = Color.FromArgb(55, 65, 183);
            public Color ParagraphColor { get; set; } = Color.Black;
        }

        public ColorizerVisitor(Colors options) { _options = options; }
        readonly Colors _options;

        public List<DocBlock> Colorize(IReadOnlyCollection<DocBlock> blocks)
        {
            return blocks.Select(b => b.Accept(this)).ToList();
        }

        DocBlock IDocVisitor<DocBlock>.Visit(DocText b) { return b; }
        DocBlock IDocVisitor<DocBlock>.Visit(DocContainer b) { return b; }
        DocBlock IDocVisitor<DocBlock>.Visit(DocBold b) { return b; }
        DocBlock IDocVisitor<DocBlock>.Visit(DocItalic b) { return b; }
        DocBlock IDocVisitor<DocBlock>.Visit(DocUnderlined b) { return b; }
        DocBlock IDocVisitor<DocBlock>.Visit(DocColor b) { return b; }
        DocBlock IDocVisitor<DocBlock>.Visit(DocParagraph b) { return new DocColor(_options.ParagraphColor, new[] { b }); }
        DocBlock IDocVisitor<DocBlock>.Visit(DocBulletList b) { return b; }
        DocBlock IDocVisitor<DocBlock>.Visit(DocDocumentTitle b) { return new DocColor(_options.DocumentTitleColor, new[] { b }); }
        DocBlock IDocVisitor<DocBlock>.Visit(DocMainTitle b) { return new DocColor(_options.MainTitleColor, new[] { b }); }
        DocBlock IDocVisitor<DocBlock>.Visit(DocTinyTitle b) { return new DocColor(_options.TinyTitleColor, new[] { b }); }
    }
}
