﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

using TechnicalTools.Model;


namespace TechnicalTools.Automation
{
    /// <summary>
    /// Allow to flag property as filled by argument on command line.
    /// Properties that are marked with this attribute are from classes that inherit from  <see cref="CommandLineTask"/>.
    /// There is also some other properties from config classes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ArgumentAttribute : Attribute, IHasFacetValues
    {
        [Description("A business or technical description about what this argument do.")]
        public string Description       { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        [Description("Tell if the argument MUST be specified.")]
        public bool   IsMandatory       { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        [Description("Default value when IsMandatory is false.")]
        public object DefaultValue      { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        [Description("Interval of value supported by argument (when there is some caveats).")]
        public string AllowedValueRange { get => GetClassFacetValue<string>(); set => SetClassFacetValue(value); }
        [Description("Indicate the name to use on command line to fill this value,\r\n" +
                     "by default the argument on command line is the same as property name (case is insensitive though).\r\n" +
                     "Value must follow pattern " + reArgumentNamePattern)]
        public string ArgName           { get { return GetClassFacetValue<string>(); } set { SetClassFacetValue(value); ValidateArgName(); } }
        [Description("Tell if this argument is to avoid. When true it means the argument may be removed later")]
        public bool   IsObsolete        { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }
        [Description("Hide the argument of task documentation except if " + nameof(AutoDoc.Help.ShowHiddenArguments) + " is true")]
        public bool Hidden { get => GetClassFacetValue<bool>(); set => SetClassFacetValue(value); }

        public    IReadOnlyDictionary<string, object>   Values { get { return  _Values; } }
        protected          Dictionary<string, object>  _Values { get { return __Values ?? (__Values = new Dictionary<string, object>()); } }
           [NonSerialized] Dictionary<string, object> __Values;
        protected void SetClassFacetValue<T>(T value, [CallerMemberName] string facetName = null)
        {
            _Values[facetName] = value;
        }
        protected T    GetClassFacetValue<T>(         [CallerMemberName] string facetName = null)
        {
            if (_Values.TryGetValue(facetName, out object v))
                return v is T ? (T)v : default(T);
            return default(T);
        }


        void ValidateArgName()
        {
            if (ArgName == null)
                return;
            if (string.IsNullOrWhiteSpace(ArgName))
                throw new TechnicalException("ArgName value is invalid!", null);
            if (ArgName != ArgName.Trim())
                throw new TechnicalException("ArgName value should not contain any space!", null);
            if (reArgumentName.IsMatch(ArgName))
                throw new TechnicalException($"ArgName value does not respect pattern \"{reArgumentName.ToString()}\"!", null);
        }
        const string reArgumentNamePattern = "[a-zA-Z_][a-zA-Z_0-9]*";
        static readonly Regex reArgumentName = new Regex("^" + reArgumentNamePattern + "$", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public string GetEffectiveArgumentName(PropertyInfo property)
        {
            return ArgName ?? property.Name;
        }
        public string AsCommandLinePrefix(PropertyInfo property)
        {
            return "--" + GetEffectiveArgumentName(property) + "=";
        }

        public string AsCommandLineArgument(PropertyInfo property, string value = null)
        {
            var quoteMaybe = value == null // case that we almost never need (maybe in some back compatibility case)
                          || !reSimpleWindowsArgumentValueThatDontNeedQuote.IsMatch(value)
                          ? "\"" : "";
            return AsCommandLinePrefix(property) + quoteMaybe + (value ?? "") + quoteMaybe;
        }
        static readonly Regex reSimpleWindowsArgumentValueThatDontNeedQuote = new Regex("^" + reArgumentNamePattern + "$", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public static Dictionary<PropertyInfo, ArgumentAttribute> GetSetOn(object obj)
        {
            IEnumerable<KeyValuePair<PropertyInfo, ArgumentAttribute>> getPropertiesOfType(Type t)
            {
                return t.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy)
                        .Select(p => new KeyValuePair<PropertyInfo, ArgumentAttribute>(p, p.GetCustomAttribute<ArgumentAttribute>()))
                        .Where(pt => pt.Value != null)
                        .Concat(t.BaseType == null
                                ? Enumerable.Empty<KeyValuePair<PropertyInfo, ArgumentAttribute>>()
                                : getPropertiesOfType(t.BaseType));
            }
            var objType = obj.GetType();
            var argProps = getPropertiesOfType(objType).GroupBy(kvp => kvp.Key.Name)
                                                        .CheckOnline(grp => grp.Any(kvp => kvp.Key.GetSetMethod(true) != null),
                                                                    (grp, i) => new TechnicalException("Property " + grp.First().Key.DeclaringType?.Name.IfNotBlankAddSuffix(".") + grp.First().Key.Name +
                                                                                                        (grp.Count() == 1 ? "" : " or one of the base property of same name") +
                                                                                                        " must have a setter according to attribute " + nameof(ArgumentAttribute) + "!",
                                                                                                        null))
                                                        .Select(grp => grp.FirstOrDefault(kvp => kvp.Key.GetSetMethod(true) != null))
                                                        .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            var duplicates = argProps.GroupBy(defArg => defArg.Value.GetEffectiveArgumentName(defArg.Key).ToLowerInvariant())
                                        .Where(grp => grp.Count() > 1)
                                        .ToList();
            if (duplicates.Any())
                throw new TechnicalException($"Task {objType} has properties bound to same ArgName: " + Environment.NewLine +
                                                duplicates.Select(grp => $" Argument \"{grp.Key}\" is bound to properties: " +
                                                                        grp.Select(defArg => defArg.Key.Name).Join()).Join(Environment.NewLine), null);

            return argProps;
        }
    }
}
