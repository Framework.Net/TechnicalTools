﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace TechnicalTools.Model.Cache
{
    public partial class CacheManager
    {
        /// <summary> L'instance de IHolder retourné doit être disposé ! </summary>
        public IHolder CreateCacheHolder<TObject>()
              where TObject : class, IHasMutableTypedId
        {
            CacheListUI<TObject> lst = GetCounterByTypeOrCreate<TObject>();
            return lst.CreateCacheHolder();
        }

        public partial interface ICacheList
        {
            IReadOnlyList<IHolder> CacheHolders { get; }
            int CacheHolderCount { get; } // Evite de locker
        }

        partial class CacheListUI<T>
        {
            public IReadOnlyList<IHolder> CacheHolders
            {
                get
                {
                    lock (this)
                        return _cacheHolders.ToList();
                }
            }
            public int CacheHolderCount { get { return _cacheHolders.Count; } }

            readonly Dictionary<ITypedId, T> _instancesHeld = new Dictionary<ITypedId, T>(); // pour garder une reference forte sur les objets
            readonly List<Holder<T>>         _cacheHolders  = new List<Holder<T>>(); // pour garder une trace des holders pris

            public Holder<T> CreateCacheHolder()
            {
                var holder = new Holder<T>(this);
                lock (this)
                {
                    _cacheHolders.Add(holder);
                    if (_cacheHolders.Count == 1)
                            foreach (var obj in GetEnumerator())
                                _instancesHeld.Add(obj.TypedId, obj);
                }
                return holder;
            }

            internal void RemoveHolder(Holder<T> holder)
            {
                lock (this)
                {
                    _cacheHolders.Remove(holder);
                    if (_cacheHolders.Count == 0)
                        _instancesHeld.Clear();
                }
            }

        }




        public interface IHolder : IDisposable
        {
            ICacheList Owner { get; }
            Type TypeHeld { get; }
        }

        public static bool CaptureStackTraceOfHolders;

        internal class Holder<T> : IHolder
            where T : class, IHasMutableTypedId
        {
            public ICacheList Owner     { get; private set; }
            public Type       TypeHeld  { get { return typeof(T); } }
            public StackTrace Stack     { get; private set; }

            internal Holder(ICacheList owner)
            {
                Owner = owner;
                if (CaptureStackTraceOfHolders)
                    Stack = new StackTrace(1, true);
            }

            public void Dispose()
            {
                if (_disposed)
                    throw new TechnicalException("This cache holder has already been disposed !", null);
                ((CacheListUI<T>)Owner).RemoveHolder(this);
                _disposed = true;
            }

            bool _disposed;

        }
    }
}

