﻿using System;


namespace TechnicalTools.Model.Cache
{
    public interface ILoadableById
    {
        void LoadById(object id);
        void Reload();
    }

    public interface ILoadableById<in TId>
    where TId : struct, IEquatable<TId>
    {
        void LoadById(TId id);
        void Reload();
    }
    public interface ILoadableByCompositeId
    {
        void LoadById(ICompositeTypedId id);
    }
}
