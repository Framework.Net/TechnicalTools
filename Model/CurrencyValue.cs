﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

using TechnicalTools;


namespace TechnicalTools.Model
{
    public struct CurrencyValue
    {
        public readonly decimal Value;

        public CurrencyISO Currency { get { return CurrencyISO.AllByCode[_ccyCode]; } }
                 readonly ushort __ccyCode;
        internal ushort _ccyCode { get { return __ccyCode == 0 ? (ushort)999 : __ccyCode; } }

        public CurrencyValue(decimal value, CurrencyISO ccy)
        {
            Value = value;
            __ccyCode = ccy.Code;
        }
        internal CurrencyValue(decimal value, ushort ccyIndex)
        {
            Value = value;
            __ccyCode = ccyIndex;
        }

        private static void CheckCurrencyEquality(CurrencyValue left, CurrencyValue right)
        {
            if (left._ccyCode != right._ccyCode)
                throw new TechnicalException("Cannot sum amount of different currency : " +
                                             left.Currency.Ccy + ", " + right.Currency.Ccy, null);
        }

        public static CurrencyValue operator +(CurrencyValue left, CurrencyValue right)
        {
            CheckCurrencyEquality(left, right);
            return new CurrencyValue(left.Value + right.Value, left._ccyCode);
        }
        public static CurrencyValue operator -(CurrencyValue left, CurrencyValue right)
        {
            CheckCurrencyEquality(left, right);
            return new CurrencyValue(left.Value - right.Value, left._ccyCode);
        }
        public static CurrencyValue operator *(CurrencyValue left, decimal factor)
        {
            return new CurrencyValue(left.Value * factor, left._ccyCode);
        }
        public static CurrencyValue operator *(decimal factor, CurrencyValue right)
        {
            return new CurrencyValue(right.Value * factor, right._ccyCode);
        }
        public static CurrencyValue operator /(CurrencyValue left, decimal divisor)
        {
            return new CurrencyValue(left.Value / divisor, left._ccyCode);
        }
        public static decimal operator /(CurrencyValue left, CurrencyValue right)
        {
            return left.Value / right.Value;
        }
        public static CurrencyValue operator -(CurrencyValue cvalue)
        {
            return new CurrencyValue(-cvalue.Value, cvalue._ccyCode);
        }

        public static CurrencyValueList operator &(CurrencyValue left, CurrencyValue right)
        {
            if (left._ccyCode == right._ccyCode)
                return new CurrencyValueList(left + right);
            else
                return new CurrencyValueList(left, right);
        }


        public override string ToString()
        {
            return ToString("n" + (Currency.MinorUnits ?? 2));
        }

        public string ToString(string format)
        {
            return Value.ToString(format) + " " + (Currency.Symbol ?? Currency.Ccy);
        }
    }

    public static class CurrencyValue_Extensions
    {
        public static CurrencyValue As(this decimal value, CurrencyISO ccy)
        {
            return new CurrencyValue(value, ccy);
        }

        public static CurrencyValueList Join(this IEnumerable<CurrencyValue> values)
        {
            return new CurrencyValueList(values, false);
        }


    }

    public class CurrencyValueList
    {
        readonly List<CurrencyValue> _list;

        public int DistinctCurrencyCount { get { return _list.Count; } }

        internal CurrencyValueList(params CurrencyValue[] cvalues)
        {
            _list = new List<CurrencyValue>(cvalues.Length);
            foreach (var cvalue in cvalues)
                _list.Add(cvalue);
        }
        internal CurrencyValueList(IEnumerable<CurrencyValue> cvalues)
        {
            _list = new List<CurrencyValue>(cvalues.Count());
            foreach (var cvalue in cvalues)
                _list.Add(cvalue);
        }
        public CurrencyValueList(IEnumerable<CurrencyValue> cvalues, bool ccyAreUnique)
        {
            _list = new List<CurrencyValue>(cvalues.Count());
            if (ccyAreUnique)
                foreach (var cvalue in cvalues)
                    _list.Add(cvalue);
            else
                foreach (var grp in cvalues.GroupBy(cvalue => cvalue._ccyCode))
                    _list.Add(grp.Count() == 1 ? grp.First() : new CurrencyValue(grp.Sum(cvalue => cvalue.Value), grp.Key));
        }
        public override string ToString()
        {
            return _list.Select(cv => cv.ToString()).Join();
        }
        public string ToString(string format)
        {
            return _list.Select(cv => cv.ToString(format)).Join();
        }

        public static CurrencyValueList operator -(CurrencyValueList cvalues)
        {
            return new CurrencyValueList(cvalues._list.Select(cvalue => -cvalue));
        }

        public static CurrencyValueList operator &(CurrencyValueList left, CurrencyValue right)
        {
            for (int i = 0; i < left._list.Count; ++i)
                if (left._list[i]._ccyCode == right._ccyCode)
                    return new CurrencyValueList(left._list.Take(i).Concat(left._list[i] + right).Concat(left._list.Skip(i + 1)));
            return new CurrencyValueList(left._list.Concat(right)); // put the new value at right
        }
        public static CurrencyValueList operator +(CurrencyValueList left, CurrencyValue right)
        {
            return left & right;
        }
        public static CurrencyValueList operator -(CurrencyValueList left, CurrencyValue right)
        {
            return left & -right;
        }

        public static CurrencyValueList operator &(CurrencyValue left, CurrencyValueList right)
        {
            for (int i = 0; i < right._list.Count; ++i)
                if (right._list[i]._ccyCode == left._ccyCode)
                    return new CurrencyValueList(right._list.Take(i).Concat(right._list[i] + left).Concat(right._list.Skip(i + 1)));
            return new CurrencyValueList(left.Concat(right._list)); // put the new value at _left_
        }
        public static CurrencyValueList operator +(CurrencyValue left, CurrencyValueList right)
        {
            return left & right;
        }
        public static CurrencyValueList operator -(CurrencyValue left, CurrencyValueList right)
        {
            return left & -right;
        }

        public static CurrencyValueList operator &(CurrencyValueList left, CurrencyValueList right)
        {
            var res = new CurrencyValueList(left._list);
            res.SumInPlace(right);
            return res;
        }
        public static CurrencyValueList operator +(CurrencyValueList left, CurrencyValueList right)
        {
            return left & right;
        }
        public static CurrencyValueList operator -(CurrencyValueList left, CurrencyValueList right)
        {
            return left & -right;
        }
        public void SumInPlace(CurrencyValue right)
        {
            bool found = false;
            for (int k = 0; k < _list.Count; ++k)
                if (right._ccyCode == _list[k]._ccyCode)
                {
                    _list[k] += right;
                    found = true;
                    break;
                }
            if (!found)
                _list.Add(right);
        }
        public void SumInPlace(CurrencyValueList right)
        {
            for (int i = 0; i < right._list.Count; ++i)
                SumInPlace(right._list[i]);
        }

        public CurrencyValueList AsSortedByAmount(ListSortDirection dir = ListSortDirection.Descending)
        {
            if (dir == ListSortDirection.Ascending)
                return new CurrencyValueList(_list.OrderBy(cv => cv.Value));
            return new CurrencyValueList(_list.OrderByDescending(cv => cv.Value));
        }
        public void SortByAmount(ListSortDirection dir = ListSortDirection.Descending)
        {
            var lst = AsSortedByAmount(dir);
            _list.Clear();
            _list.AddRange(lst._list);
        }
        public CurrencyValueList AsSortedByAbsAmount(ListSortDirection dir = ListSortDirection.Descending)
        {
            if (dir == ListSortDirection.Ascending)
                return new CurrencyValueList(_list.OrderBy(cv => Math.Abs(cv.Value)));
            return new CurrencyValueList(_list.OrderByDescending(cv => Math.Abs(cv.Value)));
        }
        public void SortByAbsAmount(ListSortDirection dir = ListSortDirection.Descending)
        {
            var lst = AsSortedByAbsAmount(dir);
            _list.Clear();
            _list.AddRange(lst._list);
        }

        public CurrencyValueList AsSortedByCurrency(ListSortDirection dir = ListSortDirection.Descending)
        {
            if (dir == ListSortDirection.Ascending)
                return new CurrencyValueList(_list.OrderBy(cv => cv.Currency.Ccy));
            return new CurrencyValueList(_list.OrderByDescending(cv => cv.Currency.Ccy));
        }
        public void SortByCurrency(ListSortDirection dir = ListSortDirection.Descending)
        {
            var lst = AsSortedByAmount(dir);
            _list.Clear();
            _list.AddRange(lst._list);
        }
    }

}
