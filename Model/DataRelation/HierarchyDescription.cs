﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Model
{
    public class HierarchyDescription : IHierarchyDescriptionOptimized
    {
        public HierarchyDescriptionGraph OwnerGraph { get; private set; }
        HierarchyDescriptionGraph IHierarchyDescription.OwnerGraph { get { return OwnerGraph; } set { OwnerGraph = value; } }


        /// <summary><see cref="IHierarchyDescription.LevelName" /></summary>
        public string LevelName { get; }

        public Relation Relation { get; set; }

        public virtual Type         For                 { get { return null; } }
        public virtual Type         ChildType           { get { return null; } }
        public HierarchyDescription ReverseDescription  { get; set; }

        /// <summary><see cref="IHierarchyDescription.GetChildren" /></summary>
        public Func<Stack<object>, IReadOnlyCollection<object>> GetChildren { get; }
        /// <summary>
        /// <see cref="IHierarchyDescription.GetChildrenCount" />
        /// Default implementation is "obj => GetChildren(obj).Count"
        /// </summary>
        public Func<Stack<object>, int> GetChildrenCount { get; }

        /// <summary><see cref="IHierarchyDescription.ChildrenDescriptions" /></summary>
        public IReadOnlyCollection<IHierarchyDescription> ChildrenDescriptions { get; protected set; }

        public HierarchyDescription(string name, Func<Stack<object>, IReadOnlyCollection<object>> getChildren, Func<Stack<object>, int> getChildrenCount = null, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions = null)
        {
            LevelName = name;
            GetChildren = getChildren;
            GetChildrenCount = getChildrenCount ?? (parents => getChildren(parents)?.Count ?? 0);
            ChildrenDescriptions = childrenDescriptions ?? new HierarchyDescription[0];
        }

        public HierarchyDescription(string name, Func<Stack<object>, IReadOnlyCollection<object>> getChildren, Func<IReadOnlyCollection<object>, IReadOnlyCollection<object>> getAllChildren, Func<Stack<object>, int> getChildrenCount = null, int averageChildrenCount = 0, int averageChildSizeInMemory = 0, IReadOnlyCollection<IHierarchyDescription> childrenDescriptions = null)
            : this(name, getChildren, getChildrenCount, childrenDescriptions)
        {
            _getAllChildren = getAllChildren;
            _AverageChildrenCount = averageChildrenCount;
            _AverageChildSizeInMemory = averageChildSizeInMemory;
        }
        readonly Func<IReadOnlyCollection<object>, IReadOnlyCollection<object>> _getAllChildren;

        int                         IHierarchyDescriptionOptimized.AverageChildrenCount     { get { return _AverageChildrenCount;     } } readonly int _AverageChildrenCount;
        int                         IHierarchyDescriptionOptimized.AverageChildSizeInMemory { get { return _AverageChildSizeInMemory; } } readonly int _AverageChildSizeInMemory;
        IReadOnlyCollection<object> IHierarchyDescriptionOptimized.PreLoadAllChildren(IReadOnlyCollection<object> allParents)
        {
            return _getAllChildren?.Invoke(allParents);
        }
    }

    public abstract class HierarchyDescriptionTyped : HierarchyDescription
    {
        internal void SetChildren(IReadOnlyList<IHierarchyDescription> childrenDescriptions)
        {
            ChildrenDescriptions = childrenDescriptions;
        }

        public HierarchyDescriptionTyped(string name, Func<Stack<object>, IReadOnlyCollection<object>> getChildren, Func<Stack<object>, int> getChildrenCount = null, IReadOnlyList<IHierarchyDescription> childrenDescriptions = null)
            : base(name, getChildren, getChildrenCount, childrenDescriptions)
        {
        }

        public HierarchyDescriptionTyped(string name, Func<Stack<object>, IReadOnlyCollection<object>> getChildren, Func<IReadOnlyCollection<object>, IReadOnlyCollection<object>> getAllChildren, Func<Stack<object>, int> getChildrenCount = null, int averageChildrenCount = 0, int averageChildSizeInMemory = 0, IReadOnlyList<IHierarchyDescription> childrenDescriptions = null)
            : base(name, getChildren, getAllChildren, getChildrenCount, averageChildrenCount, averageChildSizeInMemory, childrenDescriptions)
        {
        }
    }

    public class HierarchyDescriptionTyped<TFor, TRelated> : HierarchyDescriptionTyped
        where TFor : class
        where TRelated : class
    {
        public override Type For       { get { return typeof(TFor); } }
        public override Type ChildType { get { return typeof(TRelated); } }

        public HierarchyDescriptionTyped(string name, Func<Stack<object>, IReadOnlyCollection<object>> getChildren, Func<Stack<object>, int> getChildrenCount = null, IReadOnlyList<IHierarchyDescription> childrenDescriptions = null)
            : base(name, getChildren, getChildrenCount, childrenDescriptions)
        {
        }

        public HierarchyDescriptionTyped(string name, Func<Stack<object>, IReadOnlyCollection<object>> getChildren, Func<IReadOnlyCollection<object>, IReadOnlyCollection<object>> getAllChildren, Func<Stack<object>, int> getChildrenCount = null, int averageChildrenCount = 0, int averageChildSizeInMemory = 0, IReadOnlyList<IHierarchyDescription> childrenDescriptions = null)
            : base(name, getChildren, getAllChildren, getChildrenCount, averageChildrenCount, averageChildSizeInMemory, childrenDescriptions)
        {
        }
    }
}
