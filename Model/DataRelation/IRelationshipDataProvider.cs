﻿using System;

using System.Linq.Expressions;


namespace TechnicalTools.Model
{
    public interface IRelationshipDataProvider
    {
        /// <summary>
        /// Return the average count of related entities stored/referenced by Property "property" in Entity
        /// </summary>
        /// <returns>Size in bytes</returns>
        int GetMeanRelationCount<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> expr);

        /// <summary>
        /// Average of bytes needed in memory to store the Entity (by considering only composition)
        /// </summary>
        /// <returns>Size in bytes</returns>
        int GetMeanRecordSize<TEntity>();
    }
}
