﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Model
{
    public class HierarchyDescriptionGraph
    {
        // Types can be seen as "node"
        public HashSet<Type> Types { get; set; } = new HashSet<Type>();
        // Hierarchies can be seen as "edge"
        public HashSet<IHierarchyDescription> AllHierarchies { get; set; } = new HashSet<IHierarchyDescription>();

        public HashSet<IHierarchyDescription> TopHierarchies { get; set; } = new HashSet<IHierarchyDescription>();
    }
}
