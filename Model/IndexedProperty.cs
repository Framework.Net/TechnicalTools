﻿using System;


namespace TechnicalTools.Model
{
    /// <summary>
    /// Cette classe est destiné à permettre de simuler les propriété indexé en VB :
    /// En VB.net, le code suivant :
    /// <code>
    /// Dim obj as New TObjet()
    /// obj.Truc(1) = True
    /// </code>
    /// deviendrait :
    /// <code>
    /// TObjet obj = new TObjet();
    /// obj.Truc[1] = true;
    ///
    /// // Dans TObject, Truc est défini comme :
    /// public IndexedPropertySurrogate &lt; TObjet, int, bool &gt
    /// // Dans le constructeur :
    /// Truc = new IndexedPropertySurrogate &lt; TObjet, int, bool &gt (this, (int index) => return ..., (int index, bool value) => ...);
    /// </code>
    /// </summary>
    /// <typeparam name="TOwner">Propriétaire de la propriété indexé (pour garder une trace au cas où)</typeparam>
    /// <typeparam name="TIndex">Type de la valeur qui sert à indexer</typeparam>
    /// <typeparam name="TValue">Type de la valeur recupéré</typeparam>
    public class IndexedProperty<TIndex, TValue>
    {
        readonly object _owner;
        readonly Func<TIndex, TValue> _getter;
        readonly Action<TIndex, TValue> _setter;

        public IndexedProperty(object owner, Func<TIndex, TValue> getter, Action<TIndex, TValue> setter)
        {
            _owner = owner;
            _getter = getter;
            _setter = setter;
        }
        public TValue this[TIndex index]
        {
            get { return _getter(index); }
            set { _setter(index, value); }
        }
    }
}
