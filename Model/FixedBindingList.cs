﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ComponentModel;



namespace TechnicalTools.Model
{
    /// <summary>
    /// Cette classe permet une gestion corrigée de l'évènement de suppression de la classe BindingList du framework.
    /// En utilisant cette classe, on peut caster l'EventArgs de l'event ListChanged en ListChangedEventArgsWithRemovedItem[T]
    /// afin de récupérer, via la propriété "Item", l'objet supprimé.
    /// bug du framework : http://connect.microsoft.com/VisualStudio/feedback/details/148506/listchangedtype-itemdeleted-is-useless-because-listchangedeventargs-newindex-is-already-gone
    /// Dans l'avenir il est possible de corriger et/ou d'améliorer la robustesse technique et/ou les fonctionalités de cette liste technique
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable] // TODO (amelioration) : voir http://www.gavaghan.org/blog/2007/07/17/fixing-bindinglist-deserialization/
    public class FixedBindingList<T> : System.ComponentModel.BindingList<T>, IFixedBindingList<T>//, IFixedBindingListContraVariant<T>
    {
        public FixedBindingList()
        {
        }

        public FixedBindingList(IList<T> list)
            : base(list)
        {
        }
        public FixedBindingList(IEnumerable<T> values)
            : base(values.ToList())
        {
        }

        protected override void RemoveItem(int index)
        {
            T deleted_item = this[index];

            // On supprime l'item sans déclencher d'évènement
            // On supprime en premier (avant de declencher l'event) afin de mimer les effets de bord par defaut :
            // Certains handlers nécessitent que l'élément soit supprimés de la liste avant de recevoir la notification de changement.
            // Ils peuvent ainsi effectuer un diff entre les éléments de la liste avant et apres l'evenement (si les elements avant l'évènement etaient stocké dans un cache par exemple)
            // Note : EXCEPTION HERE : often mean AllowRemove is false !
            WithListChangedEventsDisabled(() => base.RemoveItem(index), false);

            if (RaiseListChangedEvents)
            {
                // Déclenche l'évènement en fournissant l'objet supprimé
                var deleted_arg = new ListChangedAdvancedEventArgs<T>(ListChangedType.ItemDeleted, index, index)
                {
                    ChangeStep = eChangeStep.ChangedIsDone,
                    ItemAddedOrRemoved = deleted_item
                };

                OnListChanged(deleted_arg);
            }
        }

        [Obsolete("Use " + nameof(WithListChangedEventsDisabled) + " which is just a rename of MakeOptimizedChangesWithRemoved", true)]
        [DebuggerHidden, DebuggerStepThrough]
        public void MakeOptimizedChangesWithRemoved(Action action_without_event, bool raise_reset_after = true)
        {
            WithListChangedEventsDisabled(action_without_event, raise_reset_after);
        }

        [DebuggerHidden, DebuggerStepThrough]
        public void WithListChangedEventsDisabled(Action action_without_event, bool raise_reset_after = true)
        {
            bool b = RaiseListChangedEvents;
            RaiseListChangedEvents = false;
            try
            {
                action_without_event();
            }
            finally
            {
                RaiseListChangedEvents = b;
                if (raise_reset_after && b)
                    ResetBindings();
            }

        }

        public void MoveItem(int from, int to, bool to_index_is_final_index = false)
        {
            if (from == to)
                return;
            bool b = RaiseListChangedEvents;
            RaiseListChangedEvents = false;
            try
            {
                if (from < to && !to_index_is_final_index)
                    --to;
                var item = this[from];
                this.RemoveAt(from);
                this.InsertItem(to, item);
                this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemMoved, to, from));
            }
            finally
            {
                RaiseListChangedEvents = b;
            }
        }
        public void SwapItem(int idx1, int idx2)
        {
            if (idx1 < idx2)
            {
                MoveItem(idx1, idx2);
                MoveItem(idx2 - 1, idx1);
            }
            else if (idx1 > idx2)
            { // Pas d'appel récursif "SwapItem(idx2, int idx1)" car les évènements générés ne seraient pas les mêmes.
                MoveItem(idx1, idx2);
                MoveItem(idx2 + 1, idx1);
            }
        }
    }

}
