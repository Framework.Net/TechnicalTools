using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Tools;


namespace TechnicalTools.Model
{
    public class SingletonFactory
    {
        public event Action<SingletonFactory, object> ObjectCreated;

        public T TryGet<T>()
            where T : class
        {
            object businessObject = null;
            lock (GetLockFor(typeof(T)))
            {
                _cache.TryGetValue(typeof(T), out businessObject);
                return (T)businessObject;
            }
        }

        /// <summary>
        /// Schedule an action to do only once, rigth after object of type T (or derived type) is created and initialized.
        /// If an exception occurs in the action, you have to handle the resubscring yourself.
        /// </summary>
        /// <typeparam name="T">The type of original method (the one declared with virtual or abstract jeyword)</typeparam>
        /// <param name="action">Action to do when object exist and is initialized</param>
        public void OnObjectExistsDo<T>(Action<T> action)
           where T : class
        {
            var obj = TryGet<T>();
            if (obj != null)
                action(obj);
            else
            {
                void onObjectCreated(SingletonFactory instance, object o)
                {
                    if (o is T)
                    {
                        Debug.Assert(ReferenceEquals(o, instance.TryGet<T>()));
                        instance.ObjectCreated -= onObjectCreated;
                        action((T)o);
                    }
                }
                ObjectCreated += onObjectCreated;
            }
        }

        protected static void SetInstance<TSingletonFactory>(ref TSingletonFactory instance, TSingletonFactory newValue)
            where TSingletonFactory : SingletonFactory
        {
            if (instance != null)
                if (instance._cache.Count > 0)
                    throw new TechnicalException($"You have to Create your {nameof(TSingletonFactory)} sooner in application life." + Environment.NewLine +
                                                 "Some objects have already been injected!", null);
                else if (newValue != null && !ReferenceEquals(newValue, instance))
                    newValue.ObjectCreated += instance.ObjectCreated;
            instance = newValue;
        }

        protected T GetOrAdd<T>(Func<T> create)
                   where T : class
        {
            bool raiseEvent = !_cache.TryGetValue(typeof(T), out object businessObject);
            if (raiseEvent)
                lock (GetLockFor(typeof(T)))
                {
                    raiseEvent = !_cache.TryGetValue(typeof(T), out businessObject);
                    if (raiseEvent)
                    {
                        businessObject = create();
                        var success = _cache.TryAdd(typeof(T), businessObject);
                        Debug.Assert(success);
                    }
                }
            if (raiseEvent)
            {
                ObjectTracker.Instance.Track((T)businessObject);
                ObjectCreated?.Invoke(this, businessObject);
            }
            return (T)businessObject;
        }
        object GetLockFor(Type t) { return _locksByModule.GetOrAdd(t, _ => new object()); }
        readonly ConcurrentDictionary<Type, object> _locksByModule = new ConcurrentDictionary<Type, object>();
        readonly ConcurrentDictionary<Type, object> _cache = new ConcurrentDictionary<Type, object>();

        protected T Remove<T>()
            where T : class
        {
            _cache.TryRemove(typeof(T), out object businessObject);
            return (T)businessObject;
        }

        public List<object> GetAllSingletons()
        {
            return _cache.Values.ToList();
        }
    }
}