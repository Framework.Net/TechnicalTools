﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

using TechnicalTools.Diagnostics;
using TechnicalTools.Tools;

namespace TechnicalTools.Model
{
    // ReSharper disable PossibleInterfaceMemberAmbiguity
    public interface ICompositeList : IFixedBindingList, IPropertyNotifierObject
    {
        // TODO : mettre des méthodes si besoin
    }
    // ReSharper restore PossibleInterfaceMemberAmbiguity

    public interface ICompositeList<out TItem> : ICompositeList, IFixedBindingList<TItem>
    {
        // TODO : mettre des méthodes si besoin
    }

    /// <summary>
    /// Cette classe regroupe les fonctionalités de FixedBindingList et de PropertyNotifierObject
    /// via le multihéritage par composition (ie : un mixin = un moyen de faire de l'héritage multiple "légal")
    ///
    /// Elle met à disposition du développeur quelques fonctionnalités fort utiles...
    /// </summary>
    public class CompositeList<TItem> : FixedBindingListWithNotifyProperty<TItem>, ICompositeList<TItem>, IHasCompositeOwner
    { // TODO (techno) : pour permettre de creer un debugger personalisé Voir http://www.codeproject.com/Articles/37874/CodeDom-CodeObject-Debugger-Visualizer et http://stackoverflow.com/questions/13430900/how-do-i-use-my-own-debugger-visualiser-to-edit-variables-runtime
        public CompositeList()
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
        }

        public CompositeList(IEnumerable<TItem> items)
        {
            _propertyNotifierObject = new PropertyNotifierObject(this);
            Init();
            AddRange(items);
        }

        static CompositeList()
        {
            Debug.Assert(typeof(IPropertyNotifierObject).IsAssignableFrom(typeof(ICompositeList)), "Le code utilisant _ItemAreCompositeListsToo, se sert du fait que les item de type ICompositeList peuvent être utilise comme des IPropertyNotifierObject !");
        }

        void Init()
        {
            _ItemAreCompositeListsToo = typeof(ICompositeList).IsAssignableFrom(typeof(TItem));
            _DefaultRaiseResetBindings = _propertyNotifierObject.RaiseResetBindings;
            _propertyNotifierObject.RaiseResetBindings = RaiseResetBindings;

            ManageElementImplementingIPropertyNotifierObject();
            RememberDeletedObjectsFeature_Initialize();
            CompositionManagement();
        }

        void RaiseResetBindings(ePropertyEventLock evts)
        {
            _reseting = true;
            try
            {
                 ResetBindings(); // déclenchera l'event ListChanged en premier puisque ResetBindings est de toute facon appelable publiquement
                _DefaultRaiseResetBindings(evts);
            }
            finally
            {
                _reseting = false;
            }
        }
        bool _reseting;
        Action<ePropertyEventLock> _DefaultRaiseResetBindings;

        #region Gestion automatique des relations de Composition

        [System.Runtime.InteropServices.ComVisible(false)]
        public object TechnicalOwner { get; protected set; }
        object IHasCompositeOwner.TechnicalOwner { get { return TechnicalOwner; } set { TechnicalOwner = value; } }

        public bool IsCompositeList { get; set; }

        void CompositionManagement()
        {
            // Nothing, but this would be here we initialize data of current #region
        }

        protected override void OnListChanged(ListChangedEventArgs e)
        {
            base.OnListChanged(e);
            CompositionManagement_AfterListChanged(this, e);
        }

        void CompositionManagement_AfterListChanged(object sender, ListChangedEventArgs e)
        {
            if (!IsCompositeList)
                return;
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                if (base[e.NewIndex] is IHasCompositeOwner ohco)
                {
                    Debug.Assert(ohco.TechnicalOwner == null,
                                 "L'objet ne peut avoir deux proprietaires en même temps ! " +
                                 "Il doit d'abort être détaché de son proprietaire avant de pouvoir être rattaché à un autre.");
                    ohco.TechnicalOwner = this;
                }
            }
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                if (((ListChangedAdvancedEventArgs<TItem>)e).ItemAddedOrRemoved is IHasCompositeOwner ohco)
                {
                    Debug.Assert(ohco.TechnicalOwner != null, "La propriete Owner de l'objet aurait du être défini !");
                    ohco.TechnicalOwner = null;
                }
            }
        }

        #endregion


        /*readonly*/
        bool _ItemAreCompositeListsToo; // Détecte si les items sont également des composites list afin de gérer le relai de dirty

        // Cette partie permet de gerer le relayage des évènements Dirty des sous objet de la liste vers la liste elle même et vers l'objet parent de la liste
        #region Gestion des elements en tant qu'objet implementant IPropertyNotifierObject
        /// <summary>
        /// Indique si, dès qu'une item est ajouté ou supprimé de la liste, l'état de la liste doit être considéré comme modifié (IsDirty == true).
        /// Par default a true
        /// </summary>
        public bool ConsiderDirtyOnAddRemoveEvents { get; set; } // On n'utilise pas SetProperty<bool> car il s'agit d'une propriété technique

        /// <summary>
        /// Indique si, dès qu'un item est modifié (une de ses proprietés a été altéré), on considère la liste comme modifié (IsDirty == true)
        /// Par default a false.
        ///
        /// Cette proprieté est interessante dans le cas suivant :
        /// Partons du principe que :
        ///   - On a un objet Eleve qui contient une liste de ContrôleScolaire.
        ///   - La vue qui affiche l'eleve affiche également des informations aggregés sur les controles scolaires (exemple la moyenne de l'eleve ...)
        ///   - Que la vue delegue l'edition des notes a une autre vue
        ///
        /// Lorsque l'utilisateur change les notes dans la sous vue, il est maintenant possible, simplement, de raffraichir dans la vue principale,
        /// uniquement la moyenne (et pas toutes les informations de l'eleve), comme ceci :
        ///
        /// <example>
        /// Eleve eleve = ...;
        /// eleve.Note_scolaire.ConsiderDirtyOnItemChanged = true;
        /// eleve.Note_scolaire.OnDirty += () => RaffraichirMoyennes() // On évite de tout raffraichir !</example>
        /// new FrmEditionDesNotes(eleve.Note_scolaire).Show(); // plus besoin de faire un ShowDialog() !
        /// </summary>
        public bool ConsiderDirtyOnItemChanged { get; set; } // Idem

        public virtual void Reset()
        {
            base.ClearItems();
            DeletedObjects.Clear();
            InitializeState(eState.New);
        }

        public void AcceptChanges()
        {
            UpdateItemStates();
            DeletedObjects.Clear();
            UpdateState();
        }
        public void UpdateItemStates()
        {
            if (typeof (IPropertyNotifierObject).IsAssignableFrom(typeof (TItem)))
            {
                foreach (IPropertyNotifierObject item in this)
                    item.UpdateState();
                foreach (IPropertyNotifierObject item in DeletedObjects)
                    if (item.State == eState.ToDelete)
                        item.UpdateState();
            }
        }

        void ManageElementImplementingIPropertyNotifierObject()
        {
            ConsiderDirtyOnAddRemoveEvents = true;
            ListChanged += RelayDirtyOnItemChange;
        }

        void RelayDirtyOnItemChange(object sender, ListChangedEventArgs e)
        {
            Debug.Assert(!IsReadOnly || e.ListChangedType.In(ListChangedType.PropertyDescriptorAdded, ListChangedType.PropertyDescriptorChanged, ListChangedType.PropertyDescriptorDeleted, ListChangedType.Reset),
                         "Curieux, la liste a été marquée comme ReadOnly et pourtant elle vient d'être modifiée !");

            if (ConsiderDirtyOnAddRemoveEvents && (e.ListChangedType.In(ListChangedType.ItemAdded, ListChangedType.ItemDeleted) ||
                                                   e.ListChangedType == ListChangedType.Reset /*&& Count > 0*/) || // trop tard pour tester le Count, il est a zero dans le cas d'un reset
                // BindingList declenche un event ListChanged avec un type a ItemChange quand un des objets de la liste herite de INotifyProperty et est changé
                ConsiderDirtyOnItemChanged && e.ListChangedType == ListChangedType.ItemChanged && !(e.IsTechnicalProperty() ?? false))
            {
                if (e.ListChangedType == ListChangedType.ItemChanged)
                {
                    if (base[e.NewIndex] is IPropertyNotifierObject pno)
                        _propertyNotifierObject.RaiseRelayDirtyness(pno);
                }
#if DEBUG
                SetDirtyBy(e.ListChangedType == ListChangedType.ItemAdded ? this[e.NewIndex]
                         : e.ListChangedType == ListChangedType.ItemDeleted ? ((ListChangedAdvancedEventArgs<TItem>)e).ItemAddedOrRemoved
                         : e.ListChangedType == ListChangedType.PropertyDescriptorChanged ? this[e.NewIndex]
                         : (object)this);
#else
                SetDirtyBy(this);
#endif
            }

            if (e.ListChangedType == ListChangedType.Reset && !_reseting)
                _DefaultRaiseResetBindings(ePropertyEventLock.All);
        }

        #endregion

        // Cette partie permet de garder une trace des element supprimé de la liste afin de pouvoir facilement faire un update des items un par un
        // Cela permet de faciliter le code de tout interface qui editerait cette liste.
        #region  Gestion pour garder une trace des elements supprimés
        public readonly BindingList<TItem> DeletedObjects = new BindingList<TItem>();

        public bool RememberDeletedObjects
        {
            get { return _RememberDeletedObjects; }
            set
            {
                if (value && _RememberDeletedObjects)
                    throw new TechnicalException("Cette collection ne peut être surveillée par deux user controle en même temps", null);
                if (value && !typeof(TItem).Implements(typeof(INotifyPropertyChanged)))
                    throw new TechnicalException(string.Format("La fonctionalité de mémorisation des objets supprimés ne peut pas être activéé si le type {0} n'implémente pas {1}", typeof(TItem).Name, typeof(INotifyPropertyChanged).Name), null);
                _RememberDeletedObjects = value;
            }
        }
        bool _RememberDeletedObjects;
        public bool ConsiderItemAsComposite { get; set; }

        void RememberDeletedObjectsFeature_Initialize()
        {
            ConsiderItemAsComposite = true;
            ListChanged += RememberDeletedObjectsFeature_ListChanged;
        }

        void RememberDeletedObjectsFeature_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (!RememberDeletedObjects)
                return;

            IPropertyNotifierObject item;
            if (e.ListChangedType == ListChangedType.ItemDeleted)
            {
                item = (IPropertyNotifierObject)((ListChangedAdvancedEventArgs<TItem>)e).ItemAddedOrRemoved;
                RememberDeletedObjectsFeature_OnRemovedItem(item);
            }
            else if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                item = (IPropertyNotifierObject)this[e.NewIndex];
                RememberDeletedObjectsFeature_OnAddedItem(item);
            }
        }

        private void RememberDeletedObjectsFeature_OnRemovedItem(IPropertyNotifierObject pno)
        {
            if (pno.State.In(eState.New, eState.Deleted))
                return;
            if (ConsiderItemAsComposite && pno.State.In(eState.Synchronized, eState.Unsynchronized))
                pno.UpdateState(eState.ToDelete);

            if (!DeletedObjects.Contains((TItem)pno))
                DeletedObjects.Add((TItem)pno);
        }

        private void RememberDeletedObjectsFeature_OnAddedItem(IPropertyNotifierObject pno)
        {
            Debug.Assert(pno != null);
            if (pno.State == eState.ToDelete)
                pno.UpdateState(eState.Unsynchronized); // Impossible de savoir si l'etat d'avant était Synchronized ou Unsynchronized donc par securité on met toujours Unsynchronized
            if (pno.State == eState.Deleted)
                pno.InitializeState(eState.New);
            DeletedObjects.Remove((TItem)pno);
        }
        #endregion

        public void AddRange(IEnumerable<TItem> values)
        {
            foreach (TItem item in values)
                Add(item);
        }

        public void RemoveIf(Func<TItem, bool> condition)
        {
            for (int i = Count - 1; i >= 0; --i)
                if (condition(this[i]))
                    RemoveAt(i);
        }

        protected override void ClearItems()
        {
            if (RememberDeletedObjects)
                foreach (var item in this)
                    RememberDeletedObjectsFeature_OnRemovedItem((IPropertyNotifierObject)item);

            if (_ItemAreCompositeListsToo)
            {
                foreach (var item in this)
                    ((IPropertyNotifierObject)item).DirtyChanged -= CompositeListItem_DirtyChanged;
            }

            if (Count > 0) // Sinon provoque un evenemnt reset pour rien
                base.ClearItems();
        }

        protected override void InsertItem(int index, TItem item)
        {
            DebugTools.Should(!_inserting, "Cas rare rencontré : Très difficile (et pas optimisé) d'assurer l'integrité en cas d'exception lors de l'ajout d'un item (probablement à cause évènement ListChanged) !");
            _inserting = true;
            try
            {
                // On considère l'ajout comme sûr (framework.Net)
                // Cependant il peut y avoir des exceptions dû à du code d'un développeur sur l'évènement ListChanged
                base.InsertItem(index, item);
                if (_ItemAreCompositeListsToo)
                {
                    ((IPropertyNotifierObject)item).DirtyChanged += CompositeListItem_DirtyChanged;
                    CompositeListItem_DirtyChanged(item, EventArgs.Empty);
                }
            }
            catch (Exception e)
            {
                _inserting = false;
                Debug.Assert(Count > index && ReferenceEquals(base[index], item));
                // On garanti donc l'integrité en annulant l'insertion
                bool r = RaiseListChangedEvents;
                RaiseListChangedEvents = false;
                try
                {
                    RemoveAt(index); // Normalement ca ne devrait pas thrower !
                }
                catch (Exception ee)
                {
                    // ignore !
                    e.EnrichDiagnosticWith("(Cannot revert back CompositeList item insertion because of : " + ee.Message, eExceptionEnrichmentType.Technical);
                }
                finally
                {
                    RaiseListChangedEvents = r;
                }
                throw;
            }
            _inserting = false;
        }
        bool _inserting;

        protected override void RemoveItem(int index)
        {
            if (_removing)
                DebugTools.Break("Cas rare rencontré : Très difficile (et pas optimisé) d'assurer l'integrité en cas d'exception lors de la suppression d'un item (probablement à cause évènement ListChanged) !");
            TItem removed_item = base[index];
            _removing = true;

            try
            {
                // On considère l'ajout comme sûr (framework.Net)
                // Cependant il peut y avoir des exceptions dû à du code d'un développeur sur l'evenement ListChanged
                base.RemoveItem(index);
                if (_ItemAreCompositeListsToo)
                    ((IPropertyNotifierObject)removed_item).DirtyChanged -= CompositeListItem_DirtyChanged;
            }
            catch (Exception e)
            {
                _removing = false;
                // On garanti donc l'integrité en annulant l'insertion
                bool r = RaiseListChangedEvents;
                RaiseListChangedEvents = false;
                try
                {
                    Insert(index, removed_item); // Normalement ca ne devrait pas thrower !
                }
                catch (Exception ee)
                {
                    // ignore !
                    e.EnrichDiagnosticWith("(Cannot revert back CompositeList item remove because of : " + ee.Message, eExceptionEnrichmentType.Technical);
                }
                finally
                {
                    RaiseListChangedEvents = r;
                }
                throw;
            }
            _removing = false;
        }
        bool _removing;

        private void CompositeListItem_DirtyChanged(object sender, EventArgs e)
        {
            var pno = (IPropertyNotifierObject)sender;
            _propertyNotifierObject.RaiseRelayDirtyness(pno);
            if (pno.IsDirty)
                SetDirtyBy(pno);
        }

        protected sealed override object AddNewCore()
        {   // Code calqué sur le code officiel de microsoft
            // Allow event handler to supply the new item for us
            object newItem = FireAddingNew();

            // If event handler did not supply new item, create one ourselves
            if (newItem == null)
                newItem = CreateNewItemInstance();

            // Add item to end of list. Note: If event handler returned an item not of type T,
            // the cast below will trigger an InvalidCastException. This is by design.
            Add((TItem)newItem);

            // Return new item to caller
            return newItem;
        }
        // Methode d'aide pour overrider AddNewCore
        protected object FireAddingNew()
        {
            var e = new AddingNewEventArgs(null);
            OnAddingNew(e);
            return e.NewObject;
        }

        // Indique comment la liste crée un nouvel objet quand on lui demande (grid de l'IHM etc ..)
        protected virtual TItem CreateNewItemInstance()
        {
            Type type = typeof(TItem);
            var cons = DefaultObjectFactory.ConstructorFor(type);
            return (TItem)cons();
        }

        // Le code de cette région n'a aucune intelligence autre que d'implementer IPropertyNotifierObject en deleguant les appels à _propertyNotifierObject
        // Toute cette region n'a que pour but de simuler un heritage de PropertyNotifierObject
        #region "Implementation de IPropertyNotifierObject => Delegation complete à l'objet _propertyNotifierObject"

        // Révons un peu en esperant un jour voir ce genre de chose : http://stackoverflow.com/a/255621)
        readonly PropertyNotifierObject _propertyNotifierObject;

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetProperty<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).SetProperty<T>(ref backingStore, value, onChanged, onChanging, propertyName);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).SetTechnicalProperty(ref backingStore, value, onChanged, onChanging, propertyName);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property = null, [CallerMemberName] string dependent_propertyName = null)
            where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).CurrentPropertyIsDependantOn(dependency_prop_has_expression, refresh_current_property, dependent_propertyName);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Obsolete("Utilisez SetDirtyBy si possible !")]
        public void Touch()
        {
            _propertyNotifierObject.Touch();
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetDirtyBy(object contaminating_object)
        {
            _propertyNotifierObject.SetDirtyBy(contaminating_object);
        }

        bool IList.IsReadOnly
        {
            get { return IsReadOnly; }
        }

        #region propriétés IsDirty, State et autres

        public eState State { [DebuggerHidden, DebuggerStepThrough] get { return _propertyNotifierObject.State; } }
        public event Action<object, eState> StateChanged
        {
            [DebuggerHidden, DebuggerStepThrough]
            add { _propertyNotifierObject.StateChanged += value; }
            [DebuggerHidden, DebuggerStepThrough]
            remove { _propertyNotifierObject.StateChanged -= value; }
        }
        public bool IsDirty { [DebuggerHidden, DebuggerStepThrough] get { return _propertyNotifierObject.IsDirty; } }
        public event EventHandler DirtyChanged
        {
            [DebuggerHidden, DebuggerStepThrough]
            add { _propertyNotifierObject.DirtyChanged += value; }
            [DebuggerHidden, DebuggerStepThrough]
            remove { _propertyNotifierObject.DirtyChanged -= value; }
        }
        [DebuggerHidden, DebuggerStepThrough]
        public void InitializeState(eState new_state)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).InitializeState(new_state);
        }
        [DebuggerHidden, DebuggerStepThrough]
        public void UpdateState(eState? new_state = null)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).UpdateState(new_state);
        }
        protected ePropertyEventLock LockedEvents { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).LockedEvents; } [DebuggerHidden, DebuggerStepThrough] set { ((IPropertyNotifierObject)_propertyNotifierObject).LockedEvents = value; } }
        protected bool               LockState    { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).LockState;    } [DebuggerHidden, DebuggerStepThrough] set { ((IPropertyNotifierObject)_propertyNotifierObject).LockState = value; } }

        public bool IsReadOnly
        {
            [DebuggerHidden, DebuggerStepThrough]
            get { return ((IPropertyNotifierObject)_propertyNotifierObject).IsReadOnly; }
            [DebuggerHidden, DebuggerStepThrough]
            set
            {
                ((IPropertyNotifierObject)_propertyNotifierObject).IsReadOnly = value;
                LockWhileUpdatingWith(() =>
                {
                    AllowEdit = !value;
                    AllowRemove = !value;
                    AllowNew = !value;
                });
            }
        }
        public bool IsNew { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).IsNew; } }
        public bool CreatedInCurrentSession { [DebuggerHidden, DebuggerStepThrough] get { return ((IPropertyNotifierObject)_propertyNotifierObject).CreatedInCurrentSession; } }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CheckIsNotReadOnlyForAction(string action_name = null)
        {
            try
            {
                ((IPropertyNotifierObject)_propertyNotifierObject).CheckIsNotReadOnlyForAction(action_name);
            }
            catch (Exception ex)
            {
                // On ne fournit pas ex en tant qu'inner exception car on souhaite simplement renommer le message
                throw new Exception(ex.Message.Replace(typeof(PropertyNotifierObject).Name, GetType().FullName));
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void LockWhileUpdatingWith(Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(lck, action);
        }

        #endregion propriétés IsDirty, State et autres


        #region Gestion de l'event PropertyChanged

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PropertyChangedFor<T>(Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangedFor(expression, onPropertyChanged);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void PropertyChangedFor_Remove<T>(Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangedFor_Remove(expression, onPropertyChanged);
        }

        #endregion Gestion de l'event PropertyChanged

        #region Gestion de l'event PropertyChanging

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.PropertyChangingFor<T>(Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        //FYI : where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangingFor(expression, onPropertyChanging);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.PropertyChangingFor_Remove<T>(Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        //FYI : where T : class, IPropertyNotifierObject
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChangingFor_Remove(expression, onPropertyChanging);
        }

        #endregion Gestion de l'event PropertyChanging


        #region (Super prive) Simulation de l'heritage des membres protégés
        // Cf les explications dans la même region dans PropertyNotifierObject
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            // ReSharper disable ExplicitCallerInfoArgument
            SetProperty(ref backingStore, value, onChanged, onChanging, propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            SetTechnicalProperty(ref backingStore, value, onChanged, onChanging, propertyName);
        }

        void IPropertyNotifierObject.CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property, string dependent_propertyName)
        {
            // ReSharper disable ExplicitCallerInfoArgument
            ((IPropertyNotifierObject)_propertyNotifierObject).CurrentPropertyIsDependantOn(dependency_prop_has_expression, refresh_current_property, dependent_propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
        }


        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add    { ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChanged += value; }
            remove { ((IPropertyNotifierObject)_propertyNotifierObject).PropertyChanged -= value; }
        }
        event Action<PropertyNotifierObject, ePropertyEventLock> IPropertyNotifierObject.OnResetBindings
        {
            add    { ((IPropertyNotifierObject)_propertyNotifierObject).OnResetBindings += value; }
            remove { ((IPropertyNotifierObject)_propertyNotifierObject).OnResetBindings -= value; }
        }

        ePropertyEventLock IPropertyNotifierObject.LockedEvents { get { return LockedEvents; } set { LockedEvents = value; } }
        bool               IPropertyNotifierObject.LockState { get { return LockState; } set { LockState = value; } }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            ((IPropertyNotifierObject)_propertyNotifierObject).LockWhileUpdatingWith(lck, action);
        }

        /// <summary> Fournit une méthode pour gérer la contamination du dirty </summary>
        public void DirtyChangedHandler(object sender, EventArgs e)
        {
            _propertyNotifierObject.DirtyChangedHandler(sender, e);
        }

        #endregion (Super prive) Simulation de l'heritage des membres protégés

        #endregion

    }

}
