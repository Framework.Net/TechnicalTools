﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ComponentModel;
using TechnicalTools.Tools;


namespace TechnicalTools.Model
{
    // Classe pas parfaite mais qui devrait faire le job
    //[Serializable]
    //public class FixedBindingListCombiner<T> : FixedBindingList<T>
    //{
    //    readonly FixedBindingList<T> _left;
    //    readonly FixedBindingList<T> _right;
    //    bool _doing_action_on_first_of_both;

    //    public FixedBindingListCombiner(FixedBindingList<T> left, FixedBindingList<T> right)
    //        : base(left.Concat(right).ToList())
    //    {
    //        _left = left;
    //        _right = right;
    //        _left.ListChanged += _left_ListChanged;
    //        _right.ListChanged += _right_ListChanged;
    //    }

    //    void _left_ListChanged(object sender, ListChangedEventArgs e)
    //    {
    //        if (_doing_action_on_first_of_both)
    //            return;
    //        if (e.ListChangedType == ListChangedType.ItemAdded)
    //            base.InsertItem(e.NewIndex, _left[e.NewIndex]);
    //        else if (e.ListChangedType == ListChangedType.ItemDeleted)
    //            base.RemoveItem(e.NewIndex);
    //        else if (e.ListChangedType == ListChangedType.ItemChanged)
    //            base.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, e.NewIndex));
    //        else if (e.ListChangedType == ListChangedType.Reset)
    //        {
    //            bool b = RaiseListChangedEvents;
    //            base.ClearItems();
    //            this.AddRange(_right);
    //            RaiseListChangedEvents = b;
    //            ResetBindings();
    //        }
    //    }
    //    void _right_ListChanged(object sender, ListChangedEventArgs e)
    //    {
    //        if (_doing_action_on_first_of_both)
    //            return;
    //        if (e.ListChangedType == ListChangedType.ItemAdded)
    //            base.InsertItem(e.NewIndex + _left.Count, _right[e.NewIndex]);
    //        else if (e.ListChangedType == ListChangedType.ItemDeleted)
    //            base.RemoveItem(e.NewIndex + _left.Count);
    //        else if (e.ListChangedType == ListChangedType.ItemChanged)
    //            base.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, e.NewIndex + _left.Count));
    //        else if (e.ListChangedType == ListChangedType.Reset)
    //        {
    //            bool b = RaiseListChangedEvents;
    //            base.ClearItems();
    //            this.AddRange(_left);
    //            RaiseListChangedEvents = b;
    //            ResetBindings();
    //        }
    //    }

    //    protected override void InsertItem(int index, T item)
    //    {
    //        if (_doing_action_on_first_of_both)
    //            return;
    //        _doing_action_on_first_of_both = true;
    //        if (index < _left.Count)
    //            _left.Insert(index, item);
    //        else
    //            _right.Insert(index - _left.Count, item);
    //    }
    //    protected override void SetItem(int index, T item)
    //    {
    //        if (_doing_action_on_first_of_both)
    //            return;
    //        _doing_action_on_first_of_both = true;
    //        if (index < _left.Count)
    //            _left[index] = item;
    //        else
    //            _right[index - _left.Count] = item;
    //    }
    //    protected override void RemoveItem(int index)
    //    {
    //        if (_doing_action_on_first_of_both)
    //            return;
    //        _doing_action_on_first_of_both = true;
    //        if (index < _left.Count)
    //            _left.RemoveAt(index);
    //        else
    //            _right.RemoveAt(index - _left.Count);
    //    }
    //    protected override void ClearItems()
    //    {
    //        if (_doing_action_on_first_of_both)
    //            return;
    //        _doing_action_on_first_of_both = true;
    //        _doing_action_on_first_of_both = true;
    //        _left.Clear();
    //        _right.Clear();
    //        _doing_action_on_first_of_both = false;
    //        base.ClearItems();
    //    }
    //}

    [Serializable]
    public class FixedBindingListCombiner<T> : FixedBindingList<T>
    {
        readonly FixedBindingList<T> _left;
        readonly FixedBindingList<T> _right;

        public FixedBindingListCombiner(FixedBindingList<T> left, FixedBindingList<T> right)
            : base(left.Concat(right).ToList())
        {
            _left = left;
            _right = right;
            _left.ListChanged += _left_ListChanged;
            _right.ListChanged += _right_ListChanged;
        }

        void _left_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
                base.Insert(e.NewIndex, _left[e.NewIndex]);
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
                base.RemoveItem(e.NewIndex);
            else if (e.ListChangedType == ListChangedType.ItemChanged)
                base.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, e.NewIndex));
            else if (e.ListChangedType == ListChangedType.Reset)
            {
                while (Count > _right.Count)
                    RemoveAt(0);
                for (int i = 0; i < _left.Count; ++i)
                    Insert(i, _left[i]);
            }
        }
        void _right_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded)
                base.Insert(e.NewIndex + _left.Count, _right[e.NewIndex]);
            else if (e.ListChangedType == ListChangedType.ItemDeleted)
                base.RemoveItem(e.NewIndex + _left.Count);
            else if (e.ListChangedType == ListChangedType.ItemChanged)
                base.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, e.NewIndex + _left.Count));
            else if (e.ListChangedType == ListChangedType.Reset)
            {
                while (Count > _left.Count)
                    RemoveAt(Count-1);
                for (int i = 0; i < _right.Count; ++i)
                    Insert(i + _left.Count, _left[i]);
            }
        }
    }

}
