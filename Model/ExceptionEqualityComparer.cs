﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace TechnicalTools.Model
{
    // From https://stackoverflow.com/a/35520207/294998
    public sealed class ExceptionEqualityComparer : IEqualityComparer, IEqualityComparer<Exception>
    {
        public static readonly ExceptionEqualityComparer Instance = new ExceptionEqualityComparer();

        private ExceptionEqualityComparer() { }

        public int GetHashCode(Exception obj)
        {
            return obj.Message.GetHashCode()
                 ^ (obj.StackTrace?.GetHashCode() ?? 0)
                 ^ (obj.Source?.GetHashCode() ?? 0);
        }
        bool IEqualityComparer<Exception>.Equals(Exception x, Exception y)
        {
            return x.Message == y.Message
                && x?.StackTrace == y?.StackTrace;
        }

        public int GetHashCode(object obj)
        {
            return obj is Exception ex
                 ? GetHashCode(ex)
                 : RuntimeHelpers.GetHashCode(obj);
        }
        bool IEqualityComparer.Equals(object x, object y)
        {
            return x is Exception ex1
                && y is Exception ex2
                && ex1.Equals(ex2);
        }
    }

}
