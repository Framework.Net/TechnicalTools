﻿using System;


namespace TechnicalTools.Model
{
    /// <summary>
    /// Interface pour les objets possédant un nom qui les identifie aux yeux des utilissteurs.
    /// </summary>
    public interface INamedObject
    {
        string UserFriendlyName { get; }
        string TypeName { get; }
    }
}
