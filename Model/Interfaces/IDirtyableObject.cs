﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace TechnicalTools.Model
{
    public interface IDirtyableObject
    {
        bool IsDirty { get; }

        // Touching an object makes it dirty
        void Touch();
    }

    public interface IDirtyableObjectWithNotification
    {
        event EventHandler DirtyChanged;
    }

}
