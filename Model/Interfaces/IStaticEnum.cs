﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TechnicalTools.Model.Cache;

namespace TechnicalTools.Model
{
    /// <summary>
    /// The IStaticEnum class must have all their values defined as statis readonly field and a private constructor
    /// </summary>
    public interface IStaticEnum : IHasTypedIdReadable
    {
        string Code  { get; }
        string Label { get; }
    }
    public static class IStaticEnum_Extensions
    {
        public class StaticEnumValueInfo
        {
            public string        Id        { get { return EnumValue.Id.ToStringInvariant(); } }
            public IStaticEnum   EnumValue { get; private set; }
            public FieldInfo     Field     { get; private set; }

            internal StaticEnumValueInfo(IStaticEnum value, FieldInfo field)
            {
                EnumValue = value;
                Field = field;
            }
        }


        public static IReadOnlyDictionary<string, StaticEnumValueInfo> GetValueSet<T>()
            where T : IStaticEnum
        {
            return GetValueSet(typeof(T));
        }
        public static IReadOnlyDictionary<string, StaticEnumValueInfo> GetValueSet(Type t)
        {
            if (!typeof(IStaticEnum).IsAssignableFrom(t))
                throw new TechnicalException($"Type {t.Name} does not implements {nameof(IStaticEnum)}!", null);
            var res = allEnumsAllValues.TryGetValueClass(t);
            if (res == null)
            {
                res = t.GetFields(BindingFlags.Static | BindingFlags.Public)
                       .Where(f => f.FieldType == t)
                       .Select(f => new StaticEnumValueInfo((IStaticEnum)f.GetValue(null), f))
                       .ToDictionary(fi => fi.Id);
                allEnumsAllValues.TryAdd(t, res);
            }
            return res;
        }
        static readonly ConcurrentDictionary<Type, IReadOnlyDictionary<string, StaticEnumValueInfo>> allEnumsAllValues = new ConcurrentDictionary<Type, IReadOnlyDictionary<string, StaticEnumValueInfo>>();
    }
}
