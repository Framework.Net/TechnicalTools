﻿using System;


namespace TechnicalTools.Model
{
    /// <summary>
    /// Interface que doivent implementer tous les objets "copiable"  clonable
    /// L'objet source servant à la copie n'est pas modifié, excepté pour un cas :
    /// la mise a jour de donné gerant la mise en commun des caches (plutôt rare mais possible, exemple : pattern FlyWeight)
    ///
    /// <example>
    /// // Exemple d'implémentation  (les chevrons sont remplacé par des [ ])
    /// class MyClass : ICloneable[MyClass]
    /// {
    ///    public int Data { get; private set; }
    ///    public MyClass(int data) { Data = data; }
    ///    object ICloneable.Clone()
    ///    {
    ///        return Clone(); // pas de recursion infini cat la methode actuelle est explicitement implementé et ne peut etre appelé par la classe courante
    ///    }
    ///    public MyClass Clone() // Cette methode propose un typage correct pour les developpeur (les developpeur n'auront pas besoin de caster)
    ///    {
    ///        return new MyClass(Data); // Ici on fait la copie
    ///    }
    /// }
    /// </example>
    /// </summary>
    /// <typeparam name="TImplementingClass">The class that implement ICloneable (cf CRTP pattern)</typeparam>
    public interface ICloneable<out TImplementingClass> : System.ICloneable
    {
        new TImplementingClass Clone();
    }
}
