﻿using System;
using System.Collections.Generic;
using System.ComponentModel;


namespace TechnicalTools.Model
{
    // Note : Pour les mots clefs "in" et "out" voir http://msdn.microsoft.com/en-us/library/dd997386.aspx

    /// <summary>
    /// Version de IBindingList dont les implementation corrige le bug du framework :
    /// http://connect.microsoft.com/VisualStudio/feedback/details/148506/listchangedtype-itemdeleted-is-useless-because-listchangedeventargs-newindex-is-already-gone
    /// Vous DEVEZ utiliser Les classes ListChangedEventArgsWithRemovedItem et ListChangedEventArgsWithRemovedItem pour gérer le cas Removed de l'event ListChanged
    /// Voir la classe FixedBindingList qui est l'implementation par defaut de IFixedBindingList.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IFixedBindingList<out T> : IFixedBindingList, IEnumerable<T>
    {

    }
    public interface IFixedBindingList : IBindingList
    {
        void WithListChangedEventsDisabled(Action action_without_event, bool raise_reset_after = true);
    }

    public interface IAdvancedChangeEventArgs
    {
        ListChangedType ListChangedType { get; }

        /// <summary>This is the item that has been added to or deleted from the collection.</summary>
        object ItemAddedOrRemoved { get; }
    }

    public interface IAdvancedChangeEventArgs<out T> : IAdvancedChangeEventArgs
    {
        new T               ItemAddedOrRemoved  { get; }
    }
    public interface IValidatingChangeEventArgs<out T> : IAdvancedChangeEventArgs<T>
    {
        bool Cancel { get; set; }
    }
    public interface IBeforeChangeEventArgs<out T> : IAdvancedChangeEventArgs<T>
    {
    }
    public interface IAfterChangeEventArgs<out T> : IAdvancedChangeEventArgs<T>
    {
    }

    [Serializable]
    public class ListChangedAdvancedEventArgs<T> : ListChangedEventArgs,
                                                   IValidatingChangeEventArgs<T>, IBeforeChangeEventArgs<T>, IAfterChangeEventArgs<T>
    {
        public T           ItemAddedOrRemoved   { get; internal set; } object IAdvancedChangeEventArgs.ItemAddedOrRemoved { get { return ItemAddedOrRemoved; } }
        public eChangeStep ChangeStep           { get; internal set; }
        public bool        Cancel               { get; set; }

        public ListChangedAdvancedEventArgs(ListChangedType listChangedType, int newIndex) : base(listChangedType, newIndex) { }
        public ListChangedAdvancedEventArgs(ListChangedType listChangedType, PropertyDescriptor propDesc) : base(listChangedType, propDesc) { }
        public ListChangedAdvancedEventArgs(ListChangedType listChangedType, int newIndex, PropertyDescriptor propDesc) : base(listChangedType, newIndex, propDesc) { }
        public ListChangedAdvancedEventArgs(ListChangedType listChangedType, int newIndex, int oldIndex) : base(listChangedType, newIndex, oldIndex) { }
    }

    public enum eChangeStep
    {
        ValidateChange,
        BeforeDoingChange,
        ChangedIsDone
    }
}
