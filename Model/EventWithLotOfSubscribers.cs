﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TechnicalTools.Model
{
    /// <summary>
    /// When there is a lot of subscriber (100 000), the following code is very slow :
    /// <code>
    /// foreach(var subscriber in subscribers)
    ///    ownerofAnEvent.AnyEvent += subscriber.Foo(); // Foo Could be kind of "ClearCache" method for example
    /// </code>
    /// By using this class this way, the code is a lot faster
    /// <code>
    /// readonly static links = new EventWithLofOfSubscribers&lt;TypeOfOwnerofEvent, TypeOfSubscriber&gt;(
    ///     (ownerOfEvent, h) => ownerOfEvent.AnyEvent += h,
    ///     (ownerOfEvent, h) => ownerOfEvent.AnyEvent -= h,
    ///     (ownerOfEvent, subscriber) => subscriber.Foo());
    /// );
    /// </code>
    /// </summary>
    /// <typeparam name="TOwnerOfEvent">class owning an event a lot of subscriber are interested</typeparam>
    /// <typeparam name="TSubscriber"></typeparam>
    public class EventWithLotOfSubscribers<TOwnerOfEvent, TSubscriber>
                where TOwnerOfEvent : class
                where TSubscriber   : class
    {
        readonly ConditionalWeakTable<TOwnerOfEvent, List<WeakReference<TSubscriber>>> _instances = new ConditionalWeakTable<TOwnerOfEvent, List<WeakReference<TSubscriber>>>();
        readonly Action<TOwnerOfEvent, EventHandler> _subscribe;
        readonly Action<TOwnerOfEvent, EventHandler> _unsubscribe;
        readonly Action<TOwnerOfEvent, TSubscriber>  _onEvent;
        readonly object _lock = new object();

        public EventWithLotOfSubscribers(Action<TOwnerOfEvent, EventHandler> subscribe,
                                         Action<TOwnerOfEvent, EventHandler> unsubscribe,
                                         Action<TOwnerOfEvent, TSubscriber> onEvent)
        {
            _subscribe = subscribe;
            _unsubscribe = unsubscribe;
            _onEvent = onEvent;
        }
        public void AddSubscriber(TSubscriber s, TOwnerOfEvent ownerOfEvents)
        {
            if (!_instances.TryGetValue(ownerOfEvents, out List<WeakReference<TSubscriber>>  lst))
                lock (_lock) // GetValue can execute concurrently the delegate without locking so we lock ourself
                    if (!_instances.TryGetValue(ownerOfEvents, out lst))
                    {
                        lst = new List<WeakReference<TSubscriber>>();
                        _instances.Add(ownerOfEvents, lst);
                        _subscribe(ownerOfEvents, OnEventHappened);
                    }
            lst.Add(new WeakReference<TSubscriber>(s));
        }

        void OnEventHappened(object sender, EventArgs _)
        {
            // As a MutlicastDelegate we browse all instances and call delegate
            // and if event happens twice, concurrently, this code can executed in parallel... as a normal event
            var set = sender as TOwnerOfEvent;
            if (_instances.TryGetValue(set, out List<WeakReference<TSubscriber>>  lst))
            {
                List<WeakReference<TSubscriber>> safe;
                while (true)
                {
                    try
                    {
                        safe = lst.ToList();
                        break;
                    }
                    catch // race condition while enumerating, we retry
                    {

                    }
                }
                foreach (var wr in safe) // browse lst's instances
                    if (wr.TryGetTarget(out TSubscriber instance))
                        _onEvent(set, instance);
            }
        }
    }
}
