﻿using System;
using System.Diagnostics;

using TechnicalTools.Model.Cache;
using TechnicalTools.Model.Interfaces;
using TechnicalTools.Tools;


namespace TechnicalTools.Model.Composition
{
    public class EntityPiece : PropertyNotifierObject
    {
        protected static readonly string IdPropertyName = GetMemberName.For<IHasMutableClosedId>(o => o.Id);
    }

    public abstract class EntityPiece<TOwner> : EntityPiece, IHasCompositeOwner<TOwner>, IHasMutableCompositeTypedId, ILoadableByCompositeId
           where TOwner : class, IHasMutableTypedId, ILoadableById, IPropertyNotifierObject
    {
        // Soit le Owner a été fourni car l'objet est lié a la classe proprietaire, et que l'instance du proprietaire est déjà chargé
        readonly TOwner _Owner;

        // Soit la classe parent n'a jamais été chargé (ou bien qu'on s'en moque, et dans ce cas on fourni juste son id unique)
        // On peut quand meme tenter d'avoir le owner via GetOwner(true) pour savoir si il a deja ete chargé par quelqu'un dans l'application
        public TypedId<int> OwnerTypedId { get { return _OwnerTypedId ?? (TypedId<int>)_Owner.TypedId; }
            private set
            {
                Debug.Assert(_Owner == null, "On devrait être dans un cas ou dans l'autre mais jamais les deux en même temps");
                _OwnerTypedId = value;
            }
        }
        TypedId<int>? _OwnerTypedId;

        public TOwner GetOwner(bool preventLoading = true) { return _Owner ?? (TOwner)CacheManager.Instance.Get(OwnerTypedId, preventLoading); }

        /// <summary>
        /// Constructeur pour le cas ou l'objet existe déjà en base et que l'on souhaite charger cette classe sans charger son parent
        /// </summary>
        protected EntityPiece(TypedId<int> owner_id)
        {
            _Owner = null;
            // Assert desactivé car maintenant si le cache met a jour Owner (via GetComposite), et que l'objet est new, on s'abonne au changement d'id
            //Debug.Assert(owner_id.Key > 0, "Utiliser l'autre constructeur si l'objet n'existe pas encore en base");
            OwnerTypedId = owner_id;
        }

        /// <summary>
        /// Constructeur pour le cas ou l'objet existe déjà en base ou bien est nouveau (ie: il n'a pas encore d'Id)
        /// </summary>
        protected EntityPiece(TOwner owner)
        {
            Debug.Assert(owner != null, "Utilisez l'autre constructeur si l'objet est null");
            ((IHasCompositeOwner)this).TechnicalOwner = owner;
        }



        object IHasCompositeOwner.TechnicalOwner
        {
            get { return GetOwner(); }
            set
            {
                Debug.Assert(value == null || value is TOwner);
                var owner = GetOwner();
                // Si le propriétaire est New, GetOwner peut fonctionner car il utilise OwnerTypedId,
                // En revanche si ensuite l'objet est sauvegardé et qu'un vrai ID lui est attribué, le cache ne retrouvera pas l'objet car OwnerTypedId n'est pas mis a jour
                if (owner == value && owner != null && owner.State != eState.New)
                    return;
                if (owner != null)
                    owner.IdHasChanged -= Owner_IdChanging;
                Owner_IdChanging(value, null);
                if (value != null)
                    ((IHasMutableTypedId)value).IdHasChanged += Owner_IdChanging;
            }
        }

        void Owner_IdChanging(object sender, PropertyHasChangedEventArgs _)
        {
            var new_tid = (TypedId<int>) ((IHasMutableTypedId) sender).TypedId;
            if (OwnerTypedId.Key >= 0 && new_tid.Key < 0) // cas lorsque l'objet est releasé et que son id est remis a un id temporaire pour que le cache l'oubli
                return;
            OwnerTypedId = new_tid;
            Id = new CompositeTypedId<TOwner>(GetType(), new_tid);
        }

        public CompositeTypedId<TOwner> Id
        {
            get { return _Id; }
            protected set
            {
                Debug.Assert(value.Type == GetType());
                Debug.Assert(typeof(TOwner).IsAssignableFrom(value.Key.Type));
                var old_id = _Id;
                SetTechnicalProperty(ref _Id, value);
                if (IdHasChanged != null && _Id != value)
                    IdHasChanged(this, new PropertyHasChangedEventArgs(IdPropertyName, old_id));
            }
        }
        CompositeTypedId<TOwner> _Id;
        public event PropertyHasChangedEventHandler IdHasChanged;


        IIdTuple                  IHasClosedIdReadable.Id             { get { return Id; } }
        ITypedId                   IHasTypedIdReadable.TypedId        { get { return Id; } }
        ICompositeTypedId IHasCompositeTypedIdReadable.CompositeId    { get { return Id; } set { Id = (CompositeTypedId<TOwner>)value; } }
        TOwner              IHasCompositeOwner<TOwner>.TechnicalOwner { get { return GetOwner(); } }

        ITypedId IHasTypedIdReadable.MakeTypedId(IIdTuple id)
        {
            return new CompositeTypedId<TOwner>(GetType(), (ITypedId)id);
        }
        public virtual void LoadById(ICompositeTypedId id)
        {
            // Au minimum il faut mettre à jour l'id
            Id = new CompositeTypedId<TOwner>(GetType(), OwnerTypedId);
        }
    }
}
