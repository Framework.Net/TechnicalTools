﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

using TechnicalTools.Diagnostics;
using TechnicalTools.Tools;


namespace TechnicalTools.Model
{
    /// <summary>
    /// Classe de base pour les objets souhaitant gérer les notifications de changement de propriétés
    /// <example>
    /// class Person : PropertyNotifierObject
    /// {
    ///     public Person(string firstName, string lastName, int age)
    ///     {
    ///         FirstName = firstName;
    ///         LastName = lastName;
    ///     }
    ///
    ///     public string FirstName { get { return _FirstName; } set { SetProperty(ref _FirstName, value); } }
    ///     string _FirstName; // Ne jamais toucher à cette valeur directement en ecriture , toujours passer par le setter (que vous pouvez mettre privé au besoin)
    ///
    ///     public string LastName { get { return _LastName; } set { SetProperty(ref _LastName, value, OnLastNameChange); } }
    ///     string _LastName; // Idem
    /// }
    /// </example>
    /// Si vous avez certaines propriétés en lecture seule, vous pouvez gérer / déclencher vous même un évènement de changement concernant ces propriete
    /// <example>
    /// public string FullName { get { return FirstName + " " + LastName; } }
    ///
    /// // personne n'a besoin de l'event Changing sur FirstName, donc on ne fait pas (mais on pourrait)
    /// public string FirstName { get { return _FirstName; } set { SetProperty(ref _FirstName, value, OnFirstNameChange /-*, OnFirstNameChanging *-/); } }
    /// string _FirstName;
    ///
    /// void OnFirstNameChange() { RaisePropertyChanged(GetMemberName.For&lt;Person&gt;(p => p.FullName)); }
    /// </example>
    ///
    /// Au niveau de l'abonnement aux évènement, il est possible de le faire selectivement afin de clarifier le code :
    ///
    /// Person someone = new Person(...);
    /// someone.PropertyChangingFor(p => p.Name, OnNameChange);
    ///
    /// </summary>
    [System.Runtime.InteropServices.ComVisible(true)]
    public class PropertyNotifierObject : IPropertyNotifierObject
    {
        // Parent utilise l'objet courant comme un element composite.
        // Parent est utilisé pour tracer l'objet qui a besoin d'un état
        // et est utilisé dans les évènements pour indiquer l'objet dont l'etat a changé.
        private readonly IPropertyNotifierObject Owner;

        public PropertyNotifierObject(IPropertyNotifierObject owner = null)
        {
            Owner = owner;
            RaiseResetBindings = DefaultRaiseResetBindings;
            ObjectTracker.Instance.Track(this);
        }

        /// <summary>
        /// Permet de gérer les évènements PropertyChanged, PropertyChanging, DirtyChanged
        /// Example d'utilisation :
        /// <example>
        /// public string FirstName { get { return _FirstName; } set { SetProperty(ref _FirstName, value); } }
        /// string _FirstName; // Ne jamais toucher à cette valeur directement, toujours passer par le setter
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetProperty<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            CheckIsNotReadOnlyForPropertyChange();
            _SetProperty(ref backingStore, value, onChanged, onChanging, propertyName, true);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetPropertyLazy<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            _SetProperty(ref backingStore, value, onChanged, onChanging, propertyName, false);
        }

       // [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            _SetProperty(ref backingStore, value, onChanged, onChanging, propertyName, false);
        }

        //[DebuggerHidden, DebuggerStepThrough]
        //[MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void _SetProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName, bool is_business_property)
        {
            Debug.Assert(State != eState.Deleted || LockState || !is_business_property,
                         "L'objet courant est modifié par du code alors qu'il ne represente plus uen donnée en base. " + Environment.NewLine +
                         "Si l'objet courant est en cours recyclage (chargement d'un objet avecun autre ID ou reutilisation de l'instance de l'objet pour creer un nouvel enregistrment en base)" +
                         "Passez son etat à New avant de modifier une propriété, ou bien appelez une méthode Load sur l'objet. Les méthodes Load sont capables de recycler les objets");

            DebugTools.CostlyAssert(() => IsCallerAPropertyWithName(propertyName),
                                          string.Format("SetProperty doit être appelé depuis le setter de la propriété {0}, pas ailleurs !", propertyName));
            ArithmeticExceptionHelper.CheckDoubleValue(value);

            // EqualityComparer permet de faire ce a quoi on pense si on pouvait ecrire : "backingStore == value"
            if (EqualityComparer<T>.Default.Equals(backingStore, value)) // important : Evite les faux etat de dirty ET évite la récursion infini sur IsDirty !
                return;

            OnChanging_ManageRelays(propertyName, backingStore, value); // La modelisation structurelle ne doit jamais être bloqué
#pragma warning disable 618
            if (BusinessEventsAreAllowed && !RaisePropertyChanging(propertyName, backingStore, value))
                return;
            onChanging?.Invoke(value);

            backingStore = value;

            // Apres réflexion on souhaite bien que State soit modifié APRES avoir assigné backingStore
            if (is_business_property)
                Touch();
            // ReSharper disable ExplicitCallerInfoArgument
            onChanged?.Invoke();

            if (!is_business_property && TechnicalEventsAreAllowed) // Les propriétés techniques ne doivent pas être lockés
                RaisePropertyChangedTechnical(propertyName);
            if (is_business_property && BusinessEventsAreAllowed)
                RaisePropertyChanged(propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
#pragma warning restore 618
        }

        /// <summary>
        /// Permet de faire évoluer le statut de l'objet afin qu'il indique un état "non synchronisé"
        /// </summary>
        [Obsolete("Utilisez plutot SetDirtyBy si vous connaissez l'object qui a contaminé l'objet surlequel vous appelez Touch")]
        public void Touch() // Ne pas ajouter de parametre pour indiquer la valeur de dirty souhaité car le developpeur DOIT passer par UpdateState
        {
            Debug.Assert(State != eState.Deleted, "Pourquoi modifier un objet qui n'a plus d'existence ?" +
                                                  "Passez son état à New via InitializeState(eState.New) avant de le modifier");

            if (!LockState)
                if (State == eState.Synchronized)
                    State = eState.Unsynchronized;
                else if (State == eState.New && !IsDirty) // cas lorsqu'un objet est tout neuf
                    IsDirty = true;
        }

        public void SetDirtyBy(object contaminating_object)
        {
            // ReSharper disable CSharpWarnings::CS0612
#pragma warning disable CS0618 // Le type ou le membre est obsolète
            Touch();
#pragma warning restore CS0618 // Le type ou le membre est obsolète
            // ReSharper restore CSharpWarnings::CS0612

            if (contaminating_object is IPropertyNotifierObject asPno)
                RaiseRelayDirtyness(asPno);
            else
            {
                // On ne peut rien faire pour le moment concernant contaminating_object
            }
        }

        /// <summary> Fournit une méthode pour gérer la contamination du dirty </summary>
        public void DirtyChangedHandler(object sender, EventArgs e)
        {
            var pno = sender as IPropertyNotifierObject;
            Debug.Assert(pno != null, "Vous devez faire votre propre handler !");
            if (pno.IsDirty)
                SetDirtyBy(pno);
        }


        #region propriétés State, IsDirty & IsReadOnly

        public eState State
        {
            [DebuggerHidden, DebuggerStepThrough]
            get { return _State; }
            private set
            {
                if (_State == value)
                    return;
                if (TechnicalEventsAreAllowed && !RaisePropertyChanging((PropertyNotifierObject pno) => pno.State, _State, value))
                    return;
                var stateBefore = _State;
                _State = value;
                OnStateChanged(stateBefore);
            }
        }
        eState _State = eState.New; // Toujours passer le setter de la proprieté !

        public virtual bool IsDirty
        {
            [DebuggerHidden, DebuggerStepThrough]
            get { return _IsDirty; }
            private set // Si vous souhaitez mettre IsDirty à true, appelez Touch(), ne mettez pas ce setter en protected ou public
            {
                if (_IsDirty == value)
                    return;
                _IsDirty = value;
                if (value)
                    CheckIsNotReadOnlyForPropertyChange();
                RaiseAnyObjectDirtyChanged();
                RaiseDirtyChanged();
            }
        }
        bool _IsDirty; // Toujours passer le setter de la proprieté !



        /// <summary>
        /// Propriete technique verrouillant la modification technique des propriete business
        /// Si on tente de modifier une de ces proprietes alors que IsReadOnly est à true, une exception est levé
        /// Cette propriete a simplement pour but d'aider le développeur à assurer que le comportement qu'il souhaite.
        /// Exemple : un utilisateur n'a pas le droit de modifier un objet : le developpeur peut decider de passer IsReadOnly
        ///           a true pour s'assurer que l'application ne changera jamais ces valeurs puisque l'utilisateur n'a pas les droit.
        ///           Il interdit ainsi techniquement la modification puisque selon lui il ne devrait jamais y en avoir besoin
        ///           Le développeur n'est pas obligé de faire cela, mais en général cela permet une meilleur solidité de l'application
        ///           car les erreurs sont vites trouvées
        /// </summary>
        public virtual bool IsReadOnly
        {
     //       [DebuggerHidden, DebuggerStepThrough]
            get { return __isReadonly; }
       //     [DebuggerHidden, DebuggerStepThrough]
            set
            {
                Debug.Assert(!IsDirty || !value, "L'objet devrait etre Clean avant d'etre mis en readonly !");
                SetTechnicalProperty(ref __isReadonly, value);
            }
        }
        bool __isReadonly;
        [DebuggerHidden, DebuggerStepThrough]
        void CheckIsNotReadOnlyForPropertyChange()
        {
            if (ReadOnlyCheckAsAssert)
                Debug.Assert(!IsReadOnly, "Trying to change a property on an object marked as readonly!");
            else if (IsReadOnly)
                throw new Exception("Trying to change a property on an object marked as readonly!");
        }
        public static bool ReadOnlyCheckAsAssert = false;

        [DebuggerHidden, DebuggerStepThrough]
        public virtual void CheckIsNotReadOnlyForAction(string action_name = null)
        {
            if (IsReadOnly)
                throw new Exception(string.Format("Trying to make a write action {1} on object of type {0} marked as readonly!", GetType().FullName,
                                    (string.IsNullOrEmpty(action_name) ? "" : "(" + action_name + ")")));
        }

        // Compatibilite script
        [System.Runtime.InteropServices.ComVisible(true)] // visibility anciennement declarée dans BusinessObject
        public virtual bool IsNew
        {
            [DebuggerHidden, DebuggerStepThrough]
            get { return State == eState.New; }
            set { } // laisser a vide, pour compatibilité seulement !
        }

        // Compatibilite script
        [System.Runtime.InteropServices.ComVisible(true)] // visibility anciennement declarée dans BusinessObject
        public virtual bool IsDeleted
        {
            [DebuggerHidden, DebuggerStepThrough]
            get { return State == eState.Deleted; }
        }

        // Permet de savoir que l'objet courant a été crée récemment (instance de l'application ou de la fenetre courante)
        // Cela permet de distinguer un objet nouveau enregistré (son etat passe a Synchronized) d'un objet chargé depuis la base (Synchronized aussi)
        // CreatedInCurrentSession indique si l'objet a éte créé pendant la session d'execution du logiciel.
        // CreatedInCurrentSession peut servir par exemple à fournir à l'IHM l'information suivante :
        //   L'utilisateur ayant crée l'objet, l'IHM peut lui fournir des droits d'edition temporaire sur cet objet
        //   La raison est simple : Etant donné que c'est l'utilisateur qui a créé l'objet, et même si'il n'a pas les droits d'edition,
        //   il peut vouloir le créer en le modifiant / sauvegardant plusieurs fois. Dans le cas contraire il devrait tout saisir en une seule fois sans se tromper (!!!)
        //
        // Pour que CreatedInCurrentSession repasse à false, il faut recharger l'objet depuis la base
        [System.Runtime.InteropServices.ComVisible(false)]
        public bool CreatedInCurrentSession { get { return _CreatedInCurrentSession; } private set { _CreatedInCurrentSession = value; } }
        bool _CreatedInCurrentSession = true;


        public event Action<object, eState> StateChanged; // provide the state before the change
        public event EventHandler DirtyChanged;


        /// <summary> Permet de desactiver les evenements PropertyChanging et PropertyChanged par exemple lors du chargement ou la mise à jour de l'objet courant. </summary>
        protected ePropertyEventLock LockedEvents { get; set; }
        bool TechnicalEventsAreAllowed { get { return (LockedEvents & ePropertyEventLock.Technical) == ePropertyEventLock.None; } }
        bool BusinessEventsAreAllowed { get { return (LockedEvents & ePropertyEventLock.Business) == ePropertyEventLock.None; } }
        /// <summary> Permet de locker l'etat de l'objet courant afin de gérer son chargement ou sa mise à jour. </summary>
        protected bool LockState { get; set; }

        public virtual void InitializeState(eState new_state)
        {
            CreatedInCurrentSession = new_state == eState.New;
            // prevent event from being because State can change Dirty and send an event that will contaminate all owner objet
            // Not thread safe !
            if (new_state == eState.New)
                _IsDirty = true;
            State = new_state;
            if (new_state == eState.New)
                IsDirty = false;// Un nouvel objet est considéré comme vierge
        }

        /// <summary>
        ///  Met à jour State une fois l'action de mise a jour (en adequation avec State donc un delete un update ou un create) effectué
        /// </summary>
        public void UpdateState(eState? new_state = null)
        {
            var newState = new_state
                        ?? (State.In(eState.ToDelete, eState.Deleted)
                         ? eState.Deleted
                         : eState.Synchronized);
            State = newState;
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected void RaiseStateChanged(eState stateBefore)
        {
            StateChanged?.Invoke(Owner ?? this, stateBefore);
            RaisePropertyChangedTechnical((PropertyNotifierObject pno) => pno.State);
        }

        protected void RaiseDirtyChanged()
        {
            DirtyChanged?.Invoke(Owner ?? this, EventArgs.Empty);
        }

        #region static event StateChanged



        [DebuggerHidden, DebuggerStepThrough]
        protected void RaiseAnyObjectStateChanged(eState stateBefore)
        {
            var pno = Owner ?? this;
            AnyObjectStateChanged?.Invoke(pno, stateBefore);
            //Delegate dlg;
            //if (_stateObjectListeners.TryGetValue(pno.GetType(), out dlg))
            //    ((Action<IPropertyNotifierObject, eState>)dlg)(pno, stateBefore);

        }
        public static event Action<IPropertyNotifierObject, eState> AnyObjectStateChanged;
        //static readonly Dictionary<Type, Delegate> _stateObjectListeners = new Dictionary<Type, Delegate>();

        //public static void RegisterAnyStateChanged<T>(Action<IPropertyNotifierObject, eState> action)
        //    where T : IPropertyNotifierObject
        //{
        //    var type = typeof(T);
        //    lock (_stateObjectListeners)
        //    {
        //        Delegate dlg;
        //        if (_stateObjectListeners.TryGetValue(type, out dlg))
        //            _stateObjectListeners[type] = (Action<IPropertyNotifierObject, eState>)dlg + action;
        //        else
        //            _stateObjectListeners[type] = action;
        //    }
        //}
        //public static void UnregisterAnyStateChanged<T>(Action<IPropertyNotifierObject, eState> action)
        //    where T : IPropertyNotifierObject
        //{
        //    // ReSharper disable DelegateSubtraction
        //    var type = typeof (T);
        //    lock (_stateObjectListeners)
        //    {
        //        Debug.Assert(_stateObjectListeners.ContainsKey(type));
        //        _stateObjectListeners[typeof(T)] = (Action<IPropertyNotifierObject, eState>)_stateObjectListeners[typeof(T)] - action;
        //    }
        //    // ReSharper restore DelegateSubtraction
        //}

        #endregion static event StateChanged

        public virtual Action CreateActionToRestoreState()
        {
            var state_before = State;
            return () =>
            {
                if (state_before == eState.New)
                    InitializeState(state_before);
                else
                    UpdateState(state_before);
            };
        }


        #region static event Reset

        [DebuggerHidden, DebuggerStepThrough]
        protected void RaiseAnyObjectReset()
        {
            var pno = Owner ?? this;
            if (_resetObjectListeners.TryGetValue(pno.GetType(), out Delegate dlg))
                ((EventHandler)dlg)(pno, EventArgs.Empty);
        }
        static readonly Dictionary<Type, Delegate> _resetObjectListeners = new Dictionary<Type, Delegate>();

        public static void RegisterAnyReset<T>(EventHandler action)
            where T : IPropertyNotifierObject
        {
            var type = typeof(T);
            lock (_resetObjectListeners)
            {
                if (_resetObjectListeners.TryGetValue(type, out Delegate dlg))
                    _resetObjectListeners[type] = (EventHandler)dlg + action;
                else
                    _resetObjectListeners[type] = action;
            }
        }
        public static void UnregisterAnyReset<T>(EventHandler action)
            where T : IPropertyNotifierObject
        {
            // ReSharper disable DelegateSubtraction
            var type = typeof(T);
            lock (_resetObjectListeners)
            {
                Debug.Assert(_resetObjectListeners.ContainsKey(type));
                _resetObjectListeners[typeof(T)] = (EventHandler)_resetObjectListeners[typeof(T)] - action;
            }
            // ReSharper restore DelegateSubtraction
        }

        #endregion static event Reset

        #region Debug & Analysis

        [DebuggerHidden, DebuggerStepThrough]
        protected void RaiseAnyObjectDirtyChanged()
        {
            var h = AnyObjectDirtyChanged;
            if (h != null)
                ExceptionManager.Instance.IgnoreException(() => h(Owner ?? this, EventArgs.Empty));
        }
        public static event EventHandler AnyObjectDirtyChanged;

        protected internal void RaiseRelayDirtyness(IPropertyNotifierObject source_of_dirtyness)
        {
            var h = RelayDirtyness;
            if (h != null)
            {
                if (source_of_dirtyness is PropertyNotifierObject real_source)
                    source_of_dirtyness = real_source.Owner ?? real_source;
                ExceptionManager.Instance.IgnoreException(() => h(this, new DirtynessRelayEventArgs(source_of_dirtyness, Owner ?? this, !IsDirty)));
            }
        }
        public static event EventHandler<DirtynessRelayEventArgs> RelayDirtyness;
        public class DirtynessRelayEventArgs : EventArgs
        {
            public IPropertyNotifierObject ImpactedObject { get; private set; }
            public IPropertyNotifierObject OriginObject { get; private set; }
            public bool CauseDirty { get; private set; }

            public DirtynessRelayEventArgs(IPropertyNotifierObject source, IPropertyNotifierObject target, bool cause_dirty)
            {
                OriginObject = source;
                ImpactedObject = target;
                CauseDirty = cause_dirty;
            }
        }
        #endregion Debug & Analysis

        [DebuggerHidden, DebuggerStepThrough]
        protected virtual void OnStateChanged(eState stateBefore)
        {
            IsDirty = State.In(eState.Unsynchronized, eState.New, eState.ToDelete);
            RaiseAnyObjectStateChanged(stateBefore);
            RaiseStateChanged(stateBefore);
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected void RaiseChangingDirtyChanged()
        {
            DirtyChanged?.Invoke(Owner ?? this, EventArgs.Empty);
        }

        #region Composition of PropertyNotifierObject
        /// <summary>
        /// Permet de mettre en place une relation de causalite sur le statut "IsDirty" d'un sous objet vers son objet parent (qui le contient).
        /// La relation est "structurelle". Cela signifie que peu importe quelle instance d'objet est dans la proprieté à surveiller,
        /// c'est l'objet qui est dans la propriete, a tout moment, que le relai surveillera. Le maise en place de ce relai se desinstaller et se reinstalle automatiquement si les instances d'objet.
        /// Cette méthode permet de remplacer la gestion automatiquement faite par le compilateur VB.Net, via le mot clef withevent.
        ///
        /// <example>
        /// <code>
        /// // Creation d'objet pour le test
        /// var a = LoadObjectA();
        /// var subA1 = LoadSubObjectA();
        /// var subA2 = LoadSubObjectA();
        /// Debug.Assert(!a.IsDirty);
        /// Debug.Assert(!subA1.IsDirty);
        /// Debug.Assert(!subA2.IsDirty);
        ///
        /// // Utilisation de AddDirtynessRelayFor
        /// a.AddDirtynessRelayFor((ObjectAType a2) => a2.SubA, a.SubA)
        /// a.SubA = subA1;
        /// a.UpdateState(eState.Synchronized);
        ///
        /// // Test de fonctionnement
        /// subA1.UnePropriete = UneNouvelleValeur
        /// Debug.Assert(subA1.IsDirty);
        /// Debug.Assert(a.IsDirty); // Effet de AddDirtynessRelayFor qui a relayer l'evenement isdirty de "subA1" à "a"
        /// a.subA = null;
        ///
        /// // Reinitialise
        /// a.UpdateState(eState.Synchronized);
        /// Debug.Assert(!a.IsDirty);
        ///
        /// // Autre test
        /// subA1.UnePropriete = UneAutreNouvelleValeur
        /// Debug.Assert(subA1.IsDirty);
        /// Debug.Assert(!a.IsDirty); // AddDirtynessRelayFor n'a pas d'effet car subA1 n'est plus dans la propriete subA
        /// </code>
        /// </example>
        /// Prérequis : La propriete utilise pour AddDirtyRelaysFor DOIT utiliser la méthode SetProperty.
        ///
        /// Note  : AddDirtyRelayFor ainsi que SetProperty ne doivent pas être utilisés pour les proprietes lazy.
        /// Note2 : Les proprietes à la fois lazy ET accessible en ecriture (cas rare) doivent tout faire elles mêmes, en utilisant par exemple LockWhileUpdatingWith
        /// </summary>
        /// <param name="expression">Expression permettant de designer la propriété à surveiller</param>
        /// <param name="subObject">Valeur actuelle de la propriété à surveiller (peut être null)</param>
        [DebuggerHidden, DebuggerStepThrough]
        protected void AddDirtynessRelayFor<TFrom, TTo>(Expression<Func<TFrom, TTo>> expression, TTo subObject)
            where TFrom : class, IPropertyNotifierObject
            where TTo : class, IPropertyNotifierObject
        {
            string property = GetMemberName.For(expression);
            if (!_dirtyness_propertyrelays.Contains(property))
            {
                _dirtyness_propertyrelays.Add(property);
                if (subObject != null)
                    subObject.DirtyChanged += SubObject_DirtyChanged;
            }
        }
        // Pour Kronos
        protected void AddDirtynessRelayFor(string property, IPropertyNotifierObject subObject)
        {
            if (!_dirtyness_propertyrelays.Contains(property))
            {
                _dirtyness_propertyrelays.Add(property);
                if (subObject != null)
                    subObject.DirtyChanged += SubObject_DirtyChanged;
            }
        }
        /// <summary>
        /// Defait la relation créée par AddDirtynessRelayFor
        /// </summary>
        /// <param name="expression">Expression permettant de designer la propriete à ne plus surveiller</param>
        /// <param name="obj">Valeur actuelle de la propriete a ne plus surveiller (peut être null)</param>
        [DebuggerHidden, DebuggerStepThrough]
        protected void RemoveDirtynessRelayFor<TFrom, TTo>(Expression<Func<TFrom, TTo>> expression, TTo obj)
            where TFrom : class, IPropertyNotifierObject
            where TTo : class, IPropertyNotifierObject
        {
            string property = GetMemberName.For(expression);
            if (_dirtyness_propertyrelays.Contains(property))
            {
                if (obj != null)
                    obj.DirtyChanged -= SubObject_DirtyChanged;
                _dirtyness_propertyrelays.Remove(property);
            }
        }
        [DebuggerHidden, DebuggerStepThrough]
        protected void SubObject_DirtyChanged(object dirtyable_subobject, EventArgs __)
        {
            var pno = (IPropertyNotifierObject)dirtyable_subobject;
            RaiseRelayDirtyness(pno);
            if (pno.IsDirty)
                SetDirtyBy(((IPropertyNotifierObject)dirtyable_subobject));
        }

        HashSet<string> _dirtyness_propertyrelays { get { return __dirtyness_propertyrelays ?? (__dirtyness_propertyrelays = new HashSet<string>()); } }
        HashSet<string> __dirtyness_propertyrelays;

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void OnChanging_ManageRelays<T>(string propertyName, T before, T after)
        {
            if (typeof(T).IsValueType)
                return;
            if (!_dirtyness_propertyrelays.Contains(propertyName))
                return;
            // ReSharper disable CompareNonConstrainedGenericWithNull
            if (before != null)
                ((IPropertyNotifierObject)before).DirtyChanged -= SubObject_DirtyChanged;
            if (after != null)
                ((IPropertyNotifierObject)after).DirtyChanged += SubObject_DirtyChanged;
            // ReSharper restore CompareNonConstrainedGenericWithNull
        }

        // Permet de modifier un objet sans declencher d'evenement et de changement d'etat
        // A n'utiliser que pour le lazy loading
        [DebuggerHidden, DebuggerStepThrough]
        protected void LockWhileUpdatingWith(Action action)
        {
            LockWhileUpdatingWith(ePropertyEventLock.Business, action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        protected void LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            bool lState = LockState;
            ePropertyEventLock lEvents = LockedEvents;
            try
            {
                LockState = true;
                LockedEvents |= lck;
                action();
            }
            finally
            {
                LockedEvents = lEvents;
                LockState = lState;
            }
            RaiseAnyObjectReset();
            RaiseResetBindings(lck & ~LockedEvents);
        }
        void DefaultRaiseResetBindings(ePropertyEventLock deltaEvents)
        {
            // Indiquer le delta permet aux subscribers, si ils le souhaitent, de ne gérer que ce qui a besoin d'être géré.
            // Autrement dit cela permet des optimisations...
            OnResetBindings?.Invoke(this, deltaEvents);
        }
        public Action<ePropertyEventLock> RaiseResetBindings; //= DefaultRaiseResetBindings;

        #endregion Composition of PropertyNotifierObject

        #endregion propriétés State, IsDirty & IsReadOnly





        #region Gestion de l'event PropertyChanged
        /// <summary>
        /// Enregistre un callback lorsqu'une propriété particulière de l'objet courant est modifiée.
        /// Cela rend le code plus lisibile et la recherche de comportement plus aisée dans une classe que l'on ne connait pas.
        /// Exemple d'utilisation :
        /// <example>
        /// // dans le constructeur : this.PersonEdited = person_edited;
        /// void WireUpEvents()
        /// {
        ///     this.PersonEdited.PropertyChangedFor&lt;Person&gt;(p => p.Name, PersonWatched_NameChanged);
        /// }
        /// void PersonWatched_NameChanged() { /-* to implement *-/ }
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        void IPropertyNotifierObject.PropertyChangedFor<T>(Expression<Func<T, object>> expression, Action onPropertyChange)
        //where T : class, IPropertyNotifierObject
        {
            Debug.Assert((Owner ?? this) is T,
             "Le type d'objet utilisé pour spécifier la propriété à surveiller est incompatible avec le type de l'objet surveillé !");

            string propertyNameWatched = GetMemberName.For(expression);
            //[DebuggerHidden, DebuggerStepThrough] // we would like to write this to improve debugging experience
            void handler(object _, PropertyChangedEventArgs e)
            {
                if (e.PropertyName == propertyNameWatched)
                    onPropertyChange();
            }
            _PropertyChanged += handler;

            // Sauvegarde le handler afin de pouvoir le supprimer dans PropertyChangedFor_Remove, sinon impossible à faire
            if (_changed_handlers == null)
                _changed_handlers = new Dictionary<Tuple<string, Action>, PropertyChangedEventHandler>();
            var key = Tuple.Create(propertyNameWatched, onPropertyChange);
            if (!_changed_handlers.ContainsKey(key))
                _changed_handlers.Add(key, handler);
            else
                Debug.Fail("Cette action a deja été définie pour la propriété de cet objet ! L'action sera donc réalisée deux fois !");
        }
        Dictionary<Tuple<string, Action>, PropertyChangedEventHandler> _changed_handlers;



        /// <summary>
        /// Désinscrit le callback installé par PropertyChangedFor lorsqu'une propriété particulière de l'objet courant est modifiée.
        /// L'action ne doit pas être une lambda pour fonctionner mais une méthode !
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        void IPropertyNotifierObject.PropertyChangedFor_Remove<T>(Expression<Func<T, object>> expression, Action onPropertyChanged)
        // where T : class, IPropertyNotifierObject
        {
            Debug.Assert((Owner ?? this) is T,
             "Le type d'objet utilisé pour spécifier la propriété à surveiller est incompatible avec le type de l'objet surveillé !");

            if (_changed_handlers == null)
                return;
            string propertyNameWatched = GetMemberName.For(expression);
            var key = Tuple.Create(propertyNameWatched, onPropertyChanged);
            if (!_changed_handlers.ContainsKey(key))
                return;
            _PropertyChanged -= _changed_handlers[key];
            _changed_handlers.Remove(key);
            if (_changed_handlers.Count == 0)
                _changed_handlers = null;
        }


        // Implémentation explicite pour inciter les développeurs à :
        // - overrider OnDirtyChange
        // - ou utiliser DoWhenValueChangeFor
        // Le but étant de les forcer à ne pas utiliser de chaines de caractères (nom des propriété en dur) en dur
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            [DebuggerHidden, DebuggerStepThrough]
            add    { _PropertyChanged += value; }
            [DebuggerHidden, DebuggerStepThrough]
            remove { _PropertyChanged -= value; }
        }
        event PropertyChangedEventHandler _PropertyChanged;
        event Action<PropertyNotifierObject, ePropertyEventLock> IPropertyNotifierObject.OnResetBindings
        {
            [DebuggerHidden, DebuggerStepThrough]
            add { OnResetBindings += value; }
            [DebuggerHidden, DebuggerStepThrough]
            remove { OnResetBindings -= value; }
        }
        public event Action<PropertyNotifierObject, ePropertyEventLock> OnResetBindings;

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void RaisePropertyChangedTechnical<T, TProperty>(Expression<Func<T, TProperty>> expression)
        {
#pragma warning disable 618
            // ReSharper disable ExplicitCallerInfoArgument
            RaisePropertyChangedTechnical(GetMemberName.For(expression));
            // ReSharper restore ExplicitCallerInfoArgument
#pragma warning restore 618
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void RaisePropertyChanged<T, TProperty>(Expression<Func<T, TProperty>> expression)
        {
#pragma warning disable 618
            // ReSharper disable ExplicitCallerInfoArgument
            RaisePropertyChanged(GetMemberName.For(expression));
            // ReSharper restore ExplicitCallerInfoArgument
#pragma warning restore 618
        }

        [Obsolete("Préférez la version de RaisePropertyChanged qui utilise une lambda pour indiquer la propriete à mettre à jour !", false)]
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            _PropertyChanged?.Invoke(Owner ?? this, new PropertyChangedEventArgs(propertyName));
        }

        [Obsolete("Préférez la version de RaisePropertyChanged qui utilise une lambda pour indiquer la propriete à mettre à jour !", false)]
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void RaisePropertyChangedTechnical([CallerMemberName] string propertyName = null)
        {
            _PropertyChanged?.Invoke(Owner ?? this, new TechnicalPropertyChangedEventArgs(propertyName));
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RelayPropertyChangedEvent(PropertyChangedEventArgs e)
        {
            _PropertyChanged?.Invoke(Owner ?? this, e);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RelayPropertyChangedEventHandler(object _, PropertyChangedEventArgs e)
        {
            RelayPropertyChangedEvent(e);
        }

        #endregion Gestion de l'event PropertyChanged

        #region Gestion de l'event PropertyChanging
        /// <summary>
        /// Enregistre un callback lorsqu'une propriété particulière de l'objet courant s'apprête à être modifiée.
        /// Cela rend le code plus lisibile et la recherche de comportement plus aisée dans une classe que l'on ne connait pas.
        /// Exemple d'utilisation :
        /// <example>
        /// // dans le constructeur : this.PersonEdited = person_edited;
        /// void WireUpEvents()
        /// {
        ///     this.PersonEdited.PropertyChangingFor&lt;Person&gt;(p => p.Name, PersonWatched_NameChanging);
        /// }
        /// void PersonWatched_NameChanging(e) { /-* to implement *-/ }
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        void IPropertyNotifierObject.PropertyChangingFor<T>(Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        //(FYI) where T : class, IPropertyNotifierObject
        {
            Debug.Assert((Owner ?? this) is T,
                         "Le type d'objet utilisé pour spécifier la propriété à surveiller est incompatible avec le type de l'objet surveillé !");
            string propertyNameWatched = GetMemberName.For(expression);
            _PropertyChangingFor(propertyNameWatched, onPropertyChanging);
        }
        [Obsolete("Ne pas utiliser... uniquement reservé au BindingManager du bas", false)]
        [DebuggerHidden, DebuggerStepThrough]
        void BindingPropertyChangingFor(string propertyNameWatched, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        {
            _PropertyChangingFor(propertyNameWatched, onPropertyChanging);
        }

        void _PropertyChangingFor(string propertyNameWatched, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        {
#if SUPER_DEBUG
            StackTrace dbgStack = null;
            if (DebugTools.CaptureStackTraceOnDeferredEvents)
            {
                dbgStack = new StackTrace(true);
            }
#endif
            //[DebuggerHidden, DebuggerStepThrough] // we would like to write this to improve debugging experience
            void handler(object obj, PropertyChangingEventArgs e)
            {
                if (e.PropertyName == propertyNameWatched)
                    onPropertyChanging(obj, e);
#if SUPER_DEBUG
#pragma warning disable 1717
                dbgStack = dbgStack;
#pragma warning restore 1717
#endif
            }
            PropertyChanging += handler;

            // Sauvegarde le handler afin de pouvoir le supprimer dans PropertyChangingFor_Remove, sinon impossible à faire
            if (_changing_handlers == null)
                _changing_handlers = new Dictionary<Tuple<string, Action<object, PropertyChangingEventArgs>>, PropertyChangingEventHandler>();
            var key = Tuple.Create(propertyNameWatched, onPropertyChanging);
            if (!_changing_handlers.ContainsKey(key))
                _changing_handlers.Add(key, handler);
            else
                Debug.Fail("Cette action a deja été définie pour la propriété de cet objet ! L'action sera donc réalisée deux fois !");
        }
        Dictionary<Tuple<string, Action<object, PropertyChangingEventArgs>>, PropertyChangingEventHandler> _changing_handlers;


        /// <summary>
        /// Désinscrit le callback installé par PropertyChangingFor lorsqu'une propriété particulière de l'objet courant est modifiée.
        /// L'action ne doit pas être une lambda pour fonctionner mais une méthode !
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        void IPropertyNotifierObject.PropertyChangingFor_Remove<T>(Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        //(FYI) where T : class, IPropertyNotifierObject
        {
            Debug.Assert((Owner ?? this) is T,
             "Le type d'objet utilisé pour spécifier la propriété à surveiller est incompatible avec le type de l'objet surveillé !");
            string propertyNameWatched = GetMemberName.For(expression);
            _PropertyChangingFor_Remove(propertyNameWatched, onPropertyChanging);
        }
        void _PropertyChangingFor_Remove(string propertyNameWatched, Action<object, PropertyChangingEventArgs> onPropertyChanging)
        {
            if (_changing_handlers == null)
                return;
            var key = Tuple.Create(propertyNameWatched, onPropertyChanging);
            if (!_changing_handlers.ContainsKey(key))
                return;
            PropertyChanging -= _changing_handlers[key];
            _changing_handlers.Remove(key);
            if (_changing_handlers.Count == 0)
                _changing_handlers = null;
        }

        //public delegate void PropertyChangingEventHandler(BusinessObject sender, string propertyName);
        event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Leve l'evenement PropertyChanging et retourne false si la modification a été annulé
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName"></param>
        /// <param name="before"></param>
        /// <param name="after"></param>
        /// <returns></returns>
        [Obsolete("Preferez la version de RaisePropertyChanging qui utilise une lambda pour indiquer la propriete à mettre à jour !", false)]
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected bool RaisePropertyChanging<T>(string propertyName, T before, T after)
        {
            if (PropertyChanging == null)
                return true;

            var e = new PropertyChangingEventArgs<T>(propertyName, before, after);
            PropertyChanging(Owner ?? this, e);
            if (e.Cancel && e.CancelException != null)
                throw e.CancelException;
            return !e.Cancel;
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected bool RaisePropertyChanging<TValue, TObject>(Expression<Func<TObject, object>> expression, TValue before, TValue after)
        {
            var propertyName = GetMemberName.For(expression);
#pragma warning disable 618
            return RaisePropertyChanging(propertyName, before, after);
#pragma warning restore 618
        }
        #endregion Gestion de l'event PropertyChanging

        #region Gestion de dépendances et notification d'evenèment entre propriété
        [DebuggerHidden, DebuggerStepThrough]
        void IPropertyNotifierObject.CurrentPropertyIsDependantOn<T>(Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property, /*[CallerMemberName]*/ string dependent_propertyName)
        //where T : class, IPropertyNotifierObject
        {
            Debug.Assert((Owner ?? this) is T, "Le type d'objet utilisé pour spécifier la propriété à surveiller est incompatible avec le type de l'objet surveillé !");

            DebugTools.CostlyAssert(() => IsCallerAPropertyWithName(dependent_propertyName),
                                          string.Format("SetProperty doit être appelé depuis le Getter de la propriété {0}, pas ailleurs !", dependent_propertyName));

            if (_dependencies == null)
                _dependencies = new HashSet<PropertyDependency>();

            var relation = new PropertyDependency()
            {
                DependencyProperty = GetMemberName.For(dependency_prop_has_expression),
                DependentProperty = dependent_propertyName
            };

            if (_dependencies.Add(relation)) // Si relation pas déjà défini
            {
                relation.DependentPropertyChanged = () =>
                {
                    if (refresh_current_property != null && !refresh_current_property())
                        return; // event canceled
                    // ReSharper disable CSharpWarnings::CS0612
                    #pragma warning disable CS0618 // Le type ou le membre est obsolète
                    RaisePropertyChanged(dependent_propertyName);
                    #pragma warning restore CS0618 // Le type ou le membre est obsolète
                    // ReSharper restore CSharpWarnings::CS0612
                };
                ((IPropertyNotifierObject)this).PropertyChangedFor(dependency_prop_has_expression, relation.DependentPropertyChanged);
            }
        }

        public class PropertyDependency : IEquatable<PropertyDependency>
        {
            public string DependentProperty { get; internal set; }
            public string DependencyProperty { get; internal set; }
            internal Action DependentPropertyChanged { get; set; }

            public override bool Equals(object obj)
            {
                return obj is PropertyDependency &&
                       ((PropertyDependency)obj).DependentProperty == DependentProperty &&
                       ((PropertyDependency)obj).DependencyProperty == DependencyProperty;
            }
            bool IEquatable<PropertyDependency>.Equals(PropertyDependency other) { return Equals(other); }

            public override int GetHashCode()
            {
                unchecked
                {
                    return ((DependencyProperty != null ? DependencyProperty.GetHashCode() : 0) * 397)
                          ^ (DependentProperty != null ? DependentProperty.GetHashCode() : 0);
                }
            }
        }

        HashSet<PropertyDependency> _dependencies;


        #endregion


        // Cette région implémente IPropertyNotifierObject qui permet de faire du multi heritage (des Mixin plus précisement qui sont plus "legales" d'un point de vue architeture de code)
        #region (Prive) Preoccupations architecturale du code et Check en debug

        // Cette méthode peut ralentir l'execution du logiciel, elle disparait en release
        [DebuggerHidden, DebuggerStepThrough]
        bool IsCallerAPropertyWithName(string propertyName)
        {
            var stackTrace = new StackTrace();

            int f = 2;

            System.Reflection.MethodBase caller;

            // Pour une propriete Foo, le compilateur C# crée des méthodes pour le getter et le setter dont les noms seront prefixé par "get_" "set_"
            // C'est ce qu'on teste ici (cela pourrait echouer si il existe deja une méthode du meme nom mais c'est très improbable)
            do
            {
                var stackFrames = stackTrace.GetFrames();
                Debug.Assert(stackFrames != null);
                StackFrame frame = stackFrames[f];
                ++f;
                caller = frame.GetMethod();
            } while (caller.Name.Substring(caller.Name.LastIndexOf('.') + 1) == "SetProperty");

            if (caller.Name.Equals("set_" + propertyName, StringComparison.InvariantCulture))
                return true;

            return false;
        }


        // Tout le reste sont des implementations explicites : car on veut garder l'acces "protected" sur les membres appelés
        // mais permettre de faire des mixins en passant par l'interface IPropertyNotifierObject
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            Debug.Assert(propertyName != null, "propertyName != null");
            // ReSharper disable ExplicitCallerInfoArgument
            SetProperty(ref backingStore, value, onChanged, onChanging, propertyName);
            // ReSharper restore ExplicitCallerInfoArgument
        }

        // Tout le reste sont des implementations explicites : car on veut garder l'acces "protected" sur les membres appelés
        // mais permettre de faire des mixins en passant par l'interface IPropertyNotifierObject
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.SetTechnicalProperty<T>(ref T backingStore, T value, Action onChanged, Action<T> onChanging, string propertyName)
        {
            Debug.Assert(propertyName != null, "propertyName != null");
            // ReSharper disable ExplicitCallerInfoArgument
            SetTechnicalProperty(ref backingStore, value, onChanged, onChanging, propertyName);
            // ReSharper restore ExplicitCallerInfoArgumentp
        }


        [DebuggerHidden, DebuggerStepThrough]
        void IObjectWithState.InitializeState(eState value)
        {
            InitializeState(value);
        }
        [DebuggerHidden, DebuggerStepThrough]
        void IObjectWithState.UpdateState(eState? value)
        {
            UpdateState(value);
        }

        ePropertyEventLock IPropertyNotifierObject.LockedEvents { get { return LockedEvents; } set { LockedEvents = value; } }
        bool IPropertyNotifierObject.LockState { get { return LockState; } set { LockState = value; } }

        [DebuggerHidden, DebuggerStepThrough]
        void IPropertyNotifierObject.CheckIsNotReadOnlyForAction(string action_name/* = null*/)
        {
            CheckIsNotReadOnlyForAction(action_name);
        }

        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(Action action)
        {
            LockWhileUpdatingWith(action);
        }
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void IPropertyNotifierObject.LockWhileUpdatingWith(ePropertyEventLock lck, Action action)
        {
            LockWhileUpdatingWith(lck, action);
        }


        #endregion (Prive) Preoccupations architecturale du code et Check en debug

        public interface IMemento : Model.IMemento
        {
        }
        protected class Memento : IMemento
        {
            public eState State { get; set; }
            public bool IsDirty { get; set; }
            public bool IsReadonly { get; set; }
            public bool CreatedInCurrentSession { get; set; }
            public bool LockState { get; set; }
            public ePropertyEventLock LockedEvents { get; set; }

            public Dictionary<Tuple<string, Action<object, PropertyChangingEventArgs>>, PropertyChangingEventHandler> Changing_handlers { get; set; }
            public Dictionary<Tuple<string, Action>, PropertyChangedEventHandler> Changed_handlers { get; set; }
            public HashSet<PropertyDependency> Dependencies { get; set; }
            public HashSet<string> Dirtyness_propertyrelays { get; set; }
            public Action<ePropertyEventLock> RaiseResetBindings { get; set; }

            public Action<object, eState> StateChanged { get; set; }
            public EventHandler DirtyChanged { get; set; }
            public PropertyChangedEventHandler  PropertyChanged { get; set; }
            public PropertyChangingEventHandler PropertyChanging { get; set; }
            public Action<PropertyNotifierObject, ePropertyEventLock> OnResetBindings { get; set; }
        }
        public virtual Model.IMemento CreateMemento()
        {
            var mem = new Memento();
            FillMemento(mem);
            return mem;
        }
        protected void FillMemento(Memento mem)
        {
            //mem.Owner = Owner; // readonly so we dont care
            mem.State = State;
            mem.IsDirty = IsDirty;
            mem.IsReadonly = IsReadOnly;
            mem.CreatedInCurrentSession = CreatedInCurrentSession;
            mem.LockState = LockState;
            mem.LockedEvents = LockedEvents;
            // TODO : A optimiser : pas besoin d'un dico, une liste suffit
            mem.Changing_handlers = _changing_handlers == null ? null : new Dictionary<Tuple<string, Action<object, PropertyChangingEventArgs>>, PropertyChangingEventHandler>(_changing_handlers);
            mem.Changed_handlers = _changed_handlers == null ? null : new Dictionary<Tuple<string, Action>, PropertyChangedEventHandler>(_changed_handlers);
            mem.Dependencies = _dependencies == null ? null : new HashSet<PropertyDependency>(_dependencies);
            mem.Dirtyness_propertyrelays = _dirtyness_propertyrelays == null ? null : new HashSet<string>(_dirtyness_propertyrelays);

            mem.StateChanged = StateChanged;
            mem.DirtyChanged = DirtyChanged;
            mem.PropertyChanged = _PropertyChanged;
            mem.PropertyChanging = PropertyChanging;
            mem.OnResetBindings = OnResetBindings;
            mem.RaiseResetBindings = RaiseResetBindings; // TODO c'est quoi ce champs ? ca fait pas double emploi avec OnResetBindings ?
            //var eventsField = typeof(Component).GetField("events", BindingFlags.NonPublic | BindingFlags.Instance);
            //var eventHandlerList = eventsField.GetValue(button1);
            //eventsField.SetValue(button2, eventHandlerList);
        }
        public virtual void RevertToMemento(Model.IMemento mem_)
        {
            RestoreMemento(mem_);
            RaiseResetBindings(ePropertyEventLock.All);
        }
        protected virtual void RestoreMemento(Model.IMemento mem_)
        {
            var mem = (Memento)mem_;

            //Owner = mem.Owner; // readonly so we dont care
            _State = mem.State;
            _IsDirty = mem.IsDirty;
            __isReadonly = mem.IsReadonly;
            _CreatedInCurrentSession = mem.CreatedInCurrentSession;
            LockState = mem.LockState;
            LockedEvents = mem.LockedEvents;

            if (mem.Changing_handlers == null)
                _changing_handlers = null;
            else
            {
                if (_changing_handlers == null)
                    _changing_handlers = new Dictionary<Tuple<string, Action<object, PropertyChangingEventArgs>>, PropertyChangingEventHandler>();
                _changing_handlers.Clear();
                foreach (var pair in mem.Changing_handlers)
                    _changing_handlers.Add(pair.Key, pair.Value);
            }
            if (mem.Changed_handlers == null)
                _changed_handlers = null;
            else
            {
                if (_changed_handlers == null)
                    _changed_handlers = new Dictionary<Tuple<string, Action>, PropertyChangedEventHandler>();
                _changed_handlers.Clear();
                foreach (var pair in mem.Changed_handlers)
                    _changed_handlers.Add(pair.Key, pair.Value);
            }
            if (mem.Dependencies == null)
                _dependencies = null;
            else
            {
                if (_dependencies == null)
                    _dependencies = new HashSet<PropertyDependency>();
                _dependencies.Clear();
                foreach (var item in mem.Dependencies)
                    _dependencies.Add(item);
            }
            if (mem.Dirtyness_propertyrelays == null)
                __dirtyness_propertyrelays = null;
            else
            {
                if (__dirtyness_propertyrelays == null)
                    __dirtyness_propertyrelays = new HashSet<string>();
                _dirtyness_propertyrelays.Clear();
                foreach (var item in mem.Dirtyness_propertyrelays)
                    _dirtyness_propertyrelays.Add(item);
            }

            StateChanged = mem.StateChanged;
            DirtyChanged = mem.DirtyChanged;
            _PropertyChanged = mem.PropertyChanged;
            PropertyChanging = mem.PropertyChanging;
            OnResetBindings = mem.OnResetBindings;
            RaiseResetBindings = mem.RaiseResetBindings; // TODO c'est quo ice champs ? ca fait pas double emploi avec OnResetBindings ?
            //var eventsField = typeof(Component).GetField("events", BindingFlags.NonPublic | BindingFlags.Instance);
            //var eventHandlerList = eventsField.GetValue(button1);
            //eventsField.SetValue(button2, eventHandlerList);
        }
    }

    public class TechnicalPropertyChangedEventArgs : PropertyChangedEventArgs
    {
        public TechnicalPropertyChangedEventArgs(string propertyName)
            : base(propertyName)
        {
        }
    }

    [Flags]
    public enum ePropertyEventLock
    {
        None = 0,
        Business = 1,
        Technical = 2,
        All = Business | Technical
    }
}
