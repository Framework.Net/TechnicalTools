﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ComponentModel;



namespace TechnicalTools.Model
{
    public interface IFixedBindingListAdvanced<out T> : IFixedBindingList, IEnumerable<T>
    {

    }
    public class FixedBindingListAdvanced<T> : FixedBindingList<T>, IFixedBindingListAdvanced<T>
    {
        public FixedBindingListAdvanced()
        {
            ListChanged += OnListChanged;
        }

        public FixedBindingListAdvanced(IList<T> list)
            : base(list)
        {
            ListChanged += OnListChanged;
        }
        public FixedBindingListAdvanced(IEnumerable<T> values)
            : base(values.ToList())
        {
            ListChanged += OnListChanged;
        }

        void OnListChanged(object sender, ListChangedEventArgs e)
        {
            if (e is ListChangedAdvancedEventArgs<T> ee)
            {
                Debug.Assert(ee.ChangeStep == eChangeStep.ChangedIsDone, "This is the only step that can be done with legacy event");
                AfterListChange?.Invoke(this, ee);
            }
        }

        public event EventHandler<IValidatingChangeEventArgs<T>> ListChangeValidating;
        public event EventHandler<IBeforeChangeEventArgs<T>>     BeforeListChange;
        public event EventHandler<IAfterChangeEventArgs<T>>      AfterListChange;

        //public event EventHandler Clearing;

        //protected override void ClearItems()
        //{
        //    if (Count > 0 && Clearing != null)
        //        Clearing(this, EventArgs.Empty);
        //    base.ClearItems();
        //}

        protected override void ClearItems()
        {
            if (Count == 0)
                return;
            if (!RaiseListChangedEvents)
            {
                base.ClearItems();
                return;
            }
            ListChangedAdvancedEventArgs<T> e = null;
            if (ListChangeValidating != null)
            {
                e = new ListChangedAdvancedEventArgs<T>(ListChangedType.Reset, -1) { ChangeStep = eChangeStep.ValidateChange };
                ListChangeValidating?.Invoke(this, e);
            }
            else if (AfterListChange != null || BeforeListChange != null)
                e = new ListChangedAdvancedEventArgs<T>(ListChangedType.Reset, -1);
            // no exception ? Ok continuing...
            if (BeforeListChange != null && e != null)
            {
                e.ChangeStep = eChangeStep.BeforeDoingChange;
                BeforeListChange(this, e);
            }
            base.ClearItems();

            if (AfterListChange != null && e != null)
            {
                e.ChangeStep = eChangeStep.ChangedIsDone;
                AfterListChange(this, e);
            }
        }

        protected override void InsertItem(int index, T item)
        {
            if (!RaiseListChangedEvents)
            {
                base.InsertItem(index, item);
                return;
            }
            ListChangedAdvancedEventArgs<T> e = null;
            if (ListChangeValidating != null)
            {
                e = new ListChangedAdvancedEventArgs<T>(ListChangedType.ItemAdded, index) { ChangeStep = eChangeStep.ValidateChange, ItemAddedOrRemoved = item };
                ListChangeValidating(this, e);
            }
            else if (AfterListChange != null || BeforeListChange != null)
                e = new ListChangedAdvancedEventArgs<T>(ListChangedType.ItemAdded, index) { ItemAddedOrRemoved = item };
            // no exception ? Ok continuing...
            if (BeforeListChange != null && e != null)
            {
                e.ChangeStep = eChangeStep.BeforeDoingChange;
                BeforeListChange(this, e);
            }
            base.InsertItem(index, item);

            if (AfterListChange != null && e != null)
            {
                e.ChangeStep = eChangeStep.ChangedIsDone;
                AfterListChange(this, e);
            }
        }

        protected override void RemoveItem(int index)
        {
            if (!RaiseListChangedEvents)
            {
                base.RemoveItem(index);
                return;
            }
            ListChangedAdvancedEventArgs<T> e = null;
            if (ListChangeValidating != null)
            {
                e = new ListChangedAdvancedEventArgs<T>(ListChangedType.ItemDeleted, index) { ChangeStep = eChangeStep.ValidateChange, ItemAddedOrRemoved = this[index] };
                ListChangeValidating(this, e);
            }
            else if (AfterListChange != null || BeforeListChange != null)
                e = new ListChangedAdvancedEventArgs<T>(ListChangedType.ItemDeleted, index) { ItemAddedOrRemoved = this[index] };
            // no exception ? Ok continuing...
            if (BeforeListChange != null && e != null)
            {
                e.ChangeStep = eChangeStep.BeforeDoingChange;
                BeforeListChange(this, e);
            }
            base.RemoveItem(index);

            // Dont do it because it is already done in OnListChanged
            //if (AfterListChange != null)
            //{
            //    e.ChangeStep = eChangeStep.ChangedIsDone;
            //    AfterListChange(this, e);
            //}
        }


    }

}
