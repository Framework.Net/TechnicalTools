﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnicalTools.Model;
using TechnicalTools.Tools;

namespace TechnicalTools.Model
{
    [Serializable] // TODO (amelioration) : voir http://www.gavaghan.org/blog/2007/07/17/fixing-bindinglist-deserialization/
    public class CheckableBindingList<T> : FixedBindingList<T>
    {
        public CheckableBindingList()
        {
        }

        public CheckableBindingList(IList<T> list)
            : base(list)
        {
        }
        public CheckableBindingList(IEnumerable<T> values)
            : base(values.ToList())
        {
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected override void InsertItem(int index, T item)
        {
            var h = ItemAdding;
            var h2 = ItemAdded;
            ItemAddedEventArgs e = null;
            if (h != null || h2 != null)
                e = new ItemAddedEventArgs(item, index);
            h?.Invoke(this, e);

            base.InsertItem(index, item);

            h2?.Invoke(this, e);
        }
        public event EventHandler<ItemAddingEventArgs>   ItemAdding;
        public event EventHandler<ItemAddedEventArgs>   ItemAdded;
        public class ItemAddingEventArgs : EventArgs
        {
            public T Item { get { return _Item; } } readonly T   _Item;
            public int Index { get { return _Index; } } readonly int _Index;

            [DebuggerHidden, DebuggerStepThrough]
            public ItemAddingEventArgs(T item, int index)
            {
                _Item = item;
                _Index = index;
            }
        }
        public class ItemAddedEventArgs : ItemAddingEventArgs
        {
            [DebuggerHidden, DebuggerStepThrough]
            public ItemAddedEventArgs(T item, int index)
                : base(item, index)
            {
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected override void RemoveItem(int index)
        {
            var h = ItemRemoving;
            var h2 = ItemRemoved;
            ItemRemovedEventArgs e = null;
            if (h != null || h2 != null)
                e = new ItemRemovedEventArgs(this[index], index);
            h?.Invoke(this, e);
            base.RemoveItem(index);
            h2?.Invoke(this, e);
        }
        public event EventHandler<ItemRemovingEventArgs> ItemRemoving;
        public event EventHandler<ItemRemovedEventArgs> ItemRemoved;
        public class ItemRemovingEventArgs : EventArgs
        {
            public T   Item  { get { return _Item;  } } readonly T   _Item;
            public int Index { get { return _Index; } } readonly int _Index;

            [DebuggerHidden, DebuggerStepThrough]
            public ItemRemovingEventArgs(T item, int index)
            {
                _Item = item;
                _Index = index;
            }
        }
        public class ItemRemovedEventArgs : ItemRemovingEventArgs
        {
            [DebuggerHidden, DebuggerStepThrough]
            public ItemRemovedEventArgs(T item, int index)
                : base(item, index)
            {
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected override void ClearItems()
        {
            Clearing?.Invoke(this, EventArgs.Empty);
            base.ClearItems();
            Cleared?.Invoke(this, EventArgs.Empty);
        }
        public event EventHandler Clearing;
        public event EventHandler Cleared;
    }

}
