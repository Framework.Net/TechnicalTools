﻿using System;


namespace TechnicalTools.Model
{
    [Flags]
    [Serializable]
    public enum eAccessMode
    {
        Readable = 1,
        Writeable = 2,
        ReadableWriteable = Readable | Writeable,
    }
}
