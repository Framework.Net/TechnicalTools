﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TechnicalTools.Model
{
    // From ISO 4217 (Except Antartica)
    // From http://www.currency-iso.org/dam/downloads/lists/list_one.xml
    public class CurrencyISO : IComparable, IComparable<CurrencyISO>
    {
        public string Ccy        { get; private set; }
        public ushort Code       { get; private set; } public string CodeStr    { get { return Code.ToString("000"); } }
        public string Name       { get; private set; } public bool   IsFund     { get; private set; }
        public string Symbol     { get; private set; }
        public byte?  MinorUnits { get; private set; }
        public string Countries  { get; private set; }

        public override string ToString()     { return Name; }
        public          string SymbolOrCcy()  { return Symbol ?? " " + Ccy; }
        public          string SymbolOrName() { return Symbol ?? Name; }

        public static string ToCurrencyTrigram(string ccyIdStr)
        {
            if (!ushort.TryParse(ccyIdStr, out ushort ccyId))
                throw new Exception($"\"{ccyIdStr},\" is not in expected format for a currency code!", null);
            return AllByCode[ccyId].Ccy;
        }
        public static string ToCurrencyCode(string ccyTrigram)
        {
            return AllByCcy[ccyTrigram].CodeStr;
        }

        #region All Currencies (

        public static readonly CurrencyISO AED = new CurrencyISO() { Ccy = "AED", Code = 784, Name = "UAE Dirham", Symbol = null, MinorUnits = 2, Countries = "UNITED ARAB EMIRATES (THE)" };
        public static readonly CurrencyISO AFN = new CurrencyISO() { Ccy = "AFN", Code = 971, Name = "Afghani", Symbol = null, MinorUnits = 2, Countries = "AFGHANISTAN" };
        public static readonly CurrencyISO ALL = new CurrencyISO() { Ccy = "ALL", Code = 8, Name = "Lek", Symbol = null, MinorUnits = 2, Countries = "ALBANIA" };
        public static readonly CurrencyISO AMD = new CurrencyISO() { Ccy = "AMD", Code = 51, Name = "Armenian Dram", Symbol = null, MinorUnits = 2, Countries = "ARMENIA" };
        public static readonly CurrencyISO ANG = new CurrencyISO() { Ccy = "ANG", Code = 532, Name = "Netherlands Antillean Guilder", Symbol = null, MinorUnits = 2, Countries = "CURAÇAO|SINT MAARTEN (DUTCH PART)" };
        public static readonly CurrencyISO AOA = new CurrencyISO() { Ccy = "AOA", Code = 973, Name = "Kwanza", Symbol = null, MinorUnits = 2, Countries = "ANGOLA" };
        public static readonly CurrencyISO ARS = new CurrencyISO() { Ccy = "ARS", Code = 32, Name = "Argentine Peso", Symbol = null, MinorUnits = 2, Countries = "ARGENTINA" };
        public static readonly CurrencyISO AUD = new CurrencyISO() { Ccy = "AUD", Code = 36, Name = "Australian Dollar", Symbol = null, MinorUnits = 2, Countries = "AUSTRALIA|CHRISTMAS ISLAND|COCOS (KEELING) ISLANDS (THE)|HEARD ISLAND AND McDONALD ISLANDS|KIRIBATI|NAURU|NORFOLK ISLAND|TUVALU" };
        public static readonly CurrencyISO AWG = new CurrencyISO() { Ccy = "AWG", Code = 533, Name = "Aruban Florin", Symbol = null, MinorUnits = 2, Countries = "ARUBA" };
        public static readonly CurrencyISO AZN = new CurrencyISO() { Ccy = "AZN", Code = 944, Name = "Azerbaijanian Manat", Symbol = null, MinorUnits = 2, Countries = "AZERBAIJAN" };
        public static readonly CurrencyISO BAM = new CurrencyISO() { Ccy = "BAM", Code = 977, Name = "Convertible Mark", Symbol = null, MinorUnits = 2, Countries = "BOSNIA AND HERZEGOVINA" };
        public static readonly CurrencyISO BBD = new CurrencyISO() { Ccy = "BBD", Code = 52, Name = "Barbados Dollar", Symbol = null, MinorUnits = 2, Countries = "BARBADOS" };
        public static readonly CurrencyISO BDT = new CurrencyISO() { Ccy = "BDT", Code = 50, Name = "Taka", Symbol = null, MinorUnits = 2, Countries = "BANGLADESH" };
        public static readonly CurrencyISO BGN = new CurrencyISO() { Ccy = "BGN", Code = 975, Name = "Bulgarian Lev", Symbol = null, MinorUnits = 2, Countries = "BULGARIA" };
        public static readonly CurrencyISO BHD = new CurrencyISO() { Ccy = "BHD", Code = 48, Name = "Bahraini Dinar", Symbol = null, MinorUnits = 3, Countries = "BAHRAIN" };
        public static readonly CurrencyISO BIF = new CurrencyISO() { Ccy = "BIF", Code = 108, Name = "Burundi Franc", Symbol = null, MinorUnits = 0, Countries = "BURUNDI" };
        public static readonly CurrencyISO BMD = new CurrencyISO() { Ccy = "BMD", Code = 60, Name = "Bermudian Dollar", Symbol = null, MinorUnits = 2, Countries = "BERMUDA" };
        public static readonly CurrencyISO BND = new CurrencyISO() { Ccy = "BND", Code = 96, Name = "Brunei Dollar", Symbol = null, MinorUnits = 2, Countries = "BRUNEI DARUSSALAM" };
        public static readonly CurrencyISO BOB = new CurrencyISO() { Ccy = "BOB", Code = 68, Name = "Boliviano", Symbol = null, MinorUnits = 2, Countries = "BOLIVIA (PLURINATIONAL STATE OF)" };
        public static readonly CurrencyISO BOV = new CurrencyISO() { Ccy = "BOV", Code = 984, Name = "Mvdol", Symbol = null, MinorUnits = 2, Countries = "BOLIVIA (PLURINATIONAL STATE OF)", IsFund = true };
        public static readonly CurrencyISO BRL = new CurrencyISO() { Ccy = "BRL", Code = 986, Name = "Brazilian Real", Symbol = null, MinorUnits = 2, Countries = "BRAZIL" };
        public static readonly CurrencyISO BSD = new CurrencyISO() { Ccy = "BSD", Code = 44, Name = "Bahamian Dollar", Symbol = null, MinorUnits = 2, Countries = "BAHAMAS (THE)" };
        public static readonly CurrencyISO BTN = new CurrencyISO() { Ccy = "BTN", Code = 64, Name = "Ngultrum", Symbol = null, MinorUnits = 2, Countries = "BHUTAN" };
        public static readonly CurrencyISO BWP = new CurrencyISO() { Ccy = "BWP", Code = 72, Name = "Pula", Symbol = null, MinorUnits = 2, Countries = "BOTSWANA" };
        public static readonly CurrencyISO BYN = new CurrencyISO() { Ccy = "BYN", Code = 933, Name = "Belarusian Ruble", Symbol = null, MinorUnits = 2, Countries = "BELARUS" };
        public static readonly CurrencyISO BYR = new CurrencyISO() { Ccy = "BYR", Code = 974, Name = "Belarusian Ruble", Symbol = null, MinorUnits = 0, Countries = "BELARUS" };
        public static readonly CurrencyISO BZD = new CurrencyISO() { Ccy = "BZD", Code = 84, Name = "Belize Dollar", Symbol = null, MinorUnits = 2, Countries = "BELIZE" };
        public static readonly CurrencyISO CAD = new CurrencyISO() { Ccy = "CAD", Code = 124, Name = "Canadian Dollar", Symbol = null, MinorUnits = 2, Countries = "CANADA" };
        public static readonly CurrencyISO CDF = new CurrencyISO() { Ccy = "CDF", Code = 976, Name = "Congolese Franc", Symbol = null, MinorUnits = 2, Countries = "CONGO (THE DEMOCRATIC REPUBLIC OF THE)" };
        public static readonly CurrencyISO CHE = new CurrencyISO() { Ccy = "CHE", Code = 947, Name = "WIR Euro", Symbol = null, MinorUnits = 2, Countries = "SWITZERLAND", IsFund = true };
        public static readonly CurrencyISO CHF = new CurrencyISO() { Ccy = "CHF", Code = 756, Name = "Swiss Franc", Symbol = null, MinorUnits = 2, Countries = "LIECHTENSTEIN|SWITZERLAND" };
        public static readonly CurrencyISO CHW = new CurrencyISO() { Ccy = "CHW", Code = 948, Name = "WIR Franc", Symbol = null, MinorUnits = 2, Countries = "SWITZERLAND", IsFund = true };
        public static readonly CurrencyISO CLF = new CurrencyISO() { Ccy = "CLF", Code = 990, Name = "Unidad de Fomento", Symbol = null, MinorUnits = 4, Countries = "CHILE", IsFund = true };
        public static readonly CurrencyISO CLP = new CurrencyISO() { Ccy = "CLP", Code = 152, Name = "Chilean Peso", Symbol = null, MinorUnits = 0, Countries = "CHILE" };
        public static readonly CurrencyISO CNY = new CurrencyISO() { Ccy = "CNY", Code = 156, Name = "Yuan Renminbi", Symbol = null, MinorUnits = 2, Countries = "CHINA" };
        public static readonly CurrencyISO COP = new CurrencyISO() { Ccy = "COP", Code = 170, Name = "Colombian Peso", Symbol = null, MinorUnits = 2, Countries = "COLOMBIA" };
        public static readonly CurrencyISO COU = new CurrencyISO() { Ccy = "COU", Code = 970, Name = "Unidad de Valor Real", Symbol = null, MinorUnits = 2, Countries = "COLOMBIA", IsFund = true };
        public static readonly CurrencyISO CRC = new CurrencyISO() { Ccy = "CRC", Code = 188, Name = "Costa Rican Colon", Symbol = null, MinorUnits = 2, Countries = "COSTA RICA" };
        public static readonly CurrencyISO CUC = new CurrencyISO() { Ccy = "CUC", Code = 931, Name = "Peso Convertible", Symbol = null, MinorUnits = 2, Countries = "CUBA" };
        public static readonly CurrencyISO CUP = new CurrencyISO() { Ccy = "CUP", Code = 192, Name = "Cuban Peso", Symbol = null, MinorUnits = 2, Countries = "CUBA" };
        public static readonly CurrencyISO CVE = new CurrencyISO() { Ccy = "CVE", Code = 132, Name = "Cabo Verde Escudo", Symbol = null, MinorUnits = 2, Countries = "CABO VERDE" };
        public static readonly CurrencyISO CZK = new CurrencyISO() { Ccy = "CZK", Code = 203, Name = "Czech Koruna", Symbol = null, MinorUnits = 2, Countries = "CZECH REPUBLIC (THE)" };
        public static readonly CurrencyISO DJF = new CurrencyISO() { Ccy = "DJF", Code = 262, Name = "Djibouti Franc", Symbol = null, MinorUnits = 0, Countries = "DJIBOUTI" };
        public static readonly CurrencyISO DKK = new CurrencyISO() { Ccy = "DKK", Code = 208, Name = "Danish Krone", Symbol = null, MinorUnits = 2, Countries = "DENMARK|FAROE ISLANDS (THE)|GREENLAND" };
        public static readonly CurrencyISO DOP = new CurrencyISO() { Ccy = "DOP", Code = 214, Name = "Dominican Peso", Symbol = null, MinorUnits = 2, Countries = "DOMINICAN REPUBLIC (THE)" };
        public static readonly CurrencyISO DZD = new CurrencyISO() { Ccy = "DZD", Code = 12, Name = "Algerian Dinar", Symbol = null, MinorUnits = 2, Countries = "ALGERIA" };
        public static readonly CurrencyISO EGP = new CurrencyISO() { Ccy = "EGP", Code = 818, Name = "Egyptian Pound", Symbol = null, MinorUnits = 2, Countries = "EGYPT" };
        public static readonly CurrencyISO ERN = new CurrencyISO() { Ccy = "ERN", Code = 232, Name = "Nakfa", Symbol = null, MinorUnits = 2, Countries = "ERITREA" };
        public static readonly CurrencyISO ETB = new CurrencyISO() { Ccy = "ETB", Code = 230, Name = "Ethiopian Birr", Symbol = null, MinorUnits = 2, Countries = "ETHIOPIA" };
        public static readonly CurrencyISO EUR = new CurrencyISO() { Ccy = "EUR", Code = 978, Name = "Euro", Symbol = "€", MinorUnits = 2, Countries = "EUROPEAN UNION|ÅLAND ISLANDS|ANDORRA|AUSTRIA|BELGIUM|CYPRUS|ESTONIA|FINLAND|FRANCE|FRENCH GUIANA|FRENCH SOUTHERN TERRITORIES (THE)|GERMANY|GREECE|GUADELOUPE|HOLY SEE (THE)|IRELAND|ITALY|LATVIA|LITHUANIA|LUXEMBOURG|MALTA|MARTINIQUE|MAYOTTE|MONACO|MONTENEGRO|NETHERLANDS (THE)|PORTUGAL|RÉUNION|SAINT BARTHÉLEMY|SAINT MARTIN (FRENCH PART)|SAINT PIERRE AND MIQUELON|SAN MARINO|SLOVAKIA|SLOVENIA|SPAIN" };
        public static readonly CurrencyISO FJD = new CurrencyISO() { Ccy = "FJD", Code = 242, Name = "Fiji Dollar", Symbol = null, MinorUnits = 2, Countries = "FIJI" };
        public static readonly CurrencyISO FKP = new CurrencyISO() { Ccy = "FKP", Code = 238, Name = "Falkland Islands Pound", Symbol = null, MinorUnits = 2, Countries = "FALKLAND ISLANDS (THE) [MALVINAS]" };
        public static readonly CurrencyISO GBP = new CurrencyISO() { Ccy = "GBP", Code = 826, Name = "Pound Sterling", Symbol = "£", MinorUnits = 2, Countries = "UNITED KINGDOM OF GREAT BRITAIN AND NORTHERN IRELAND (THE)|GUERNSEY|ISLE OF MAN|JERSEY" };
        public static readonly CurrencyISO GEL = new CurrencyISO() { Ccy = "GEL", Code = 981, Name = "Lari", Symbol = null, MinorUnits = 2, Countries = "GEORGIA" };
        public static readonly CurrencyISO GHS = new CurrencyISO() { Ccy = "GHS", Code = 936, Name = "Ghana Cedi", Symbol = null, MinorUnits = 2, Countries = "GHANA" };
        public static readonly CurrencyISO GIP = new CurrencyISO() { Ccy = "GIP", Code = 292, Name = "Gibraltar Pound", Symbol = null, MinorUnits = 2, Countries = "GIBRALTAR" };
        public static readonly CurrencyISO GMD = new CurrencyISO() { Ccy = "GMD", Code = 270, Name = "Dalasi", Symbol = null, MinorUnits = 2, Countries = "GAMBIA (THE)" };
        public static readonly CurrencyISO GNF = new CurrencyISO() { Ccy = "GNF", Code = 324, Name = "Guinea Franc", Symbol = null, MinorUnits = 0, Countries = "GUINEA" };
        public static readonly CurrencyISO GTQ = new CurrencyISO() { Ccy = "GTQ", Code = 320, Name = "Quetzal", Symbol = null, MinorUnits = 2, Countries = "GUATEMALA" };
        public static readonly CurrencyISO GYD = new CurrencyISO() { Ccy = "GYD", Code = 328, Name = "Guyana Dollar", Symbol = null, MinorUnits = 2, Countries = "GUYANA" };
        public static readonly CurrencyISO HKD = new CurrencyISO() { Ccy = "HKD", Code = 344, Name = "Hong Kong Dollar", Symbol = null, MinorUnits = 2, Countries = "HONG KONG" };
        public static readonly CurrencyISO HNL = new CurrencyISO() { Ccy = "HNL", Code = 340, Name = "Lempira", Symbol = null, MinorUnits = 2, Countries = "HONDURAS" };
        public static readonly CurrencyISO HRK = new CurrencyISO() { Ccy = "HRK", Code = 191, Name = "Kuna", Symbol = null, MinorUnits = 2, Countries = "CROATIA" };
        public static readonly CurrencyISO HTG = new CurrencyISO() { Ccy = "HTG", Code = 332, Name = "Gourde", Symbol = null, MinorUnits = 2, Countries = "HAITI" };
        public static readonly CurrencyISO HUF = new CurrencyISO() { Ccy = "HUF", Code = 348, Name = "Forint", Symbol = null, MinorUnits = 2, Countries = "HUNGARY" };
        public static readonly CurrencyISO IDR = new CurrencyISO() { Ccy = "IDR", Code = 360, Name = "Rupiah", Symbol = null, MinorUnits = 2, Countries = "INDONESIA" };
        public static readonly CurrencyISO ILS = new CurrencyISO() { Ccy = "ILS", Code = 376, Name = "New Israeli Sheqel", Symbol = null, MinorUnits = 2, Countries = "ISRAEL" };
        public static readonly CurrencyISO INR = new CurrencyISO() { Ccy = "INR", Code = 356, Name = "Indian Rupee", Symbol = null, MinorUnits = 2, Countries = "BHUTAN|INDIA" };
        public static readonly CurrencyISO IQD = new CurrencyISO() { Ccy = "IQD", Code = 368, Name = "Iraqi Dinar", Symbol = null, MinorUnits = 3, Countries = "IRAQ" };
        public static readonly CurrencyISO IRR = new CurrencyISO() { Ccy = "IRR", Code = 364, Name = "Iranian Rial", Symbol = null, MinorUnits = 2, Countries = "IRAN (ISLAMIC REPUBLIC OF)" };
        public static readonly CurrencyISO ISK = new CurrencyISO() { Ccy = "ISK", Code = 352, Name = "Iceland Krona", Symbol = null, MinorUnits = 0, Countries = "ICELAND" };
        public static readonly CurrencyISO JMD = new CurrencyISO() { Ccy = "JMD", Code = 388, Name = "Jamaican Dollar", Symbol = null, MinorUnits = 2, Countries = "JAMAICA" };
        public static readonly CurrencyISO JOD = new CurrencyISO() { Ccy = "JOD", Code = 400, Name = "Jordanian Dinar", Symbol = null, MinorUnits = 3, Countries = "JORDAN" };
        public static readonly CurrencyISO JPY = new CurrencyISO() { Ccy = "JPY", Code = 392, Name = "Yen", Symbol = null, MinorUnits = 0, Countries = "JAPAN" };
        public static readonly CurrencyISO KES = new CurrencyISO() { Ccy = "KES", Code = 404, Name = "Kenyan Shilling", Symbol = null, MinorUnits = 2, Countries = "KENYA" };
        public static readonly CurrencyISO KGS = new CurrencyISO() { Ccy = "KGS", Code = 417, Name = "Som", Symbol = null, MinorUnits = 2, Countries = "KYRGYZSTAN" };
        public static readonly CurrencyISO KHR = new CurrencyISO() { Ccy = "KHR", Code = 116, Name = "Riel", Symbol = null, MinorUnits = 2, Countries = "CAMBODIA" };
        public static readonly CurrencyISO KMF = new CurrencyISO() { Ccy = "KMF", Code = 174, Name = "Comoro Franc", Symbol = null, MinorUnits = 0, Countries = "COMOROS (THE)" };
        public static readonly CurrencyISO KPW = new CurrencyISO() { Ccy = "KPW", Code = 408, Name = "North Korean Won", Symbol = null, MinorUnits = 2, Countries = "KOREA (THE DEMOCRATIC PEOPLE’S REPUBLIC OF)" };
        public static readonly CurrencyISO KRW = new CurrencyISO() { Ccy = "KRW", Code = 410, Name = "Won", Symbol = null, MinorUnits = 0, Countries = "KOREA (THE REPUBLIC OF)" };
        public static readonly CurrencyISO KWD = new CurrencyISO() { Ccy = "KWD", Code = 414, Name = "Kuwaiti Dinar", Symbol = null, MinorUnits = 3, Countries = "KUWAIT" };
        public static readonly CurrencyISO KYD = new CurrencyISO() { Ccy = "KYD", Code = 136, Name = "Cayman Islands Dollar", Symbol = null, MinorUnits = 2, Countries = "CAYMAN ISLANDS (THE)" };
        public static readonly CurrencyISO KZT = new CurrencyISO() { Ccy = "KZT", Code = 398, Name = "Tenge", Symbol = null, MinorUnits = 2, Countries = "KAZAKHSTAN" };
        public static readonly CurrencyISO LAK = new CurrencyISO() { Ccy = "LAK", Code = 418, Name = "Kip", Symbol = null, MinorUnits = 2, Countries = "LAO PEOPLE’S DEMOCRATIC REPUBLIC (THE)" };
        public static readonly CurrencyISO LBP = new CurrencyISO() { Ccy = "LBP", Code = 422, Name = "Lebanese Pound", Symbol = null, MinorUnits = 2, Countries = "LEBANON" };
        public static readonly CurrencyISO LKR = new CurrencyISO() { Ccy = "LKR", Code = 144, Name = "Sri Lanka Rupee", Symbol = null, MinorUnits = 2, Countries = "SRI LANKA" };
        public static readonly CurrencyISO LRD = new CurrencyISO() { Ccy = "LRD", Code = 430, Name = "Liberian Dollar", Symbol = null, MinorUnits = 2, Countries = "LIBERIA" };
        public static readonly CurrencyISO LSL = new CurrencyISO() { Ccy = "LSL", Code = 426, Name = "Loti", Symbol = null, MinorUnits = 2, Countries = "LESOTHO" };
        public static readonly CurrencyISO LYD = new CurrencyISO() { Ccy = "LYD", Code = 434, Name = "Libyan Dinar", Symbol = null, MinorUnits = 3, Countries = "LIBYA" };
        public static readonly CurrencyISO MAD = new CurrencyISO() { Ccy = "MAD", Code = 504, Name = "Moroccan Dirham", Symbol = null, MinorUnits = 2, Countries = "MOROCCO|WESTERN SAHARA" };
        public static readonly CurrencyISO MDL = new CurrencyISO() { Ccy = "MDL", Code = 498, Name = "Moldovan Leu", Symbol = null, MinorUnits = 2, Countries = "MOLDOVA (THE REPUBLIC OF)" };
        public static readonly CurrencyISO MGA = new CurrencyISO() { Ccy = "MGA", Code = 969, Name = "Malagasy Ariary", Symbol = null, MinorUnits = 2, Countries = "MADAGASCAR" };
        public static readonly CurrencyISO MKD = new CurrencyISO() { Ccy = "MKD", Code = 807, Name = "Denar", Symbol = null, MinorUnits = 2, Countries = "MACEDONIA (THE FORMER YUGOSLAV REPUBLIC OF)" };
        public static readonly CurrencyISO MMK = new CurrencyISO() { Ccy = "MMK", Code = 104, Name = "Kyat", Symbol = null, MinorUnits = 2, Countries = "MYANMAR" };
        public static readonly CurrencyISO MNT = new CurrencyISO() { Ccy = "MNT", Code = 496, Name = "Tugrik", Symbol = null, MinorUnits = 2, Countries = "MONGOLIA" };
        public static readonly CurrencyISO MOP = new CurrencyISO() { Ccy = "MOP", Code = 446, Name = "Pataca", Symbol = null, MinorUnits = 2, Countries = "MACAO" };
        public static readonly CurrencyISO MRO = new CurrencyISO() { Ccy = "MRO", Code = 478, Name = "Ouguiya", Symbol = null, MinorUnits = 2, Countries = "MAURITANIA" };
        public static readonly CurrencyISO MUR = new CurrencyISO() { Ccy = "MUR", Code = 480, Name = "Mauritius Rupee", Symbol = null, MinorUnits = 2, Countries = "MAURITIUS" };
        public static readonly CurrencyISO MVR = new CurrencyISO() { Ccy = "MVR", Code = 462, Name = "Rufiyaa", Symbol = null, MinorUnits = 2, Countries = "MALDIVES" };
        public static readonly CurrencyISO MWK = new CurrencyISO() { Ccy = "MWK", Code = 454, Name = "Malawi Kwacha", Symbol = null, MinorUnits = 2, Countries = "MALAWI" };
        public static readonly CurrencyISO MXN = new CurrencyISO() { Ccy = "MXN", Code = 484, Name = "Mexican Peso", Symbol = null, MinorUnits = 2, Countries = "MEXICO" };
        public static readonly CurrencyISO MXV = new CurrencyISO() { Ccy = "MXV", Code = 979, Name = "Mexican Unidad de Inversion (UDI)", Symbol = null, MinorUnits = 2, Countries = "MEXICO", IsFund = true };
        public static readonly CurrencyISO MYR = new CurrencyISO() { Ccy = "MYR", Code = 458, Name = "Malaysian Ringgit", Symbol = null, MinorUnits = 2, Countries = "MALAYSIA" };
        public static readonly CurrencyISO MZN = new CurrencyISO() { Ccy = "MZN", Code = 943, Name = "Mozambique Metical", Symbol = null, MinorUnits = 2, Countries = "MOZAMBIQUE" };
        public static readonly CurrencyISO NAD = new CurrencyISO() { Ccy = "NAD", Code = 516, Name = "Namibia Dollar", Symbol = null, MinorUnits = 2, Countries = "NAMIBIA" };
        public static readonly CurrencyISO NGN = new CurrencyISO() { Ccy = "NGN", Code = 566, Name = "Naira", Symbol = null, MinorUnits = 2, Countries = "NIGERIA" };
        public static readonly CurrencyISO NIO = new CurrencyISO() { Ccy = "NIO", Code = 558, Name = "Cordoba Oro", Symbol = null, MinorUnits = 2, Countries = "NICARAGUA" };
        public static readonly CurrencyISO NOK = new CurrencyISO() { Ccy = "NOK", Code = 578, Name = "Norwegian Krone", Symbol = "kr", MinorUnits = 2, Countries = "BOUVET ISLAND|NORWAY|SVALBARD AND JAN MAYEN" };
        public static readonly CurrencyISO NPR = new CurrencyISO() { Ccy = "NPR", Code = 524, Name = "Nepalese Rupee", Symbol = null, MinorUnits = 2, Countries = "NEPAL" };
        public static readonly CurrencyISO NZD = new CurrencyISO() { Ccy = "NZD", Code = 554, Name = "New Zealand Dollar", Symbol = null, MinorUnits = 2, Countries = "COOK ISLANDS (THE)|NEW ZEALAND|NIUE|PITCAIRN|TOKELAU" };
        public static readonly CurrencyISO OMR = new CurrencyISO() { Ccy = "OMR", Code = 512, Name = "Rial Omani", Symbol = null, MinorUnits = 3, Countries = "OMAN" };
        public static readonly CurrencyISO PAB = new CurrencyISO() { Ccy = "PAB", Code = 590, Name = "Balboa", Symbol = null, MinorUnits = 2, Countries = "PANAMA" };
        public static readonly CurrencyISO PEN = new CurrencyISO() { Ccy = "PEN", Code = 604, Name = "Sol", Symbol = null, MinorUnits = 2, Countries = "PERU" };
        public static readonly CurrencyISO PGK = new CurrencyISO() { Ccy = "PGK", Code = 598, Name = "Kina", Symbol = null, MinorUnits = 2, Countries = "PAPUA NEW GUINEA" };
        public static readonly CurrencyISO PHP = new CurrencyISO() { Ccy = "PHP", Code = 608, Name = "Philippine Peso", Symbol = null, MinorUnits = 2, Countries = "PHILIPPINES (THE)" };
        public static readonly CurrencyISO PKR = new CurrencyISO() { Ccy = "PKR", Code = 586, Name = "Pakistan Rupee", Symbol = null, MinorUnits = 2, Countries = "PAKISTAN" };
        public static readonly CurrencyISO PLN = new CurrencyISO() { Ccy = "PLN", Code = 985, Name = "Zloty", Symbol = null, MinorUnits = 2, Countries = "POLAND" };
        public static readonly CurrencyISO PYG = new CurrencyISO() { Ccy = "PYG", Code = 600, Name = "Guarani", Symbol = null, MinorUnits = 0, Countries = "PARAGUAY" };
        public static readonly CurrencyISO QAR = new CurrencyISO() { Ccy = "QAR", Code = 634, Name = "Qatari Rial", Symbol = null, MinorUnits = 2, Countries = "QATAR" };
        public static readonly CurrencyISO RON = new CurrencyISO() { Ccy = "RON", Code = 946, Name = "Romanian Leu", Symbol = null, MinorUnits = 2, Countries = "ROMANIA" };
        public static readonly CurrencyISO RSD = new CurrencyISO() { Ccy = "RSD", Code = 941, Name = "Serbian Dinar", Symbol = null, MinorUnits = 2, Countries = "SERBIA" };
        public static readonly CurrencyISO RUB = new CurrencyISO() { Ccy = "RUB", Code = 643, Name = "Russian Ruble", Symbol = null, MinorUnits = 2, Countries = "RUSSIAN FEDERATION (THE)" };
        public static readonly CurrencyISO RWF = new CurrencyISO() { Ccy = "RWF", Code = 646, Name = "Rwanda Franc", Symbol = null, MinorUnits = 0, Countries = "RWANDA" };
        public static readonly CurrencyISO SAR = new CurrencyISO() { Ccy = "SAR", Code = 682, Name = "Saudi Riyal", Symbol = null, MinorUnits = 2, Countries = "SAUDI ARABIA" };
        public static readonly CurrencyISO SBD = new CurrencyISO() { Ccy = "SBD", Code = 90, Name = "Solomon Islands Dollar", Symbol = null, MinorUnits = 2, Countries = "SOLOMON ISLANDS" };
        public static readonly CurrencyISO SCR = new CurrencyISO() { Ccy = "SCR", Code = 690, Name = "Seychelles Rupee", Symbol = null, MinorUnits = 2, Countries = "SEYCHELLES" };
        public static readonly CurrencyISO SDG = new CurrencyISO() { Ccy = "SDG", Code = 938, Name = "Sudanese Pound", Symbol = null, MinorUnits = 2, Countries = "SUDAN (THE)" };
        public static readonly CurrencyISO SEK = new CurrencyISO() { Ccy = "SEK", Code = 752, Name = "Swedish Krona", Symbol = null, MinorUnits = 2, Countries = "SWEDEN" };
        public static readonly CurrencyISO SGD = new CurrencyISO() { Ccy = "SGD", Code = 702, Name = "Singapore Dollar", Symbol = null, MinorUnits = 2, Countries = "SINGAPORE" };
        public static readonly CurrencyISO SHP = new CurrencyISO() { Ccy = "SHP", Code = 654, Name = "Saint Helena Pound", Symbol = null, MinorUnits = 2, Countries = "SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA" };
        public static readonly CurrencyISO SLL = new CurrencyISO() { Ccy = "SLL", Code = 694, Name = "Leone", Symbol = null, MinorUnits = 2, Countries = "SIERRA LEONE" };
        public static readonly CurrencyISO SOS = new CurrencyISO() { Ccy = "SOS", Code = 706, Name = "Somali Shilling", Symbol = null, MinorUnits = 2, Countries = "SOMALIA" };
        public static readonly CurrencyISO SRD = new CurrencyISO() { Ccy = "SRD", Code = 968, Name = "Surinam Dollar", Symbol = null, MinorUnits = 2, Countries = "SURINAME" };
        public static readonly CurrencyISO SSP = new CurrencyISO() { Ccy = "SSP", Code = 728, Name = "South Sudanese Pound", Symbol = null, MinorUnits = 2, Countries = "SOUTH SUDAN" };
        public static readonly CurrencyISO STD = new CurrencyISO() { Ccy = "STD", Code = 678, Name = "Dobra", Symbol = null, MinorUnits = 2, Countries = "SAO TOME AND PRINCIPE" };
        public static readonly CurrencyISO SVC = new CurrencyISO() { Ccy = "SVC", Code = 222, Name = "El Salvador Colon", Symbol = null, MinorUnits = 2, Countries = "EL SALVADOR" };
        public static readonly CurrencyISO SYP = new CurrencyISO() { Ccy = "SYP", Code = 760, Name = "Syrian Pound", Symbol = null, MinorUnits = 2, Countries = "SYRIAN ARAB REPUBLIC" };
        public static readonly CurrencyISO SZL = new CurrencyISO() { Ccy = "SZL", Code = 748, Name = "Lilangeni", Symbol = null, MinorUnits = 2, Countries = "SWAZILAND" };
        public static readonly CurrencyISO THB = new CurrencyISO() { Ccy = "THB", Code = 764, Name = "Baht", Symbol = null, MinorUnits = 2, Countries = "THAILAND" };
        public static readonly CurrencyISO TJS = new CurrencyISO() { Ccy = "TJS", Code = 972, Name = "Somoni", Symbol = null, MinorUnits = 2, Countries = "TAJIKISTAN" };
        public static readonly CurrencyISO TMT = new CurrencyISO() { Ccy = "TMT", Code = 934, Name = "Turkmenistan New Manat", Symbol = null, MinorUnits = 2, Countries = "TURKMENISTAN" };
        public static readonly CurrencyISO TND = new CurrencyISO() { Ccy = "TND", Code = 788, Name = "Tunisian Dinar", Symbol = null, MinorUnits = 3, Countries = "TUNISIA" };
        public static readonly CurrencyISO TOP = new CurrencyISO() { Ccy = "TOP", Code = 776, Name = "Pa’anga", Symbol = null, MinorUnits = 2, Countries = "TONGA" };
        public static readonly CurrencyISO TRY = new CurrencyISO() { Ccy = "TRY", Code = 949, Name = "Turkish Lira", Symbol = null, MinorUnits = 2, Countries = "TURKEY" };
        public static readonly CurrencyISO TTD = new CurrencyISO() { Ccy = "TTD", Code = 780, Name = "Trinidad and Tobago Dollar", Symbol = null, MinorUnits = 2, Countries = "TRINIDAD AND TOBAGO" };
        public static readonly CurrencyISO TWD = new CurrencyISO() { Ccy = "TWD", Code = 901, Name = "New Taiwan Dollar", Symbol = null, MinorUnits = 2, Countries = "TAIWAN (PROVINCE OF CHINA)" };
        public static readonly CurrencyISO TZS = new CurrencyISO() { Ccy = "TZS", Code = 834, Name = "Tanzanian Shilling", Symbol = null, MinorUnits = 2, Countries = "TANZANIA, UNITED REPUBLIC OF" };
        public static readonly CurrencyISO UAH = new CurrencyISO() { Ccy = "UAH", Code = 980, Name = "Hryvnia", Symbol = null, MinorUnits = 2, Countries = "UKRAINE" };
        public static readonly CurrencyISO UGX = new CurrencyISO() { Ccy = "UGX", Code = 800, Name = "Uganda Shilling", Symbol = null, MinorUnits = 0, Countries = "UGANDA" };
        public static readonly CurrencyISO USD = new CurrencyISO() { Ccy = "USD", Code = 840, Name = "US Dollar", Symbol = "$", MinorUnits = 2, Countries = "UNITED STATES OF AMERICA (THE)|AMERICAN SAMOA|BONAIRE, SINT EUSTATIUS AND SABA|BRITISH INDIAN OCEAN TERRITORY (THE)|ECUADOR|EL SALVADOR|GUAM|HAITI|MARSHALL ISLANDS (THE)|MICRONESIA (FEDERATED STATES OF)|NORTHERN MARIANA ISLANDS (THE)|PALAU|PANAMA|PUERTO RICO|TIMOR-LESTE|TURKS AND CAICOS ISLANDS (THE)|UNITED STATES MINOR OUTLYING ISLANDS (THE)|VIRGIN ISLANDS (BRITISH)|VIRGIN ISLANDS (U.S.)" };
        public static readonly CurrencyISO USN = new CurrencyISO() { Ccy = "USN", Code = 997, Name = "US Dollar (Next day)", Symbol = null, MinorUnits = 2, Countries = "UNITED STATES OF AMERICA (THE)", IsFund = true };
        public static readonly CurrencyISO UYI = new CurrencyISO() { Ccy = "UYI", Code = 940, Name = "Uruguay Peso en Unidades Indexadas (URUIURUI)", Symbol = null, MinorUnits = 0, Countries = "URUGUAY", IsFund = true };
        public static readonly CurrencyISO UYU = new CurrencyISO() { Ccy = "UYU", Code = 858, Name = "Peso Uruguayo", Symbol = null, MinorUnits = 2, Countries = "URUGUAY" };
        public static readonly CurrencyISO UZS = new CurrencyISO() { Ccy = "UZS", Code = 860, Name = "Uzbekistan Sum", Symbol = null, MinorUnits = 2, Countries = "UZBEKISTAN" };
        public static readonly CurrencyISO VEF = new CurrencyISO() { Ccy = "VEF", Code = 937, Name = "Bolívar", Symbol = null, MinorUnits = 2, Countries = "VENEZUELA (BOLIVARIAN REPUBLIC OF)" };
        public static readonly CurrencyISO VND = new CurrencyISO() { Ccy = "VND", Code = 704, Name = "Dong", Symbol = null, MinorUnits = 0, Countries = "VIET NAM" };
        public static readonly CurrencyISO VUV = new CurrencyISO() { Ccy = "VUV", Code = 548, Name = "Vatu", Symbol = null, MinorUnits = 0, Countries = "VANUATU" };
        public static readonly CurrencyISO WST = new CurrencyISO() { Ccy = "WST", Code = 882, Name = "Tala", Symbol = null, MinorUnits = 2, Countries = "SAMOA" };
        public static readonly CurrencyISO XAF = new CurrencyISO() { Ccy = "XAF", Code = 950, Name = "CFA Franc BEAC", Symbol = null, MinorUnits = 0, Countries = "CAMEROON|CENTRAL AFRICAN REPUBLIC (THE)|CHAD|CONGO (THE)|EQUATORIAL GUINEA|GABON" };
        public static readonly CurrencyISO XAG = new CurrencyISO() { Ccy = "XAG", Code = 961, Name = "Silver", Symbol = null, MinorUnits = null, Countries = "ZZ11_Silver" };
        public static readonly CurrencyISO XAU = new CurrencyISO() { Ccy = "XAU", Code = 959, Name = "Gold", Symbol = null, MinorUnits = null, Countries = "ZZ08_Gold" };
        public static readonly CurrencyISO XBA = new CurrencyISO() { Ccy = "XBA", Code = 955, Name = "Bond Markets Unit European Composite Unit (EURCO)", Symbol = null, MinorUnits = null, Countries = "ZZ01_Bond Markets Unit European_EURCO" };
        public static readonly CurrencyISO XBB = new CurrencyISO() { Ccy = "XBB", Code = 956, Name = "Bond Markets Unit European Monetary Unit (E.M.U.-6)", Symbol = null, MinorUnits = null, Countries = "ZZ02_Bond Markets Unit European_EMU-6" };
        public static readonly CurrencyISO XBC = new CurrencyISO() { Ccy = "XBC", Code = 957, Name = "Bond Markets Unit European Unit of Account 9 (E.U.A.-9)", Symbol = null, MinorUnits = null, Countries = "ZZ03_Bond Markets Unit European_EUA-9" };
        public static readonly CurrencyISO XBD = new CurrencyISO() { Ccy = "XBD", Code = 958, Name = "Bond Markets Unit European Unit of Account 17 (E.U.A.-17)", Symbol = null, MinorUnits = null, Countries = "ZZ04_Bond Markets Unit European_EUA-17" };
        public static readonly CurrencyISO XCD = new CurrencyISO() { Ccy = "XCD", Code = 951, Name = "East Caribbean Dollar", Symbol = null, MinorUnits = 2, Countries = "ANGUILLA|ANTIGUA AND BARBUDA|DOMINICA|GRENADA|MONTSERRAT|SAINT KITTS AND NEVIS|SAINT LUCIA|SAINT VINCENT AND THE GRENADINES" };
        public static readonly CurrencyISO XDR = new CurrencyISO() { Ccy = "XDR", Code = 960, Name = "SDR (Special Drawing Right)", Symbol = null, MinorUnits = null, Countries = "INTERNATIONAL MONETARY FUND (IMF)" };
        public static readonly CurrencyISO XOF = new CurrencyISO() { Ccy = "XOF", Code = 952, Name = "CFA Franc BCEAO", Symbol = null, MinorUnits = 0, Countries = "BENIN|BURKINA FASO|CÔTE D'IVOIRE|GUINEA-BISSAU|MALI|NIGER (THE)|SENEGAL|TOGO" };
        public static readonly CurrencyISO XPD = new CurrencyISO() { Ccy = "XPD", Code = 964, Name = "Palladium", Symbol = null, MinorUnits = null, Countries = "ZZ09_Palladium" };
        public static readonly CurrencyISO XPF = new CurrencyISO() { Ccy = "XPF", Code = 953, Name = "CFP Franc", Symbol = null, MinorUnits = 0, Countries = "FRENCH POLYNESIA|NEW CALEDONIA|WALLIS AND FUTUNA" };
        public static readonly CurrencyISO XPT = new CurrencyISO() { Ccy = "XPT", Code = 962, Name = "Platinum", Symbol = null, MinorUnits = null, Countries = "ZZ10_Platinum" };
        public static readonly CurrencyISO XSU = new CurrencyISO() { Ccy = "XSU", Code = 994, Name = "Sucre", Symbol = null, MinorUnits = null, Countries = "SISTEMA UNITARIO DE COMPENSACION REGIONAL DE PAGOS \"SUCRE\"" };
        public static readonly CurrencyISO XTS = new CurrencyISO() { Ccy = "XTS", Code = 963, Name = "Codes specifically reserved for testing purposes", Symbol = null, MinorUnits = null, Countries = "ZZ06_Testing_Code" };
        public static readonly CurrencyISO XUA = new CurrencyISO() { Ccy = "XUA", Code = 965, Name = "ADB Unit of Account", Symbol = null, MinorUnits = null, Countries = "MEMBER COUNTRIES OF THE AFRICAN DEVELOPMENT BANK GROUP" };
        public static readonly CurrencyISO XXX = new CurrencyISO() { Ccy = "XXX", Code = 999, Name = "The codes assigned for transactions where no currency is involved", Symbol = "¤", MinorUnits = null, Countries = "ZZ07_No_Currency" };
        public static readonly CurrencyISO YER = new CurrencyISO() { Ccy = "YER", Code = 886, Name = "Yemeni Rial", Symbol = null, MinorUnits = 2, Countries = "YEMEN" };
        public static readonly CurrencyISO ZAR = new CurrencyISO() { Ccy = "ZAR", Code = 710, Name = "Rand", Symbol = null, MinorUnits = 2, Countries = "LESOTHO|NAMIBIA|SOUTH AFRICA" };
        public static readonly CurrencyISO ZMW = new CurrencyISO() { Ccy = "ZMW", Code = 967, Name = "Zambian Kwacha", Symbol = null, MinorUnits = 2, Countries = "ZAMBIA" };
        public static readonly CurrencyISO ZWL = new CurrencyISO() { Ccy = "ZWL", Code = 932, Name = "Zimbabwe Dollar", Symbol = null, MinorUnits = 2, Countries = "ZIMBABWE" };

        #endregion All Currencies

        public static readonly IReadOnlyDictionary<string, CurrencyISO> AllByCcy =
            typeof(CurrencyISO).GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                               .Where(field => field.FieldType == typeof(CurrencyISO))
                               .Select(field => (CurrencyISO)field.GetValue(null))
                               .ToDictionary(property => property.Ccy);

        public static readonly IReadOnlyDictionary<ushort, CurrencyISO> AllByCode = AllByCcy.ToDictionary(kvp => kvp.Value.Code, kvp => kvp.Value);

        public int CompareTo(CurrencyISO other) // For grid in generic UI library (like for example devexpress)
        {
            if (other == null)
                return 1;
            return Ccy.CompareTo(other.Ccy);
        }
        public int CompareTo(object obj)
        {
            return CompareTo(obj as CurrencyISO);
        }
    }
}
