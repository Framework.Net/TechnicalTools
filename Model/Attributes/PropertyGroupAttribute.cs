﻿using System;



namespace TechnicalTools.Model.Attributes
{
    public class PropertyGroupAttribute : Attribute
    {
        public string GroupName { get; set; }
        public PropertyGroupAttribute(string groupName)
        {
            GroupName = groupName;
        }
    }

}
