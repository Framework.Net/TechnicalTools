﻿using System;



namespace TechnicalTools.Model.Attributes
{
    public class NecessityAttribute: Attribute
    {
        protected NecessityAttribute()
        {
        }
    }
    public class UnknownNecessity : NecessityAttribute
    {
    }

    public class IsOptional : NecessityAttribute
    {
    }
    public class IsMandatory : NecessityAttribute
    {
    }
    public class MayBeMandatory : NecessityAttribute
    {
    }
}
