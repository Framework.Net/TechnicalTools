﻿using System;

using TechnicalTools.Extensions.Data;


namespace TechnicalTools.Model.Cache
{
    public interface IHasClosedIdReadable
    {
        /// <summary>
        /// Retourne l'id unique désignant l'objet. L'id est unique par rapport aux objets de même type.
        /// </summary>
        [IsTechnicalProperty(HideFromUser = true)]
        IIdTuple Id { get; }
    }

    public interface IHasMutableClosedId : IHasClosedIdReadable
    {
        event PropertyHasChangedEventHandler IdHasChanged;
    }
}
