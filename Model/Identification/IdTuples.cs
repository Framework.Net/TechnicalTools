﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;


namespace TechnicalTools.Model.Cache
{
    public interface IIdTuple
    {
        IComparable[] Keys { get; }
        ITypedId ToTypedId(Type type);
        ReadOnlyCollection<Type> KeyTypes { get; }
        IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable;
    }
    public interface IIdTupleWithOnlyOneKey : IIdTuple
    {
        object Id1 { get; }
    }

    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdTuple<T1> : IIdTupleWithOnlyOneKey, IEquatable<IdTuple<T1>>
        where T1 : IEquatable<T1>, IComparable<T1>, IComparable
    {
        public IdTuple(T1 id1)
        {
            _Id1 = id1;
        }

        public T1 Id1 { get { return _Id1; } } readonly T1   _Id1;
        object IIdTupleWithOnlyOneKey.Id1 { get { return _Id1; } }

        public ReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new [] { typeof(T1) });

        public IComparable[] Keys { get { return new IComparable[] { _Id1 }; } }
        public TypedId<IdTuple<T1>> ToTypedId(Type type) { return new TypedId<IdTuple<T1>>(type, this); } ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { return new IdTuple<T1, TNewKey>(_Id1, new_key); }

        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }
        public override string ToString() { return AsDebugString; }


        #region Equality members

        public bool Equals(IdTuple<T1> other)
        {
            return EqualityComparer<T1>.Default.Equals(_Id1, other._Id1);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdTuple<T1> && Equals((IdTuple<T1>)obj);
        }

        public override int GetHashCode()
        {
            return EqualityComparer<T1>.Default.GetHashCode(_Id1);
        }

        public static bool operator ==(IdTuple<T1> left, IdTuple<T1> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTuple<T1> left, IdTuple<T1> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }


    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdTuple<T1, T2> : IIdTuple, IEquatable<IdTuple<T1, T2>>
        where T1 : IEquatable<T1>, IComparable<T1>, IComparable
        where T2 : IEquatable<T2>, IComparable<T2>, IComparable
    {
        public IdTuple(T1 id1, T2 id2)
        {
            _Id1 = id1;
            _Id2 = id2;
        }

        public T1 Id1 { get { return _Id1; } } readonly T1   _Id1;
        public T2 Id2 { get { return _Id2; } } readonly T2   _Id2;

        public ReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new [] { typeof(T1), typeof(T2) });

        public IComparable[] Keys { get { return new IComparable[] { _Id1, _Id2 }; } }
        public TypedId<IdTuple<T1, T2>> ToTypedId(Type type) { return new TypedId<IdTuple<T1, T2>>(type, this); } ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { return new IdTuple<T1, T2, TNewKey>(_Id1, _Id2, new_key); }

        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }
        public override string ToString() { return AsDebugString; }


        #region Equality members

        public bool Equals(IdTuple<T1, T2> other)
        {
            return EqualityComparer<T1>.Default.Equals(_Id1, other._Id1) &&
                   EqualityComparer<T2>.Default.Equals(_Id2, other._Id2);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdTuple<T1, T2> && Equals((IdTuple<T1, T2>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_Id1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_Id2);
                return hashCode;
            }
        }

        public static bool operator ==(IdTuple<T1, T2> left, IdTuple<T1, T2> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTuple<T1, T2> left, IdTuple<T1, T2> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdTuple<T1, T2, T3> : IIdTuple, IEquatable<IdTuple<T1, T2, T3>>
        where T1 : IEquatable<T1>, IComparable<T1>, IComparable
        where T2 : IEquatable<T2>, IComparable<T2>, IComparable
        where T3 : IEquatable<T3>, IComparable<T3>, IComparable
    {
        public IdTuple(T1 id1, T2 id2, T3 id3)
        {
            _Id1 = id1;
            _Id2 = id2;
            _Id3 = id3;
        }

        public T1 Id1 { get { return _Id1; } } readonly T1   _Id1;
        public T2 Id2 { get { return _Id2; } } readonly T2   _Id2;
        public T3 Id3 { get { return _Id3; } } readonly T3   _Id3;

        public ReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new [] { typeof(T1), typeof(T2), typeof(T3) });

        public IComparable[] Keys { get { return new IComparable[] { _Id1, _Id2, _Id3 }; } }
        public TypedId<IdTuple<T1, T2, T3>> ToTypedId(Type type) { return new TypedId<IdTuple<T1, T2, T3>>(type, this); } ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { return new IdTuple<T1, T2, T3, TNewKey>(_Id1, _Id2, _Id3, new_key); }

        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }
        public override string ToString() { return AsDebugString; }

        #region Equality members

        public bool Equals(IdTuple<T1, T2, T3> other)
        {
            return EqualityComparer<T1>.Default.Equals(_Id1, other._Id1) &&
                   EqualityComparer<T2>.Default.Equals(_Id2, other._Id2) &&
                   EqualityComparer<T3>.Default.Equals(_Id3, other._Id3);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdTuple<T1, T2, T3> && Equals((IdTuple<T1, T2, T3>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_Id1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_Id2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_Id3);
                return hashCode;
            }
        }

        public static bool operator ==(IdTuple<T1, T2, T3> left, IdTuple<T1, T2, T3> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTuple<T1, T2, T3> left, IdTuple<T1, T2, T3> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdTuple<T1, T2, T3, T4> : IIdTuple, IEquatable<IdTuple<T1, T2, T3, T4>>
        where T1 : IEquatable<T1>, IComparable<T1>, IComparable
        where T2 : IEquatable<T2>, IComparable<T2>, IComparable
        where T3 : IEquatable<T3>, IComparable<T3>, IComparable
        where T4 : IEquatable<T4>, IComparable<T4>, IComparable
    {
        public IdTuple(T1 id1, T2 id2, T3 id3, T4 id4)
        {
            _Id1 = id1;
            _Id2 = id2;
            _Id3 = id3;
            _Id4 = id4;
        }

        public T1 Id1 { get { return _Id1; } } readonly T1   _Id1;
        public T2 Id2 { get { return _Id2; } } readonly T2   _Id2;
        public T3 Id3 { get { return _Id3; } } readonly T3   _Id3;
        public T4 Id4 { get { return _Id4; } } readonly T4   _Id4;

        public ReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new [] { typeof(T1), typeof(T2), typeof(T3), typeof(T4) });

        public IComparable[] Keys { get { return new IComparable[] { _Id1, _Id2, _Id3, _Id4 }; } }
        public TypedId<IdTuple<T1, T2, T3, T4>> ToTypedId(Type type) { return new TypedId<IdTuple<T1, T2, T3, T4>>(type, this); } ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { return new IdTuple<T1, T2, T3, T4, TNewKey>(_Id1, _Id2, _Id3, _Id4, new_key); }

        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }
        public override string ToString() { return AsDebugString; }

        #region Equality members

        public bool Equals(IdTuple<T1, T2, T3, T4> other)
        {
            return EqualityComparer<T1>.Default.Equals(_Id1, other._Id1) &&
                   EqualityComparer<T2>.Default.Equals(_Id2, other._Id2) &&
                   EqualityComparer<T3>.Default.Equals(_Id3, other._Id3) &&
                   EqualityComparer<T4>.Default.Equals(_Id4, other._Id4);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdTuple<T1, T2, T3, T4> && Equals((IdTuple<T1, T2, T3, T4>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_Id1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_Id2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_Id3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_Id4);
                return hashCode;
            }
        }

        public static bool operator ==(IdTuple<T1, T2, T3, T4> left, IdTuple<T1, T2, T3, T4> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTuple<T1, T2, T3, T4> left, IdTuple<T1, T2, T3, T4> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdTuple<T1, T2, T3, T4, T5> : IIdTuple, IEquatable<IdTuple<T1, T2, T3, T4, T5>>
        where T1 : IEquatable<T1>, IComparable<T1>, IComparable
        where T2 : IEquatable<T2>, IComparable<T2>, IComparable
        where T3 : IEquatable<T3>, IComparable<T3>, IComparable
        where T4 : IEquatable<T4>, IComparable<T4>, IComparable
        where T5 : IEquatable<T5>, IComparable<T5>, IComparable
    {
        public IdTuple(T1 id1, T2 id2, T3 id3, T4 id4, T5 id5)
        {
            _Id1 = id1;
            _Id2 = id2;
            _Id3 = id3;
            _Id4 = id4;
            _Id5 = id5;
        }

        public T1 Id1 { get { return _Id1; } } readonly T1   _Id1;
        public T2 Id2 { get { return _Id2; } } readonly T2   _Id2;
        public T3 Id3 { get { return _Id3; } } readonly T3   _Id3;
        public T4 Id4 { get { return _Id4; } } readonly T4   _Id4;
        public T5 Id5 { get { return _Id5; } } readonly T5   _Id5;

        public ReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new [] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5) });

        public IComparable[] Keys { get { return new IComparable[] { _Id1, _Id2, _Id3, _Id4, _Id5 }; } }
        public TypedId<IdTuple<T1, T2, T3, T4, T5>> ToTypedId(Type type) { return new TypedId<IdTuple<T1, T2, T3, T4, T5>>(type, this); } ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { return new IdTuple<T1, T2, T3, T4, T5, TNewKey>(_Id1, _Id2, _Id3, _Id4, _Id5, new_key); }

        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }
        public override string ToString() { return AsDebugString; }

        #region Equality members

        public bool Equals(IdTuple<T1, T2, T3, T4, T5> other)
        {
            return EqualityComparer<T1>.Default.Equals(_Id1, other._Id1) &&
                   EqualityComparer<T2>.Default.Equals(_Id2, other._Id2) &&
                   EqualityComparer<T3>.Default.Equals(_Id3, other._Id3) &&
                   EqualityComparer<T4>.Default.Equals(_Id4, other._Id4) &&
                   EqualityComparer<T5>.Default.Equals(_Id5, other._Id5);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdTuple<T1, T2, T3, T4, T5> && Equals((IdTuple<T1, T2, T3, T4, T5>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_Id1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_Id2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_Id3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_Id4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(_Id5);
                return hashCode;
            }
        }

        public static bool operator ==(IdTuple<T1, T2, T3, T4, T5> left, IdTuple<T1, T2, T3, T4, T5> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTuple<T1, T2, T3, T4, T5> left, IdTuple<T1, T2, T3, T4, T5> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdTuple<T1, T2, T3, T4, T5, T6> : IIdTuple, IEquatable<IdTuple<T1, T2, T3, T4, T5, T6>>
        where T1 : IEquatable<T1>, IComparable<T1>, IComparable
        where T2 : IEquatable<T2>, IComparable<T2>, IComparable
        where T3 : IEquatable<T3>, IComparable<T3>, IComparable
        where T4 : IEquatable<T4>, IComparable<T4>, IComparable
        where T5 : IEquatable<T5>, IComparable<T5>, IComparable
        where T6 : IEquatable<T6>, IComparable<T6>, IComparable
    {
        public IdTuple(T1 id1, T2 id2, T3 id3, T4 id4, T5 id5, T6 id6)
        {
            _Id1 = id1;
            _Id2 = id2;
            _Id3 = id3;
            _Id4 = id4;
            _Id5 = id5;
            _Id6 = id6;
        }

        public T1 Id1 { get { return _Id1; } } readonly T1   _Id1;
        public T2 Id2 { get { return _Id2; } } readonly T2   _Id2;
        public T3 Id3 { get { return _Id3; } } readonly T3   _Id3;
        public T4 Id4 { get { return _Id4; } } readonly T4   _Id4;
        public T5 Id5 { get { return _Id5; } } readonly T5   _Id5;
        public T6 Id6 { get { return _Id6; } } readonly T6   _Id6;

        public ReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new []{ typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6) });

        public IComparable[] Keys { get { return new IComparable[] { _Id1, _Id2, _Id3, _Id4, _Id5, _Id6 }; } }
        public TypedId<IdTuple<T1, T2, T3, T4, T5, T6>> ToTypedId(Type type) { return new TypedId<IdTuple<T1, T2, T3, T4, T5, T6>>(type, this); } ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { throw new InvalidOperationException(); }

        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }
        public override string ToString() { return AsDebugString; }

        #region Equality members

        public bool Equals(IdTuple<T1, T2, T3, T4, T5, T6> other)
        {
            return EqualityComparer<T1>.Default.Equals(_Id1, other._Id1) &&
                   EqualityComparer<T2>.Default.Equals(_Id2, other._Id2) &&
                   EqualityComparer<T3>.Default.Equals(_Id3, other._Id3) &&
                   EqualityComparer<T4>.Default.Equals(_Id4, other._Id4) &&
                   EqualityComparer<T5>.Default.Equals(_Id5, other._Id5) &&
                   EqualityComparer<T6>.Default.Equals(_Id6, other._Id6);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdTuple<T1, T2, T3, T4, T5, T6> && Equals((IdTuple<T1, T2, T3, T4, T5, T6>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<T1>.Default.GetHashCode(_Id1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_Id2);
                hashCode = (hashCode * 397) ^ EqualityComparer<T3>.Default.GetHashCode(_Id3);
                hashCode = (hashCode * 397) ^ EqualityComparer<T4>.Default.GetHashCode(_Id4);
                hashCode = (hashCode * 397) ^ EqualityComparer<T5>.Default.GetHashCode(_Id5);
                return hashCode;
            }
        }

        public static bool operator ==(IdTuple<T1, T2, T3, T4, T5, T6> left, IdTuple<T1, T2, T3, T4, T5, T6> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTuple<T1, T2, T3, T4, T5, T6> left, IdTuple<T1, T2, T3, T4, T5, T6> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

}
