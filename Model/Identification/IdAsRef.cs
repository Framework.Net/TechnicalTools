﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;


namespace TechnicalTools.Model.Cache
{
    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdAsRef : IIdTuple, IEquatable<IdAsRef>
    {
        public static readonly IdAsRef NoId = new IdAsRef(null); // Cette valeur n'a aucun sens
        public static IdAsRef Create<T>(T obj)
            where T : IHasMutableClosedId, IComparable
        {
            return new IdAsRef(obj);
        }

        private IdAsRef(IHasMutableClosedId obj)
        {
            _Object = obj;
        }

        public IHasMutableClosedId Object { get { return _Object; } } readonly IHasMutableClosedId _Object;

        public ReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new [] { typeof(IHasMutableClosedId) });

        public IComparable[] Keys { get { return new IComparable[] { (IComparable)_Object }; } }
        public TypedId<IdAsRef> ToTypedId(Type type) { return new TypedId<IdAsRef>(type, this); } ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public IIdTuple ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { throw new InvalidOperationException(); }

        private string AsDebugString { get { return "[" + (_Object == null ? "NO ID" : nameof(IdAsRef) + " = " + GetRandomThenImmutableId.For(_Object)) + "]"; } }
        public override string ToString() { return AsDebugString; }

        class GetRandomThenImmutableId
        {
            public static int For(IHasMutableClosedId obj)
            {
                // En C++, cette methode pourrait simplement retourner l'adresse mémoire de obj
                // En C# cela est impossible car le GC peut deplacer l'objet a n'importe quel moment
                // On est donc obligé d'utiliser une ConditionalWeakTable
                return _debugIds.GetValue(obj, _ => new GetRandomThenImmutableId())._Value;
            }
            private GetRandomThenImmutableId() { }
            readonly int _Value = Interlocked.Increment(ref _valueSeed);
            static int _valueSeed;

            static readonly ConditionalWeakTable<IHasMutableClosedId, GetRandomThenImmutableId> _debugIds = new ConditionalWeakTable<IHasMutableClosedId, GetRandomThenImmutableId>();
        }


        #region Equality members

        public bool Equals(IdAsRef other)
        {
            return ReferenceEquals(_Object, other._Object);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdAsRef && Equals((IdAsRef)obj);
        }

        public override int GetHashCode()
        {
            return _Object is null ? 0 : GetRandomThenImmutableId.For(_Object);
        }

        public static bool operator ==(IdAsRef left, IdAsRef right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdAsRef left, IdAsRef right)
        {
            return !left.Equals(right);
        }
        #endregion
    }
}
