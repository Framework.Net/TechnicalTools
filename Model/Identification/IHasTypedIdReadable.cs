﻿using System;

using TechnicalTools.Extensions.Data;


namespace TechnicalTools.Model.Cache
{
    public interface IHasTypedIdReadable : IHasClosedIdReadable
    {
        [IsTechnicalProperty(HideFromUser = true)]
        ITypedId TypedId { get; }
        ITypedId MakeTypedId(IIdTuple id);
    }

    public interface IHasTypedIdReadable<TIdTuple> : IHasTypedIdReadable
         where TIdTuple : struct, IEquatable<TIdTuple>, IIdTuple
    {
        [IsTechnicalProperty(HideFromUser = true)]
        new TypedId<TIdTuple> TypedId { get; }
        TypedId<TIdTuple> MakeTypedId(TIdTuple id);
    }
}
