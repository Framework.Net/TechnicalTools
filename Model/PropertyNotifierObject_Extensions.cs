﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;


namespace TechnicalTools.Model
{
    /// Ces extensions sont des raccourci aux methodes des objets heritant de PropertyNotifierObject
    /// Elles permettent seulement d'eviter de fournir le paramètre générique => Meilleur autocompletion => developper friendly
    /// A voir a l'usage si on garde ou on vire ces extension
    public static class PropertyNotifierObject_Extensions
    {
        /// <summary>
        /// Raccourci pour <see cref="IPropertyNotifierObject.PropertyChangedFor{T}">PropertyNotifierObject.PropertyChangedFor</see>
        /// <example>
        /// this.PersonEdited.PropertyChangedFor(p => p.Name, PersonWatched_NameChanged);
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void PropertyChangedFor<T>(this T obj, Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            obj.PropertyChangedFor(expression, onPropertyChanged);
        }

        /// <summary>
        /// Raccourci pour <see cref="IPropertyNotifierObject.PropertyChangedFor_Remove{T}">PropertyNotifierObject.PropertyChangedFor_Remove</see>
        /// <example>
        /// this.PersonEdited.PropertyChangedFor_Remove(p => p.Name, PersonWatched_NameChanged);
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void PropertyChangedFor_Remove<T>(this T obj, Expression<Func<T, object>> expression, Action onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            obj.PropertyChangedFor_Remove(expression, onPropertyChanged);
        }

        /// <summary>
        /// Raccourci pour <see cref="IPropertyNotifierObject.PropertyChangingFor{T}">PropertyNotifierObject.PropertyChangingFor</see>
        /// <example>
        /// this.PersonEdited.PropertyChangingFor(p => p.Name, e => Debug.Assert(string.IsNullOrEmpty(e.NewValue)));
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void PropertyChangingFor<T>(this T obj, Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanging)
            where T : class, IPropertyNotifierObject
        {
            obj.PropertyChangingFor(expression, onPropertyChanging);
        }

        /// <summary>
        /// Raccourci pour <see cref="IPropertyNotifierObject.PropertyChangedFor_Remove{T}">PropertyNotifierObject.PropertyChangedFor_Remove</see>
        /// <example>
        /// this.PersonEdited.PropertyChangedFor_Remove(p => p.Name, PersonWatched_NameChanged);
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void PropertyChangingFor_Remove<T>(this T obj, Expression<Func<T, object>> expression, Action<object, PropertyChangingEventArgs> onPropertyChanged)
            where T : class, IPropertyNotifierObject
        {
            obj.PropertyChangingFor_Remove(expression, onPropertyChanged);
        }

        /// <summary>
        /// Raccourci pour <see cref="IPropertyNotifierObject.PropertyChangingForObsolete{T,TProperty}">PropertyNotifierObject.PropertyChangingFor</see>
        /// <example>
        /// this.PersonEdited.PropertyChangingFor(p => p.Name, e => Debug.Assert(string.IsNullOrEmpty(e.NewValue)));
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Obsolete("", true)]
        public static void PropertyChangingForObsolete<T, TProperty>(this T obj, Expression<Func<T, TProperty>> expression, Action<T, PropertyChangingEventArgs<TProperty>> onPropertyChanging)
            where T : class, IPropertyNotifierObject
        {
            //obj.PropertyChangingForObsolete(expression, onPropertyChanging);
        }

        /// <summary>
        /// Raccourci pour <see cref="IPropertyNotifierObject.PropertyChangingFor_RemoveObsolete{T,TProperty}">PropertyNotifierObject.PropertyChangingFor_Remove</see>
        /// <example>
        /// this.PersonEdited.PropertyChangingFor_Remove(p => p.Name, PersonWatched_NameChanging);
        /// </example>
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [Obsolete("", true)]
        public static void PropertyChangingFor_RemoveObsolete<T, TProperty>(this T obj, Expression<Func<T, TProperty>> expression, Action<T, PropertyChangingEventArgs<TProperty>> onPropertyChanging)
            where T : class, IPropertyNotifierObject
        {
            //obj.PropertyChangingFor_RemoveObsolete(expression, onPropertyChanging);
        }


        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void CurrentPropertyIsDependantOn<T>(this T obj, Expression<Func<T, object>> dependency_prop_has_expression, Func<bool> refresh_current_property = null, [CallerMemberName] string dependent_propertyName = null)
            where T : class, IPropertyNotifierObject
        {
            obj.CurrentPropertyIsDependantOn(dependency_prop_has_expression, refresh_current_property, dependent_propertyName);
        }
    }
}
