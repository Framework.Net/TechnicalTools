﻿using System;
using System.Xml.Linq;


namespace TechnicalTools.Tools.Logging
{
    public interface ILogSerializer
    {
        string Serialize(LogContext ctx);
    }
    public class LogXmlSerializer : ILogSerializer
    {
        public bool IgnoreNonDeterministicValue { get; set; }

        public LogXmlSerializer()
        {
        }

        public string Serialize(LogContext ctx)
        {
            return SerializeToXML(ctx).ToString();
        }

        public virtual XElement SerializeToXML(LogContext ctx)
        {
            var nRoot = new XElement("CallLog", (IgnoreNonDeterministicValue ? null : new XAttribute("ThreadId", ctx.ThreadId)));
            if (ctx.RootCall != null)
            {
                var nFirstCall = ToXml(ctx.RootCall);
                nRoot.Add(nFirstCall);
            }
            return nRoot;
        }
        protected XElement ToXml(LogMemberCall log)
        {
            var nLog = new XElement(log.IsFunction.Value ? "Function" : "Sub", new XAttribute("Name", log.MemberName));

            if (log.Args != null)
            {
                var nArgs = new XElement("Args");
                foreach (var arg in log.Args)
                    nArgs.Add(new XElement("Arg", new XAttribute("Key", arg.Key),
                                                   new XAttribute("Value", arg.Value == null ? "<NULL>" : arg.Value.ToString())));
                nLog.Add(nArgs);
            }

            foreach (var subLog in log.Calls)
                nLog.Add(ToXml(subLog));
            if (!IgnoreNonDeterministicValue)
                nLog.Add(new XElement("Perf",      new XAttribute("Start", log.StartTime),
                            log.EndTime.HasValue ? new XAttribute("Duration", new TimeSpan(log.EndTime.Value.Ticks - log.StartTime.Ticks)) : null));

            if (log.Exception != null)
            {
                var nEx = new XElement("Exception");
                nLog.Add(nEx);
                var ex = log.Exception;
                while (true)
                {
                    nEx.Add(new XAttribute("Message", ex.Message));
                    if (!IgnoreNonDeterministicValue && ex.StackTrace != null)
                        nEx.Add(new XAttribute("StackTrace", ex.StackTrace));
                    ex = ex.InnerException;
                    if (ex == null)
                        break;

                    var n = new XElement("InnerException", new XAttribute("TODO", "TODO"));
                    nEx.Add(n);
                    nEx = n;
                }
            }
            else if (log.IsFunction ?? false)
            {
                var nResult = new XElement("Result");
                if (log.Result == null)
                    nResult.Add(new XAttribute("IsNull", true));
                else
                    nResult.Value = log.Result.ToString();
                nLog.Add(nResult);
            }
            return nLog;
        }
    }
}
