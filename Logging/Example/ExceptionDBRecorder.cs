﻿using System;
using System.Linq;
using System.Diagnostics;


namespace TechnicalTools.Tools.Logging.Example
{
    //public class ExceptionDBRecorder
    //{
    //    readonly ILogSerializer _serializer;
    //    TaskQueue<LogContext> _taskQueue;

    //    public ExceptionDBRecorder(ILogSerializer serializer)
    //    {
    //        _serializer = serializer;
    //        Start();
    //    }

    //    public void Start()
    //    {
    //        if (_taskQueue == null)
    //            _taskQueue = new TaskQueue<LogContext>(1, Send);
    //    }

    //    public void StopAndWait()
    //    {
    //        _taskQueue.Dispose();
    //        _taskQueue = null;
    //    }

    //    public void ScheduleSend(LogContext ctx)
    //    {
    //        if (_taskQueue == null)
    //            throw new ArgumentException("ExceptionDBRecorder has not been started!");
    //        if (ctx.RootCall == null)
    //            throw new ArgumentException("LogContext is empty (ie RootCall is null)");
    //        ctx.IncrementReferenceCounter(this); // Prevent the context from being reused by Logger
    //        _taskQueue.EnqueueTask(ctx);
    //    }

    //    void Send(LogContext ctx)
    //    {
    //        try
    //        {
    //            string logs = _serializer.Serialize(ctx);
    //            Debug.Assert(ctx.RootCall != null, "cf ScheduleSend");
    //            Debug.Assert(ctx.RootCall.EndTime.HasValue); // ExceptionDBRecorder envoie un log quand celui-ci est sorti de toutes les méthodes logguées.
    //                                                         // La premiere action logguée est donc terminée (réussie ou en échec)
    //            var log = new LogError()
    //            {
    //                Method = ctx.RootCall.MemberName,
    //                Request_Id = ctx.ThreadId,
    //                FullLog = logs,
    //                ErrorMessage = ctx.RootCall.Exception == null ? null : ctx.RootCall.Exception.Message, // TODO : utiliser un gestionniaire de Exception => string pour gerer tous les cas (inner exception etc)
    //                WhenUTC = ctx.RootCall.StartTime,
    //                Duration = ctx.RootCall.EndTime.Value - ctx.RootCall.StartTime,
    //                Args = ctx.RootCall.Args == null ? ""
    //                     : string.Join(", ", ctx.RootCall.Args.Select(kvp => kvp.Key + ": " + Convert.ToString(kvp.Value ?? "<NULL>")))

    //            };
    //            DAO.Instance.CreateInDatabase(log);
    //        }
    //        catch (Exception ex)
    //        {
    //            Console.WriteLine(ex);
    //        }
    //        finally
    //        {
    //            ctx.DecrementReferenceCounter(this); // Allow loggerto recycle item !
    //        }
    //    }
    //}
}
