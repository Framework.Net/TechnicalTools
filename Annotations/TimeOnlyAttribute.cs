﻿using System;
using System.Diagnostics;


namespace TechnicalTools.Annotations
{
    /// <summary>
    /// Indique qu'une valeur de type DateTime n'est pas censé contenir de partie Date mais seulement une partie Time.
    /// Lorsque le type DateTime est utilisé pour representer des durée ou des moments de la journée (exemple 16h, l'apres midi), on devrait logiquement lesm igrer en TimeSpan.
    /// Mais il n'est pas toujours possible de migrer les DateTime en TimeSpan facilement / rapidement.
    /// Cet attribut permet d'expliciter, en attendant la migration en Timespan, que seul la partie Time est (doit être) utilisé, que la partier Date est donc supposer être toujours à la valeur min.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
    public class TimeOnlyAttribute : System.Attribute, IArgumentValidationAttribute
    {
        public void Validate(object value, string argumentName)
        {
            Debug.Assert(value is DateTime);
            if (((DateTime) value).Date != DateTime.MinValue.Date)
                throw new Exception(string.Format("DateTime {0} should not contains date part, only time", argumentName));
        }
    }
}
