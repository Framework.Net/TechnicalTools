﻿using System;


namespace TechnicalTools.Annotations
{
    public interface IArgumentValidationAttribute
    {
        void Validate(object value, string argumentName);
    }
}
