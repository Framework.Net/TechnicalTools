﻿using System;
using System.Collections.Concurrent;
using System.Reflection;

namespace TechnicalTools.Annotations
{
    /// <summary>
    /// Allow you to name a class or interface using a business Name.
    /// For example it is useful for UI class that wrap a real business class.
    /// </summary>
    public class BusinessNameAttribute : Attribute
    {
        public string Name       { get; set; }
        public string Group      { get; set; }
        public string SuperGroup { get; set; }

        public BusinessNameAttribute(string businessName, string group = null, string superGroup = null)
        {
            Name = businessName;
            Group = group;
            SuperGroup = superGroup;
        }

        public void CleanCache()
        {
            _businessNameAttributeByTypes.Clear();
            _businessNameAttributeByProperties.Clear();
        }


        public static BusinessNameAttribute Get(Type type)
        {
            if (_businessNameAttributeByTypes.TryGetValue(type, out BusinessNameAttribute res))
                return res;
            res = Extract(type);
            _businessNameAttributeByTypes[type] = res;
            return res;
        }
        static readonly ConcurrentDictionary<Type, BusinessNameAttribute> _businessNameAttributeByTypes = new ConcurrentDictionary<Type, BusinessNameAttribute>();
        static BusinessNameAttribute Extract(Type type)
        {
            while (type != typeof(void) && type != typeof(object) &&
                   type != null) // case for interface
            {
                var att = type.GetCustomAttribute<BusinessNameAttribute>();
                if (att != null)
                    return att;
                type = type.BaseType;
            }
            return null;
        }

        public static BusinessNameAttribute Get(PropertyInfo prop)
        {
            if (_businessNameAttributeByProperties.TryGetValue(prop, out BusinessNameAttribute res))
                return res;
            res = Extract(prop);
            _businessNameAttributeByProperties[prop] = res;
            return res;
        }
        static readonly ConcurrentDictionary<PropertyInfo, BusinessNameAttribute> _businessNameAttributeByProperties = new ConcurrentDictionary<PropertyInfo, BusinessNameAttribute>();
        static BusinessNameAttribute Extract(PropertyInfo prop)
        {
            return prop.GetCustomAttribute<BusinessNameAttribute>(true);
        }

        public static string GetNameFor(Type type)
        {
            return Get(type)?.Name;
        }
    }
}
