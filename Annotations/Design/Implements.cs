﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TechnicalTools.Annotations.Design
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple=true)]
    public class Implements : EntityAttribute
    {
        public Type Interface { get { Debug.Assert(IsBuilt); return _InterfaceType; } }

        public Implements(Type interfaceType, params object[] genericParams)
        {
            _BaseInterfaceType = interfaceType;
            if (genericParams != null && !genericParams.All(p => p is string || p is Type))
                throw new Exception("Seul les parametres de tpe Type ou string sont acceptés !");
            if (genericParams != null && genericParams.Any(p => p is string) && !_BaseInterfaceType.IsGenericTypeDefinition)
                throw new Exception("La classe ou l'interface n'est pas générique ! Il ne peut y avoir de parametres");
            _GenericParamsUnresolved = genericParams;
            _GenericParams = new Type[genericParams.Length];
        }
        readonly Type _BaseInterfaceType;
        readonly object[] _GenericParamsUnresolved;  // Soit un Type, soit une string dans le cou on reference un parametre generique
        Type[] _GenericParams;
        Type _InterfaceType;

        public override bool IsBuilt
        {
            get { return _InterfaceType != null; }
        }

        public override void Build(Type thisClass)
        {
            _InterfaceType = BuildType(thisClass, _BaseInterfaceType, _GenericParamsUnresolved, ref _GenericParams);
        }
    }
}
