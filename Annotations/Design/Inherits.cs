﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TechnicalTools.Annotations.Design
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple=false)]
    public class Inherits : EntityAttribute
    {
        public Type BaseClass { get { Debug.Assert(IsBuilt); return _BaseClass; } }

        public Inherits(Type baseClassType, params object[] genericParams)
        {
            _BaseClassType = baseClassType;
            if (genericParams != null && !genericParams.All(p => p is string || p is Type))
                throw new Exception("Seul les parametres de tpe Type ou string sont acceptés !");
            if (genericParams != null && genericParams.Any(p => p is string) && !_BaseClassType.IsGenericTypeDefinition)
                throw new Exception("La classe ou l'interface n'est pas générique ! Il ne peut y avoir de parametres");
            _GenericParamsUnresolved = genericParams;
            _GenericParams = new Type[genericParams.Length];
        }
        Type _BaseClassType;
        readonly object[] _GenericParamsUnresolved;  // Soit un Type, soit une string dans le cou on reference un parametre generique
        Type[] _GenericParams;
        Type _BaseClass;

        public override bool IsBuilt
        {
            get { return _BaseClass != null; }
        }

        public override void Build(Type thisClass)
        {
            _BaseClass = BuildType(thisClass, _BaseClassType, _GenericParamsUnresolved, ref _GenericParams);
        }

    }
}
