﻿using System;
using System.ComponentModel;


namespace TechnicalTools.Annotations
{
    /// <summary>
    /// Provide a clean way to store todo list in code.
    /// Comment in code are not retrievable, Attributes are (with reflection)
    /// So why keep bother with comment anyway ?
    /// </summary>
    public class ToDoAttribute : Attribute
    {
        public string               Comment      { get; set; }

        public IsAbout              About        { get; set; }
        public eWorkPriority        Priority     { get; set; }
        public eTimeRequiredInDay   RequiredTime { get; set; }
        public HelpNeeded           HelpNeeded   { get; set; }
        public eMoscow              Moscow       { get; set; }
        public eACID                ACID         { get; set; }
        public eFrequency           Frequency    { get; set; }

        public ToDoAttribute(string comment = null)
        {
            Comment = comment;
        }
    }

    public enum eWorkPriority
    {
        Lowest,
        Low,
        Medium,
        Hight,
        Highest, // Need to do this after what we currently do
        Urgent // Need to stop what we do (curreand do this instead
    }

    [Flags]
    public enum HelpNeeded
    {
        None = 0,

        TechLead = 1 << 0,
        DevelopperColleague = 1 << 2,

        DevelopperFromOtherTeam = 1 << 3,
        TechLeadFromOtherTeam = 1 << 4,

        Support = 1 << 5,
        ProductManager = 1 << 6, // Someone that understand the product
        ExternalSupport = 1 << 7, // calling support  of a product we use
        ExternalProductManager = 1 << 8, // Meeting with another company

        DatabaseAdmin = 1 << 9, // For a database tweak & trick ... or a treat

        Treasury = 1 << 10, // For money or buying tools
    }


    public enum eTimeRequiredInDay
    {
        OneHour = 10,
        TwoHour = 25,
        HalfADay = 50,
        OneDay = 100,
        TwoDays = 200,
        ThreeDays = 300,
        OneWeek = 500,
        TwoWeeks = 1000,
        OneMonth = 2100,
        TwoMonth = 4200,
        ThreeMonth = 6300,
        OneSemester = 12600,
        OneYear = 25200,
    }

    public enum eMoscow
    {
        [Description("")]
        None = 0,

        Must,
        Should,
        Could,
        [Description("Won't")]
        Wont, // this time ;)
    }

    public enum eACID
    {
        [Description("")]
        None = 0,

        Atomicity,
        Consistency,
        Isolation,
        Durability,
    }

    public enum eFrequency
    {
        Never = 0,

        Rarely,
        HardlyEver,
        Occasionally,
        Sometimes,
        Often,
        VeryOften,
        Usually,
        NearlyAlways,
        Always,
    }

    [Flags]
    public enum IsAbout
    {
        None = 0, // The most important ... need you to put a topic on it

        Bug = 1 << 1,
        EdgeCase = 1 << 2,
        Improvement = 1 << 3,

        NeedSupport = 1 << 9, // need support for a technical product where developpers are the client (library support, database admin etc)
        NeedProductManager = 1 << 6,

        Performance = 1 << 7,
        ProductFeature = 1 << 8,

        LegacyCleaning = 1 << 17,
        Renaming = 1 << 18,
        DataQuality = 1 << 19,
        Refactoring = 1 << 20,
        ThreadSafety = 1 << 21,
        Immutability = 1 << 22,

        Documentation = 1 << 23,
        NeedStudy = 1 << 24,

        Database = 1 << 25,
        Business = 1 << 26,
        UI = 1 << 27,

        ExperimentalIdea = 1 << 31,
    }
}
