using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TechnicalTools.Analysis;


namespace TechnicalTools.Tests.Analysis
{
    [TestClass]
    public class XmlSkeletonFinder_CodeSqlGeneratorTests
    {
        [ClassInitialize]
        public static void ClassInitialize(TestContext ctx)
        {
        }

        [DebuggerHidden, DebuggerStepThrough]
        static void Test(string xmlData, string expectedXmlSkeleton, string expectedSqlCode, string expectedCSharpCode)
        {
            Test(new string[] { xmlData }, expectedXmlSkeleton, expectedSqlCode, expectedCSharpCode);
        }
        static void Test(string[] xmlData, string expectedXmlSkeleton, string expectedSqlCode, string expectedCSharpCode)
        {
            var finder = new XmlSkeletonFinder();
            var doc = finder.BuildSkeletonFromText(xmlData, xmlData.Length, new Progress<string>());
            if (doc.ToString() != expectedXmlSkeleton.Trim())
                throw new Exception("Test failed!");
            var sqlCode = finder.GenerateSqlTableScriptForXmlNode(doc.Root);
            if (sqlCode.ToSqlScript(true) != expectedSqlCode.Trim())
                throw new Exception("Test failed!");
            var cSharpCode = finder.GenerateParsingCodeCSharp(sqlCode, true);
            if (!XmlSkeletonFinder.PiecesOfCodeAreSimilar(cSharpCode, expectedCSharpCode.Trim()))
                throw new Exception("Test failed!");
        }


        [TestMethod]
        public void SimpleTest1()
        {
            Test(
@"<a>aaa</a>",

@"<!--Probability: 100,00% parent node count:1-->
<a>aaa</a>",

@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [a] nvarchar(max) NULL
)",
@"public class Parser : TechnicalTools.Analysis.XmlSkeletonFinder.BaseParser
{
    public class A
    {
        public long Id_a { get; set; }
        public string Text { get; set; }
    }

    protected override void DoParse(XElement node)
    {
        Result.Add(typeof(A), new List<object>());
        var a = ParseA(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;
        if (results.TryGetValue(typeof(A), out lst))
        {
            saveAll(lst);
        }
    }

    A ParseA(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new A();
        parsedObj.Text = GetText(node);

        return parsedObj;
    }
}"
);
        }

        [TestMethod]
        public void SimpleTest2()
        {
            Test(
@"<a><b><c>blah<d> foo </d></c></b></a>",
@"<!--Probability: 100,00% parent node count:1-->
<a>
  <!--Probability: 100,00% parent node count:1-->
  <b>
    <!--Probability: 100,00% parent node count:1-->
    <c>blah<!--Probability: 100,00% parent node count:1--><d> foo </d></c>
  </b>
</a>",
@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [b_c] nvarchar(max) NULL,
    [b_c_d] nvarchar(max) NULL
)",
@"public class Parser : TechnicalTools.Analysis.XmlSkeletonFinder.BaseParser
{
    public class A
    {
        public long Id_a { get; set; }
        public string B_c { get; set; }
        public string B_c_d { get; set; }
    }

    protected override void DoParse(XElement node)
    {
        Result.Add(typeof(A), new List<object>());
        var a = ParseA(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;
        if (results.TryGetValue(typeof(A), out lst))
        {
            saveAll(lst);
        }
    }

    A ParseA(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new A();
        parsedObj.B_c = GetText(GetElement(GetElement(node, ""b"", true), ""c"", true));
        parsedObj.B_c_d = GetText(GetElement(GetElement(GetElement(node, ""b"", true), ""c"", true), ""d"", true));

        return parsedObj;
    }
}");
        }

        [TestMethod]
        public void SimpleTest3()
        {
            Test(
@"<a><address><line> blabla </line><line/></address></a>",
@"<!--Probability: 100,00% parent node count:1-->
<a>
  <!--Probability: 100,00% parent node count:1-->
  <address>
    <!--Probability: 100,00% parent node count:1-->
    <line> blabla </line>
    <line />
  </address>
</a>",
@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [address_line] nvarchar(max) NULL
)",
@"public class Parser : TechnicalTools.Analysis.XmlSkeletonFinder.BaseParser
{
    public class A
    {
        public long Id_a { get; set; }
        public string Address_line { get; set; }
    }

    protected override void DoParse(XElement node)
    {
        Result.Add(typeof(A), new List<object>());
        var a = ParseA(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;
        if (results.TryGetValue(typeof(A), out lst))
        {
            saveAll(lst);
        }
    }

    A ParseA(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new A();
        parsedObj.Address_line = Join("" "",  GetElements(GetElement(node, ""address"", true), ""line"")
                                           .Select(elt => GetText(elt))
                                           .Where(str => !string.IsNullOrWhiteSpace(str)));

        return parsedObj;
    }
}");
        }

        [TestMethod]
        public void SimpleTest4()
        {
            Test(
@"<a><w> foo <x> ignored </x> bar </w></a>",
@"<!--Probability: 100,00% parent node count:1-->
<a>
  <!--Probability: 100,00% parent node count:1-->
  <w> foo <!--Probability: 100,00% parent node count:1--><x> ignored </x></w>
</a>",
@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [w] nvarchar(max) NULL,
    [w_x] nvarchar(max) NULL
)",
@"public class Parser : TechnicalTools.Analysis.XmlSkeletonFinder.BaseParser
{
    public class A
    {
        public long Id_a { get; set; }
        public string W { get; set; }
        public string W_x { get; set; }
    }

    protected override void DoParse(XElement node)
    {
        Result.Add(typeof(A), new List<object>());
        var a = ParseA(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;
        if (results.TryGetValue(typeof(A), out lst))
        {
            saveAll(lst);
        }
    }

    A ParseA(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new A();
        parsedObj.W = GetText(GetElement(node, ""w"", true));
        parsedObj.W_x = GetText(GetElement(GetElement(node, ""w"", true), ""x"", true));

        return parsedObj;
    }
}");
        }
        [TestMethod]
        public void SimpleTest5()
        {
            Test(
@"<a>
     <field1></field1>
     <DirtyObject> foo <field> ignored </field> bar </DirtyObject>
     <DirtyObject/></a>",
@"<!--Probability: 100,00% parent node count:1-->
<a>
  <!--Probability: 100,00% parent node count:1-->
  <field1></field1>
  <!--Probability: 100,00% parent node count:1-->
  <DirtyObject> foo <!--Probability: 50,00% parent node count:2--><field> ignored </field></DirtyObject>
  <DirtyObject />
</a>",
@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [field1] nvarchar(max) NULL
)


CREATE TABLE [Dbo].[DirtyObject]
(
    [Id_DirtyObject] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [a_Id] bigint NOT NULL  FOREIGN KEY REFERENCES [Dbo].[a](Id_a),
    [DirtyObject] nvarchar(max) NULL,
    [field] nvarchar(max) NULL
)",
@"public class Parser : TechnicalTools.Analysis.XmlSkeletonFinder.BaseParser
{
    public class A
    {
        public long Id_a { get; set; }
        public string Field1 { get; set; }
    }

    public class DirtyObject
    {
        public long Id_DirtyObject { get; set; }
        public long A_Id { get; set; }
        public string Text { get; set; }
        public string Field { get; set; }
    }

    protected override void DoParse(XElement node)
    {
        Result.Add(typeof(A), new List<object>());
        Result.Add(typeof(DirtyObject), new List<object>());
        var a = ParseA(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;
        if (results.TryGetValue(typeof(A), out lst))
        {
            saveAll(lst);
            foreach (var obj in lst.Cast<A>())
            {
                foreach (var child in ChildrenOf<DirtyObject>(obj))
                    child.A_Id = obj.Id_a;
            }
        }

        if (results.TryGetValue(typeof(DirtyObject), out lst))
        {
            saveAll(lst);
        }
    }

    A ParseA(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new A();
        parsedObj.Field1 = GetText(GetElement(node, ""field1"", true));


        foreach (var child in GetElements(node, ""DirtyObject""))
        {
            var parsedChild = ParseDirtyObject(child);
            AddResult(parsedChild);
            AddChild(parsedObj, parsedChild);
        }

        return parsedObj;
    }

    DirtyObject ParseDirtyObject(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new DirtyObject();
        parsedObj.Text = GetText(node);
        parsedObj.Field = GetText(GetElement(node, ""field"", false));

        return parsedObj;
    }
}");
        }

        [TestMethod]
        public void SimpleTest6()
        {
            Test(
@"<a><b><c>blah<d>foo</d><d /></c><c /></b><b /></a>",
@"<!--Probability: 100,00% parent node count:1-->
<a>
  <!--Probability: 100,00% parent node count:1-->
  <b>
    <!--Probability: 50,00% parent node count:2-->
    <c>blah<!--Probability: 50,00% parent node count:2--><d>foo</d><d /></c>
    <c />
  </b>
  <b />
</a>",
@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [b_c] nvarchar(max) NULL,
    [b_c_d] nvarchar(max) NULL
)",
@"public class Parser : TechnicalTools.Analysis.XmlSkeletonFinder.BaseParser
{
    public class A
    {
        public long Id_a { get; set; }
        public string B_c { get; set; }
        public string B_c_d { get; set; }
    }

    protected override void DoParse(XElement node)
    {
        Result.Add(typeof(A), new List<object>());
        var a = ParseA(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;
        if (results.TryGetValue(typeof(A), out lst))
        {
            saveAll(lst);
        }
    }

    A ParseA(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new A();
        parsedObj.B_c = Join("" "", GetElements(node, ""b"").SelectMany(n => GetElements(n, ""c""))
                                      .Select(elt => GetText(elt))
                                      .Where(str => !string.IsNullOrWhiteSpace(str)));
        parsedObj.B_c_d = Join("" "", GetElements(node, ""b"").SelectMany(n => GetElements(n, ""c"")).SelectMany(n => GetElements(n, ""d""))
                                        .Select(elt => GetText(elt))
                                        .Where(str => !string.IsNullOrWhiteSpace(str)));

        return parsedObj;
    }
}");
        }

        [TestMethod]
        public void SimpleTest7()
        {
            Test(
@"<a><b><c><f1>1</f1><f2>2</f2><f3>3</f3></c><c/></b></a>",
@"<!--Probability: 100,00% parent node count:1-->
<a>
  <!--Probability: 100,00% parent node count:1-->
  <b>
    <!--Probability: 100,00% parent node count:1-->
    <c>
      <!--Probability: 50,00% parent node count:2-->
      <f1>1</f1>
      <!--Probability: 50,00% parent node count:2-->
      <f2>2</f2>
      <!--Probability: 50,00% parent node count:2-->
      <f3>3</f3>
    </c>
    <c />
  </b>
</a>",
@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [b_c_f1] nvarchar(max) NULL,
    [b_c_f2] nvarchar(max) NULL,
    [b_c_f3] nvarchar(max) NULL
)",
@"public class Parser : TechnicalTools.Analysis.XmlSkeletonFinder.BaseParser
{
    public class A
    {
        public long Id_a { get; set; }
        public string B_c_f1 { get; set; }
        public string B_c_f2 { get; set; }
        public string B_c_f3 { get; set; }
    }

    protected override void DoParse(XElement node)
    {
        Result.Add(typeof(A), new List<object>());
        var a = ParseA(node);
        AddResult(a);
    }
    public override void SaveAll(Dictionary<Type, List<object>> results, Action<List<object>> saveAll)
    {
        List<object> lst;
        if (results.TryGetValue(typeof(A), out lst))
        {
            saveAll(lst);
        }
    }

    A ParseA(XElement node)
    {
        _metNodes.Add(node);
        var parsedObj = new A();
        parsedObj.B_c_f1 = Join("" "",  GetElements(GetElement(node, ""b"", true), ""c"").Select(n => GetElement(n, ""f1"", false))
                                     .Select(elt => GetText(elt))
                                     .Where(str => !string.IsNullOrWhiteSpace(str)));
        parsedObj.B_c_f2 = Join("" "",  GetElements(GetElement(node, ""b"", true), ""c"").Select(n => GetElement(n, ""f2"", false))
                                     .Select(elt => GetText(elt))
                                     .Where(str => !string.IsNullOrWhiteSpace(str)));
        parsedObj.B_c_f3 = Join("" "",  GetElements(GetElement(node, ""b"", true), ""c"").Select(n => GetElement(n, ""f3"", false))
                                     .Select(elt => GetText(elt))
                                     .Where(str => !string.IsNullOrWhiteSpace(str)));

        return parsedObj;
    }
}");
        }


        [TestMethod]
        public void FeatureNotImplementedYet_Fails()
        {
            Test(
@"<a>
    <k ><MustBeDefinedOnce><field></field><dummy>42</dummy></MustBeDefinedOnce><MustBeDefinedOnce/></k >
    <k2><MustBeDefinedOnce><field></field><dummy>51</dummy></MustBeDefinedOnce><MustBeDefinedOnce/></k2>
</a>",
@"<!--Probability: 100,00% parent node count:1-->
<a>
  <!--Probability: 100,00% parent node count:1-->
  <k>
    <!--Probability: 100,00% parent node count:2-->
    <MustBeDefinedOnce>
      <!--Probability: 50,00% parent node count:1-->
      <field></field>
      <!--Probability: 50,00% parent node count:1-->
      <dummy>42</dummy>
    </MustBeDefinedOnce>
    <MustBeDefinedOnce />
  </k>
  <!--Probability: 100,00% parent node count:1-->
  <k2>
    <!--Probability: 100,00% parent node count:2-->
    <MustBeDefinedOnce>
      <!--Probability: 50,00% parent node count:1-->
      <field></field>
      <!--Probability: 50,00% parent node count:1-->
      <dummy>51</dummy>
    </MustBeDefinedOnce>
    <MustBeDefinedOnce />
  </k2>
</a>",
@"CREATE TABLE [Dbo].[a]
(
    [Id_a] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1)
)


CREATE TABLE [Dbo].[k_MustBeDefinedOnce]
(
    [Id_MustBeDefinedOnce] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [a_Id] bigint NOT NULL  FOREIGN KEY REFERENCES [Dbo].[a](Id_a),
    [field] nvarchar(max) NULL,
    [dummy] nvarchar(max) NULL
)

CREATE TABLE [Dbo].[k2_MustBeDefinedOnce]
(
    [Id_MustBeDefinedOnce] BIGINT NOT NULL  PRIMARY KEY  IDENTITY(1,1),
    [a_Id] bigint NOT NULL  FOREIGN KEY REFERENCES [Dbo].[a](Id_a),
    [field] nvarchar(max) NULL,
    [dummy] nvarchar(max) NULL
)",
@"");
        }
    }
}
