﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TechnicalTools.Tools.Tests // Do not change sub namespace !
{

    [TestClass]
    public class CSVSmartParser_Tests
    {
        [TestMethod]
        public void _1_Simple_Cases()
        {
            var parser = new CSVSmartParser();

            var res1 = parser.Read(new[] { "", "", "" });
            Assert.AreEqual(res1.titles.Count(), 0, "Because empty lines at beginning are skipped");
            Assert.AreEqual(res1.records.Count(), 0);

            var res2 = parser.Read(new[] { "", "", "", "Answer 42", "", "", "", }); // with empty string and empty records
            Assert.AreEqual(res2.titles.Count(), 1, "Because it is string");
            Assert.AreEqual(res2.titles.First().Title, "Answer 42");
            Assert.AreEqual(res2.records.Count(), 0);

            var res3 = parser.Read(new[] { "42" });
            Assert.AreEqual(res3.titles.Count(), 1, "dummy title");
            Assert.AreEqual(res3.records.Count(), 1, "because 42 is not only string but a number so it is interpreted as data");
            Assert.AreEqual(res3.records.Cast<IDictionary<string, object>>().First().First().Value, (byte)42);

            var res4 = parser.Read(new[] { "answer", "42" });
            Assert.AreEqual(res4.titles.Count(), 1);
            Assert.AreEqual(res4.records.Count(), 1);
            Assert.AreEqual(res4.records.Cast<IDictionary<string,object>>().First()[res4.titles.First().FieldName], (byte)42);

            var res5 = parser.Read(new[] { "\"answer; or not?\"", "42\t43" }); // case when missing title and with bad separator inside caption while \t is the good one
            Assert.AreEqual(res5.titles.Count(), 2);
            Assert.AreEqual(res5.records.Count(), 1);
            Assert.AreEqual(res5.records.Cast<IDictionary<string, object>>().First()[res5.titles.First().FieldName], (byte)42);
            Assert.AreEqual(res5.records.Cast<IDictionary<string, object>>().First()[res5.titles.Last().FieldName], (byte)43);

            var res6 = parser.Read(new[] { "Super Title", "title 1", "42;43" }); // case when missing a Super Title and a title
            Assert.AreEqual(res6.titles.Count(), 2);
            Assert.AreEqual(res6.titles.First().SuperTitle, "Super Title");
            Assert.AreEqual(res6.titles.Last().SuperTitle, "Super Title");
            Assert.AreEqual(res6.records.Count(), 1);
            Assert.AreEqual(res6.records.Cast<IDictionary<string, object>>().First()[res6.titles.First().FieldName], (byte)42);
            Assert.AreEqual(res6.records.Cast<IDictionary<string, object>>().First()[res6.titles.Last().FieldName], (byte)43);
        }

        [TestMethod]
        public void _2_Lines_Are_Incomplete_Or_Not_Same_Length()
        {
            var parser = new CSVSmartParser();

            var res = parser.Read(new[] { ";;B", ";a2;b1;b2;b3", "11;12;21;22;23;24", "1011;1012;1021;1022;1023;1024" }); // missing "A" and some ; in super title, "b4" in title
            Assert.AreEqual(res.titles.Count(), 6);
            Assert.AreEqual(res.titles[0].SuperTitle, "");
            Assert.AreEqual(res.titles[1].SuperTitle, ""); Assert.AreEqual(res.titles[1].Title, "a2");
            Assert.AreEqual(res.titles[2].SuperTitle, "B"); Assert.AreEqual(res.titles[2].Title, "b1");
            Assert.AreEqual(res.titles[3].SuperTitle, "B"); Assert.AreEqual(res.titles[3].Title, "b2");
            Assert.AreEqual(res.titles[4].SuperTitle, "B"); Assert.AreEqual(res.titles[4].Title, "b3");
            Assert.AreEqual(res.titles[5].SuperTitle, "B");
            Assert.AreEqual(res.records.Count(), 2);
        }
        [TestMethod]
        public void _3_First_Lines_Are_Not_Titles_And_Conversion_Fallbacks()
        {
            var parser = new CSVSmartParser();
            // Test when first line is data but incomplete, data are converted back
            var res = parser.Read(new[] { "1;2;3", "value1;value2;313;1234567", "11;12;13;14", "21;22;23;24" });
            Assert.AreEqual(res.titles.Count(), 4);
            Assert.AreEqual(res.records.Count(), 4);
            var item1 = res.records.Cast<IDictionary<string, object>>().First();
            Assert.AreEqual(item1[res.titles[0].FieldName], "1");
            Assert.AreEqual(item1[res.titles[1].FieldName], "2");
            Assert.AreEqual(item1[res.titles[2].FieldName], (short)3);
            Assert.AreEqual(item1[res.titles[3].FieldName], null);
            var item2 = res.records.Cast<IDictionary<string, object>>().Skip(1).First();
            Assert.AreEqual(item2[res.titles[0].FieldName], "value1");
            Assert.AreEqual(item2[res.titles[1].FieldName], "value2");
            Assert.AreEqual(item2[res.titles[2].FieldName], (short)313);
            Assert.AreEqual(item2[res.titles[3].FieldName], 1234567);
            var item3 = res.records.Cast<IDictionary<string, object>>().Skip(2).First();
            Assert.AreEqual(item3[res.titles[0].FieldName], "11");
            Assert.AreEqual(item3[res.titles[1].FieldName], "12");
            Assert.AreEqual(item3[res.titles[2].FieldName], (short)13);
            Assert.AreEqual(item3[res.titles[3].FieldName], 14);
            var item4 = res.records.Cast<IDictionary<string, object>>().Skip(3).First();
            Assert.AreEqual(item4[res.titles[0].FieldName], "21");
            Assert.AreEqual(item4[res.titles[1].FieldName], "22");
            Assert.AreEqual(item4[res.titles[2].FieldName], (short)23);
            Assert.AreEqual(item4[res.titles[3].FieldName], 24);
        }
    }
}
