﻿using System;

using TechnicalTools.Tools;

using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TechnicalTools.Tests.Tools
{

    [TestClass()]
    public class CommandLineParsing_Test
    {
        ///// <summary>
        ///// Run test. Just check the returned list is empty or display it.
        ///// </summary>
        ///// <returns>Tuple element is (test name => explanation of problem)</returns>
        //public static List<Tuple<string, string>> RunTests()
        //{
        //    var tests = new List<Tuple<string, Action>>()
        //    {
        //        Tuple.Create("TestSimple", (Action)TestSimple)
        //    };

        //    return tests.Select(tuple =>
        //    {
        //        try
        //        {
        //            tuple.Item2();
        //        }
        //        catch (Exception ex)
        //        {
        //            return Tuple.Create(tuple.Item1, ex.Message);
        //        }
        //        return null;
        //    })
        //    .Where(t => t != null)
        //    .ToList();
        //}

        #region Simple Test

        [TestMethod()]
        public void TestSimple()
        {
            var parser = new CommandLineParser();
            var foo = new FooType();
            var usage = parser.DisplayUsageForParsing(foo);
            Assert.AreEqual(Foo1ExpectedUsage, usage);
            // L'argument StringValue serait passé en erivant : --StringValue="What's app or wazaaaaa ?"
            // Windows vire les double quotes en parsant
            string[] args = new[] { "--IntValue=42", "--BoolValue=True", "--EnumValue=Bar", "--stringvalue=What's app or wazaaaaa ?" };
            parser.ParseAndHydrate(args, foo);
            Assert.AreEqual(42, foo.IntValue);
            Assert.AreEqual(true, foo.BoolValue);
            Assert.AreEqual(AnEnum.Bar, foo.EnumValue);
            Assert.AreEqual("What's app or wazaaaaa ?", foo.StringValue);
        }


        enum AnEnum // private is not a problem !
        {
            None = 0,
            Foo,
            Bar,
        }
        class FooType // private is not a problem !
        {
            [Usage("I advise you to choose 42 ! Why ? Because 42 !!!")]
            public int IntValue { get; set; }
            public decimal DecimalValue { get; set; }
            public bool BoolValue { get; set; }
            public AnEnum EnumValue { get; set; }
            [Usage("Any string you want !")]
            public string StringValue { get; set; }
        }
        const string Foo1ExpectedUsage = @"Usage:

--IntValue=<Int32> : I advise you to choose 42 ! Why ? Because 42 !!!

--DecimalValue=<Decimal>

--BoolValue=<Boolean>

--EnumValue={None|Foo|Bar}

--StringValue=<String> : Any string you want !";

        #endregion Simple Test

        #region complex Test

        [TestMethod()]
        public void TestComplex()
        {
            var parser = new CommandLineParser();
            var node = new Node();
            var usage = parser.DisplayUsageForParsing(node);
            Assert.AreEqual(NodeExpectedUsage, usage);
            string[] args = new[] { "--Foo={ IntValue=42, BoolValue=True, EnumValue=Bar, StringValue=NoQuoteString }" };
            try
            {
                parser.ParseAndHydrate(args, node);
                Assert.Fail("L'argument est mal nommé ! C'est pas Foo mais FirstFoo ou SecondFoo");
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex.Message.Contains("--Foo"));
            }
            parser.ParseAndHydrate(new[] { args[0].Replace("--Foo", "--FirstFoo") }, node);

            Assert.AreEqual(0, node.FirstFoo.IntValue);
            Assert.AreEqual(true, node.FirstFoo.BoolValue);
            Assert.AreEqual(AnEnum.Bar, node.FirstFoo.EnumValue);
            Assert.AreEqual("NoQuoteString", node.FirstFoo.StringValue);

            Assert.IsNull(node.SecondFoo);
        }



        class Node // private is not a problem !
        {
            public FooType FirstFoo { get; set; }

            [Usage("Because the more the merrier !")]
            public FooType SecondFoo { get; set; }
        }
        const string NodeExpectedUsage = @"Usage:

--FirstFoo={A=1, B=2...} :
                           Use (as first parameter) --Type={ FooType }
                           to create object of specified type (only allowed if the program let this value empty)
                           Here are the usage of each type :

                           For FooType
                              IntValue=<Int32> : I advise you to choose 42 ! Why ? Because 42 !!!

                              DecimalValue=<Decimal>

                              BoolValue=<Boolean>

                              EnumValue={None|Foo|Bar}

                              StringValue=<String> : Any string you want !

--SecondFoo={A=1, B=2...} : Because the more the merrier !
                            Use (as first parameter) --Type={ FooType }
                            to create object of specified type (only allowed if the program let this value empty)
                            Here are the usage of each type :

                            For FooType : etc ... See previous type arguments for type FooType and arg FirstFoo";

        #endregion complex Test
    }
}
