﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using TechnicalTools.Model;

namespace TechnicalTools.MetaProgramming.ExpressionVisitors
{
    /// <summary>
    /// Represents a visitor for converting expression trees to another tree.
    /// </summary>
    /// <remarks>
    /// This class is designed to be inherited to create more specialized
    /// classes whose functionality requires traversing, examining or copying
    /// an expression tree.
    /// Code is bases on implementation of Microsoft's class "ExpressionVisitor"
    /// </remarks>
    public abstract partial class ExpressionConverterVisitor<T>
    {
        protected ExpressionConverterVisitor()
        {
            _dispatcher = new ExpressionConverterCallDispatcher<T>(this);
        }
        readonly ExpressionConverterCallDispatcher<T> _dispatcher;

        /// <summary>
        /// Dispatches the expression to one of the more specialized visit methods in this class.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        public virtual T Visit(Expression node)
        {
            return _dispatcher.VisitDispatch(node);
        }

        #region Helpers

        /// <summary>
        /// Dispatches the list of expressions to one of the more specialized visit methods in this class.
        /// </summary>
        /// <param name="nodes">The expressions to visit.</param>
        /// <returns>The modified expression list, if any of the elements were modified;
        /// otherwise, returns the original expression list.</returns>
        public IReadOnlyCollection<T> Visit(ReadOnlyCollection<Expression> nodes)
        {
            T[] newNodes = null;
            for (int i = 0, n = nodes.Count; i < n; i++)
            {
                T node = Visit(nodes[i]);
                if (node != null)
                {
                    if (newNodes == null)
                        newNodes = new T[nodes.Count];
                    newNodes[i] = node;
                }
            }
            return newNodes ?? Array.Empty<T>();
        }

        protected internal T[] VisitArguments(IArgumentProvider nodes)
        {
            T[] values = null;
            for (int i = 0, n = nodes.ArgumentCount; i < n; i++)
            {
                Expression curNode = nodes.GetArgument(i);
                T value = Visit(curNode);
                if (value != null)
                {
                    if (values == null)
                        values = new T[nodes.ArgumentCount];
                    values[i] = value;
                }
            }
            return values ?? Array.Empty<T>();
        }

        /// <summary>
        /// Visits all nodes in the collection using a specified element visitor.
        /// </summary>
        /// <typeparam name="T2">The type of the nodes.</typeparam>
        /// <param name="nodes">The nodes to visit.</param>
        /// <param name="elementVisitor">A delegate that visits a single element,
        /// optionally replacing it with a element.</param>
        /// <returns>The modified node list, if any of the elements were modified;
        /// otherwise, returns the original node list.</returns>
        public static IReadOnlyCollection<T2> Visit<T2>(ReadOnlyCollection<T2> nodes, Func<T2, T2> elementVisitor)
        {
            T2[] newNodes = null;
            for (int i = 0, n = nodes.Count; i < n; i++)
                newNodes[i] = elementVisitor(nodes[i]);

            return newNodes;
        }

        protected virtual T Collapse(T value)
        {
            return value;
        }
        protected virtual T Collapse(params T[] values)
        {
            IEnumerable<T> asEnumerable = values;
            return Collapse(asEnumerable);
        }
        protected virtual T Collapse(IEnumerable<T> values)
        {
            if (values == null)
                return default(T);
            var res = default(T);
            bool alreadyOne = false;
            foreach (var v in values)
                if (!EqualityComparer<T>.Default.Equals(v, default(T)))
                    if (alreadyOne)
                        throw new TechnicalException("Does not know how to collapse values!, null", null);
                    else
                    {
                        alreadyOne = true;
                        res = v;
                    }
            return res;
        }

        #endregion Helpers

        /// <summary>
        /// Visits the children of the <see cref="BinaryExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitBinary(BinaryExpression node)
        {
            // Walk children in evaluation order: left, conversion, right
            return Collapse(
                Visit(node.Left),
                Visit(node.Conversion), // was VisitAndConvert(node.Conversion, "VisitBinary");
                Visit(node.Right));
        }

        /// <summary>
        /// Visits the children of the <see cref="BlockExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitBlock(BlockExpression node)
        {
            return Collapse(node.Expressions.Select(Visit)
                    .Concat(node.Variables.Select(Visit)));
        }

        /// <summary>
        /// Visits the children of the <see cref="ConditionalExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitConditional(ConditionalExpression node)
        {
            return Collapse(
                Visit(node.Test),
                Visit(node.IfTrue),
                Visit(node.IfFalse));
        }

        /// <summary>
        /// Visits the <see cref="ConstantExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitConstant(ConstantExpression node)
        {
            return Collapse(default(T));
        }

        /// <summary>
        /// Visits the <see cref="DebugInfoExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitDebugInfo(DebugInfoExpression node)
        {
            return Collapse(default(T));
        }

        /// <summary>
        /// Visits the children of the <see cref="DynamicExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitDynamic(DynamicExpression node)
        {
            return Collapse(VisitArguments(node));
        }

        /// <summary>
        /// Visits the <see cref="DefaultExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitDefault(DefaultExpression node)
        {
            return Collapse(default(T));
        }

        /// <summary>
        /// Visits the children of the extension expression.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        /// <remarks>
        /// This can be overridden to visit or rewrite specific extension nodes.
        /// If it is not overridden, this method will call <see cref="Expression.VisitChildren" />,
        /// which gives the node a chance to walk its children. By default,
        /// <see cref="Expression.VisitChildren" /> will try to reduce the node.
        /// </remarks>
        [Obsolete("Do not know how to migrate!", true)]
        protected internal virtual T VisitExtension(Expression node)
        {
            throw new TechnicalException("Not handled", null);
            //return node.VisitChildren(this);
            //return Collapse(default(T));
        }

        /// <summary>
        /// Visits the children of the <see cref="GotoExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitGoto(GotoExpression node)
        {
            return Collapse(VisitLabelTarget(node.Target),
                            Visit(node.Value));
        }

        /// <summary>
        /// Visits the children of the <see cref="InvocationExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitInvocation(InvocationExpression node)
        {
            return Collapse(Visit(node.Expression).Yield()
                    .Concat(VisitArguments(node)));
        }

        /// <summary>
        /// Visits the <see cref="LabelTarget" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitLabelTarget(LabelTarget node)
        {
            return Collapse(default(T));
        }

        /// <summary>
        /// Visits the children of the <see cref="LabelExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitLabel(LabelExpression node)
        {
            return Collapse(VisitLabelTarget(node.Target),
                            Visit(node.DefaultValue));
        }

        /// <summary>
        /// Visits the children of the <see cref="Expression&lt;T2&gt;" />.
        /// </summary>
        /// <typeparam name="T2">The type of the delegate.</typeparam>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitLambda<T2>(Expression<T2> node)
        {
            return Collapse(Visit(node.Body).Yield()
                    .Concat(node.Parameters.Select(Visit))); // was VisitAndConvert(node.Parameters, "VisitLambda");
        }

        /// <summary>
        /// Visits the children of the <see cref="LoopExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitLoop(LoopExpression node)
        {
            return Collapse(
                VisitLabelTarget(node.BreakLabel),
                VisitLabelTarget(node.ContinueLabel),
                Visit(node.Body));
        }

        /// <summary>
        /// Visits the children of the <see cref="MemberExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitMember(MemberExpression node)
        {
            return Collapse(Visit(node.Expression));
        }

        /// <summary>
        /// Visits the children of the <see cref="IndexExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitIndex(IndexExpression node)
        {
            return Collapse(Visit(node.Object).Yield()
                    .Concat(VisitArguments(node)));
        }

        /// <summary>
        /// Visits the children of the <see cref="MethodCallExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitMethodCall(MethodCallExpression node)
        {
            return Collapse(Visit(node.Object).Yield()
                    .Concat(VisitArguments(node)));
        }

        /// <summary>
        /// Visits the children of the <see cref="NewArrayExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitNewArray(NewArrayExpression node)
        {
            return Collapse(Visit(node.Expressions));
        }

        /// <summary>
        /// Visits the children of the <see cref="NewExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")]
        protected internal virtual T VisitNew(NewExpression node)
        {
            return Collapse(Visit(node.Arguments));
        }

        /// <summary>
        /// Visits the <see cref="ParameterExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitParameter(ParameterExpression node)
        {
            return Collapse(default(T));
        }

        /// <summary>
        /// Visits the children of the <see cref="RuntimeVariablesExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitRuntimeVariables(RuntimeVariablesExpression node)
        {
            return Collapse(node.Variables.Select(Visit)); // was VisitAndConvert(node.Variables, "VisitRuntimeVariables");
        }

        /// <summary>
        /// Visits the children of the <see cref="SwitchCase" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitSwitchCase(SwitchCase node)
        {
            return Collapse(Visit(node.TestValues)
                    .Concat(Visit(node.Body)));
        }

        /// <summary>
        /// Visits the children of the <see cref="SwitchExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitSwitch(SwitchExpression node)
        {
            return Collapse(Visit(node.SwitchValue).Yield()
                    .Concat(node.Cases.Select(VisitSwitchCase))
                    .Concat(Visit(node.DefaultBody)));
        }

        /// <summary>
        /// Visits the children of the <see cref="CatchBlock" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitCatchBlock(CatchBlock node)
        {
            return Collapse(
                Visit(node.Variable), // was VisitAndConvert(node.Variable, "VisitCatchBlock");
                Visit(node.Filter),
                Visit(node.Body));
        }

        /// <summary>
        /// Visits the children of the <see cref="TryExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitTry(TryExpression node)
        {
            return Collapse(Visit(node.Body).Yield()
                    .Concat(node.Handlers.Select(VisitCatchBlock))
                    .Concat(Visit(node.Finally))
                    .Concat(Visit(node.Fault)));
        }

        /// <summary>
        /// Visits the children of the <see cref="TypeBinaryExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitTypeBinary(TypeBinaryExpression node)
        {
            return Collapse(Visit(node.Expression));
        }

        /// <summary>
        /// Visits the children of the <see cref="UnaryExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitUnary(UnaryExpression node)
        {
            return Collapse(Visit(node.Operand));
        }

        /// <summary>
        /// Visits the children of the <see cref="MemberInitExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitMemberInit(MemberInitExpression node)
        {
            return Collapse(Visit(node.NewExpression).Yield() // was VisitAndConvert(node.NewExpression, "VisitMemberInit");
                    .Concat(node.Bindings.Select(VisitMemberBinding)));
        }

        /// <summary>
        /// Visits the children of the <see cref="ListInitExpression" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected internal virtual T VisitListInit(ListInitExpression node)
        {
            return Collapse(Visit(node.NewExpression).Yield() // was VisitAndConvert(node.NewExpression, "VisitListInit");
                    .Concat(node.Initializers.Select(VisitElementInit)));
        }

        /// <summary>
        /// Visits the children of the <see cref="ElementInit" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitElementInit(ElementInit node)
        {
            return Collapse(Visit(node.Arguments));
        }

        /// <summary>
        /// Visits the children of the <see cref="MemberBinding" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitMemberBinding(MemberBinding node)
        {
            switch (node.BindingType)
            {
                case MemberBindingType.Assignment:
                    return Collapse(VisitMemberAssignment((MemberAssignment)node));
                case MemberBindingType.MemberBinding:
                    return Collapse(VisitMemberMemberBinding((MemberMemberBinding)node));
                case MemberBindingType.ListBinding:
                    return Collapse(VisitMemberListBinding((MemberListBinding)node));
                default:
                    throw new TechnicalException($"Unhandled Binding Type {node.BindingType}!", null);
            }
        }

        /// <summary>
        /// Visits the children of the <see cref="MemberAssignment" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitMemberAssignment(MemberAssignment node)
        {
            return Collapse(Visit(node.Expression));
        }

        /// <summary>
        /// Visits the children of the <see cref="MemberMemberBinding" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitMemberMemberBinding(MemberMemberBinding node)
        {
            return Collapse(node.Bindings.Select(VisitMemberBinding));
        }

        /// <summary>
        /// Visits the children of the <see cref="MemberListBinding" />.
        /// </summary>
        /// <param name="node">The expression to visit.</param>
        /// <returns>The modified expression, if it or any subexpression was modified;
        /// otherwise, returns the original expression.</returns>
        protected virtual T VisitMemberListBinding(MemberListBinding node)
        {
            return Collapse(node.Initializers.Select(VisitElementInit));
        }
    }

    public abstract partial class ExpressionConverterVisitor<T> : IExpressionConverterVisitor<T>
    {
        #region Originals methods are protected internal

        T IExpressionConverterVisitor<T>.VisitBinary(BinaryExpression node) { return VisitBinary(node); }
        T IExpressionConverterVisitor<T>.VisitBlock(BlockExpression node) { return VisitBlock(node); }
        T IExpressionConverterVisitor<T>.VisitConditional(ConditionalExpression node) { return VisitConditional(node); }
        T IExpressionConverterVisitor<T>.VisitConstant(ConstantExpression node) { return VisitConstant(node); }
        T IExpressionConverterVisitor<T>.VisitDebugInfo(DebugInfoExpression node) { return VisitDebugInfo(node); }
        T IExpressionConverterVisitor<T>.VisitDynamic(DynamicExpression node) { return VisitDynamic(node); }
        T IExpressionConverterVisitor<T>.VisitDefault(DefaultExpression node) { return VisitDefault(node); }
        [Obsolete("Do not know how to migrate!", true)]
        T IExpressionConverterVisitor<T>.VisitExtension(Expression node) { return VisitExtension(node); }
        T IExpressionConverterVisitor<T>.VisitGoto(GotoExpression node) { return VisitGoto(node); }
        T IExpressionConverterVisitor<T>.VisitInvocation(InvocationExpression node) { return VisitInvocation(node); }
        T IExpressionConverterVisitor<T>.VisitLabel(LabelExpression node) { return VisitLabel(node); }

        T IExpressionConverterVisitor<T>.VisitLambda<T2>(Expression<T2> node) { return VisitLambda<T2>(node); }
        T IExpressionConverterVisitor<T>.VisitLoop(LoopExpression node) { return VisitLoop(node); }
        T IExpressionConverterVisitor<T>.VisitMember(MemberExpression node) { return VisitMember(node); }
        T IExpressionConverterVisitor<T>.VisitIndex(IndexExpression node) { return VisitIndex(node); }
        T IExpressionConverterVisitor<T>.VisitMethodCall(MethodCallExpression node) { return VisitMethodCall(node); }
        T IExpressionConverterVisitor<T>.VisitNewArray(NewArrayExpression node) { return VisitNewArray(node); }
        T IExpressionConverterVisitor<T>.VisitNew(NewExpression node) { return VisitNew(node); }
        T IExpressionConverterVisitor<T>.VisitParameter(ParameterExpression node) { return VisitParameter(node); }
        T IExpressionConverterVisitor<T>.VisitRuntimeVariables(RuntimeVariablesExpression node) { return VisitRuntimeVariables(node); }
        T IExpressionConverterVisitor<T>.VisitSwitch(SwitchExpression node) { return VisitSwitch(node); }
        T IExpressionConverterVisitor<T>.VisitTry(TryExpression node) { return VisitTry(node); }
        T IExpressionConverterVisitor<T>.VisitTypeBinary(TypeBinaryExpression node) { return VisitTypeBinary(node); }
        T IExpressionConverterVisitor<T>.VisitUnary(UnaryExpression node) { return VisitUnary(node); }
        T IExpressionConverterVisitor<T>.VisitMemberInit(MemberInitExpression node) { return VisitMemberInit(node); }
        T IExpressionConverterVisitor<T>.VisitListInit(ListInitExpression node) { return VisitListInit(node); }

        #endregion Originals methods are protected internal

        #region Originals methods are protected

        T IExpressionConverterVisitor<T>.VisitLabelTarget(LabelTarget node) { return VisitLabelTarget(node); }
        T IExpressionConverterVisitor<T>.VisitSwitchCase(SwitchCase node) { return VisitSwitchCase(node); }
        T IExpressionConverterVisitor<T>.VisitCatchBlock(CatchBlock node) { return VisitCatchBlock(node); }
        T IExpressionConverterVisitor<T>.VisitElementInit(ElementInit node) { return VisitElementInit(node); }
        T IExpressionConverterVisitor<T>.VisitMemberBinding(MemberBinding node) { return VisitMemberBinding(node); }
        T IExpressionConverterVisitor<T>.VisitMemberAssignment(MemberAssignment node) { return VisitMemberAssignment(node); }
        T IExpressionConverterVisitor<T>.VisitMemberMemberBinding(MemberMemberBinding node) { return VisitMemberMemberBinding(node); }
        T IExpressionConverterVisitor<T>.VisitMemberListBinding(MemberListBinding node) { return VisitMemberListBinding(node); }

        #endregion Originals methods are protected
    }
}
