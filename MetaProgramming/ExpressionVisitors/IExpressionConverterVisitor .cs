﻿using System;

using System.Linq.Expressions;


namespace TechnicalTools.MetaProgramming.ExpressionVisitors
{
    public interface IExpressionConverterVisitor<T>
    {
        T Visit(Expression node);

        #region Originals methods are protected internal

        T VisitBinary(BinaryExpression node);
        T VisitBlock(BlockExpression node);
        T VisitConditional(ConditionalExpression node);
        T VisitConstant(ConstantExpression node);
        T VisitDebugInfo(DebugInfoExpression node);
        T VisitDynamic(DynamicExpression node);
        T VisitDefault(DefaultExpression node);
        T VisitExtension(Expression node);
        T VisitGoto(GotoExpression node);
        T VisitInvocation(InvocationExpression node);
        T VisitLabel(LabelExpression node);

        T VisitLambda<T2>(Expression<T2> node);
        T VisitLoop(LoopExpression node);
        T VisitMember(MemberExpression node);
        T VisitIndex(IndexExpression node);
        T VisitMethodCall(MethodCallExpression node);
        T VisitNewArray(NewArrayExpression node);
        T VisitNew(NewExpression node);
        T VisitParameter(ParameterExpression node);
        T VisitRuntimeVariables(RuntimeVariablesExpression node);
        T VisitSwitch(SwitchExpression node);
        T VisitTry(TryExpression node);
        T VisitTypeBinary(TypeBinaryExpression node);
        T VisitUnary(UnaryExpression node);
        T VisitMemberInit(MemberInitExpression node);
        T VisitListInit(ListInitExpression node);

        #endregion Originals methods are protected internal

        #region Originals methods are protected

        T VisitLabelTarget(LabelTarget node);
        T VisitSwitchCase(SwitchCase node);
        T VisitCatchBlock(CatchBlock node);
        T VisitElementInit(ElementInit node);
        T VisitMemberBinding(MemberBinding node);
        T VisitMemberAssignment(MemberAssignment node);
        T VisitMemberMemberBinding(MemberMemberBinding node);
        T VisitMemberListBinding(MemberListBinding node);

        #endregion Originals methods are protected
    }
}
