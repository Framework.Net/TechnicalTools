﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;


namespace TechnicalTools.MetaProgramming.ExpressionVisitors
{
    /// <summary>
    /// Convert expression (conditional expression only) to graphviz representation (optimized for dot engine)
    /// To visualize the result text, paste it in one these websites:
    /// https://edotor.net/ (recommended, less buggy and with zoom feature)
    /// http://graphviz.it/#/gallery/ctext.gv
    /// https://dreampuf.github.io/GraphvizOnline/
    /// http://www.webgraphviz.com/
    /// http://sandbox.kidstrythisathome.com/erdos/
    /// </summary>
    public class ConditionalExpressionToGraphvizVisitor : ConditionalExpressionVisitor
    {
        public string Result { get { return _sb.ToString() + Environment.NewLine + "}"; } }

        public ConditionalExpressionToGraphvizVisitor()
        {
            _sb.AppendLine(@"digraph g
{
  graph [fontsize=30 labelloc=""t"" label="""" splines=true overlap=false rankdir = ""LR""];
  ratio = auto;");
        }
        StringBuilder _sb = new StringBuilder();

        string GetName(Expression exp)
        {
            if (exp == null)
                return null;
            string name;
            if (!_nodeNames.TryGetValue(exp, out name))
            {
                name = "n" + _nodeNames.Count.ToStringInvariant();
                _nodeNames.Add(exp, name);
            }
            return name;
        }
        Dictionary<Expression, string> _nodeNames = new Dictionary<Expression, string>();

        void Dump(Expression node, string desc, IReadOnlyDictionary<string, Expression> subNodes, IReadOnlyDictionary<string, string> nodeFields = null)
        {
            nodeFields = nodeFields ?? defaultNodeFields;
            var nl = Environment.NewLine;
            var content = @"<table border=""1"" cellborder=""1"" cellpadding=""0"" cellspacing=""0"" bgcolor=""white"">"
                        + nl + @"  <tr><td bgcolor=""black"" align=""center""><font color=""white"">" + HtmlEscape(node.GetType().ToSmartString()) + "</font></td></tr>"
                        + nl + @"  <tr><td bgcolor=""grey""  align=""center""><font color=""white"">" + HtmlEscape(node.Type.ToSmartString()) + " | " + node.NodeType + @"</font></td></tr>"
                        + nl + @"  <tr><td align=""left"">" + HtmlEscape(desc) + "</td></tr>";
            if (subNodes.Count > 0)
                content += nl + @"  <tr><td align=""left"">"
                        + nl + @"    <table border=""0"" cellborder=""0"" cellpadding=""10"" cellspacing=""0"">"
                        + subNodes.Select((kvp, i) => nl + $@"      <tr><td cellpadding=""0"" border=""0"" width=""0px"">&bull;</td>"
                                                    + $@"<td align=""left"" port=""p{i.ToStringInvariant()}"">{kvp.Key}{(kvp.Value == null ? HtmlEscape(" <NULL>") : "")}</td></tr>")
                                  .Join("")
                        + nl + @"    </table>"
                        + nl + @"  </td></tr>";
            content += nl + @"</table>";

            _sb.AppendLine("  " + GetName(node) + " [" + nodeFields.Select(kvp => kvp.Key + " = \"" + kvp.Value + "\"").Join()
                                                      + " label="
                                                      + (content.StartsWith("<") ? "<" + nl + content + nl + ">" : content)
                                                + " ];");

            int n = 0;
            foreach (var subNode in subNodes.Values)
            {
                if (subNode != null)
                    _sb.AppendLine(GetName(node) + ":p" + n.ToStringInvariant() + " -> " + GetName(subNode) + " [penwidth = 1];");
                ++n;
            }
        }
        static string HtmlEscape(string str)
        {
            return str.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
        }
        static readonly IReadOnlyDictionary<string, string> defaultNodeFields = new Dictionary<string, string>()
            {
                { "style", "filled, bold" },
                { "penwidth", "5" },
                { "fillcolor", "white" },
                { "fontname", "Courier New" },
                { "shape", "none" } // Mrecord does not make "port" in label to work
            };

        public override Expression Visit(Expression node)
        {
            return base.Visit(node);
        }
        protected override Expression VisitLambda<TFunc>(Expression<TFunc> node)
        {
            var res = base.VisitLambda(node);
            var desc = "ReturnType: " + node.ReturnType.ToSmartString()
                     + "  TailCall: " + node.TailCall
                     + (string.IsNullOrWhiteSpace(node.Name) ? "" : "  Name:" + node.Name);
            var subNodes = node.Parameters.Select((p, i) => new { p, i })
                                          .ToDictionary(p => "var " + p.i, p => (Expression)p.p);
            subNodes.Add("Body", node.Body);
            Dump(node, desc, subNodes);
            return res;
        }


        protected override Expression VisitBinary(BinaryExpression node)
        {
            var res = base.VisitBinary(node);
            var desc = "CanReduce: " + node.CanReduce
                     + "  IsLifted: " + node.IsLifted
                     + "  IsLiftedToNull: " + node.IsLiftedToNull
                     + (node.Method == null ? "" : "  Method: " + node.Method.Name);
            var subNodes = new Dictionary<string, Expression>();
            subNodes.Add("Left", node.Left);
            subNodes.Add("Conversion", node.Conversion);
            subNodes.Add("Right", node.Right);
            Dump(node, desc, subNodes);
            return res;
        }

        protected override Expression VisitUnary(UnaryExpression node)
        {
            var res = base.VisitUnary(node);
            var desc = "CanReduce: " + node.CanReduce
                     + "  IsLifted: " + node.IsLifted
                     + "  IsLiftedToNull: " + node.IsLiftedToNull
                     + (node.Method == null ? "" : "  Method: " + node.Method.Name);
            var subNodes = new Dictionary<string, Expression>();
            subNodes.Add("Operand", node.Operand);
            Dump(node, desc, subNodes);
            return res;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            var res = base.VisitMember(node);
            var desc = "CanReduce: " + node.CanReduce
                     + "  Member: " + node.Member.Name;
            var subNodes = new Dictionary<string, Expression>();
            subNodes.Add("Expression", node.Expression);
            Dump(node, desc, subNodes);
            return res;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var res = base.VisitMethodCall(node);
            var desc = "CanReduce: " + node.CanReduce
                     + "  Method: " + node.Method.Name;
            var subNodes = new Dictionary<string, Expression>();
            int i = 0;
            foreach (var arg in node.Arguments)
                subNodes.Add("Arg " + ++i, arg);
            subNodes.Add("Object", node.Object);
            Dump(node, desc, subNodes);
            return res;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var res = base.VisitConstant(node);
            var desc = "CanReduce: " + node.CanReduce
                     + "  Value: " + (node.Value?.ToString() ?? "<NULL>") + (node.Value == null ? "" : "(of type " + node.Value.GetType().ToSmartString() + ")");
            var subNodes = new Dictionary<string, Expression>();
            Dump(node, desc, subNodes);
            return res;
        }

        protected override Expression VisitNewArray(NewArrayExpression node)
        {
            var res = base.VisitNewArray(node);
            var desc = "CanReduce: " + node.CanReduce;
            var subNodes = new Dictionary<string, Expression>();
            int i = 0;
            foreach (var exp in node.Expressions)
                subNodes.Add("Item " + ++i, exp);
            Dump(node, desc, subNodes);
            return res;
        }
        protected override Expression VisitParameter(ParameterExpression node)
        {
            var res = base.VisitParameter(node);
            var desc = "CanReduce: " + node.CanReduce
                     + "  Name: " + node.Name
                     + "  IsByRef: " + node.IsByRef;
            var subNodes = new Dictionary<string, Expression>();
            Dump(node, desc, subNodes);
            return res;
        }
    }
}
