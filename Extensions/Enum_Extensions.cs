﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;


namespace TechnicalTools
{
    /// <summary> EnumExtensions </summary>
    public static class Enum_Extensions
    {
        ///<summary>
        /// Allow developer to get the string related to enum value by looking for attribute System.ComponentModel.DescriptionAttribute
        /// example :
        /// [System.ComponentModel.Description("Enum value with space for example")] EnumValueWithSpaceForExample
        /// If DescriptionAttribute is not used the default ToString() method is called on enum value and returned
        /// <remarks>Note that the result of this method is undefined for alias enum value
        /// (ie: values defined in enum that share the same real integer value).</remarks>
        /// </summary>
        public static string GetDescription(this Enum value)
        {
            return GetAttachedData(value, _getDescription);
        }
        static readonly Func<DescriptionAttribute, string> _getDescription = att => att.Description;

        /// <summary>
        /// WARNING: For one TAttribute type you have to use the same lambda across all application !
        /// So this is good for private enum in your on namespace.
        /// This is why keyword "this" that make this method an extensions method has been disabled,
        /// wrap it in your extenison method
        /// </summary>
        public static string GetAttachedData<TAttribute>(/*this*/ Enum value, Func<TAttribute, string> getData)
            where TAttribute : Attribute
        {
            // To be able to use foo.bar?.GetDescription()
            // knowing that if bar is null, runtime still call extension method with null
            if (value == null)
                return null;
            var descDic = Descriptions.GetOrAdd(typeof(TAttribute), _ => new ConcurrentDictionary<Enum, string>());
            // make this faster if needed one day, see http://ayende.com/blog/3885/dictionary-enum-t-puzzler
            //   and http://www.codeproject.com/Articles/33528/Accelerating-Enum-Based-Dictionaries-with-Generic
            // it allow to divide by 8 execution time
            if (!descDic.ContainsKey(value))
            {
                Type enum_type = value.GetType();
                if (enum_type.IsDefined(typeof(FlagsAttribute), false))
                {
                    var dicValues = ValuesForFlaggedEnumTypes.GetOrAdd(typeof(TAttribute), _ => new ConcurrentDictionary<Type, IReadOnlyList<IEnumValueDescriptionPair>>());
                    // For flagged Enum, we _HAVE_TO_ initialize all the defaut value (especially the value that implemented with 0 value)
                    // The other value, which are combinations and are not defiend in enum,
                    // won't suffer of the deconstruction problem because they will be decomposed as values already treated here
                    // To test corner cases, just use System.Reflection.MethodAttributes which is a good test case!
                    foreach (var pair in dicValues.GetOrAdd(enum_type, et => EnumerateValuesAndAttachedDataOf(et, getData)))
                        if (!descDic.ContainsKey(pair.Value)) // Prevent alias in enum (we cannot handle them)
                            descDic.TryAdd(pair.Value, pair.Description);
                    // If the value has just been added by the previous loop
                    // It prevents the following code to be executed for default value
                    // Otherwise the result would be weird (especially if Convert.ToInt64(value) == 0L)
                    if (descDic.ContainsKey(value))
                        return descDic[value];

                    return descDic.GetOrAdd(value, val => val.ToString()
                                                             .Split(new[] { ", " }, StringSplitOptions.None)
                                                             .Select(v => (Enum)Enum.Parse(enum_type, v))
                                                             // Prevent infinite recursion and polluting the result if 0 value are not defiend as first in enum
                                                             // (see Implementation of ToString for enum marked with FlagsAttribute)
                                                             .Where(v => Convert.ToInt64(v) != 0L)
                                                             .Select(v => v.GetDescription())
                                                             .Join());
                }
                string default_result = value.ToString();
                FieldInfo fi = enum_type.GetField(default_result);

                if (fi == null)
                    return default_result;
                var da = (TAttribute)Attribute.GetCustomAttribute(fi, typeof(TAttribute));
                descDic.TryAdd(value, da == null ? default_result : getData(da));
            }
            return descDic[value];
        }
        // Storage is : Type of Attribute looked for => Type of enum user is interested in => name or data
        static readonly ConcurrentDictionary<Type, ConcurrentDictionary<Enum, string>> Descriptions = new ConcurrentDictionary<Type, ConcurrentDictionary<Enum, string>>();
        static readonly ConcurrentDictionary<Type, ConcurrentDictionary<Type, IReadOnlyList<IEnumValueDescriptionPair>>> ValuesForFlaggedEnumTypes = new ConcurrentDictionary<Type, ConcurrentDictionary<Type, IReadOnlyList<IEnumValueDescriptionPair>>>();

        /// <summary>Enumerate description already retrieved</summary>
        public static IReadOnlyList<IEnumValueDescriptionPair> EnumerateValuesAndDescriptionOf<TEnum>()
        {
            return EnumerateValuesAndAttachedDataOf(typeof(TEnum), _getDescription);
        }
        static IReadOnlyList<EnumValueDescriptionPair> EnumerateValuesAndAttachedDataOf<TAttribute>(Type enum_type, Func<TAttribute, string> getDescription)
            where TAttribute : Attribute
        {
            var dic = EnumValuesAndNames.GetOrAdd(typeof(TAttribute), _ => new ConcurrentDictionary<Type, List<EnumValueDescriptionPair>>());
            return dic.GetOrAdd(enum_type, et =>
            {
                // It is safer to use GetNames instead of GetValues
                // Because when calling ToString on a value we can have something different than its true name
                // Especially if there are multiple enum values defined for value 0.
                // Same if there is only one value defined to 0 but this value is not the first one declared
                // (In that case, it depends on internal implementation of enum)
                return Enum.GetNames(et).Select(name =>
                {
                    FieldInfo fi = et.GetField(name);
                    Debug.Assert(fi != null);
                    var da = (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(TAttribute));
                    var v = (Enum)fi.GetValue(null);
                    var pair = new EnumValueDescriptionPair(v, da == null ? name : getDescription((TAttribute)(Attribute)da));
                    return pair;
                }).ToList(); // cannot use ToDictionary here because of potential "alias" in enum which share the same value
            });
        }
        // Storage is : Type of Attribute looked for => Type of enum user is interested in => name or data
        static readonly ConcurrentDictionary<Type, ConcurrentDictionary<Type, List<EnumValueDescriptionPair>>> EnumValuesAndNames = new ConcurrentDictionary<Type, ConcurrentDictionary<Type, List<EnumValueDescriptionPair>>>();

        /// <summary></summary>
        public interface IEnumValueDescriptionPair
        {
            /// <summary></summary>
            Enum Value { get; }
            /// <summary></summary>
            string Description { get; }
        }

        internal class EnumValueDescriptionPair : IEnumValueDescriptionPair
        {
            public Enum Value { get; }
            public string Description { get; }

            public EnumValueDescriptionPair(Enum value, string description)
            {
                Value = value;
                Description = description;
            }
        }
    }
}
