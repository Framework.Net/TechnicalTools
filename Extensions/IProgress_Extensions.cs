﻿using System;


namespace TechnicalTools
{
    public static class IProgress_Extensions
    {
        public static IProgress<T> WrapReportWith<T>(this IProgress<T> report, Func<T, T> select)
        {
            return new ProgressWrapper<T>(report, select);
        }
        public static IProgress<string> WrapReportWith(this IProgress<string> report, string outerTaskName)
        {
            const string indent = "   ";
            report.Report(outerTaskName);
            var newReporter = new ProgressWrapper<string>(report, str => outerTaskName + Environment.NewLine +
                                                          indent + str.Replace(Environment.NewLine, Environment.NewLine + indent));
            return newReporter;
        }

        class ProgressWrapper<T> : IProgress<T>
        {
            readonly IProgress<T> _wrapped;
            public Func<T, T> _wrap;

            public ProgressWrapper(IProgress<T> wrapped, Func<T, T> wrap)
            {
                _wrapped = wrapped;
                _wrap = wrap;
            }

            public void Report(T value)
            {
                _wrapped.Report(_wrap(value));
            }
        }
    }
    public static class ProgressEx<T>
    {
        public static Progress<T> Default { get; set; } = new Progress<T>();
    }
}