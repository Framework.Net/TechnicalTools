﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;


namespace TechnicalTools
{
    public static class Path_Extensions
    {

        // Return the path as a relative path against againstThisPath
        // Or a full path if drive letter has nothing in common
        // exemple :
        // GetRelativePathTo(@"C:\a\",     @"C:\a\b\c") => "b\c"
        // GetRelativePathTo(@"C:\a",      @"C:\aa\b\c") => "..\aa\b\c" // string.StartWith is not enough here
        // GetRelativePathTo(@"C:\a\\b",   @"C:\a\bb\\c") => "..\bb\c" // Not the errors with two '\', the result fix it
        // GetRelativePathTo(@"C:\foo",    @"D:\bar\") => "D:\bar"
        // GetRelativePathTo(@"C:\a\b\..", @"C:\a\b\c\d\..\e") => "b\c\e"
        // GetRelativePathTo(@"C:\a\b\..", @"\d\e") => "..\d\e"
        // GetRelativePathTo(@"C:\a\b\..", @"..\e") => "..\e"
        public static string GetRelativePathTo(this string againstThisPath, string targetPath)
        {
            againstThisPath = Path.GetFullPath(againstThisPath);
            if (targetPath.StartsWith(@"\"))
                targetPath = againstThisPath.Remove(2) + targetPath;
            else if (targetPath.Length < 2 || targetPath[1] != ':')
                targetPath = Path.Combine(againstThisPath, targetPath);
            targetPath = Path.GetFullPath(targetPath);

            var parts1 = againstThisPath.Split(new[] { Path.DirectorySeparatorChar }).NotBlank().ToArray();
            var parts2 = targetPath.Split(new[] { Path.DirectorySeparatorChar }).NotBlank().ToArray();

            if (parts1[0].Length >= 2 && parts1[0][1] == ':' &&
                parts2[0].Length >= 2 && parts2[0][1] == ':' &&
                parts1[0] != parts2[0])
                return parts2.Join("" + Path.DirectorySeparatorChar);

            var commonCount = parts1.TakeWhile((f, i) => i < parts2.Length && f == parts2[i]).Count();
            return Enumerable.Range(0, parts1.Length - commonCount)
                             .Select(_ => "..")
                             .Concat(parts2.Skip(commonCount))
                             .Join("" + Path.DirectorySeparatorChar);
        }

        public static Process OpenFile(string filePath)
        {
            try
            {
                var path = Path.GetFullPath(filePath);
                var psi = new ProcessStartInfo
                {
                    FileName = path,
                    WorkingDirectory = Path.GetDirectoryName(path) ?? Path.GetPathRoot(path)
                };
                return Process.Start(psi);
            }
            catch (Exception ex) when (ex.EnrichDiagnosticWithAndReturnFalse($"Error while opening exported file \"{filePath}\" !", eExceptionEnrichmentType.UserUnderstandable))
            {
                throw;
            }
        }
    }
}
