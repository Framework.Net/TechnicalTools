﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;


namespace TechnicalTools
{
    public static class XElement_Extensions
    {
        public static IEnumerable<XElement> EnumerateAllSubElements(this XElement parent)
        {
            yield return parent;
            foreach (var node in parent.Elements())
                foreach (var subnode in node.EnumerateAllSubElements())
                    yield return subnode;
        }

        public static string GetText(this XElement node, string separator = " ")
        {
            return string.Join(separator, node.Nodes()
                                              .OfType<XText>()
                                              .Select(txt => txt.Value)
                                              .Where(str => !string.IsNullOrWhiteSpace(str)));
        }

        public static string PathOf(this XElement node, XElement root = null)
        {
            if (node == null)
                return null;
            if (node == (root ?? node.Document.Root))
                return (root ?? node.Document.Root).Name.LocalName;
            var path = PathOf(node.Parent, root);
            return path + (path == null ? "" : "/") + node.Name.LocalName;
        }

        /// <summary>
        /// Get the index of the given XElement relative to its
        /// siblings with identical names. If the given element has no parent (-1 is returned).
        /// </summary>
        /// <param name="element">
        /// The element to get the index of.
        /// </param>
        public static int IndexForThisName(this XElement element)
        {
            if (element == null)
                throw new ArgumentNullException("element");

            if (element.Parent == null)
                return -1;

            int i = 0;
            foreach (var sibling in element.Parent.Elements(element.Name))
            {
                if (sibling == element)
                    return i;
                i++;
            }

            throw new InvalidOperationException("element has been removed from its parent.");
        }

    }
}
