﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;


namespace TechnicalTools
{
    public static partial class Exception_Extensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [DebuggerHidden, DebuggerStepThrough]
        public static void ThrowIfNotNull(this Exception ex)
        {
            if (ex != null)
                throw ex;
        }

        /// <summary>
        /// This method just rethrow an exception without reseting stacktrace !
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        public static Exception Rethrow(this Exception ex)
        {
            ExceptionDispatchInfo.Capture(ex).Throw();
            Debug.Fail("Unreachable code reached");
            // Just to make compiler happy and user code to use throw keyword again
            // to make compiler happy again
            return ex;
        }

        public static long GetUniqueBugId(this Exception ex)
        {
            if (!ex.Data.Contains(UniqueBugIdKey))
                ex.Data[UniqueBugIdKey] = DateTime.UtcNow.Ticks;
            return (long)ex.Data[UniqueBugIdKey];
        }
        const string UniqueBugIdKey = "UniqueBugId";


        public static bool IsFileAlreadyOpened(this Exception ex)
        {
            return ex is IOException &&
                   ((ex.HResult & 0xFFFF) == SystemErrorCode.ERROR_DISK_FULL ||
                    (ex.HResult & 0xFFFF) == SystemErrorCode.ERROR_HANDLE_DISK_FULL ||
                    (ex.HResult & 0xFFFF) == SystemErrorCode.ERROR_SHARING_VIOLATION);
        }


        public static Exception SetStackTrace(this Exception target, StackTrace stack)
        {
            var getStackTraceString = TRACE_TO_STRING_MI.Invoke(stack, new [] { Enum.GetValues(TRACE_FORMAT_TI).GetValue(0) });
            STACK_TRACE_STRING_FI.SetValue(target, getStackTraceString);
            return target;
        }

        static readonly FieldInfo STACK_TRACE_STRING_FI = typeof(Exception).GetField("_stackTraceString", BindingFlags.NonPublic | BindingFlags.Instance);
        static readonly Type TRACE_FORMAT_TI = typeof(StackTrace).GetNestedType("TraceFormat", BindingFlags.NonPublic);
        static readonly MethodInfo TRACE_TO_STRING_MI = typeof(StackTrace).GetMethod("ToString", BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { TRACE_FORMAT_TI }, null);
    }

    // Valeurs identifiable dans (Exception.HResult & 0xFFFF)
    //
    // Exception.HResult est public depuis le framework 4.5
    // Avant il fallait utiliser la methode System.Runtime.InteropServices.Marshal.GetHRForException(ex)
    // qui a aussi pour effect secondaire de setter IErrorInfo du thread courant.
    // This can cause unexpected results for methods like the ThrowExceptionForHR methods that
    // default to using the IErrorInfo of the current thread if it is set.
    public static class SystemErrorCode
    {
        // From https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx

        [Description("The process cannot access the file because it is being used by another process.")]
        public static readonly int ERROR_SHARING_VIOLATION = 32; // == 0x20
        [Description("The process cannot access the file because another process has locked a portion of the file.")]
        public static readonly int ERROR_LOCK_VIOLATION = 33; // == 0x21

        [Description("There is not enough space on the disk.")]
        public static readonly int ERROR_DISK_FULL = 112; // == 0x70 // There is not enough space on the disk.
        [Description("The disk is full.")]
        public static readonly int ERROR_HANDLE_DISK_FULL = 39; // == 0x27
    }
}
