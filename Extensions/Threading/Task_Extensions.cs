﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TechnicalTools
{
    /// <summary>
    /// These extensions are made to solve this problem:
    /// When WinForm call GetFormatedData().Result, how to guarantee the code won't deadlock ?
    /// Currently, because of the ConfigureAwait(false), the DoSomething(t) part canot be scheudle on winform UI thread
    /// But ConfigureAwait is not directly  useable on task instance because it does not return a Task object
    /// <code>
    /// // (How to?) Turn this code:
    /// void async Task GetFormatedData()
    /// {
    ///   Task t = GetDatasourceFromElseWhere();
    ///   await t.ConfigureAwait(false);
    ///   return DoSomething(t);
    /// }
    /// // To an equivalent without async / await
    /// void Task GetFormatedData()
    /// {
    ///   return  GetDatasourceFromElseWhere().ContinueWithAwaitFalse(DoSomething);
    /// }
    /// </code>
    /// <remarks>With the help of https://stackoverflow.com/a/44136947/294998</remarks>
    /// </summary>
    public static class Task_Extensions
    {
        /// <summary>
        /// <example>
        /// In an async method :
        ///   Task t = ...;
        ///   await t.ConfigureAwait(false);
        ///   return DoSomething(t);
        /// Equivalent without async/await word:
        ///   Task t = ...;
        ///   return t.ContinueWithAwaitFalse(DoSomething);
        /// </example>
        /// </summary>
        public static Task ContinueWithAwaitFalse<TResult>(this Task<TResult> t, Action<Task<TResult>> callback)
        {
            var continuation = t.ContinueWith(callback, CancellationToken.None,
                                              TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.DenyChildAttach,
                                              TaskScheduler.Default);
            return continuation;
        }
        /// <summary> See doc in overload that take Action&lt;Task&lt;T&gt;&gt; </summary>
        public static Task ContinueWithAwaitFalse(this Task t, Action<Task> callback)
        {
            var continuation = t.ContinueWith(callback, CancellationToken.None,
                                              TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.DenyChildAttach,
                                              TaskScheduler.Default);
            return continuation;
        }
        /// <summary> See doc in overload that take Action&lt;Task&lt;T&gt;&gt; </summary>
        public static Task<TResult2> ContinueWithAwaitFalse<TResult, TResult2>(this Task<TResult> t, Func<Task<TResult>, TResult2> callback)
        {
            var continuation = t.ContinueWith(callback, CancellationToken.None,
                                              TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.DenyChildAttach,
                                              TaskScheduler.Default);
            return continuation;
        }
        /// <summary> See doc in overload that take Action&lt;Task&lt;T&gt;&gt; </summary>
        public static Task<TResult2> ContinueWithAwaitFalse<TResult2>(this Task t, Func<Task, TResult2> callback)
        {
            var continuation = t.ContinueWith(callback, CancellationToken.None,
                                              TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.DenyChildAttach,
                                              TaskScheduler.Default);
            return continuation;
        }
    }
}
