﻿using System;
using System.Linq;

using TechnicalTools.Model;


namespace TechnicalTools
{
    public static class LookUp_Extensions
    {
        public static LookupAsLookup<TKey, TValue> AsLookUp<TKey, TValue>(this Lookup<TKey, TValue> lu)
        {
            return new LookupAsLookup<TKey, TValue>(lu);
        }
    }

}
