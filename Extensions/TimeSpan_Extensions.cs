﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace TechnicalTools
{
    public static partial class TimeSpan_Extensions
    {
        /// <summary>
        /// Convertit une durée (TimeSpan) en chaine de caractere au format "##y ##mo ##d ##h ##m ##s ###ms"
        /// Les unites vide ne sont pas affiché si les nombre sont 0
        /// </summary>
        /// <param name="ts">La durée à convertir</param>
        /// <param name="default_unit">Si ts = TimeSpan.Zero, et que default_unit est specifié, la chaine renvoyé est "0" suivi de l'unite.
        ///                            Si ts = TimeSpan.Zero, et que default_unit n'est pas specifié, la chaine videest renvoyé.</param>
        public static string ToHumanReadableShortNotation(this TimeSpan ts, string default_unit = null)
        {
            return ToHumanReadableShortNotation((TimeSpan?)ts, default_unit);
        }

        public static string ToHumanReadableShortNotation(this TimeSpan? ts, string default_unit = null)
        {
            if (!ts.HasValue)
                return null;
            int days = ts.Value.Days;

            string res = "";
            int y = days / 365;
            if (y > 0)
            {
                res += y + "y";
                days %= 365;
            }
            int m = days / 30;
            if (m > 0)
            {
                if (res.Length > 0) res += " ";
                res += m + "mo";
                days %= 30;
            }
            if (days > 0)
            {
                if (res.Length > 0) res += " ";
                res += days + "d";
            }

            if (ts.Value.Hours > 0)
            {
                if (res.Length > 0) res += " ";
                res += ts.Value.Hours + "h";
            }
            if (ts.Value.Minutes > 0)
            {
                bool only_hour_min = res.Contains("h") && ts.Value.Add(new TimeSpan(0, -(int)ts.Value.TotalMinutes, 0)).Ticks == 0;
                if (res.Length > 0 && !only_hour_min) res += " ";
                res += ts.Value.Minutes + (only_hour_min ? "" : "m");
            }
            if (ts.Value.Seconds > 0)
            {
                if (res.Length > 0) res += " ";
                res += ts.Value.Seconds + "s";
            }
            if (ts.Value.Milliseconds > 0)
            {
                if (res.Length > 0) res += " ";
                res += ts.Value.Milliseconds + "ms";
            }
            if (ts.Value.Ticks % 10000 > 0)
            {
                if (res.Length > 0) res += " ";
                res += ((double)ts.Value.Ticks % 10000 / 10) + "µs";
            }
            // pour au moins afficher "0s" car la chaine vide est interprété comme null
            if (res.Length == 0)
                res += "0" + (default_unit ?? "s");
            return res;
        }

        public static TimeSpan Sum(this IEnumerable<TimeSpan> source)
        {
            return source.Aggregate(TimeSpan.Zero, (t1, t2) => t1 + t2);
        }


        public static TimeSpan Min(this TimeSpan ts1, TimeSpan ts2)
        {
            return new TimeSpan(Math.Min(ts1.Ticks, ts2.Ticks));
        }
        public static TimeSpan Max(this TimeSpan ts1, TimeSpan ts2)
        {
            return new TimeSpan(Math.Max(ts1.Ticks, ts2.Ticks));
        }

        public static TimeSpan AddHours(this TimeSpan ts, int hours)
        {
            return ts + new TimeSpan(hours, 0, 0);
        }
        public static TimeSpan AddMinutes(this TimeSpan ts, int minutes)
        {
            return ts + new TimeSpan(0, minutes, 0);
        }
        public static TimeSpan AddSeconds(this TimeSpan ts, int seconds)
        {
            return ts + new TimeSpan(0, 0, seconds);
        }
        public static TimeSpan AddMilliSeconds(this TimeSpan ts, int milliseconds)
        {
            return ts + new TimeSpan(0, 0, 0, 0, milliseconds);
        }
    }


    public static partial class TimeSpan_Extensions
    {
        /// <summary>
        /// Convertit une durée en chaine de caractere
        /// </summary>
        /// <param name="span">La durée a convertir</param>
        /// <param name="max_details">Niveau de details requis.
        ///     Si par exemple la dure contient des heure il n'est peut etre pas pertinent d'afficher les milli-secondes,
        ///     max_details permet d'indiquer combien d'unité doivent être affiché en dessous de l'unité la plus grande.</param>
        /// <returns></returns>
        public static string ToReadableString(this TimeSpan span, int max_details = 3)
        {
            string res = "";
            if (span.Ticks < 0)
                res += "-";
            int detail = 0; // 1 = ms , 2 = s, 3 = m  etc

            do
            {
                int tmp = System.Math.Abs(span.Days / 30);
                if (tmp != 0)
                {
                    res += tmp.ToString() + "month" + (tmp > 1 ? "s" : "") + " ";
                    detail = System.Math.Max(detail, 6);
                }

                tmp = System.Math.Abs(span.Days);
                if (tmp != 0)
                {
                    res += tmp.ToString() + "d ";
                    detail = System.Math.Max(detail, 5);
                }

                if (detail >= 4 + max_details) break;
                tmp = System.Math.Abs(span.Hours);
                if (tmp != 0)
                {
                    res += tmp.ToString() + "h ";
                    detail = System.Math.Max(detail, 4);
                }

                if (detail >= 3 + max_details) break;
                tmp = System.Math.Abs(span.Minutes);
                if (tmp != 0)
                {
                    res += tmp.ToString() + "m ";
                    detail = System.Math.Max(detail, 3);
                }

                if (detail >= 2 + max_details) break;
                tmp = System.Math.Abs(span.Seconds);
                if (tmp != 0)
                {
                    int tmp2 = System.Math.Abs(span.Milliseconds);
                    res += tmp.ToString()
                        + (tmp2 != 0 && max_details >= 5 - 1 ? "." + tmp2.ToString("000") : "") + "s ";
                    detail = System.Math.Max(detail, 2);
                }

                if (detail >= 1 + max_details) break;
                tmp = System.Math.Abs(span.Milliseconds);
                if (tmp != 0)
                {
                    res += tmp.ToString() + "ms ";
                    detail = System.Math.Max(detail, 1);
                }
            }
            while (false);
            return res.Length > 0 ? res.Remove(res.Length - 1) : res;
        }

        // Cette methode est l'inverse de ToTimeSpanFromShortNOtation
        public static string ToShortNotationForLongTimeSpan(this TimeSpan? ts, string default_unit = null)
        {
            if (!ts.HasValue)
                return null;
            int days = ts.Value.Days;

            string res = "";
            int y = days / 365;
            if (y > 0)
            {
                res += y + "y";
                days %= 365;
            }
            int m = days / 30;
            if (m > 0)
            {
                if (res.Length > 0) res += " ";
                res += m + "mo";
                days %= 30;
            }
            if (days > 0)
            {
                if (res.Length > 0) res += " ";
                res += days + "j";
            }

            if (ts.Value.Hours > 0)
            {
                if (res.Length > 0) res += " ";
                res += ts.Value.Hours + "h";
            }
            if (ts.Value.Minutes > 0)
            {
                bool only_hour_min = res.Contains("h") && ts.Value.Add(new TimeSpan(0, -(int)ts.Value.TotalMinutes, 0)).Ticks == 0;
                if (res.Length > 0 && !only_hour_min) res += " ";
                res += ts.Value.Minutes + (only_hour_min ? "" : "m");
            }
            if (ts.Value.Seconds > 0)
            {
                if (res.Length > 0) res += " ";
                res += ts.Value.Seconds + "s";
            }
            if (ts.Value.Milliseconds > 0)
            {
                if (res.Length > 0) res += " ";
                res += ts.Value.Milliseconds + "ms";
            }

            // pour au moins afficher "0s" car la chaine vide est interprété comme null
            if (res.Length == 0)
                res += "0" + (default_unit ?? "s");
            return res;
        }
        // L'inverse de cette methode est ToTimeSpanFromShortNotation
        public static string ToShortNotationForLongTimeSpan(this TimeSpan ts, string default_unit = null)
        {
            return ToShortNotationForLongTimeSpan((TimeSpan?)ts, default_unit);
        }

        // Cette methode est l'inverse de ToShortNotationForLongTimeSpan
        public static bool ToTimeSpanFromShortNotation(this string str_ts, out TimeSpan? parsed_value, string default_unit = null)
        {
            parsed_value = null;
            if (str_ts == null || str_ts.Trim().Length == 0)
                return true;
            if (str_ts.Trim().Replace("0", "").Length == 0)
            {
                parsed_value = TimeSpan.Zero;
                return true;
            }
            Match m = reTime.Match(str_ts);
            if (!m.Success || m.Value != str_ts)
            {
                if (string.IsNullOrEmpty(default_unit))
                    return false;
                // On reessaye avec l'unite par defaut.
                // On ajoute pas l'unite par defaut directement a str_ts avant le premier test car si l'utilisateur saisie "10j 10",
                // on doit parser ça comme 10 jours et 10 heures et non pas 10 jours et 10 minutes si default_unit = "m"
                m = reTime.Match(str_ts + default_unit);
                if (!m.Success || m.Value != str_ts + default_unit)
                    return false;
            }

            // We always need at least one character in order to the regexp be anchored correctly
            // Exemple : if user enter "1 00" instead of "1h00" the value would be parsed as "1 year 00 month"
            if (!str_ts.Any(c => c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z') && string.IsNullOrEmpty(default_unit))
                return false;

            int days = 0;
            if (m.Groups["years"].Success)
                days += 365 * int.Parse(m.Groups["years"].Value);
            if (m.Groups["months"].Success)
                days += 30 * int.Parse(m.Groups["months"].Value);
            if (m.Groups["days"].Success)
                days += int.Parse(m.Groups["days"].Value);
            parsed_value = new TimeSpan(days,
                                       m.Groups["hours"].Success ? int.Parse(m.Groups["hours"].Value) : 0,
                                       m.Groups["minutes"].Success ? int.Parse(m.Groups["minutes"].Value) : 0,
                                       m.Groups["seconds"].Success ? int.Parse(m.Groups["seconds"].Value) : 0,
                                       m.Groups["ms"].Success ? int.Parse(m.Groups["ms"].Value) : 0);
            return true;
        }
        static readonly Regex reTime = new Regex("^(" +
                                                 " *((?<years>[0-9]+) *(y|years?|an?|ann[eé]e?s?))?" +
                                                 " *((?<months>[0-9]+) *(M|mo|mois))?" +
                                                 " *((?<days>[0-9]+) *(j|j|jours?))?" +
                                                 " *((?<hours>[0-9]+) *(h|hours?|heures?))?" +
                                                 " *((?<minutes>[0-9]+) *(m|minutes?))?" +
                                                 " *((?<seconds>[0-9]+) *(s|seconde?s?))?" +
                                                 " *((?<ms>[0-9]+) *(ms|milliseconde?s?))?" +
                                                 "|" +
                                                 " *((?<hours>[0-9]+) *(h|hours?|heures?))?" +
                                                 " *(?<minutes>[0-9]+)?" +
                                                 ") *$", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.ExplicitCapture);

    }
}
