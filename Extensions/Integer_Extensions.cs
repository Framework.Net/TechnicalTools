﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnicalTools
{
    public static class Integer_Extensions
    {
        public static ushort ToGrayCode(this ushort num)
        {
            return (ushort)((num >> 1) ^ num);
        }

        public static ushort FromGrayCode(this ushort num)
        {
            uint temp = (uint)(num ^ (num >> 8));
            temp ^= (temp >> 4);
            temp ^= (temp >> 2);
            temp ^= (temp >> 1);
            return (ushort)temp;
        }
        public static uint ToGrayCode(this uint num)
        {
            return (num >> 1) ^ num;
        }

        public static uint FromGrayCode(this uint num)
        {
            uint temp = num ^ (num >> 16);
            temp ^= temp >> 8;
            temp ^= temp >> 4;
            temp ^= temp >> 2;
            temp ^= temp >> 1;
            return temp;
        }

        public static ulong ToGrayCode(this ulong num)
        {
            return (uint)((num >> 1) ^ num);
        }

        public static ulong FromGrayCode(this ulong num)
        {
            ulong temp = num ^ (num >> 32);
            temp ^= temp >> 16;
            temp ^= temp >> 8;
            temp ^= temp >> 4;
            temp ^= temp >> 2;
            temp ^= temp >> 1;
            return temp;
        }

        public static string ToRoman(this uint n)
        {
            // Code inspired from https://stackoverflow.com/a/4986630
            var sb = new StringBuilder();
            string[] huns = new[] { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
            string[] tens = new[] { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
            string[] ones = new[] { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };

            //  Add 'M' until we drop below 1000.
            while (n >= 1000)
            {
                sb.Append('M');
                n -= 1000;
            }

            // Add each of the correct elements, adjusting as we go.
            sb.Append(huns[n / 100]); n = n % 100;
            sb.Append(tens[n / 10]); n = n % 10;
            sb.Append(ones[n]);
            return sb.ToString();
        }
    }
}
