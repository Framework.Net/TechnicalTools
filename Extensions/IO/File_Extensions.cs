﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Security.Cryptography;

namespace TechnicalTools
{
    public static class File_Extensions
    {
        // Fonction particulierement utile pour lire un fichier deja ouvert par Excel ou OpenOffice
        public static IEnumerable<string> ReadLinesFromFileAlreadyOpen(string filename, Encoding encoding = null)
        {
            using (var stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                foreach (var line in ReadLinesFromStream(stream, encoding))
                    yield return line;
        }
        public static IEnumerable<string> ReadLinesFromStream(Stream stream, Encoding encoding = null)
        {
            using (var reader = new StreamReader(stream, encoding ?? Encoding.UTF8))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    yield return line;
            }
        }

        public static void BackupFileAndReplaceBy(string originalFileToReplace, string newTmpFile, uint maxVersionOfFile = 10)
        {
            if (File.Exists(originalFileToReplace + ".bak"))
                BackupOrDelete(originalFileToReplace, ".bak", maxVersionOfFile);
            File.Move(originalFileToReplace, originalFileToReplace + ".bak");
            File.Move(newTmpFile, originalFileToReplace);
        }
        static void BackupOrDelete(string file, string bakExt, uint maxVersionOfFile, uint? i = null)
        {
            // Note : when i is null i.ToString() == "";
            Debug.Assert(maxVersionOfFile > 0);
            if (i >= maxVersionOfFile) // too many recursive call
                File.Delete(file + bakExt + i.ToString());
            else
            {
                if (File.Exists(file + bakExt + ((i ?? 0) + 1))) // file to backup exit
                    BackupOrDelete(file, bakExt, maxVersionOfFile, (i ?? 0) + 1); // we save it too !
                File.Move(file + bakExt + i, file + bakExt + ((i ?? 0) + 1));
            }
        }

        public static string GetMD5HashFromFile(string filename)
        {
            using (var md5 = MD5.Create()) // MD5
            using (var stream = File.OpenRead(filename))
            {
                var bs = md5.ComputeHash(stream);
                var sb = new StringBuilder(bs.Length * 2);
                foreach (byte b in bs)
                    sb.Append(b.ToString("x2"));
                return sb.ToString();
            }
        }
    }
}
