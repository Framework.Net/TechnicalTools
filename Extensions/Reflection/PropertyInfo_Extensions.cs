﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

using TechnicalTools.Extensions.Data;


namespace TechnicalTools
{
    public static class PropertyInfo_Extensions
    {
        public static bool IsStatic(this PropertyInfo pi)
        {
            return pi.GetGetMethod() != null && pi.GetGetMethod().IsStatic ||
                   pi.GetSetMethod() != null && pi.GetSetMethod().IsStatic;
        }

        public static bool IsOverride(this PropertyInfo pi)
        {
            var mget = pi.GetGetMethod(true);
            var mset = pi.GetSetMethod(true);

            return mget != null && mget.IsOverride() ||
                   mset != null && mset.IsOverride();
        }

        public static bool IsAutoProperty(this PropertyInfo prop)
        {
            if (!prop.CanWrite || !prop.CanRead)
                return false;

            return prop.DeclaringType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                                     .Any(f => f.Name.Contains("<" + prop.Name + ">"));
        }

        public static bool IsBrowsable(this PropertyInfo prop)
        {
            if (!prop.CanRead)
                return false;

            return prop.GetCustomAttribute<BrowsableAttribute>()?.Browsable ?? true;
        }

        public static bool IsTechnicalProperty(this PropertyInfo prop)
        {
            if (!prop.CanRead)
                return false;

            return prop.GetCustomAttribute<IsTechnicalPropertyAttribute>() != null;
        }
        public static bool IsTechnicalPropertyHidden(this PropertyInfo prop)
        {
            if (!prop.CanRead)
                return false;
            //return prop.GetCustomAttributes().OfType<IsTechnicalPropertyAttribute>().FirstOrDefault()?.HideFromUser ?? false;
            return prop.GetCustomAttribute<IsTechnicalPropertyAttribute>()?.HideFromUser ?? false;
        }


        //MethodInfo setMethod = property.GetSetMethod();
        //if (setMethod != null && setMethod.GetParameters().Length == 1)
        //{
        //    var target = Expression.Parameter(typeof(T));
        //    var value = Expression.Parameter(typeof(object));
        //    var body = Expression.Call(target, setMethod, Expression.Convert(value, property.PropertyType));
        //    return Expression.Lambda<Action<T, object>>(body, target, value)
        //        .Compile();
        //}
        //else
        //{
        //    return null;
        //}

        /// <summary>
        /// Cree un délégué optimisé pour setter une valeur dans une propriété (environ 15 fois plus rapide que GetValue ou SetValue sur une PropertyInfo).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        public static Action<T, object> MakeSetterDelegate<T>(PropertyInfo property)
        {
            // From http://stackoverflow.com/questions/4085798/creating-an-performant-open-delegate-for-an-property-setter-or-getter

            MethodInfo setMethod = property.GetSetMethod();

            // On test length = 1 car en VB il existe des propriétés indexées !
            if (setMethod == null || setMethod.GetParameters().Length != 1)
                return null;

            var untyped = (Setter<T>) Activator.CreateInstance(typeof (Setter<,>).MakeGenericType(typeof (T), property.PropertyType),
                                                                setMethod);
            return untyped.Set;
        }

    }

    abstract class Setter<T>
    {
        public abstract void Set(T obj, object value);
    }

    class Setter<TTarget, TValue> : Setter<TTarget>
    {
        readonly Action<TTarget, TValue> _del;

        public Setter(MethodInfo method)
        {
            _del = (Action<TTarget, TValue>)
                   Delegate.CreateDelegate(typeof (Action<TTarget, TValue>), method);
        }

        public override void Set(TTarget obj, object value)
        {
            _del(obj, (TValue) value);
        }
    }
}
