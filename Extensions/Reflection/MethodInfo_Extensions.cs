﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;


namespace TechnicalTools
{
    public static class MethodInfo_Extensions
    {
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static object InvokeAndCleanException(this MethodInfo m, object obj, object[] parameters)
        {
            try
            {
                return m.Invoke(obj, parameters);
            }
            catch (TargetInvocationException ex)
            {
                throw ex.InnerException.Rethrow();
            }
        }
    }
}
