﻿using System;


namespace TechnicalTools
{
    public static class Decimal_Extensions
    {
        // Repond à ce besoin : http://stackoverflow.com/a/7983330/294998
        // Basiquement la precision est inclue dans un decimal. Une valeur SQL numeric(14,4)
        // sera donc affiché par defaut avec 4 decimales apres la virgule (meme si les decimales sont à zéro)
        // Cette extension permet de supprimer la précision interne a decimal sans modifier la valeur.
        // Quand une bibliotheque externe ou le debugguer affiche les données, elles sont donc "correctement" affichées.
        public static decimal Normalize(this decimal value)
        {
            return value / 1.000000000000000000000000000000000m; // 33 zeros
        }

        // from https://stackoverflow.com/a/42265036/294998
        public static int GetEncodedSignificativeDigitsAfterDecimalPoint(this decimal value)
        {
            int[] bits = decimal.GetBits(value);

            int count = bits[3] >> 16 & 255; // 4        or byte count = (byte)(bits[3] >> 16);
            return count;
        }
        // from https://stackoverflow.com/a/42265036/294998
        public static int GetNeededSignificativeDigitsAfterDecimalPoint(this decimal value)
        {
            decimal n = value / 1.000000000000000000000000000000m;  // -123.4567m
            int[] bits = decimal.GetBits(n);
            int count = bits[3] >> 16 & 255; // 4        or byte count = (byte)(bits[3] >> 16);
            return count;
        }
    }
}
