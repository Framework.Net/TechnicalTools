﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TechnicalTools
{
    public static class DateTime_Extensions
    {
        public static DateTime NotSoonerThan(this DateTime dt, DateTime minDtAllowed)
        {
            return dt < minDtAllowed ? minDtAllowed : dt;
        }
        public static DateTime NotLaterThan(this DateTime dt, DateTime maxDtAllowed)
        {
            return dt > maxDtAllowed ? maxDtAllowed : dt;
        }

        public static DateTime? MinOrNull(DateTime? a, DateTime? b)
        {
            if (a.HasValue)
                if (b.HasValue)
                    return new DateTime(Math.Min(a.Value.Ticks, b.Value.Ticks));
                else
                    return a;
            else
                return b;
        }
        public static DateTime? NullOrMin(DateTime? a, DateTime? b)
        {
            if (a.HasValue && b.HasValue)
                return new DateTime(Math.Min(a.Value.Ticks, b.Value.Ticks));
            return null;
        }
        public static DateTime? MaxOrNull(DateTime? a, DateTime? b)
        {
            if (a.HasValue)
                if (b.HasValue)
                    return new DateTime(Math.Max(a.Value.Ticks, b.Value.Ticks));
                else
                    return a;
            else
                return b;
        }
        public static DateTime? NullOrMax(DateTime? a, DateTime? b)
        {
            if (a.HasValue && b.HasValue)
                return new DateTime(Math.Max(a.Value.Ticks, b.Value.Ticks));
            return null;
        }

        public static IEnumerable<DateTime> EnumerateEachDayTo(this DateTime from, DateTime toIncluded)
        {
            Diagnostics.DebugTools.Assert(from.Date == from);
            Diagnostics.DebugTools.Assert(toIncluded.Date == toIncluded);

            // just in case
            from = from.Date;
            toIncluded = toIncluded.Date;

            while (from <= toIncluded)
            {
                yield return from;
                from = from.AddDays(1);
            }
        }

        public static DateTime RoundUp(this DateTime dt, TimeSpan atomicValue)
        {
            return new DateTime(((dt.Ticks + atomicValue.Ticks - 1) / atomicValue.Ticks) * atomicValue.Ticks);
        }
        public static DateTime RoundDown(this DateTime dt, TimeSpan atomicValue)
        {
            return new DateTime(dt.Ticks - dt.Ticks % atomicValue.Ticks);
        }
        public static DateTime Round(this DateTime dt, TimeSpan atomicValue)
        {
            return new DateTime((dt.Ticks + atomicValue.Ticks / 2) / atomicValue.Ticks * atomicValue.Ticks);
        }


        public static DateTime AddBusinessDays(this DateTime dt, int days, HashSet<DateTime> holidays = null)
        {
            var res = dt;
            while (days > 0)
            {
                res = res.AddDays(1);
                while (res.DayOfWeek == DayOfWeek.Sunday ||
                       res.DayOfWeek == DayOfWeek.Saturday ||
                       (holidays?.Contains(res) ?? false))
                        res = res.AddDays(1);
                --days;
            }
            while (days < 0)
            {
                res = res.AddDays(-1);
                while (res.DayOfWeek == DayOfWeek.Sunday ||
                       res.DayOfWeek == DayOfWeek.Saturday ||
                       (holidays?.Contains(res) ?? false))
                    res = res.AddDays(-1);
                ++days;
            }
            return res;
        }

        public static DateTime AddWorkingDays(this DateTime dt, int days, HashSet<DateTime> holidays = null)
        {
            var res = dt;
            while (days > 0)
            {
                res = res.AddDays(1);
                while (res.DayOfWeek == DayOfWeek.Sunday ||
                       (holidays?.Contains(res) ?? false))
                    res = res.AddDays(1);
                --days;
            }
            while (days < 0)
            {
                res = res.AddDays(-1);
                while (res.DayOfWeek == DayOfWeek.Sunday ||
                       (holidays?.Contains(res) ?? false))
                    res = res.AddDays(-1);
                ++days;
            }
            return res;
        }

        public static DateTime TruncateUnderSeconds(this DateTime dt)
        {
            return dt.AddTicks(-(dt.Ticks % TimeSpan.TicksPerSecond));
        }
        public static DateTime TruncateUnderMilliSeconds(this DateTime dt)
        {
            return dt.AddTicks(-(dt.Ticks % TimeSpan.TicksPerMillisecond));
        }
    }
}
