﻿using System;


namespace TechnicalTools.Extensions.Data
{
    /// <summary>
    /// Allow to flag property that are not interesting for user
    /// For example all foreign key and technical id are technical links that user don't care about.
    /// These links are often obvious in the UI, by the way data are presented
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class IsTechnicalPropertyAttribute : Attribute
    {
        public bool HideFromUser { get; set; }
    }
}
