﻿using System;
using System.Data.Common;

using TechnicalTools.Tools;


namespace TechnicalTools
{
    public static class DbConnection_Extensions
    {
        public static void OpenOrWaitUntilOpen(this DbConnection con)
        {
            SqlRetry.Instance.OpenOrWaitUntilOpen(con);
        }
    }
}
