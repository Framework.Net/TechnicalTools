﻿using System;


namespace TechnicalTools
{
    public static class Action_Extensions
    {
        public static void DoNothing()                             { /* Nothing */ }
        public static void DoNothing<T>(T value)                   { /* Nothing */ }
        public static void DoNothing<T1, T2>(T1 value1, T2 value2) { /* Nothing */ }
        /* Put as many overloads as you want */

        public static Action WrapLambda(this Action defaultAction, Action prefix, Action suffix)
        {
            return () =>
            {
                prefix();
                try
                {
                    defaultAction();
                }
                finally
                {
                    suffix();
                }
            };
        }
    }
}
