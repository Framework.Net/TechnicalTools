﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;


namespace TechnicalTools
{
    public static class String_Extensions
    {
        public static string RemoveTrailingSpaces(this string strCode)
        {
            var cleaned = strCode;
            var spaceAndNewline = " " + Environment.NewLine;
            do
            {
                strCode = cleaned;
                cleaned = strCode.Replace(spaceAndNewline, Environment.NewLine);
            }
            while (cleaned != strCode);
            return cleaned;
        }
        public static string RemoveBlankLines(this string strCode)
        {
            var cleaned = strCode;
            var twoNewLine = Environment.NewLine + Environment.NewLine;
            do
            {
                strCode = cleaned;
                cleaned = strCode.Replace(twoNewLine, Environment.NewLine);
            }
            while (cleaned != strCode);
            return cleaned;
        }
        public static string TrimInternalSpaces(this string str)
        {
            string clean = str;
            do
            {
                str = clean;
                clean = str.Replace("  ", " ");
            }
            while (clean != str);
            return str;
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static Stream AsStream(this string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static string ToMD5Hash(this string str)
        {
            return Encoding.UTF8.GetBytes(str).ToMD5Hash();
        }
        public static string ToMD5Hash(this byte[] bs)
        {
            // Note : if code throw here because of FIPS compliancy,
            // add <enforceFIPSPolicy enabled="false"/> in config file (inside )
            // in <configuration> <runtime>
            var md5 = MD5.Create(); // this is a better way to create a MD5 hasher than MD5.Create, because this way is FIPS compliant
            bs = md5.ComputeHash(bs);
            var sb = new StringBuilder(bs.Length * 2);
            foreach (byte b in bs)
                sb.Append(b.ToString("x2"));
            return sb.ToString();
        }

        public static string ToSHA256Hash(this string str)
        {
            return Encoding.UTF8.GetBytes(str).ToSHA256Hash();
        }
        public static string ToSHA256Hash(this byte[] bs)
        {
            // Note : if code throw here because of FIPS compliancy,
            // add <enforceFIPSPolicy enabled="false"/> in config file (inside )
            // in <configuration> <runtime>
            var hasher = SHA256.Create(); // SHA256 is already FIPs compliant, unlike SHA256Managed (which is the true equivalent of MD5class )
            bs = hasher.ComputeHash(bs);
            var sb = new StringBuilder(bs.Length * 2);
            foreach (byte b in bs)
                sb.Append(b.ToString("x2"));

            return sb.ToString();

        }

        public static string ReplaceInvalidCharacterInFilename(this string filename)
        {
            return string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        }

        /// <summary>
        /// Truncate a string if needed and add ellipsis is wanted by developper so that the result string is not more than specified length
        /// </summary>
        public static string Truncate(this string str, int charCountToKeepAtMax, string ellipsis = null)
        {
            if (ellipsis?.Length > charCountToKeepAtMax)
                throw new ArgumentException("Argument " + nameof(ellipsis) + " cannot be longer than " + charCountToKeepAtMax + " (ie: " + nameof(charCountToKeepAtMax) + " arg)", nameof(ellipsis));
            if (str == null)
                return null;
            if (str.Length <= charCountToKeepAtMax)
                return str;
            if (ellipsis == null)
                return str.Remove(charCountToKeepAtMax);
            return str.Remove(charCountToKeepAtMax - ellipsis.Length) + ellipsis;
        }

        public static string TruncateFrom(this string str, string substr, int startIndex = 0)
        {
            return str.Remove(str.IndexOf(substr, startIndex));
        }
        public static string TruncateFromLast(this string str, string substr, int startIndex = -1)
        {
            return str.Remove(str.LastIndexOf(substr, startIndex < 0 ? str.Length + startIndex : startIndex));
        }

        /// <summary> Insère un espace avant chaque lettre majuscule (sauf si plusieurs lettres majuscules se suivent... dans ce cas on garde le bloc de lettre majuscule) </summary>
        public static string Uncamelify(this string str)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < str.Length; ++i)
            {
                var c = str[i];
                if (c == ' ' || c == '_')
                    sb.Append(' ');
                else
                {
                    if (char.IsDigit(c) &&
                           // text already exist before and is not a space
                           sb.Length > 0 && sb[sb.Length - 1] != ' ' &&
                           // if the char before is not upper (abbeviation) or there is a lower character after)
                           (char.IsLower(sb[sb.Length - 1]) ||
                            i + 1 < str.Length && char.IsLower(str[i + 1])))
                    {
                        sb.Append(' ');
                        sb.Append(c);
                    }
                    else if (char.IsUpper(c) &&
                           // text already exist before and is not a space
                           sb.Length > 0 && sb[sb.Length - 1] != ' ' &&
                           // if the char before is not upper (abbeviation) or there is a lower character after)
                           (char.IsLower(sb[sb.Length - 1]) ||
                            i + 1 < str.Length && char.IsLower(str[i + 1])))
                    {
                        sb.Append(' ');
                        sb.Append(c);
                    }
                    else
                        sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string Capitalize(this string s)
        {
            if (String.IsNullOrEmpty(s)) return s;
            return s.Substring(0, 1).ToUpperInvariant() + s.Substring(1);
        }

        public static string IfBlankUse(this string str, string substitute)
        {
            if (string.IsNullOrWhiteSpace(str))
                return substitute;
            return str;
        }

        public static string IfNotBlankAddSuffix(this string str, string suffix)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;
            return str + suffix;
        }
        public static string IfNotBlankAddPrefix(this string str, string prefix)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;
            return prefix + str;
        }
        public static string AsPrefixForIfNotBlank(this string prefix, string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;
            return prefix + str;
        }
        public static string IfNotBlankSurroundWith(this string str, string prefix, string suffix)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;
            return prefix + str + suffix;
        }

        public static string AddRequiredSeparator(this string str, string separator, string remaining)
        {
            if (string.IsNullOrEmpty(str))
                return remaining;
            if (string.IsNullOrEmpty(remaining))
                return str;
            return str + separator + remaining;
        }

        public static string AddIndented(this string str, string append)
        {
            var nl = Environment.NewLine;
            return str + append.Replace(nl, nl + new string(' ', str.Trim(nl.ToArray()).Length));
        }

        public static string SkipLines(this string str, int count)
        {
            int start = 0;
            while (count > 0)
            {
                int index = str.IndexOf('\n', start);
                if (index == -1)
                {
                    start = str.Length;
                    break;
                }
                start = index + 1;
                --count;
            }
            if (start == 0)
                return str;
            return str.Substring(start);
        }

        public enum ePadding
        {
            None,
            Left,
            Right
        }
        public static StringBuilder AlignAndMerge(this IReadOnlyCollection<IReadOnlyList<string>> propEltsDefs, IReadOnlyList<ePadding> paddings, StringBuilder sb, string indentation = "", string columnSeparator = " ", bool keepEmptyColumn = false)
        {
            sb = sb ?? new StringBuilder();
            if (propEltsDefs.Count == 0)
                return sb;
            Debug.Assert(propEltsDefs.All(propElts => propElts.Count == paddings.Count));

            var eltMaxLengths = new int[paddings.Count];
            for (int i = 0; i < paddings.Count; ++i)
                foreach (var propElts in propEltsDefs)
                    eltMaxLengths[i] = Math.Max(eltMaxLengths[i], propElts[i].Length);

            foreach (var propElts in propEltsDefs)
            {
                sb.Append(indentation);
                for (int i = 0; i < propElts.Count; ++i)
                {
                    var elt = propElts[i];
                    if (paddings[i] == ePadding.Left)
                        sb.Append(elt.PadLeft(eltMaxLengths[i]));
                    else
                        sb.Append(elt.PadRight(eltMaxLengths[i]));
                    if (eltMaxLengths[i] > 0 || keepEmptyColumn)
                        sb.Append(columnSeparator);
                }
                sb.Length = sb.Length - 1;
                sb.TrimEnd();
                sb.AppendLine();
            }
            sb.Length = sb.Length - Environment.NewLine.Length;
            return sb;
        }

        public static string RemoveDiacritics(this string s)
        {
            if(s is null)
            {
                return null;
            }

            char[] filtered = s.Normalize(NormalizationForm.FormD)
                               .Where(c => char.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                               .ToArray();

            return new string(filtered).Normalize(NormalizationForm.FormC);
        }
    }
}
