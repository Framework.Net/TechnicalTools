﻿using System;


namespace TechnicalTools
{
    public static class Func_Extensions
    {
        public static bool ReturnTrue () { return true; }
        public static bool ReturnFalse() { return false; }
    }
    public static class Func_Extensions<T>
    {
        public static bool ReturnTrue (T _) { return true; }
        public static bool ReturnFalse(T _) { return false; }
        public static T Identity(T x) { return x; }
    }
    public static class Func_Extensions<T1, T2>
    {
        public static bool ReturnTrue (T1 a, T2 b) { return true; }
        public static bool ReturnFalse(T1 a, T2 b) { return false; }
    }
    public static class Func_Extensions<T1, T2, T3>
    {
        public static bool ReturnTrue (T1 a, T2 b, T3 c) { return true; }
        public static bool ReturnFalse(T1 a, T2 b, T3 c) { return false; }
    }
    public static class Func_Extensions<T1, T2, T3, T4>
    {
        public static bool ReturnTrue (T1 a, T2 b, T3 c, T4 d) { return true; }
        public static bool ReturnFalse(T1 a, T2 b, T3 c, T4 d) { return false; }
    }
    public static class Func_Extensions<T1, T2, T3, T4, T5>
    {
        public static bool ReturnTrue (T1 a, T2 b, T3 c, T4 d, T5 e) { return true; }
        public static bool ReturnFalse(T1 a, T2 b, T3 c, T4 d, T5 e) { return false; }
    }
    public static class Func_Extensions<T1, T2, T3, T4, T5, T6>
    {
        public static bool ReturnTrue (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f) { return true; }
        public static bool ReturnFalse(T1 a, T2 b, T3 c, T4 d, T5 e, T6 f) { return false; }
    }
    public static class Func_Extensions<T1, T2, T3, T4, T5, T6, T7>
    {
        public static bool ReturnTrue (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g) { return true; }
        public static bool ReturnFalse(T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g) { return false; }
    }
    public static class Func_Extensions<T1, T2, T3, T4, T5, T6, T7, T8>
    {
        public static bool ReturnTrue (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h) { return true; }
        public static bool ReturnFalse(T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h) { return false; }
    }
}
