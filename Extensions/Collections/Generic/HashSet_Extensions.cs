﻿using System;
using System.Collections.Generic;


namespace TechnicalTools
{
    public static class HashSet_Extensions
    {
        public static HashSet<T> Empty<T>()
        {
            return EmptyRepository<T>.Instance;
        }
        static class EmptyRepository<T>
        {
            public static readonly HashSet<T> Instance = new HashSet<T>();
        }
    }
}
