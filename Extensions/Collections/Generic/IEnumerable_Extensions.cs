﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using TechnicalTools.Model;
using TechnicalTools.Tools;


namespace TechnicalTools
{
    public static class IEnumerableGeneric_Extensions
    {
        // TakeWhile ne prend pas le premier element qui ne match plus la condition
        // Cette methode prend tout tout jusqu'a ce qu'un item valide une condition.
        // L'item qui valide la condition est aussi renvoyé dans l'ensemble.
        // Les elements suivant ne sont pas pris.
        public static IEnumerable<TSource> TakeUntil<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, bool takeMatchingItemToo = true)
        {
            Debug.Assert(source != null);
            Debug.Assert(predicate != null);
            foreach (var item in source)
            {
                if (predicate(item))
                {
                    if (takeMatchingItemToo)
                        yield return item;
                    yield break;
                }
                yield return item;
            }
        }

        public static IEnumerable<TSource> SkipUntil<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, bool skipMatchingItemToo = true)
        {
            Debug.Assert(source != null);
            Debug.Assert(predicate != null);

            var enumerator = source.GetEnumerator();
            while (enumerator.MoveNext())
                if (predicate(enumerator.Current))
                {
                    if (!skipMatchingItemToo)
                        yield return enumerator.Current;
                    while (enumerator.MoveNext())
                        yield return enumerator.Current;
                    break;
                }
            enumerator.Dispose();
        }

        /// <summary>
        /// Consume the n first items using <paramref name="consume"/>,
        /// and enumerate the remaining items in <paramref name="source"/>
        /// </summary>
        public static IEnumerable<TSource> ConsumeThenEnumerate<TSource>(this IEnumerable<TSource> source, int cnt, Action<TSource> consume)
        {
            Debug.Assert(source != null);

            var enumerator = source.GetEnumerator();
            while (enumerator.MoveNext())
                if (cnt > 0)
                {
                    --cnt;
                    consume(enumerator.Current);
                }
                else
                    yield return enumerator.Current;
            enumerator.Dispose();
        }

        public static IEnumerable<TSource> TeeOnEnumerate<TSource>(this IEnumerable<TSource> source, Action<TSource> consumeOnTee)
        {
            Debug.Assert(source != null);

            var enumerator = source.GetEnumerator();
            while (enumerator.MoveNext())
            {
                yield return enumerator.Current;
                consumeOnTee(enumerator.Current);
            }
            enumerator.Dispose();
        }

        public static string ConcatAsString<T>(this IEnumerable<T> lst, string separator)
        {
            return string.Join(separator, lst.Select(t => t.ToString()));
        }

        public static IEnumerable<T> Concat<T>(this IEnumerable<T> lst, T another_item)
        {
            foreach (var item in lst)
                yield return item;
            yield return another_item;
        }
        public static IEnumerable<T> Concat<T>(this T another_item, IEnumerable<T> lst)
        {
            yield return another_item;
            foreach (var item in lst)
                yield return item;
        }
        public static IEnumerable<T> Concat<T>(this IEnumerable<T> lst, T another_item, int count)
        {
            foreach (var item in lst)
                yield return item;
            while (--count >= 0)
                yield return another_item;
        }
        public static IEnumerable<T> ConcatNullable<T>(this IEnumerable<T> first, IEnumerable<T> second)
        {
            if (first == null)
                return second;
            if (second == null)
                return first;
            return first.Concat(second);
        }

        public static IEnumerable<T> Except<T>(this IEnumerable<T> lst, T another_item, int count = 1)
        {
            return lst.Except(Enumerable.Range(0, count).Select(_ => another_item));
        }

        public static IEnumerable<TQ> SelectNotNull<T, TQ>(this IEnumerable<T> lst, Func<T, TQ> selector)
            where TQ : class
        {
            return lst?.Select(selector)
                       .Where(output => output != null);
        }
        public static IEnumerable<TQ> SelectNotNull<T, TQ>(this IEnumerable<T> lst, Func<T, TQ?> selector)
            where TQ : struct
        {
            return lst?.Select(selector)
                       .Where(output => output.HasValue)
                       .Select(output => output.Value);
        }

        public static IEnumerable<T> Flatten<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.SelectMany(x => x);
        }
        public static IEnumerable<T> SelectMany<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.SelectMany(x => x);
        }

        public static IEnumerable<T> NotNull<T>(this IEnumerable<T> lst)
            where T : class
        {
            if (lst != null)
                foreach (T value in lst)
                    if (value != null)
                        yield return value;
        }
        public static IEnumerable<string> NotBlank(this IEnumerable<string> lst)
        {
            return lst.Where(str => !string.IsNullOrWhiteSpace(str));
        }
        public static IEnumerable<char> NotBlank(this IEnumerable<char> lst)
        {
            return lst.Where(c => c != char.MinValue && !char.IsWhiteSpace(c));
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static string Join<T>(this IEnumerable<T> lst, string separator = ", ", Func<T, string> transform = null)
        {
            if (lst == null)
                return null;
            if (transform == null)
                return string.Join(separator, lst);
            return string.Join(separator, lst.Select(transform));
        }

        public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> source, int last_item_count_to_enumerate)
        {
            if (last_item_count_to_enumerate <= 0)
                yield break;

            var lastItems = new T[last_item_count_to_enumerate];
            int i = 0;
            bool full = false;
            foreach (var item in source)
            {
                lastItems[i] = item;
                ++i;
                if (i == last_item_count_to_enumerate)
                {
                    i = 0;
                    full = true;
                }
            }
            if (!full) // number of item in source is < last_item_count_to_enumerate
            {
                last_item_count_to_enumerate = i; // reduce the number of item to get
                i = 0; // so the oldest is at index 0
            }

            while (--last_item_count_to_enumerate >= 0)
            {
                yield return lastItems[i];
                i = (i + 1) % lastItems.Length;
            }
        }

        public static IEnumerable<TSource> SkipLast<TSource>(this IEnumerable<TSource> source, int n = 1)
        {
            Debug.Assert(n >= 0);
            if (n < 0)
                n = 0;
            var buffer = new Queue<TSource>(n + 1);
            foreach (TSource value in source)
            {
                buffer.Enqueue(value);
                --n;
                if (n < 0)
                {
                    yield return buffer.Dequeue();
                    ++n;
                }
            }
        }

        public static IntervalIntegerInclusiveSet ToIntegerSet<TItem>(this IEnumerable<TItem> source, Func<TItem, long> getValue)
        {
            return new IntervalIntegerInclusiveSet(source.Select(getValue));
        }
        public static IntervalIntegerInclusiveSet ToIntegerSet(this IEnumerable<long> source)
        {
            return new IntervalIntegerInclusiveSet(source);
        }



        public static IEnumerable<T> OfType<T>(this IEnumerable<T> source, Type type)
        {
            foreach (var obj in source)
                if (type.IsInstanceOfType(obj))
                    yield return obj;
        }
        public static System.Collections.IList ToTypedList<T>(this IEnumerable<T> source, Type newType)
        {
            Debug.Assert(typeof(T).IsAssignableFrom(newType));
            var mCast = mgCast.MakeGenericMethod(newType);
            var sourceCasted = mCast.Invoke(null, new object[] { source });
            var mToList = mgToList.MakeGenericMethod(newType);
            var res = mToList.Invoke(null, new object[] { sourceCasted });
            return (System.Collections.IList)res;
        }
        static readonly MethodInfo mgCast = typeof(Enumerable).GetMethod(nameof(Enumerable.Cast)).ThrowIfNull();
        static readonly MethodInfo mgToList = typeof(Enumerable).GetMethod(nameof(Enumerable.ToList)).ThrowIfNull();
        public static IFixedBindingList ToTypedFixedBindingList<T>(this IEnumerable<T> source, Type newType)
        {
            Debug.Assert(typeof(T).IsAssignableFrom(newType));
            var mCast = mgCast.MakeGenericMethod(newType);
            var sourceCasted = mCast.Invoke(null, new object[] { source });
            var mToFixedBindingList = mgToFixedBindingList.MakeGenericMethod(newType);
            var res = mToFixedBindingList.Invoke(null, new object[] { sourceCasted });
            return (IFixedBindingList)res;
        }
        static readonly MethodInfo mgToFixedBindingList = typeof(IEnumerableGeneric_Extensions).GetMethod(nameof(IEnumerableGeneric_Extensions.ToFixedBindingList)).ThrowIfNull();

        [Obsolete("Remplacer par OfType !", true)]
        public static IEnumerable<TInterface> Implementing<TInterface>(this IEnumerable<object> source)
            where TInterface : class
        {
            foreach (var item in source)
                if (item == null || item is TInterface)
                    yield return (TInterface)item;
        }

        // Remplace FirstOrDefault qui ne renvoie pas null
        public static DateTime? FirstOrNull(this IEnumerable<DateTime> values)
        {
            foreach (DateTime dt in values)
                return dt;
            return null;
        }
        // Remplace FirstOrDefault qui ne renvoie pas null
        public static DateTime? LastOrNull(this IEnumerable<DateTime> values)
        {
            // ReSharper disable PossibleMultipleEnumeration
            if (values.Any())
                return values.Last();
            // ReSharper restore PossibleMultipleEnumeration
            return null;
        }

        // Semble dérisoire mais ca facilite la lecture
        public static bool IsEmpty<TSource>(this IEnumerable<TSource> source)
        {
            Debug.Assert(source != null);
            return !source.Any();
        }

        // Same As List<T>.FindIndex
        // Summary:
        //     Searches for an element that matches the conditions defined by the specified
        //     predicate, and returns the zero-based index of the first occurrence within
        //     the entire System.Collections.Generic.List<T>.
        //
        // Parameters:
        //   match:
        //     The System.Predicate<T> delegate that defines the conditions of the element
        //     to search for.
        //
        // Returns:
        //     The zero-based index of the first occurrence of an element that matches the
        //     conditions defined by match, if found; otherwise, –1.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     match is null.
        public static int FindIndex<T>(this IEnumerable<T> lst, Predicate<T> match)
        {
            int i = 0;
            foreach (var t in lst)
                if (match(t))
                    return i;
                else
                    ++i;
            return -1;
        }
        // Same As List<T>.FindIndex
        // Summary:
        //     Searches for an element that matches the conditions defined by the specified
        //     predicate, and returns the zero-based index of the first occurrence within
        //     the range of elements in the System.Collections.Generic.List<T> that extends
        //     from the specified index to the last element.
        //
        // Parameters:
        //   startIndex:
        //     The zero-based starting index of the search.
        //
        //   match:
        //     The System.Predicate<T> delegate that defines the conditions of the element
        //     to search for.
        //
        // Returns:
        //     The zero-based index of the first occurrence of an element that matches the
        //     conditions defined by match, if found; otherwise, –1.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     match is null.
        //
        //   System.ArgumentOutOfRangeException:
        //     startIndex is outside the range of valid indexes for the System.Collections.Generic.List<T>.
        public static int FindIndex<T>(this IEnumerable<T> lst, int startIndex, Predicate<T> match)
        {
            int i = startIndex;
            foreach (var t in lst.Skip(startIndex))
                if (match(t))
                    return i;
                else
                    ++i;
            return -1;
        }
        // Same As List<T>.FindIndex
        // Summary:
        //     Searches for an element that matches the conditions defined by the specified
        //     predicate, and returns the zero-based index of the first occurrence within
        //     the range of elements in the System.Collections.Generic.List<T> that starts
        //     at the specified index and contains the specified number of elements.
        //
        // Parameters:
        //   startIndex:
        //     The zero-based starting index of the search.
        //
        //   count:
        //     The number of elements in the section to search.
        //
        //   match:
        //     The System.Predicate<T> delegate that defines the conditions of the element
        //     to search for.
        //
        // Returns:
        //     The zero-based index of the first occurrence of an element that matches the
        //     conditions defined by match, if found; otherwise, –1.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     match is null.
        //
        //   System.ArgumentOutOfRangeException:
        //     startIndex is outside the range of valid indexes for the System.Collections.Generic.List<T>.-or-count
        //     is less than 0.-or-startIndex and count do not specify a valid section in
        //     the System.Collections.Generic.List<T>.
        public static int FindIndex<T>(this IEnumerable<T> lst, int startIndex, int count, Predicate<T> match)
        {
            int i = startIndex;
            foreach (var t in lst.Skip(startIndex))
                if (match(t))
                    return i;
                else if (--count == 0)
                    return -1;
                else
                    ++i;
            return -1;
        }

        public static string FindDuplicatesAndMakeReport<T, TProperty>(this IEnumerable<T> objects, Func<T, TProperty> getValueThatMustBeUnique, Func<T, string> getObjectName = null)
        {
            getObjectName = getObjectName ?? (o => o.ToString());

            return objects.FindDuplicates(getValueThatMustBeUnique)
                .Select(grp => "With value of \"" + grp.Key.ToString() + "\" : " +
                               grp.Select(obj => Environment.NewLine + " - " + getObjectName(obj)).Join())
                .Join(Environment.NewLine + Environment.NewLine);
        }
        public static IEnumerable<IGrouping<TProperty,T>> FindDuplicates<T, TProperty>(this IEnumerable<T> objects, Func<T, TProperty> getValueThatMustBeUnique)
        {
            return objects.GroupBy(getValueThatMustBeUnique)
                          .Where(grp => grp.Skip(1).Any());
        }
        public static IEnumerable<IGrouping<T, T>> FindDuplicates<T>(this IEnumerable<T> objects)
        {
            return FindDuplicates(objects, o => o);
        }
        public static bool ContainsDuplicates<T>(this IEnumerable<T> lst, bool optimist_on_finding_duplicate_quickly = false)
        {
            if (optimist_on_finding_duplicate_quickly)
            {
                var set = new HashSet<T>();
                foreach (var item in lst)
                    if (!set.Add(item))
                        return true;
            }
            else
            {
                int count = 0;
                var set = new HashSet<T>(lst.TeeOnEnumerate(_ => ++count));
                return set.Count != count;
            }
            return false;
        }
        public static IEnumerable<TItem> IntersectAll<TItem>(this IEnumerable<IEnumerable<TItem>> listOfLists)
        {
            Debug.Assert(listOfLists != null);

            HashSet<TItem> h = null;
            foreach (var lst in listOfLists.ConsumeThenEnumerate(1, first => h = new HashSet<TItem>(first)))
                h.IntersectWith(lst);
            return h ?? (IEnumerable<TItem>)new List<TItem>();
        }
        /// <summary>
        ///  Allow to check value of an ienumerable, without enumerating twice, and throw if predicate is not true for an item
        /// Basically, this two snippets would be equivalent (if the query was enumerable multiple times):
        /// <code>
        /// // Problem of this code is: We HAVE TO to assume "enumerable" is enumerable twice or more we should a beter interface like IReadOnlyCollection/IReadOnlyList
        /// var enumerable = ... a list enumerable
        /// int i = -1;
        /// var first = enumerable.FirstOrDefault(item => { ++i; return !predicate(item); });
        /// if (first != null)
        ///     onFail(first, i);
        /// return enumerable;
        /// </code>
        /// <code>
        /// Here we DO NOT have to assume "enumerable" is enumerable twice or more, IEnumerable type is fine
        /// var queryable = ... a list enumerable
        /// return queryable.CheckOnline(predicate, onfail);
        /// </code>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="predicate"></param>
        /// <param name="onFail">Take item, </param>
        /// <returns></returns>
        public static IEnumerable<T> CheckOnline<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate, Func<T, int, Exception> onFail)
        {
            int i = 0;
            foreach (var elt in enumerable)
            {
                if (!predicate(elt))
                    throw onFail(elt, i);
                yield return elt;
                ++i;
            }
        }

        public static bool EqualsAsSet<TItem>(this IEnumerable<TItem> list1, IEnumerable<TItem> list2)
            //where TItemList : IEnumerable<TItem>
        {
            var cnt = new Dictionary<TItem, int>();
            foreach (TItem s in list1)
                if (cnt.ContainsKey(s))
                    cnt[s]++;
                else
                    cnt.Add(s, 1);
            foreach (TItem s in list2)
                if (cnt.ContainsKey(s))
                    cnt[s]--;
                else
                    return false;
            return cnt.Values.All(c => c == 0);
        }

        public class SequenceEqualsEqualityComparer<T> : IEqualityComparer<IEnumerable<T>>
        {
            public static readonly SequenceEqualsEqualityComparer<T> Instance = new SequenceEqualsEqualityComparer<T>();
            public bool Equals(IEnumerable<T> x, IEnumerable<T> y)
            {
                return ReferenceEquals(x, y) || (x != null && y != null && x.SequenceEqual(y));
            }

            public int GetHashCode(IEnumerable<T> obj)
            {
                Debug.Assert(obj != null); // .Net contract
                //if (obj == null)
                //    return 0;
                return obj.Count();
            }
        }

        public static string CreateUnique(this IEnumerable<string> source, string rootName, bool withSpace = true)
        {
            return CreateUnique(source, s => s, rootName, withSpace);
        }
        public static string CreateUnique<TSource>(this IEnumerable<TSource> source, Func<TSource, string> getName, string rootName, bool withSpace = true)
        {
            var names = source.Select(getName).ToList();
            names.AddRange(names.Where(str => str.EndsWith(" ")).Select(str => str.TrimEnd()).ToList());
            int i = 0;
            string name;
            do
            {
                ++i;
                name = (rootName + (withSpace ? " " : "") + i).Trim();
            } while (names.Contains(name));

            return name;
        }

        public static void Sort<T>(this FixedBindingList<T> blst, Comparison<T> comparer = null)
        {
            var lst = blst.ToList();
            if (comparer == null)
                lst.Sort();
            else
                lst.Sort(comparer);

            blst.WithListChangedEventsDisabled(() =>
            {
                blst.Clear();
                foreach (var e in lst)
                    blst.Add(e);
            });
        }

        [DebuggerHidden, DebuggerStepThrough]
        public static Exception ToException(this IReadOnlyList<Exception> lst)
        {
            Debug.Assert(lst != null);
            Debug.Assert(lst.Any());

            if (lst.Skip(1).Any()) // Count >= 2
                return new AggregateException(lst);

            return lst.First();
        }


        public static BindingList<T> ToBindingList<T>(this IEnumerable<T> src)
        {
            return new BindingList<T>(src.ToList());
        }
        public static FixedBindingList<T> ToFixedBindingList<T>(this IEnumerable<T> values)
        {
            return new FixedBindingList<T>(values);
        }
        public static Dictionary<TKey, List<T>> GroupByToDictionary<T, TKey>(this IEnumerable<T> source, Func<T, TKey> getKey)
        {
            return source.GroupBy(getKey)
                         .ToDictionary(grp => grp.Key, grp => grp.ToList());
        }
        public static Dictionary<TKey, TValue> GroupByToDictionary<T, TKey, TValue>(this IEnumerable<T> source, Func<T, TKey> getKey, Func<IGrouping<TKey, T>, TValue> getValue)
        {
            return source.GroupBy(getKey)
                         .ToDictionary(grp => grp.Key, getValue);
        }
        public static Dictionary<TKey, Dictionary<TKey2, List<T>>> GroupByToDictionary2<T, TKey, TKey2>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2)
        {
            return GroupByToDictionary2(source, getKey1, getKey2, grp => grp.ToList());
        }
        public static Dictionary<TKey, Dictionary<TKey2, TValue>> GroupByToDictionary2<T, TKey, TKey2, TValue>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<IGrouping<TKey2, T>, TValue> getValue)
        {
            #if DEBUG
            var trueGetKey1 = getKey1;
            getKey1 = t =>
            {
                var result = trueGetKey1(t);
                Debug.Assert(result != null);
                return result;
            };
            #endif
            return source.GroupBy(getKey1)
             .ToDictionary(grp => grp.Key,
                           grp => grp.GroupBy(getKey2)
                                     .ToDictionary(grp2 => grp2.Key, getValue));
        }
        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, List<T>>>> GroupByToDictionary3<T, TKey, TKey2, TKey3>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3)
        {
            return GroupByToDictionary3(source, getKey1, getKey2, getKey3, grp => grp.ToList());
        }
        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, TValue>>> GroupByToDictionary3<T, TKey, TKey2, TKey3, TValue>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<IGrouping<TKey3, T>, TValue> getValue)
        {
            return source.GroupBy(getKey1)
             .ToDictionary(grp => grp.Key,
                           grp => grp.GroupBy(getKey2)
                                     .ToDictionary(grp2 => grp2.Key,
                                                   grp2 => grp2.GroupBy(getKey3)
                                                               .ToDictionary(grp3 => grp3.Key,
                                                                             getValue)));
        }

        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, Dictionary<TKey4, List<T>>>>> GroupByToDictionary4<T, TKey, TKey2, TKey3, TKey4>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<T, TKey4> getKey4)
        {
            return GroupByToDictionary4(source, getKey1, getKey2, getKey3, getKey4, grp => grp.ToList());
        }
        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, Dictionary<TKey4, TValue>>>> GroupByToDictionary4<T, TKey, TKey2, TKey3, TKey4, TValue>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<T, TKey4> getKey4, Func<IGrouping<TKey4, T>, TValue> getValue)
        {
            return source.GroupBy(getKey1)
             .ToDictionary(grp => grp.Key,
                           grp => grp.GroupBy(getKey2)
                                     .ToDictionary(grp2 => grp2.Key,
                                                   grp2 => grp2.GroupBy(getKey3)
                                                               .ToDictionary(grp3 => grp3.Key,
                                                                             grp3 => grp3.GroupBy(getKey4)
                                                                                         .ToDictionary(grp4 => grp4.Key,
                                                                                                       getValue))));
        }

        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, Dictionary<TKey4, Dictionary<TKey5, List<T>>>>>> GroupByToDictionary5<T, TKey, TKey2, TKey3, TKey4, TKey5>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<T, TKey4> getKey4, Func<T, TKey5> getKey5)
        {
            return GroupByToDictionary5(source, getKey1, getKey2, getKey3, getKey4, getKey5, grp => grp.ToList());
        }
        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, Dictionary<TKey4, Dictionary<TKey5, TValue>>>>> GroupByToDictionary5<T, TKey, TKey2, TKey3, TKey4, TKey5, TValue>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<T, TKey4> getKey4, Func<T, TKey5> getKey5, Func<IGrouping<TKey5, T>, TValue> getValue)
        {
            return source.GroupBy(getKey1)
             .ToDictionary(grp => grp.Key,
                           grp => grp.GroupBy(getKey2)
                                     .ToDictionary(grp2 => grp2.Key,
                                                   grp2 => grp2.GroupBy(getKey3)
                                                               .ToDictionary(grp3 => grp3.Key,
                                                                             grp3 => grp3.GroupBy(getKey4)
                                                                                         .ToDictionary(grp4 => grp4.Key,
                                                                                                       grp4 => grp4.GroupBy(getKey5)
                                                                                                                   .ToDictionary(grp5 => grp5.Key,
                                                                                                                                 getValue)))));
        }

        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, Dictionary<TKey4, Dictionary<TKey5, Dictionary<TKey6, List<T>>>>>>> GroupByToDictionary6<T, TKey, TKey2, TKey3, TKey4, TKey5, TKey6>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<T, TKey4> getKey4, Func<T, TKey5> getKey5, Func<T, TKey6> getKey6)
        {
            return GroupByToDictionary6(source, getKey1, getKey2, getKey3, getKey4, getKey5, getKey6, grp => grp.ToList());
        }
        public static Dictionary<TKey, Dictionary<TKey2, Dictionary<TKey3, Dictionary<TKey4, Dictionary<TKey5, Dictionary<TKey6, TValue>>>>>> GroupByToDictionary6<T, TKey, TKey2, TKey3, TKey4, TKey5, TKey6, TValue>(this IEnumerable<T> source, Func<T, TKey> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<T, TKey4> getKey4, Func<T, TKey5> getKey5, Func<T, TKey6> getKey6, Func<IGrouping<TKey6, T>, TValue> getValue)
        {
            return source.GroupBy(getKey1)
             .ToDictionary(grp => grp.Key,
                           grp => grp.GroupBy(getKey2)
                                     .ToDictionary(grp2 => grp2.Key,
                                                   grp2 => grp2.GroupBy(getKey3)
                                                               .ToDictionary(grp3 => grp3.Key,
                                                                             grp3 => grp3.GroupBy(getKey4)
                                                                                         .ToDictionary(grp4 => grp4.Key,
                                                                                                       grp4 => grp4.GroupBy(getKey5)
                                                                                                                   .ToDictionary(grp5 => grp5.Key,
                                                                                                                                 grp5 => grp5.GroupBy(getKey6)
                                                                                                                                             .ToDictionary(grp6 => grp6.Key,
                                                                                                                                                           getValue))))));
        }

        //public static ILookup<TKey1, ILookup<TKey2, T>> ToLookUp2<T, TKey1, TKey2>(this IEnumerable<T> source, Func<T, TKey1> getKey1, Func<T, TKey2> getKey2)
        //{
        //    return source.GroupBy(x => getKey1(x))
        //                 .Select(grpX => grpX.ToLookup(x => getKey2(x)))
        //                 .ToLookup(lu => getKey1(lu.First().First()));
        //}
        //public static ILookup<TKey1, ILookup<TKey2, ILookup<TKey3, T>>> ToLookUp3<T, TKey1, TKey2, TKey3>(this IEnumerable<T> source, Func<T, TKey1> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3)
        //{
        //    return source.GroupBy(x => getKey1(x))
        //                 .Select(grpX => grpX.ToLookUp2(getKey2, getKey3))
        //                 .ToLookup(lu2 => getKey1(lu2.First().First()
        //                                             .First().First()));
        //}
        //public static ILookup<TKey1, ILookup<TKey2, ILookup<TKey3, ILookup<TKey4, T>>>> ToLookUp4<T, TKey1, TKey2, TKey3, TKey4>(this IEnumerable<T> source, Func<T, TKey1> getKey1, Func<T, TKey2> getKey2, Func<T, TKey3> getKey3, Func<T, TKey4> getKey4)
        //{
        //    return source.GroupBy(x => getKey1(x))
        //                 .Select(grpX => grpX.ToLookUp3(getKey2, getKey3, getKey4))
        //                 .ToLookup(lu2 => getKey1(lu2.First().First()
        //                                             .First().First()
        //                                             .First().First()));
        //}


        public static ConcurrentDictionary<TKey, T> ToConcurrentDictionary<T, TKey>(this IEnumerable<T> src, Func<T, TKey> getKey)
        {
            return new ConcurrentDictionary<TKey, T>(src.Select(x => new KeyValuePair<TKey, T>(getKey(x), x)));
        }
        public static ConcurrentDictionary<TKey, TValue> ToConcurrentDictionary<T, TKey, TValue>(this IEnumerable<T> src, Func<T, TKey> getKey, Func<T, TValue> getValue)
        {
            return new ConcurrentDictionary<TKey, TValue>(src.Select(x => new KeyValuePair<TKey, TValue>(getKey(x), getValue(x))));
        }
        public static SortedDictionary<TKey, TValue> ToSortedDictionary<T, TKey, TValue>(this IEnumerable<T> src, Func<T, TKey> getKey, Func<T, TValue> getValue)
        {
            return new SortedDictionary<TKey, TValue>(src.ToDictionary(getKey, getValue));
        }
        public static SortedDictionary<TKey, T> ToSortedDictionary<T, TKey>(this IEnumerable<T> src, Func<T, TKey> getKey)
        {
            return new SortedDictionary<TKey, T>(src.ToDictionary(getKey, x => x));
        }

        //public static IEnumerable<TSource> Last<TSource>(this IEnumerable<TSource> source, int n = 1)
        //{
        //   TODO
        //}

        //public static int IndexOf<T>(this IEnumerable<T> enumerable, Func<T, bool> condition)
        //{
        //    int i = 0;
        //    foreach (T obj in enumerable)
        //        if (condition(obj))
        //            return i;
        //        else
        //            ++i;
        //    return -1;
        //}
        //public static int LastIndexOf<T>(this IEnumerable<T> enumerable, Func<T, bool> condition)
        //{
        //    int i = 0;
        //    foreach (T obj in enumerable)
        //        if (condition(obj))
        //            return i;
        //        else
        //            ++i;
        //    return -1;
        //}

    }
}
