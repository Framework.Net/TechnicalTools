﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace TechnicalTools
{
    public static partial class IList_Extensions
    {
        public static int IndexOf(this IList items, object obj)
        {
            if (items == null) throw new ArgumentNullException("items");

            int index = 0;
            foreach (var item in items)
            {
                if (item == obj)
                    return index;
                index++;
            }
            return -1;
        }

        public static int IndexOf(this IList items, Func<object, bool> predicate)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (predicate == null) throw new ArgumentNullException("predicate");

            int index = 0;
            foreach (var item in items)
            {
                if (predicate(item))
                    return index;
                index++;
            }
            return -1;
        }

        public static int IndexOf<T>(this IList<T> items, Func<T, bool> predicate)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (predicate == null) throw new ArgumentNullException("predicate");

            int index = 0;
            foreach (var item in items)
            {
                if (predicate(item))
                    return index;
                index++;
            }
            return -1;
        }
    }
}
