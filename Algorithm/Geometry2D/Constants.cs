﻿using System;


namespace TechnicalTools.Algorithm.Geometry2D
{
    // Code en majorité provenant de https://martin-thoma.com/how-to-check-if-two-line-segments-intersect/
    public static class Constants
    {
        public static readonly double EPSILON = 0.000001;
    }

}
