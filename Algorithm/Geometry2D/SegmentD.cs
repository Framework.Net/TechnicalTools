﻿using System;

using WinPoint = System.Drawing.Point;


namespace TechnicalTools.Algorithm.Geometry2D
{
    public class SegmentD : IElement
    {
        public PointD First { get; set; }
        public PointD Second { get; set; }

        public SegmentD(WinPoint f, WinPoint s)
            : this(f.X, f.Y, s.X, s.Y)
        {
        }
        public SegmentD(PointD f, PointD s)
        {
            First = f;
            Second = s;
        }
        public SegmentD(double x1, double y1, double x2, double y2)
        {
            First = new PointD(x1, y1);
            Second = new PointD(x2, y2);
        }


        public double Length
        {
            get { return First.SquareDistance(Second); }
        }
        public RectangleD AsBoundingBox()
        {
            return new RectangleD(Math.Min(First.X, Second.X), Math.Min(First.Y, Second.Y),
                                 Math.Abs(First.X - Second.X), Math.Abs(First.Y - Second.Y));
        }

        // Return minimum distance between line segment segment and point p
        // from https://stackoverflow.com/a/1501725/294998
        public double SquareDistance(PointD p)
        {
            double length = Length;
            if (length == 0.0)
            {
                // v == w case
                // Consider the line extending the segment, parameterized as v + t (w - v).
                // We find projection of point p onto the line.
                // It falls where t = [(p-v) . (w-v)] / |w-v|^2
                // We clamp t from [0,1] to handle points outside the segment vw.
                return First.SquareDistance(p);
            }
            var t = ((p.X - First.X) * (Second.X - First.X) + (p.Y - First.Y) * (Second.Y - First.Y)) / length;
            t = Math.Max(0, Math.Min(1, t));
            // process projection of p on segment
            var projection = new PointD(First.X + t * (Second.X - First.X),
                                       First.Y + t * (Second.Y - First.Y));
            return p.SquareDistance(projection);
        }

        /**
         * Check if line segments intersect
         * @param a first line segment
         * @param b second line segment
         * @return <code>true</code> if lines do intersect,
         *         <code>false</code> otherwise
         */
        public bool DoIntersectWith(SegmentD b)
        {
            RectangleD box1 = this.AsBoundingBox();
            RectangleD box2 = b.AsBoundingBox();
            return box1.DoIntersectWith(box2)
                    && IsCrossingOrTouching(b)
                    && b.IsCrossingOrTouching(this);
        }


        /**
         * Check if line segment first touches or crosses the line that is
         * defined by line segment second.
         *
         * @param first line segment interpreted as line
         * @param second line segment
         * @return <code>true</code> if line segment first touches or
         *                           crosses line second,
         *         <code>false</code> otherwise.
         */
        public bool IsCrossingOrTouching(SegmentD s)
        {
            return Contains(s.First)
                || Contains(s.Second)
                || (IsPointRightOfLine(s.First)
                    ^ IsPointRightOfLine(s.Second));
        }

        /**
         * Checks if a Point is on a line
         * @param a line (interpreted as line, although given as line
         *                segment)
         * @param b point
         * @return <code>true</code> if point is on line, otherwise
         *         <code>false</code>
         */
        public bool Contains(PointD p)
        {
            // Move the image, so that a.first is on (0|0)
            var aTmp = new SegmentD(new PointD(0, 0),
                                   new PointD(Second.X - First.X, Second.Y - First.Y));
            PointD bTmp = new PointD(p.X - First.X, p.Y - First.Y);
            double r = aTmp.Second.CrossProduct(bTmp);
            return Math.Abs(r) < Constants.EPSILON;
        }

        /**
         * Checks if a point is right of a line. If the point is on the
         * line, it is not right of the line.
         * @param a line segment interpreted as a line
         * @param b the point
         * @return <code>true</code> if the point is right of the line,
         *         <code>false</code> otherwise
         */
        public bool IsPointRightOfLine(PointD p)
        {
            // Move the image, so that a.first is on (0|0)
            var aTmp = new SegmentD(new PointD(0, 0),
                                   new PointD(Second.X - First.X, Second.Y - First.Y));
            PointD bTmp = new PointD(p.X - First.X, p.Y - First.Y);
            return aTmp.Second.CrossProduct(bTmp) < 0;
        }

        /* You know that lines a and b have an intersection and now you want to get it!
         * Return "null" if line does not intersect, a Point or a Segment
         */
        public IElement GetIntersection(SegmentD s)
        {
             if (!DoIntersectWith(s))
                return null;
            /* the intersection [(p1.X,p1.Y), (p2.X, p2.Y)]
               it might be a line or a single point. If it is a line,
               then x1 = x2 and y1 = p2.Y.  */
            PointD p1 = new PointD(0, 0);
            PointD p2 = p1;

            if (First.X == Second.X)
            {
                // Case (A)
                // As a is a perfect vertical line, it cannot be represented
                // nicely in a mathematical way. But we directly know that
                //
                p1.X = First.X;
                p2.X = p1.X;
                if (s.First.X == s.Second.X)
                {
                    // Case (AA): all x are the same!
                    // Normalize
                    if (First.Y > Second.Y)
                    {
                        return new SegmentD(Second, First).GetIntersection(s);
                    }
                    if (s.First.Y > s.Second.Y)
                    {
                        s = new SegmentD(s.Second, s.First);
                    }
                    if (First.Y > s.First.Y)
                    {
                        return s.GetIntersection(this);
                    }

                    // Now we know that the y-value of a.first is the
                    // lowest of all 4 y values
                    // this means, we are either in case (AAA):
                    //   a: x--------------x
                    //   b:    x---------------x
                    // or in case (AAB)
                    //   a: x--------------x
                    //   b:    x-------x
                    // in both cases:
                    // get the relavant y intervall
                    p1.Y = s.First.Y;
                    p2.Y = Math.Min(Second.Y, s.Second.Y);
                    return new SegmentD(p1, p2);
                }
                else
                {
                    // Case (AB)
                    // we can mathematically represent line b as
                    //     y = m*x + t <=> t = y - m*x
                    // m = (y1-p2.Y)/(p1.X-p2.X)
                    var m = (s.First.Y - s.Second.Y)
                          / (s.First.X - s.Second.X);
                    var t = s.First.Y - m * s.First.X;
                    p1.Y = (float)m * p1.X + t;
                    //p2.Y = p1.Y;
                    return p1;
                }
            }
            else if (s.First.X == s.Second.X)
            {
                // Case (B)
                // essentially the same as Case (AB), but with
                // a and b switched
                p1.X = s.First.X;
                p2.X = p1.X;

                var m = (First.Y - Second.Y)
                      / (First.X - Second.X);
                var t = First.Y - m * First.X;
                p1.Y = m * p1.X + t;
                //p2.Y = p1.Y;
                return p1;
            }
            else
            {
                // Case (C)
                // Both lines can be represented mathematically
                double ma, mb, ta, tb;
                ma = (First.Y - Second.Y) /
                     (First.X - Second.X);
                mb = (s.First.Y - s.Second.Y) /
                     (s.First.X - s.Second.X);
                ta = First.Y - ma * First.X;
                tb = s.First.Y - mb * s.First.X;
                if (ma == mb)
                {
                    // Case (CA)
                    // both lines are in parallel. As we know that they
                    // intersect, the intersection could be a line
                    // when we rotated this, it would be the same situation
                    // as in case (AA)

                    // Normalize
                    if (First.X > Second.X)
                    {
                        return new SegmentD(Second, First).GetIntersection(s);
                    }
                    if (s.First.X > s.Second.X)
                    {
                        s = new SegmentD(s.Second, s.First);
                    }
                    if (First.X > s.First.X)
                    {
                        return s.GetIntersection(this);
                    }

                    // get the relavant x intervall
                    p1.X = s.First.X;
                    p2.X = Math.Min(Second.X, s.Second.X);
                    p1.Y = ma * p1.X + ta;
                    p2.Y = ma * p2.X + ta;
                    return new SegmentD(p1, p2);
                }
                else
                {
                    // Case (CB): only a point as intersection:
                    // y = ma*x+ta
                    // y = mb*x+tb
                    // ma*x + ta = mb*x + tb
                    // (ma-mb)*x = tb - ta
                    // x = (tb - ta)/(ma-mb)
                    p1.X = (tb - ta) / (ma - mb);
                    p1.Y = ma * p1.X + ta;
                    //p2.X = p1.X;
                    //p2.Y = p1.Y;
                    return p1;
                }
            }
        }

    }

}
