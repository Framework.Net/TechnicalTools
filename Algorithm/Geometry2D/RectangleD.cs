﻿using System;
using System.Collections.Generic;
using WinPoint = System.Drawing.Point;


namespace TechnicalTools.Algorithm.Geometry2D
{
    public struct RectangleD : IElement
    {
        public RectangleD(double x, double y, double width, double height) : this()
        {
            Left = x;
            Top = y;
            Width = width;
            Height = height;
        }
        public double Bottom { get { return Top + Height; } }
        public double Height { get; set; }
        public double Left { get; set; }
        public double Right { get { return Left + Width; } }
        public double Top { get; set; }
        public double Width { get; set; }


        /**
         * Check if bounding boxes do intersect. If one bounding box
         * touches the other, they do intersect.
         * @param a first bounding box
         * @param b second bounding box
         * @return <code>true</code> if they intersect,
         *         <code>false</code> otherwise.
         */
        public bool DoIntersectWith(RectangleD b)
        {
            return Left <= b.Right
                && Right >= b.Left
                && Top <= b.Bottom
                && Bottom >= b.Top;
        }

        // Return an empty enumeration (if no intersection) or filled with crossing Point(s) or a Segment
        public IEnumerable<IElement> GetIntersectionsWith(SegmentD s)
        {
            var res1 = s.GetIntersection(new SegmentD(Right - 1, Top, Left, Top)); // ligne du haut
            var res2 = Height == 0 ? null : s.GetIntersection(new SegmentD(Right - 1, Bottom - 1, Right - 1, Top)); // ligne de droite
            var res3 = Width == 0 ? null : s.GetIntersection(new SegmentD(Left, Top, Left, Bottom - 1)); // ligne de gauche
            var res4 = Height == 0 ? null : s.GetIntersection(new SegmentD(Left, Bottom - 1, Right - 1, Bottom - 1)); // ligne du bas

            // TODO : On devrait comparer les element car il peut y en avoir en double
            // Exemple 1 : Si segment passe par un coin, le point d'intersection va etre present deux fois dans la liste
            // Exemple 2 : Si segment recouvre completement la ligne du haut, les deux coins vont etre present deux fois
            return new[] { res1, res2, res3, res4 }.NotNull();
        }
    }

}
