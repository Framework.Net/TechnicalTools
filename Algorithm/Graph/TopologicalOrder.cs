﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TechnicalTools.Algorithm.Graph
{
    public static class TopologicalOrderSimple
    {
        // Renvoie la liste des sommets dans l'ordre topologique.
        // Le noeud racine (celui qui reference tous les autres) se trouve au debut.
        // (ie : Pour inserer ces objets dans une base il faudrait donc commencer par le dernier puis revenir vers le premier)
        // Throw en cas de cycle
        public static List<TNode> DoTopologicalSort<TNode>(TNode root, Func<TNode, IEnumerable<TNode>> getTargetedNodes, bool detectAutoReferencementToo = false)
            where TNode : class
        {
            var nodes = new HashSet<TNode>();
            var todo = new Queue<TNode>();
            todo.Enqueue(root);
            while (todo.Count > 0)
            {
                var n = todo.Dequeue();
                if (nodes.Contains(n))
                    continue;
                nodes.Add(n);
                foreach (var subn in getTargetedNodes(n))
                    todo.Enqueue(subn);
            }
            return DoTopologicalSort(nodes.ToList(), getTargetedNodes, detectAutoReferencementToo);
        }

        // Renvoie la liste des sommets dans l'ordre topologique.
        // Le noeud racine (celui qui reference tous les autres) se trouve au debut.
        // (ie : Pour inserer ces objets dans une base il faudrait donc commencer par le dernier puis revenir vers le premier)
        // Throw en cas de cycle
        public static List<TNode> DoTopologicalSort<TNode>(IEnumerable<TNode> nodes, Func<TNode, IEnumerable<TNode>> getTargetedNodes, bool detectAutoReferencementToo = false)
            where TNode : class
        {
            return DoTopologicalSort(nodes, getTargetedNodes, (from, to, defaultAction) =>
            {
                if (detectAutoReferencementToo && from == to)
                    return;
                defaultAction(from, to);
            });
        }

        static void DefaultCycleBehavior<TNode>(TNode from, TNode to, Action<TNode, TNode> _)
        {
            DefaultCycleBehavior(from, to);
        }
        static void DefaultCycleBehavior<TNode>(TNode from, TNode to)
        {
            throw new HasCycleException<TNode>(from, to);
        }

        // Renvoie la liste des sommets dans l'ordre topologique.
        // Le noeud racine (celui qui reference tous les autres) se trouve au debut.
        // (ie : Pour inserer ces objets dans une base il faudrait donc commencer par le dernier puis revenir vers le premier)
        // Throw en cas de cycle
        public static List<TNode> DoTopologicalSort<TNode>(IEnumerable<TNode> nodes, Func<TNode, IEnumerable<TNode>> getTargetedNodes, Action<TNode, TNode, Action<TNode, TNode>> onCycle)
            where TNode : class
        {
            var result = new List<TNode>();
            var visitStatuses = nodes.ToDictionary(n => n, n => 0); // Le tableau qui indiquera la couleur et donc le traitement d'un sommet.
            // Toutes les états sont initialisés à 0
            // 0 = non traité, 1 = en cours de visite, 2 = visité et traité
            void visit(TNode from, TNode to)
            {
                if (visitStatuses[to] == 1) // if node has a temporary mark then stop (not a DAG)
                    onCycle(from, to, DefaultCycleBehavior);
                else if (visitStatuses[to] == 0) // if node is not marked (i.e. has not been visited yet)
                {
                    visitStatuses[to] = 1;
                    foreach (var x in getTargetedNodes(to))
                        if (x != to) // Auto référencement
                            visit(to, x);
                        else
                            onCycle(to, x, DefaultCycleBehavior);

                    visitStatuses[to] = 2;
                    result.Insert(0, to);
                }
            }

            foreach (var to in nodes)
                if (visitStatuses[to] == 0)
                    visit(null, to);

            return result;
        }
        public class HasCycleException<TNode> : Exception
        {
            TNode From { get; }
            TNode To   { get; }
            public HasCycleException(TNode from, TNode to)
                : base("Graph contains a cycle!")
            {
                From = from;
                To = to;
            }
        }
    }
}
