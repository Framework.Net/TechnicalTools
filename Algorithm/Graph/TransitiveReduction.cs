﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Algorithm.Graph
{
    // From jgrapht Library : jgrapht-core/src/main/java/org/jgrapht/alg/TransitiveReduction.java
    public class TransitiveReduction
    {
        public static readonly TransitiveReduction Instance = new TransitiveReduction();

        private TransitiveReduction() { }


        /**
        * This method will remove all transitive edges from the graph passed as
        * input parameter.
        *
        * <p>
        * You may want to clone the graph before, as transitive edges will be
        * pitilessly removed.
        * <p>
        *
        * <p>
        * e.g.
        *
        * <pre>
        * {
        *     &#64;code
        *     DirectedGraph<V, T> soonToBePrunedDirectedGraph;
        *
        *     TransitiveReduction.INSTANCE.reduce(soonToBePrunedDirectedGraph);
        *
        *     // pruned !
        * }
        * </pre>
        * </p>
        *
        * @param directedGraph
        *            the directed graph that will be reduced transitively
        */
        public void Reduce<V, E>(IReadOnlyCollection<V> vertex, IReadOnlyCollection<E> edges, Func<E, V> getSource, Func<E, V> getTarget, Func<V, IReadOnlyList<E>> getEdges, Action<V, V> removeEdge)
        {
            List<V> vertices = new List<V>(vertex);
            int n = vertices.Count;
            bool[,] originalMatrix = new bool[n, n]; // By default, all bits in the set initially have the value false.'

            // initialize matrix with edges
            foreach (var edge in edges)
            {
                V v1 = getSource(edge);
                V v2 = getTarget(edge);
                int v_1 = vertices.IndexOf(v1);
                int v_2 = vertices.IndexOf(v2);
                originalMatrix[v_1, v_2] = true;
            }
            // create path matrix from original matrix
            bool[,] pathMatrix = originalMatrix;
            TransformToPathMatrix(pathMatrix, n);
            // create reduced matrix from path matrix
            bool[,] transitivelyReducedMatrix = pathMatrix;
            ApplyTransitiveReduction(transitivelyReducedMatrix, n);
            // remove edges from the DirectedGraph which are not in the reduced
            // matrix
            for (int i = n-1; i >= 0; --i)
                for (int j = n - 1; j >= 0; --j)
                    if (!transitivelyReducedMatrix[i, j])
                        removeEdge(vertices[i], vertices[j]);
        }

        /**
        * The matrix passed as input parameter will be transformed into a path
        * matrix.
        *
        * <p>
        * This method is package visible for unit testing, but it is meant as a
        * private method.
        * </p>
        *
        * @param matrix
        *            the original matrix to transform into a path matrix
        *
        */
        static void TransformToPathMatrix(bool[,] matrix, int length)
        {
            // compute path matrix
            for (int i = 0; i < length; i++)
                for (int j = 0; j < length; j++)
                {
                    if (i == j)
                        continue;
                    if (matrix[j,i])
                        for (int k = 0; k < length; k++)
                            if (!matrix[j,k])
                                matrix[j,k]=  matrix[i,k];
                }
        }

        /**
        * The path matrix passed as input parameter will be transformed into a
        * transitively reduced matrix.
        *
        * <p>
        * This method is package visible for unit testing, but it is meant as a
        * private method.
        * </p>
        *
        * @param pathMatrix
        *            the path matrix to reduce
        *
        */
        static void ApplyTransitiveReduction(bool[,] pathMatrix, int length)
        {
            // transitively reduce
            for (int j = 0; j < length; j++)
                for (int i = 0; i < length; i++)
                    if (pathMatrix[i, j])
                        for (int k = 0; k < length; k++)
                            if (pathMatrix[j, k])
                                pathMatrix[i, k] = false;
        }
    }
}
