﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TechnicalTools.Algorithm.Graph.TopologicalOrder
{
    public class StronglyConnectedComponentList<T> : IEnumerable<StronglyConnectedComponent<T>>
    {
        readonly LinkedList<StronglyConnectedComponent<T>> _collection;

        public StronglyConnectedComponentList()
        {
            this._collection = new LinkedList<StronglyConnectedComponent<T>>();
        }

        public StronglyConnectedComponentList(IEnumerable<StronglyConnectedComponent<T>> collection)
        {
            this._collection = new LinkedList<StronglyConnectedComponent<T>>(collection);
        }

        public void Add(StronglyConnectedComponent<T> scc)
        {
            this._collection.AddLast(scc);
        }

        public int Count
        {
            get
            {
                return this._collection.Count;
            }
        }

        public IEnumerator<StronglyConnectedComponent<T>> GetEnumerator()
        {
            return this._collection.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this._collection.GetEnumerator();
        }

        public IEnumerable<StronglyConnectedComponent<T>> IndependentComponents()
        {
            return this.Where(c => !c.IsCycle);
        }

        public IEnumerable<StronglyConnectedComponent<T>> Cycles()
        {
            return this.Where(c => c.IsCycle);
        }
    }
}