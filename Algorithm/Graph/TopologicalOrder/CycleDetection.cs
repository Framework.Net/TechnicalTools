﻿using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools;


namespace TechnicalTools.Algorithm.Graph.TopologicalOrder
{
    public static class CycleDetection
    {
        public static string GetCycleAsString<TNode, TEdge>(IReadOnlyCollection<TNode> nodes,
                                                            Func<TNode, string> getNodeName,
                                                            Func<TNode, IEnumerable<TEdge>> getEdges,
                                                            Func<TEdge, TNode> getTarget,
                                                            Func<TEdge, string> getEdgeName)
        {
            var components = GetCyclesInSubGraph(nodes, getEdges, getTarget);
            string report = components.Cycles().Select(compo => compo.OrderBy(v => v.Index).Select(vertex => getNodeName(vertex.Value) + "(" + vertex.Index + ")").Join(" --> ")).Join(Environment.NewLine);

            //foreach (var component in components)
            //{
            //    if (component.IsCycle)
            //    {
            //        var components2 = GetCyclesInSubGraph(component.Select(v => v.Value).ToList(), getEdges, getTarget);
            //        string report2 = components.Cycles().Select(compo => compo.Select(vertex => vertex.Value.Name).Join(" --> ")).Join(Environment.NewLine);
            //        MessageBox.Show(report2);
            //    }
            //}
            return report;
        }

        static StronglyConnectedComponentList<TNode> GetCyclesInSubGraph<TNode, TEdge>(IReadOnlyCollection<TNode> nodes,
                                                                                       Func<TNode, IEnumerable<TEdge>> getEdges,
                                                                                       Func<TEdge, TNode> getTarget)
        {
            var g = nodes.ToDictionary(t => t, t => new Vertex<TNode>(t));
            foreach (var t in nodes)
                foreach (var fk in getEdges(t))
                {
                    var from = getTarget(fk);
                    var to = getTarget(fk);
                    if (nodes.Contains(to))
                        g[from].Dependencies.Add(g[to]);
                }
            var detector = new StronglyConnectedComponentFinder<TNode>();
            var components = detector.DetectCycle(g.Values);
            return components;
        }
    }
}
