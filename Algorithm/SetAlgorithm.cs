﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace TechnicalTools.Algorithm
{
    public static class SetAlgorithm
    {
        // This method is equivalent to EnumerateSubSetsOf(source, true, true, EnumerateSetDefinitionNaturally(source.Count))
        public static IEnumerable<List<T>> EnumerateSubSetsOfViaLinq<T>(IEnumerable<T> source)
        {
            return _EnumerateSubSetsOfViaLinq(source.Reverse());
        }
        static IEnumerable<List<T>> _EnumerateSubSetsOfViaLinq<T>(IEnumerable<T> source)
        {
            if (!source.Any())
                return new[] { new List<T>() };

            var element = source.Take(1);

            var haveNots = _EnumerateSubSetsOfViaLinq(source.Skip(1));
            var haves = haveNots.Select(set => set.Concat(element).ToList());

            return haveNots.Concat(haves);
        }

        public static IEnumerable<List<T>> EnumerateSubSetsOf<T>(List<T> source, bool withEmpty = false, bool withFullSet = false, IEnumerable<int> setDefinitionEnumerator = null)
        {
            setDefinitionEnumerator = setDefinitionEnumerator ?? EnumerateSetDefinitionNaturally(source.Count);

            if (source.Count > 16)
                throw new Exception("Too many item in set, please reconsider your algorithm.");
            if (withEmpty)
                yield return new List<T>(0);

            foreach (var setDefinition in setDefinitionEnumerator)
            {
                var set = new List<T>(source.Count);
                int p = 1;
                int i = 0;
                while (p < ushort.MaxValue)
                {
                    if ((setDefinition & p) != 0)
                        set.Add(source[i]);
                    ++i;
                    p *= 2;
                }
                yield return set;
            }
            if (withFullSet)
                yield return source;
        }

        public static IEnumerable<int> EnumerateSetDefinitionNaturally(int setCardinality)
        {
            if (setCardinality == 0)
                return new int[0] { };
            return Enumerable.Range(1, (int)Math.Pow(2, setCardinality) - 1 /* empty set */ - 1 /* full set */);
        }
        public static IEnumerable<int> EnumerateSetDefinitionPrioritizingFirst(int setCardinality)
        {
            if (setCardinality == 0)
                return new int[0] { };
            return Enumerable.Range(1, (int)Math.Pow(2, setCardinality) - 1 /* empty set */ - 1 /* full set */)
                             .OrderBy(n => ((n ^ (n - 1)) + 1)) // Index Of Least Significant Bit Set To 1
                             .ThenBy(n => (int)Math.Log(n, 2)) // Max Power of 2
                             .ThenByDescending(n => (~n ^ (~n - 1)) + 1) // // Index Of Least Significant Bit Set To 0
                             .ThenByDescending(n => n); // prioritize the set that consumes a lot of items
        }

        public static string ShowSetDefinitionEnumerator(IEnumerable<int> setDefinitionEnumerator)
        {
            return string.Join(Environment.NewLine,
                               setDefinitionEnumerator.Select(n => n.ToString().PadLeft(5, '0') +
                                                                   " : " + Convert.ToString(n, 2).PadLeft(15, '0')));
        }


        public static void RunTests()
        {
            var letters = new List<char> { 'A', 'B', 'C', 'D' };
            var subsets1 = SetAlgorithm.EnumerateSubSetsOf(letters, true, true).ToList();
            var subsets2 = SetAlgorithm.EnumerateSubSetsOfViaLinq(letters).ToList();
            Debug.Assert(subsets1.Count == subsets2.Count);
            for (int i = 0; i < subsets1.Count; ++i)
            {
                Debug.Assert(subsets1[i].Count == subsets2[i].Count);
                Debug.Assert(subsets1[i].SequenceEqual(subsets2[i]));
            }
        }

    }
}
