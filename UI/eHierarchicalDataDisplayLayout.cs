﻿using System;


namespace TechnicalTools.UI
{
    /// <summary>
    /// Define different ways to display data where item are organized in a hierarchy with multiple levels
    /// </summary>
    public enum eHierarchicalDataDisplayLayout
    {
        /// <summary> Operations are shown without any hierarchy on the same level </summary>
        Flat,
        /// <summary> Operations are shown under their parent when they have a parent operation, even if parent operation does not match the search</summary>
        ParentHierarchy,
        /// <summary> Like ParentHierarchy but hierarchies are full loaded (all (grand) children of all operations visible with ParentHierarchy)</summary>
        FullHierarchy
    }
}
