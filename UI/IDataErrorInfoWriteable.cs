﻿using System;
using System.ComponentModel;


namespace TechnicalTools.UI
{
    public interface IDataErrorInfoWriteable : IDataErrorInfo
    {
        new string this[string columnName] { get; set; }
    }
}
