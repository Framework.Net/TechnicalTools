﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;


namespace TechnicalTools
{
    public class BackgroundUITask
    {
        #region Attente sur un control

        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// Le resultat de l'action prenant du temps est passe en argument au callback quand tout s'est bien terminé.
        /// </summary>
        /// <typeparam name="TOutData">Type de la valeur retourné par l'action prenant du temps</typeparam>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="description">Petite description sous le petit texte</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void LoadDataOnControl<TOutData>(Control ctl_to_block, string caption, string description, Func<TOutData> actionTakingTime, Action<TOutData> async_callback, Action<Exception> async_finally = null)
        {
            // TODO : Utiliser http://www.c-sharpcorner.com/UploadFile/Nildo%20Soares%20de%20Araujo/TransparentControls11152005074108AM/TransparentControls.aspx
            //   Pour faire une truc transparent potable !
            // serControlWaitForm.ShowWaitForm(parent_ctl); // ne fonctionne pas, je fais donc moi même
            var pnl = new Task(ctl_to_block);
            if (ctl_to_block != null)
            {
                pnl.MaskLayer.Caption = caption;
                pnl.MaskLayer.Description = description ?? "Please wait...";
            }

            Form relatedForm = null;

            var context = SynchronizationContext.Current;
            var main_culture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();

#if SUPER_DEBUG
            System.Diagnostics.StackTrace dbgStack = null;
            if (DebugTools.CaptureStackTraceOnDeferredEvents)
            {
                dbgStack = new System.Diagnostics.StackTrace(true);
            }
#endif

            // Doc sur les background worker : http://msdn.microsoft.com/en-us/library/cc221403(v=vs.95).aspx
            var bw = CreateBackgroundWorker(caption);
            //bw.WorkerSupportsCancellation = true;
            //bw.WorkerReportsProgress = true; // Le report se fait en faisant bw.ReportProgress de 0 à 100 (le zero est peut etre interdit);
            bw.DoWork += (object _, DoWorkEventArgs e) =>
            {
                // Recopie la même culture que le thread principale
                Thread.CurrentThread.CurrentCulture = main_culture;
                e.Result = actionTakingTime();
            };
            bw.RunWorkerCompleted += (object _, RunWorkerCompletedEventArgs e) =>
            {
                // Fait disparaitre la form d'attente
                if (ctl_to_block != null)
                    context.Post(__ =>
                    {
                        relatedForm = pnl.MaskLayer.ParentForm;
                        if (pnl.MaskLayer.ParentForm != null && !pnl.MaskLayer.ParentForm.IsDisposed)
                            pnl.MakeDisappear();
                    }, null);

                if (async_callback != null && e.Error == null)
                {
                    // Indique a l'appelant qu'on a le résultat du calcul
                    context.Post(__ =>
                    {
                        if (ctl_to_block == null ||
                            relatedForm != null && !relatedForm.IsDisposed/* && async_callback != null*/)
                            async_callback((TOutData)e.Result);
                    }, null);
                }

                if (async_finally != null)
                {
                    context.Post(__ =>
                    {
                        if (ctl_to_block == null ||
                            relatedForm != null && !relatedForm.IsDisposed)
                            async_finally(e.Error);
                    }, null);
                }
            };
            bw.RunWorkerAsync();
        }
        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// Le resultat de l'action prenant du temps est passe en argument au callback quand tout s'est bien terminé.
        /// </summary>
        /// <typeparam name="TOutData">Type de la valeur retourné par l'action prenant du temps</typeparam>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void LoadDataOnControl<TOutData>(Control ctl_to_block, string caption, Func<TOutData> actionTakingTime, Action<TOutData> async_callback, Action<Exception> async_finally = null)
        {
            LoadDataOnControl(ctl_to_block, caption, null, actionTakingTime, async_callback, async_finally);
        }
        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// </summary>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void LoadDataOnControl(Control ctl_to_block, string caption, Action actionTakingTime, Action async_callback, Action<Exception> async_finally = null)
        {
            LoadDataOnControl(ctl_to_block, caption,
                              () => { actionTakingTime(); return true; },
                              async_callback == null ? (Action<bool>)null : dummy_result => async_callback(),
                              async_finally);
        }
        /// <summary>
        /// Bloque l'acces a un control en le recouvrant (temporairement) d'un voile transparent gris
        /// comprenant en son milieu un petit texte, tout en executant, dans un autre thread, une action prenant du temps.
        /// Finalement, le voile disparait et les autres callbacks sont appelées dans le thread UI courant
        /// </summary>
        /// <param name="ctl_to_block">Controle a bloquer</param>
        /// <param name="caption">Petit texte a afficher au milieu du voile gris qui bloque le control</param>
        /// <param name="description">Petite description sous le petit texte</param>
        /// <param name="actionTakingTime">Action (methode ou lambda) qui prend du temps.</param>
        /// <param name="async_callback">Action a appeler une fois que l'action s'est terminée (sans planter)</param>
        /// <param name="async_finally">Action appelé en tout dernier, dans tous les cas avec un argument Exception non null si il y a eu une exeption</param>
        public static void LoadDataOnControl(Control ctl_to_block, string caption, string description, Action actionTakingTime, Action async_callback, Action<Exception> async_finally = null)
        {
            LoadDataOnControl(ctl_to_block, caption, description,
                              () => { actionTakingTime(); return true; },
                              async_callback == null ? (Action<bool>)null : dummy_result => { async_callback(); },
                              async_finally);
        }
        #endregion Attente sur un control

        public interface IWaitLayer
        {
            string  Caption                 { get; set; }
            string  Description             { get; set; }
            Bitmap  Background              { get; set; } // Draw this image to simulate transparent background
            void    SetMaskedControl(Control masked_control);
            double  ProgressionInPercent    { get; set; }

            #region already implemented by usercontrol
            bool    Visible                 { get; set; }
            Control Parent                  { get; set; }
            Form    ParentForm              { get; }
            Point   Location                { get; set; }
            Size    Size                    { get; set; }

            void BringToFront();
            void Invalidate();
            #endregion already implemented by usercontrol
        }

        public static          Func<IWaitLayer> CreateWaitLayer { get; set; }
        public static readonly Func<IWaitLayer> CreateDefaultWaitLayer = () => new DefaultWaitLayer();
        public static double   BackgroundTransparency = 0.75;

        static BackgroundUITask()
        {
            CreateWaitLayer = CreateDefaultWaitLayer;
        }

        public static Func<string, IBackgroundWorker> CreateBackgroundWorker = name => new DefaultBackgroundWorker();
        class Task
        {
                   Control          CtlToAttach     { get; set; }
                   ContainerControl CtlToAttachForm { get; set; }
            public IWaitLayer       MaskLayer       { get; private set; }

            internal Task(Control ctl_to_attach)
            {
                CtlToAttach = ctl_to_attach;

                if (CtlToAttach == null) return;

                MaskLayer = CreateWaitLayer();
                MaskLayer.SetMaskedControl(ctl_to_attach);

                MaskLayer.Caption = "";
                MaskLayer.Description = "";

                //_background = new Bitmap(CtlToAttach.Size.Width, CtlToAttach.Size.Height);
                //CtlToAttach.DrawToBitmap(_background, new Rectangle(Point.Empty, CtlToAttach.Size));

                CtlToAttach.Move += CtlToAttach_Move;
                CtlToAttach.Resize += CtlToAttach_Resize;
                CtlToAttach.VisibleChanged += CtlToAttach_VisibleChanged;

                SetPositionAndSizeToCoverParentControl();
                RefreshBackgroundImage();

                MaskLayer.Visible = CtlToAttach.Visible;
            }

            public void MakeDisappear()
            {
                if (CtlToAttach == null) return;

                MaskLayer.Visible = false;
                MaskLayer.Parent = null;
                CtlToAttach.Move -= CtlToAttach_Move;
                CtlToAttach.Resize -= CtlToAttach_Resize;
                CtlToAttach.VisibleChanged -= CtlToAttach_VisibleChanged;
                CtlToAttach = null;
                CtlToAttachForm = null;
            }

            void SetPositionAndSizeToCoverParentControl()
            {
                if (CtlToAttach == null || CtlToAttach.Disposing) return;

                CtlToAttachForm = CtlToAttach.FindForm();// ?? CtlToAttach as ContainerControl;
                MaskLayer.Parent = CtlToAttachForm;
                MaskLayer.Location = MaskLayer.ParentForm.PointToClient(CtlToAttach.PointToScreen(new Point(0, 0)));
                MaskLayer.Size = CtlToAttach.Size; // appelle ReplaceProgressPanel() via l'event Resize;

                MaskLayer.BringToFront();
            }

            void RefreshBackgroundImage()
            {
                if (CtlToAttach == null) return;

                Bitmap backgroundImage = null;
                ExceptionManager.Instance.IgnoreException(() => backgroundImage = GetControlImage(CtlToAttach));
                if (backgroundImage == null)
                    return;
                MaskLayer.Size = backgroundImage.Size;
                MaskLayer.Background = backgroundImage;

                var g = Graphics.FromImage(MaskLayer.Background);

                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
                using (var br = new SolidBrush(Color.FromArgb((int)(BackgroundTransparency * 256), CtlToAttach.BackColor.R, CtlToAttach.BackColor.G, CtlToAttach.BackColor.B)))
                    g.FillRectangle(br, new Rectangle(Point.Empty, MaskLayer.Size));

                // Important sinon seul le fond du progresspanel montre la nouvelle image
                MaskLayer.Invalidate();
            }


            private Bitmap GetControlImage(Control ctl)
            {
                var size = new Size(Math.Max(1, ctl.Size.Width), Math.Max(1, ctl.Size.Height));
                var bm = new Bitmap(size.Width, size.Height);
                // Quand Size est de taille 1 x 1, on obtient ici une exception de type
                // System.ArgumentException: Parameter is not valid
                // Cette erreur viendrait des profondeur de GDI+ ... Aucune idée de pourquoi !
                // Les erreurs GDI sont réputées pour ne pas etre precise

                ctl.DrawToBitmap(bm, new Rectangle(Point.Empty, size));

                if (!(ctl is Form frm))
                    return bm;

                // Si c'est une form, on ne garde que le contenu de la form
                if (frm.ClientSize == frm.Size) // Form sans bord ou docke dans un DocumentManager (devexpress)
                    return bm;

                Point origin = frm.PointToScreen(new Point(0, 0));
                int dx = origin.X - frm.Left;
                int dy = origin.Y - frm.Top;

                // Copy the client area into a new Bitmap.
                int wid = Math.Max(1, frm.ClientSize.Width);
                int hgt = Math.Max(1, frm.ClientSize.Height); // Lors de la fermeture d'un ecran (event FormClosing) il est arrivé que cette valeur soit égale à -12 !
                var clientAreaImage = new Bitmap(wid, hgt);
                using (Graphics gr = Graphics.FromImage(clientAreaImage))
                    gr.DrawImage(bm, 0, 0, new Rectangle(dx, dy, wid, hgt), GraphicsUnit.Pixel);

                bm.Dispose();
                return clientAreaImage;
            }


            void CtlToAttach_Resize(object sender, EventArgs e)
            {
                if (CtlToAttach.Disposing) return;
                SetPositionAndSizeToCoverParentControl();
                RefreshBackgroundImage();
            }

            void CtlToAttach_Move(object sender, EventArgs e)
            {
                if (CtlToAttach.Disposing) return;
                SetPositionAndSizeToCoverParentControl();
            }

            void CtlToAttach_VisibleChanged(object sender, EventArgs e)
            {
                MaskLayer.Visible = CtlToAttach.Visible;
            }
        }


        class DefaultWaitLayer : IWaitLayer
        {
            public string Caption               { get; set; }
            public string Description           { get; set; }
            public Bitmap Background            { get; set; }
            public double ProgressionInPercent  { get; set; }

            public void SetMaskedControl(Control masked_control) { _masked_control = masked_control; }
            Control _masked_control;

            public bool    Visible      { get { return _masked_control.Enabled; } set { _masked_control.Enabled = value; } }

            public Control Parent       { get; set; }
            public Form    ParentForm   { get { return null; } }
            public Point   Location     { get; set; }
            public Size    Size         { get; set; }

            void IWaitLayer.BringToFront() { /* Nothing */ }
            void IWaitLayer.Invalidate()   { /* Nothing */ }
        }


        public interface IBackgroundWorker
        {
            event DoWorkEventHandler DoWork;
            event RunWorkerCompletedEventHandler RunWorkerCompleted;
            void RunWorkerAsync();
        }

        public class DefaultBackgroundWorker : BackgroundWorker, IBackgroundWorker
        {
        }
    }
}
