﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace TechnicalTools.Tools
{
    public static class SqlConnectionTracer
    {
        public static SqlConnection Create(string connectionString)
        {
            var con = new SqlConnection(connectionString);
            con.StateChange += Con_StateChange;
            con.Disposed += Con_Disposed;
            return con;
        }




        static void Con_StateChange(object sender, StateChangeEventArgs e)
        {
            var con = sender as SqlConnection;
            Debug.Assert(e.CurrentState != ConnectionState.Fetching);
            if (e.CurrentState == ConnectionState.Open ||
                e.CurrentState == ConnectionState.Executing)
            {
                //Debug.WriteLine("SqlConnection " + con.ClientConnectionId + " : StateChanged from " + e.OriginalState + " to " + e.CurrentState);
                _ConnectionsKnown.TryAdd(con, con.ClientConnectionId);
            }
            else
            {
                // we use clientConnectionId because ClientConnectionId of connection return Guid.Empty currently
                _ConnectionsKnown.TryRemove(con, out var _);
                //Debug.WriteLine("SqlConnection " + clientConnectionId + " : StateChanged from " + e.OriginalState + " to " + e.CurrentState);
            }
        }

        public static Dictionary<string, List<SqlConnection>> GetReport()
        {
            return _ConnectionsKnown.GroupBy(con => con.Key.ConnectionString)
                                    .ToDictionary(grp => grp.Key, grp => grp.Select(kvp => kvp.Key).ToList());
        }

        static readonly ConcurrentDictionary<SqlConnection, Guid> _ConnectionsKnown = new ConcurrentDictionary<SqlConnection, Guid>();
        private static void Con_Disposed(object sender, EventArgs e)
        {
            var con = sender as SqlConnection;
            con.StateChange -= Con_StateChange;
            con.Disposed -= Con_Disposed;
            //Debug.WriteLine("SqlConnection " + _ConnectionsKnown[con] + " : Disposed!");
            //Debug.Assert(!_ConnectionsKnown.ContainsKey(con));
        }

    }
}
