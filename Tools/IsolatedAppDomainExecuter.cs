﻿using System;


namespace TechnicalTools.Tools
{
    // From https://bitlush.com/blog/executing-code-in-a-separate-application-domain-using-c-sharp
    public sealed class IsolatedAppDomainExecuter<T> : IDisposable
        where T : MarshalByRefObject
    {
        private AppDomain _domain;

        public IsolatedAppDomainExecuter()
        {
            _domain = AppDomain.CreateDomain("Isolated:" + Guid.NewGuid(),
               null, AppDomain.CurrentDomain.SetupInformation);

            Type type = typeof(T);

            Value = (T)_domain.CreateInstanceAndUnwrap(type.Assembly.FullName, type.FullName);
        }

        public T Value { get; }

        public void Dispose()
        {
            if (_domain != null)
            {
                AppDomain.Unload(_domain);

                _domain = null;
            }
        }
    }
}
