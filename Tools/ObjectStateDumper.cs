﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using TechnicalTools.Model.Cache;

namespace TechnicalTools.Tools
{
    public static class ObjectStateDumper
    {
        public static string Dump(object obj, bool multiline = true, string indent = "", string indentElt = "\t", int maxRecursion = 0)
        {
            if (obj == null)
                return "<NULL>";

            FieldInfo[] fields = fieldsFor.GetOrAdd(obj.GetType(), t => t.GetFields());
            return _Dump(obj, multiline, indent, indentElt, fields, maxRecursion);
        }
        static readonly ConcurrentDictionary<Type, FieldInfo[]> fieldsFor = new ConcurrentDictionary<Type, FieldInfo[]>();

        public static string DumpInterface<TInterface>(TInterface obj, bool multiline = true, string indent = "", string indentElt = "\t", int maxRecursion = 0)
            where TInterface : class
        {
            if (obj == null)
                return "<NULL>";

            PropertyInfo[] props = propertiesFor.GetOrAdd(typeof(TInterface), t => t.GetProperties());
            return _Dump(obj, multiline, indent, indentElt, props, maxRecursion);
        }
        static readonly ConcurrentDictionary<Type, PropertyInfo[]> propertiesFor = new ConcurrentDictionary<Type, PropertyInfo[]>();
        static string _Dump(object obj, bool multiline, string indent, string indentElt, IEnumerable<MemberInfo> mis, int maxRecursion)
        {
            var sb = new StringBuilder();
            sb.Append(indent);
            sb.Append("{");
            if (multiline)
                sb.AppendLine();
            else
                sb.Append(" ");
            foreach (var mi in mis)
            {
                if (multiline)
                    sb.Append(indent);
                sb.Append(mi.Name);
                sb.Append(": ");
                var mt = MemberTypeOf(mi);
                object value;
                try { value = GetValueOf(mi, obj); }
                catch (Exception ex)
                {
                    if (ex is TargetInvocationException)
                        ex = ex.InnerException;
                    value = "Exception getting value: " + ex.ToString();
                }
                if (value is IHasTypedIdReadable)
                    value = (value as IHasTypedIdReadable).TypedId;

                if (value == null)
                    sb.Append("<NULL>");
                else if (value == DBNull.Value)
                    sb.Append("<DBNULL>");
                else if (value is string)
                {
                    sb.Append("\"");
                    sb.Append(((string)value).Replace("\\", "\\\\").Replace("\r", @"\r").Replace("\n", @"\n").Replace("\"", "\\\""));
                    sb.Append("\"");
                }
                else if (value is ITypedId)
                {
                    sb.Append((value as ITypedId).AsDebugString);
                }
                else if (value.GetType().IsValueType)
                {
                    sb.Append(value.ToStringInvariant());
                }
                else if (maxRecursion > 0)
                    sb.Append(Dump(value, multiline, multiline ? indent + indentElt : indent, indentElt, maxRecursion - 1));
                else
                    sb.Append(value.ToStringInvariant().Replace(Environment.NewLine, Environment.NewLine + indent));

                if (multiline)
                    sb.AppendLine();
                else
                    sb.Append(", ");
            }
            if (multiline)
                sb.Append(indent);
            else
                sb.Append(" ");
            sb.Append("}");
            return sb.ToString();
        }


        static object GetValueOf(MemberInfo mi, object obj)
        {
            if (mi is FieldInfo)
                return (mi as FieldInfo).GetValue(obj);
            else if (mi is PropertyInfo)
                return (mi as PropertyInfo).GetValue(obj);
            throw new ArgumentException("mi not handled", mi.GetType().Name);
        }
        static Type MemberTypeOf(MemberInfo mi)
        {
            if (mi is FieldInfo)
                return (mi as FieldInfo).FieldType;
            else if (mi is PropertyInfo)
                return (mi as PropertyInfo).PropertyType;
            throw new ArgumentException("mi not handled", mi.GetType().Name);
        }
    }
}
