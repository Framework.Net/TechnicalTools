﻿using System;
using System.Collections.Generic;


namespace TechnicalTools.Tools
{
    /// <summary>
    /// Basic owner/dependency relationship for threaded micro service.
    /// </summary>
    /// <remarks>
    /// Please note that each dependency returned by <see cref="GetDependencies"/> has "this" stored as a owner.
    /// But each owner that called Addowner does not return "this" when calling <see cref="GetDependencies"/> on it.
    /// </remarks>
    public interface ITinyService : IMultipleTimeOwnable
    {
        /// <summary>
        /// Indicate the service is running or in pause.
        /// When false, new item/request wont' be handled or queued , but the queued one, will be treated.
        /// </summary>
        bool Enabled    { get; }
        /// <summary>
        /// Indicate the service must only queue new items/requests and because a resource is unavailable to treat them.
        /// When <see cref="IsStopping"/> is true, setting value to false makes the queue to be discard and the service to exits.
        /// </summary>
        bool IsReady    { get; }

        /// <summary>
        /// Indicate the service is stopping. Queued items will be treated before stopping except if <see cref="IsReady"/> is to false.
        /// </summary>
        bool IsStopping { get; }

        /// <summary>
        /// Minimum time (in ms) between items/requests handling. This value is to optimize performance by bulk processing items instead of treating them one by one.
        /// This is particularly efficient for database operations because when the first request occurs, the service wait for a duration defined by this property,
        /// to collect multiple request and save them together in database
        /// </summary>
        TimeSpan MinDeferredTime { get; }

        /// <summary> In case items/requests handling fails, this is the time to wait before trying again.</summary>
        TimeSpan RetryInterval { get; }

        /// <summary>
        /// Other services the service depends on.
        /// When current service has dependencies, it adds itself as owner to them (see <see cref="AddOwner(object)" />).
        /// And it releases them when stopped.
        /// </summary>
        List<ITinyService> GetDependencies();
    }
}
