﻿using System;
using System.Collections.Generic;
using System.Threading;


namespace TechnicalTools.Tools
{
    // Inspired from  http://stackoverflow.com/a/1656662
    public class TaskQueue<T> : IDisposable
        where T : class
    {
        readonly string _poolName;
        readonly object locker = new object();
        readonly Thread[] workers;
        readonly Queue<T> taskQ = new Queue<T>();
        readonly Action<T> _thConsume;

        public TaskQueue(string poolName, int workerCount, Action<T> thConsume)
        {
            _poolName = poolName;
            workers = new Thread[workerCount];
            _thConsume = thConsume;

            // Create and start a separate thread for each worker
            for (int i = 0; i < workerCount; i++)
            {
                workers[i] = new Thread(Consume)
                {
                    Name = _poolName + " " + (i + 1)
                };
                workers[i].Start();
            }
        }

        public void Dispose()
        {
            if (Disposed)
                return;
            // Enqueue one null task per worker to make each exit.
            foreach (Thread worker in workers)
                _EnqueueTask(null);
            Disposed = true;
            foreach (Thread worker in workers)
                worker.Join();
        }
        public bool Disposed { get; private set; }

        public void EnqueueTask(T task)
        {
            if (Disposed)
                throw new InvalidOperationException("Task queue cannot handle new request, because it has been disposed!");
            if (task == null)
                throw new ArgumentNullException("task"); // reservé pour usage interne (cf Dispose)
            _EnqueueTask(task);
        }

        void _EnqueueTask(T task)
        {
            lock (locker)
            {
                taskQ.Enqueue(task);
                Monitor.PulseAll(locker);
            }
        }

        void Consume()
        {
            while (true)
            {
                T task;
                lock (locker)
                {
                    while (taskQ.Count == 0) Monitor.Wait(locker);
                    task = taskQ.Dequeue();
                }
                if (task == null) // This signals our exit
                    return;
                _thConsume(task);
            }
        }
    }
}
