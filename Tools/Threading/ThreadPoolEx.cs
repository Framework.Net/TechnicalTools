﻿using System;

using System.Windows.Forms;


namespace TechnicalTools.Tools.Threading
{
    /// <summary>
    /// See base class
    /// </summary>
    public class ThreadPoolEx : TechnicalTools.Tools.Threading.ThreadPool
    {
        public ThreadPoolEx(string name)
            : base(name)
        {
            Application.ApplicationExit += (_, __) => Dispose();
        }
    }
}
