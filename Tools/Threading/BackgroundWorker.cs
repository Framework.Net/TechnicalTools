﻿using System;
using System.ComponentModel;
using System.Threading;


namespace TechnicalTools.Tools.Threading
{
    // Reprend le même comportement qu'un BackGroundWorker classique, à l'exception que le pool utilise est celui definit dans TechnicalTools.
    // Cela rend possible la configuration des threads au sein d'un pool.
    public class BackgroundWorker : Component
    {
        public string                       Name { get; set; }

        bool                                _cancellationPending;
        bool                                _isRunning;
        AsyncOperation                      _asyncOperation;
        readonly SendOrPostCallback         _operationCompleted;
        readonly SendOrPostCallback         _progressReporter;

        public BackgroundWorker(string name)
        {
            Name = name;

            WorkerSupportsCancellation = false;
            WorkerReportsProgress = false;

            _operationCompleted = AsyncOperationCompleted;
            _progressReporter = ProgressReporter;
        }

        private void AsyncOperationCompleted(object arg)
        {
            _isRunning = false;
            _cancellationPending = false;
            OnRunWorkerCompleted((RunWorkerCompletedEventArgs)arg);
        }

        [Browsable(false)]
        public bool CancellationPending
        {
            get { return _cancellationPending; }
        }

        public virtual void CancelAsync()
        {
            if (!WorkerSupportsCancellation)
                throw new InvalidOperationException("Background worker doesn't support cancellation!");

            _cancellationPending = true;
        }

        public event DoWorkEventHandler DoWork;

        public bool IsBusy { get { return _isRunning; } }

        protected virtual void OnDoWork(DoWorkEventArgs e)
        {
            DoWork?.Invoke(this, e);
        }

        protected virtual void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
        {
            RunWorkerCompleted?.Invoke(this, e);
        }

        protected virtual void OnProgressChanged(ProgressChangedEventArgs e)
        {
            ProgressChanged?.Invoke(this, e);
        }

        public event ProgressChangedEventHandler ProgressChanged;

        private void ProgressReporter(object arg)
        {
            OnProgressChanged((ProgressChangedEventArgs)arg);
        }

        public void ReportProgress(int percentProgress)
        {
            ReportProgress(percentProgress, null);
        }

        public void ReportProgress(int percentProgress, object userState)
        {
            if (!WorkerReportsProgress)
                throw new InvalidOperationException("BackgroundWorker doesn't report progress!");

            var args = new ProgressChangedEventArgs(percentProgress, userState);

            if (_asyncOperation != null)
                _asyncOperation.Post(_progressReporter, args);
            else
                _progressReporter(args);
        }

        public void RunWorkerAsync()
        {
            RunWorkerAsync(null);
        }

        public void RunWorkerAsync(object argument)
        {
            if (_isRunning)
                throw new InvalidOperationException("BackgroundWorker is already running!");

            _isRunning = true;
            _cancellationPending = false;

            _asyncOperation = AsyncOperationManager.CreateOperation(null);
            ThreadInfo = GetPool().Start(Name, () =>
                {
                    try
                    {
                        WorkerThreadStart(argument);
                    }
                    finally
                    {
                        ThreadInfo = null;
                    }
                });
        }

        protected volatile ThreadPool.IThreadInfo ThreadInfo;

        protected virtual ThreadPool GetPool() { return _pool; }
        static readonly ThreadPool _pool = new ThreadPool("BackgroundWorker pool");

        public event RunWorkerCompletedEventHandler RunWorkerCompleted;

        [DefaultValue(false)]
        public bool WorkerReportsProgress { get; set; }

        [DefaultValue(false)]
        public bool WorkerSupportsCancellation { get; set; }



        private void WorkerThreadStart(object argument)
        {
            object workerResult = null;
            Exception error = null;
            bool cancelled = false;

            try
            {
                var doWorkArgs = new DoWorkEventArgs(argument);
                OnDoWork(doWorkArgs);
                if (doWorkArgs.Cancel)
                    cancelled = true;
                else
                    workerResult = doWorkArgs.Result;
            }
            catch (Exception exception)
            {
                error = exception;
            }

            var e = new RunWorkerCompletedEventArgs(workerResult, error, cancelled);

            _asyncOperation.PostOperationCompleted(_operationCompleted, e);
        }

    }
}
