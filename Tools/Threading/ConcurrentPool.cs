﻿using System;
using System.Collections.Concurrent;



namespace TechnicalTools.Tools.Threading
{
    public class ConcurrentPool<T>
        where T : class
    {
        readonly ConcurrentBag<T> _objects;
        readonly Func<T>          _objectGenerator;

        public ConcurrentPool(Func<T> objectGenerator)
        {
            _objects = new ConcurrentBag<T>();
            _objectGenerator = objectGenerator ?? throw new ArgumentNullException(string.Format("Constructor for type {0} must be provided!", typeof(T).Name));
        }

        public T TryGetInstance()
        {
            if (_objects.TryTake(out T item))
                return item;
            return null;
        }

        public T GetOrCreateInstance()
        {
            if (_objects.TryTake(out T item))
                return item;
            return _objectGenerator();
        }

        public void PutInstance(T item)
        {
            _objects.Add(item);
        }

        public int Count { get { return _objects.Count; } }
    }
}
