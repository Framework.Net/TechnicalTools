﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using StdThread = System.Threading.Thread;

namespace TechnicalTools.Tools.Threading
{
    public class Thread : INotifyPropertyChanged
    {
        public static bool RunAsyncCodeSynchronously { get; set; }
        public static ApartmentState DefaultApartmentState { get; set; } = ApartmentState.Unknown;

        public Thread               Parent            { get { return _Parent; } private set { _Parent = value; _PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Parent")); } } Thread _Parent;
        public StdThread            InnerThread       { get; private set; }
        public static List<Thread>  AllThreads        { get { return ThreadsByStdThreads.Values.ToList(); } }
        public static Thread        CurrentThread     { get { return ThreadsByStdThreads[StdThread.CurrentThread]; } }
        public static string        CurrentThreadName { get { return ThreadsByStdThreads.ContainsKey(StdThread.CurrentThread) ? CurrentThread.Name : System.Threading.Thread.CurrentThread.Name; } }



        public static event Action<Thread> ThreadCreated;
        public static event Action<Thread> ThreadDone;
        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged { add { _PropertyChanged += value; } remove { _PropertyChanged -= value; } }
        event PropertyChangedEventHandler _PropertyChanged;

        public static TimeSpan? WaitTimeBeforeThreadEnd { get; set; }

        static Thread()
        {
            // Initialise l'encapsulation du thread principal
            // On part de l'hypothèse que le thread principal est le thread courant quand le premier TechnicalTools.Tools.Threading.Thread est créé
            // Cela permet de rendre les données publiques "complètes"
            var mainThread = new Thread()
                {
                    InnerThread = System.Threading.Thread.CurrentThread,
                    TaskId = 0,
                    LastTaskStartTime = DateTime.Now // pas tout a fait vrai mais s'approche de la verité
                };
            ThreadsByStdThreads.TryAdd(mainThread.InnerThread, mainThread);
        }
        public static void ForceInitialization()
        {
            // Rien, juste pour que le constructeur static s'exécute
        }

        static readonly ConcurrentDictionary<StdThread, Thread> ThreadsByStdThreads = new ConcurrentDictionary<StdThread, Thread>();

        // Le framework n'autorise pas de changer le nom du thread :(, on le stocke donc dans une variable temporaire
        public string Name
        {
            get { return _name ?? InnerThread.Name; }
            set { _name = value; _PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name")); }
        }
        string _name;

        public ThreadPriority Priority
        {
            get { return InnerThread.Priority; }
            set { InnerThread.Priority = value; }
        }

        public bool IsBackground
        {
            get { return InnerThread.IsBackground; }
            set { InnerThread.IsBackground = value; }
        }

        public bool IsAlive { get { return InnerThread.IsAlive; } }

        private Thread()
        {
        }

        public Thread(string name, ThreadStart start, ThreadPriority priority = ThreadPriority.BelowNormal)
            : this(name, start, priority, false)
        {
        }
        public Thread(string name, ParameterizedThreadStart start, ThreadPriority priority = ThreadPriority.BelowNormal)
            : this(name, start, priority, false)
        {
        }

        internal Thread(string name, ThreadStart              start, ThreadPriority priority, bool fromThreadPool) : this(name, _ => start(), priority, fromThreadPool) { ObjectTracker.Instance.Track(this); }
        internal Thread(string name, ParameterizedThreadStart start, ThreadPriority priority, bool fromThreadPool)
            : this()
        {
            var task = fromThreadPool ? start : PrepareAsThreadTask(name, start, false);

            _start = obj =>
                {
                    Log("Started");
                    try
                    {
                        task(obj);
                        Log("Done");
                    }
                    catch (Exception ex)
                    {
                        Log("Aborted by unrecoverable exception " + ex.Message);
                    }
                    finally
                    {
                        ThreadsByStdThreads.TryRemove(InnerThread, out var dummy);
                        Debug.Assert(dummy != null);
                        ThreadDone?.Invoke(this);
                        if (WaitTimeBeforeThreadEnd.HasValue)
                            Sleep((int)WaitTimeBeforeThreadEnd.Value.TotalMilliseconds);
                    }
                };
            InnerThread = new StdThread(_start)
                {
                    Name = name,
                    Priority = priority
                };
            InnerThread.SetApartmentState(DefaultApartmentState);
            ThreadsByStdThreads.TryAdd(InnerThread, this);
            ThreadCreated?.Invoke(this);
        }
        readonly ParameterizedThreadStart _start;

        public void Start()
        {
            Start(null);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public void Start(object parameter)
        {
            if (RunAsyncCodeSynchronously)
                _start(parameter);
            else
                InnerThread.Start();
        }

        public bool Join(int millisecondsTimeout)
        {
            Log("Joining...");
            var res = InnerThread.Join(millisecondsTimeout);
            Log("Joined!");
            return res;
        }
        public void Join()
        {
            Log("Joining...");
            InnerThread.Join();
            Log("Joined!");
        }

        [Obsolete("Utiliser Abort est une TRÈS TRÈS TRÈS mauvaise idée !" +
                  "Faites les choses proprement : Prévoyez un flag dans votre code, que le thread va régulierement checker")]
        public void Abort()
        {
            Log("Aborting...");
            // Balance une exception de type ThreadAbortException instoppable,
            // Voir UnexpectedExceptionManager.OnNewUnhandledException pour plus de détail
            InnerThread.Abort();
        }

        public void SetApartmentState(ApartmentState state)
        {
            InnerThread.SetApartmentState(state);
        }
        public ApartmentState GetApartmentState()
        {
            return InnerThread.GetApartmentState();
        }

        public static void Sleep(int millisecondsTimeout)
        {
            System.Threading.Thread.Sleep(millisecondsTimeout);
        }

        [Conditional("DEBUG")]
        void Log(string str)
        {
            _log.Debug(string.Format("Id={0}, Name={1}{2}: ", InnerThread.ManagedThreadId, Name, TaskId.HasValue ? ", Task " + TaskId : "") + str);
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(Thread));



        #region Partie Task

        public int?       TaskId    { get { return _TaskId;    } private set { _TaskId    = value; _PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TaskId"));    } } int?       _TaskId;
        public eTaskState TaskState { get { return _TaskState; } private set { _TaskState = value; _PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TaskState")); } } eTaskState _TaskState;

        public static TimeSpan? WaitTimeAfterEachTask { get; set; }
        public static bool CaptureTaskRunLocation { get; set; }
        // Pour debug uniquement, ralenti les thread entre chaque tache pour pouvoir les voir dans le thread manager
        //public static TimeSpan? WaitTimeAfterEachTask { get; set; }


        public System.Threading.ThreadState ThreadState
        {
            get { return InnerThread.ThreadState; }
        }

        public DateTime? LastTaskStartTime { get; private set; }
        public TimeSpan? LastTaskDuration { get; private set; }

        public StackTrace RunLocation { get; private set; }

        internal ParameterizedThreadStart PrepareAsThreadTask(string task_name, ParameterizedThreadStart task, bool forPooling) // je suis pas en train de réinventer les Tasks la ?!
        {
            // Met a jour son parent
            ThreadsByStdThreads.TryGetValue(StdThread.CurrentThread, out Thread parent);
            Debug.Assert(parent != null, "Essayer d'appeler ForceInitialization, dans le thread principale au demarrage de l'application." + Environment.NewLine +
                                         "Si c'est déjà fait alors le fait que cet assertion fails est très surprenant... A étudier !");
            Parent = parent;
            // Génère un nouvelId pour debugger. L'id du thread ne suffit pas car un meme thread peut servir à plusieurs tache (ex : Threadpool)
            TaskId = Interlocked.Increment(ref _taskIdSeed);
            TaskState = eTaskState.NoTask;
            LastTaskStartTime = null;
            LastTaskDuration = null;
            // ReSharper disable RedundantAssignment
            return obj =>
            {
                if (CaptureTaskRunLocation)
                    RunLocation = new StackTrace(1, true);
                string original_name = Name;
                Log("Started");
                try
                {
                    Name = task_name;
                    TaskState = eTaskState.Running;
                    // ne throw que pour les exceptions non gerable/catchable
                    var ex = ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                    {
                        LastTaskStartTime = DateTime.Now;
                        try
                        {
                            task(obj);
                        }
                        finally
                        {
                            LastTaskDuration = DateTime.Now - LastTaskStartTime;
                        }
                    });
                    if (ex == null)
                    {
                        TaskState = eTaskState.Done;
                        Log("Done (in " + LastTaskDuration.Value.ToHumanReadableShortNotation() + ")");
                    }
                    else
                    {
                        TaskState = eTaskState.Failed;
                        Log("Failed with error: " + Environment.NewLine + ExceptionManager.Instance.Format(ex));
                    }
                }
                catch (Exception ex)
                {
                    TaskState = eTaskState.Failed;
                    Log("Failed with uncatchable error: " + Environment.NewLine + ExceptionManager.Instance.Format(ex));
                    // Etant donné que c'est une exception non gérable, l'exception va continuer à remonter
                }
                finally
                {
                    if (forPooling && WaitTimeAfterEachTask.HasValue)
                        System.Threading.Thread.Sleep((int)WaitTimeAfterEachTask.Value.TotalMilliseconds);
                    Name = original_name;
                    TaskId = null;
                    Parent = null;
                }
            };
            // ReSharper restore RedundantAssignment
        }
        static int _taskIdSeed;

        #endregion

    }

    public enum eTaskState
    {
        [Description("")]
        NoTask = 0,
        Running,
        Done,
        [Description("Cancel by developper")]
        Cancel,
        [Description("Failed with error")]
        Failed
    }
}
