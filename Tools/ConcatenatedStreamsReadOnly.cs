﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;


namespace TechnicalTools.Tools
{
    public class ConcatenatedStreamsReadOnly : Stream
    {
        public IReadOnlyList<Stream> Streams { get; }

        // Force at least one stream to exist so it make all code simpler
        public ConcatenatedStreamsReadOnly(Stream firstStream, params Stream[] otherStreams)
            : this(firstStream, (IEnumerable<Stream>)otherStreams) { }
        public ConcatenatedStreamsReadOnly(Stream firstStream, IEnumerable<Stream> otherStreams)
        {
            if (firstStream == null)
                throw new ArgumentNullException(nameof(firstStream));
            if (!firstStream.CanRead)
                throw new ArgumentOutOfRangeException(nameof(firstStream), "Stream is not readable!");
            if (!otherStreams.All(s => s.CanRead))
                throw new ArgumentOutOfRangeException(nameof(otherStreams), "A stream is not readable!");
            otherStreams = otherStreams ?? Enumerable.Empty<Stream>();
            // Read all streams to ensure the value returned by properties of this are consistent.
            // This also allow user to seek from last stream to first (or intermediate)
            Streams = new[] { firstStream }.Concat(otherStreams).ToList();
            _currentStream = firstStream;
            _relativePosition = firstStream.Position;
            _globalPosition = _relativePosition;
            _streamIndex = 0;
            _immutableLength = Length; // Test Length does not throw
        }
        readonly long _immutableLength;
        Stream _currentStream;
        int _streamIndex; // index in Streams
        long _relativePosition; // inside currentstream
        long _globalPosition; // Position through all streams, not only _currentStream, so _GlobalPosition can be >> _currentStream.Length



        // Implementation of these properties are made this way, but they ARE considered immutable.
        // if they are not immutable it would means stream are mutable in size seek would be not consistentso their value is kind of immutable if underlying streams are too
        // We don't fix their value though in readonly backing field just in case some fools want to do crazy stuff...
        public override bool CanRead => Streams.All(s => s.CanRead);
        public override bool CanSeek => Streams.All(s => s.CanSeek);
        public override bool CanWrite => false;
        // Because we allow user to write on any underlying stream too, this value may change
        public override long Length => Streams.Select(s => s.Length).DefaultIfEmpty(0).Sum();
        public override void SetLength(long value) { throw new InvalidOperationException("This stream is readonly!"); }

        public override long Position
        {
            get { return _globalPosition; }
            set { Seek(value - _globalPosition, SeekOrigin.Current); }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            // Here we cannot guarantee all the streams (except the one that will become the current) are well configured
            // to be usable by other methods (ex: Read). So these methods have to Seek when current stream change
            // offset is seen as a global offset that spread over all other streams

            Debug.Assert(Length == _immutableLength);

            if (origin == SeekOrigin.End)
                offset = _immutableLength - offset;
            else if (origin == SeekOrigin.Current)
                offset = _globalPosition + offset;
            else if (origin != SeekOrigin.Begin)
                throw new ArgumentException("Invalid SeekOrigin");
            if (offset < 0 || offset > _immutableLength)
                throw new ArgumentOutOfRangeException(nameof(offset));
            long globalPosition = 0;
            for (int i = 0; i < Streams.Count; ++i)
            {
                var s = Streams[i];
                if (offset > s.Length ||
                    offset == s.Length && i < Streams.Count)
                {
                    offset -= Streams[i].Length;
                    globalPosition += Streams[i].Length;
                    ++i;
                }
                else
                {
                    var offsetToDo = offset - s.Position;
                    var realOffset = s.Seek(offsetToDo, SeekOrigin.Current);
                    globalPosition += realOffset; // realOffset and offsetToDo can be different

                    _relativePosition = realOffset;
                    _globalPosition = globalPosition;
                    _currentStream = s;
                    _streamIndex = i;
                    return _globalPosition;
                }
            }
            throw new InvalidOperationException("Invalid state exception");
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            bool keepReading;
            int totalRead = 0;
            // This loop is not mandatory.
            // It is just because we want to be nice with user and flattenize the streams.
            // It makes _possible_ to read all the content of all the streams in one call to Read
            do
            {
                int read;
                // End of stream met: Can happend if length of first stream(s) is/are zero!
                while (true) // this loop is to skip all empty stream or stream where position is at the end
                {
                    if (_currentStream.Position != _relativePosition) // one of the return below does not guarantee this ocnditions is always falsebecause Seek method does not guarantee this
                    {
                        var rp = _currentStream.Seek(_relativePosition, SeekOrigin.Begin);
                        if (rp != _relativePosition)
                            if (totalRead > 0) // we have read something at least! So we return it, Next time seek will have to work or throw
                                return totalRead;
                            else // On other hand if we have read nothing we throw an exception
                                throw new Exception("One underlying stream is not seekable!");
                    }
                    read = _currentStream.Read(buffer, offset, count);
                    if (read != 0)
                        break;
                    // We have read all in _currentStream
                    if (_streamIndex + 1 >= Streams.Count)
                    {
                        Debug.Assert(_globalPosition == Length, "One or many underlying streams seem mutable, this is not consistently handled by this class!");
                        return totalRead;
                    }
                    ++_streamIndex;
                    _currentStream = Streams[_streamIndex];
                    // At this point this value is what we want the stream"s position to be, it maybe not the case
                    // the sync is done at next loop
                    _relativePosition = 0;
                }
                totalRead += read;
                _relativePosition += read;
                offset += read;
                count -= read;
                keepReading = count > 0; // user still want to read
                // and we are at the end of a current stream
                // (so this is NOT the current stream that is blocked for any reason)
                keepReading &= _relativePosition == _currentStream.Length;
                _globalPosition += read;
            } while (keepReading);
            return totalRead;
        }
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException("This stream is readonly!");
        }

        public override void Flush()
        {
            foreach (var str in Streams)
                str.Flush();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                foreach (var str in Streams)
                    str.Dispose();
            base.Dispose(disposing);
        }
    }
}
