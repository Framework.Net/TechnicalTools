﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;

using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;


namespace TechnicalTools.Tools
{
    public class SqlRetry
    {
        public int MaxTry { get; set; } = 10;

        public static SqlRetry Instance        { get { return ThreadInstance ?? DefaultInstance; } }
        public static SqlRetry DefaultInstance { get; protected set; } = new SqlRetry();
        public static SqlRetry ThreadInstance  { get { return _threadInstances.Value; } set { _threadInstances.Value = value; } }
        static readonly ThreadLocal<SqlRetry> _threadInstances = new ThreadLocal<SqlRetry>(() => null);

        public SqlRetry() { }

        protected static readonly ILogger _log = LogManager.Default.GetLogger(typeof(SqlRetry));

        // Permet d'encapsuler l'execution d'une requete afin de fournir un moyen globalde gérer les problemes récurrent et independant de la requete
        // Les évènements ci-dessous se déclencheront pour chaque cas ((exemple les deconnexion, les timeouts, ...)
        // Voir aussi https://github.com/App-vNext/Polly si la gestion se complexifie.
        public T HandleRetry<T>(SqlCommand cmd, bool canRetry, Func<T> process)
        {
            int tries = 0;
            //cmd.CommandTimeout = 30; // inutile c'est deja la valeur par defaut
            while (true)
            {
                try
                {
                    return process();
                }
                catch (Exception ex) when (ShouldRetry(ex, ref tries, canRetry, null, cmd))
                {
                    LogAndWait(ex, tries, null, cmd);
                    if (cmd != null && cmd.Connection.State != ConnectionState.Open)
                        cmd.Connection.OpenOrWaitUntilOpen();
                    continue;
                }
            }
        }

        // Permet d'encapsuler l'execution d'une requete afin de fournir un moyen globalde gérer les problemes récurrent et independant de la requete
        // Les évènements ci-dessous se déclencheront pour chaque cas ((exemple les deconnexion, les timeouts, ...)
        // Voir aussi https://github.com/App-vNext/Polly si la gestion se complexifie.
        public T HandleRetry<T>(string actionName, bool canRetry, Func<T> process)
        {
            int tries = 0;
            //cmd.CommandTimeout = 30; // inutile c'est deja la valeur par defaut
            while (true)
            {
                try
                {
                    return process();
                }
                catch (Exception ex) when (ShouldRetry(ex, ref tries, canRetry, actionName, null))
                {
                    LogAndWait(ex, tries, actionName, null);
                    continue;
                }
            }
        }

        public virtual bool ShouldRetry(Exception ex, ref int tries, bool canRetry, string actionName, SqlCommand cmd)
        {
            if (!canRetry)
                return false;
            var errorCategory = GetErrorCategory(ex, cmd);
            if (errorCategory != eNetworkIssue.TooBad && tries++ < MaxTry)
                return Retried?.Invoke(ex, errorCategory) ?? true;
            if (tries > 1)
                // We do not log here because exception is not catched and swallowed.
                // It will be responsability of calling code (outside this class).
                // We just add metadata...
                ex.EnrichDiagnosticWith($"Application has retried {tries} times " + (actionName == null ? " this database operation" : "this action \"" + actionName + "\"") + "! See your logs for more information.", eExceptionEnrichmentType.UserUnderstandable);
            return false;
        }

        public event Func<Exception, eNetworkIssue, bool> Retried;

        protected virtual void LogAndWait(Exception ex, int tries, string actionName, SqlCommand cmd)
        {
            var sqlEx = ex as SqlException;
            var waitTime = tries * 2;
            _log.Warn($"Will retry (n° {tries} / {MaxTry}), in {waitTime} seconds, "
                     + (actionName != null ? $"operation \"{actionName}\"" : "a database operation")
                     + " that threw exception HResult:" + ex.HResult
                     + (sqlEx == null ? "" : ", Number:" + sqlEx.Number)
                     + " with Message: " + ExceptionManager.Instance.Format(ex));
            System.Threading.Thread.Sleep(2000);
        }

        public void OpenOrWaitUntilOpen(DbConnection con)
        {
            int tries = 0;
            do
                try
                {
                    con.Open();
                    return;
                }
                catch (Exception ex) when (ShouldRetry(ex, ref tries, true, "Opening connection to " + GetConnectionStringWithoutsecurityData(con), null))
                {
                    LogAndWait(ex, tries, "Opening connection to " + GetConnectionStringWithoutsecurityData(con), null);
                    continue;
                }
            while (true);
        }
        protected static string GetConnectionStringWithoutsecurityData(DbConnection con)
        {
            var b = new DbConnectionStringBuilder
            {
                ConnectionString = con.ConnectionString
            };
            // Remove are not case sensitive
            b.Remove("password");// For Sql
            b.Remove("pwd"); // For oracle (not sure)
            return b.ConnectionString;
        }

        public enum eNetworkIssue
        {
            TooBad = 0, // unknown or too bad error to ignore it
            Disconnection, // Disconnection before any side effect occurs
            Interruption // Interuption while an operation is in progress, leaving state in almost unknown state
        }

        protected virtual eNetworkIssue GetErrorCategory(Exception ex, SqlCommand cmd)
        {
            // TODO : voir dans System.Data.SqlClient.TdsEnums les codes...

            // When Open method take too long to get a connection from pool, we just try again
            // Message: Le délai d'attente s'est écoulé avant obtention d'une connexion du pool. Ceci est probablement dû au fait que toutes les connexions regroupées sont en cours d'utilisation et que la taille maximale du pool a été atteinte.
            if (ex.HResult == -2146233079 && ex is InvalidOperationException)
                return ex.Message.Contains(" ConnectionString ")
                     ? eNetworkIssue.TooBad  // Message : "La propriété ConnectionString n'a pas été initialisée."
                     : eNetworkIssue.Disconnection;

            if (!(ex is SqlException sqlex)) // We handle only SqlException now
                return eNetworkIssue.TooBad;

            int? newTimeOut = null;
            if (sqlex.HResult == -2146232060)
            {
                // Detection of TimeOut case
                // Le délai d'attente pour la connexion expiré. Le délai d'attente s'est écoulé lors de la tentative de consommation de l'accusé réception de la poignée de main de préconnexion. Cela peut être dû à un échec de la poignée de main de préconnexion ou au fait que le serveur n'a pas pu répondre dans les temps.  Le temps écoulé lors de la tentative de connexion à ce serveur était de - [Préconnexion] initialisation=3069; poignée de main=161128; )
                if (sqlex.Number == -2)
                {
                    if (cmd != null)
                    {
                        newTimeOut = ReconnectAfterTimeOut(ex, cmd.CommandText) ? Math.Max(cmd.CommandTimeout * 2, 500) : (int?)null;
                        if (newTimeOut.HasValue)
                            cmd.CommandTimeout = newTimeOut.Value;
                    }
                    return eNetworkIssue.Interruption;
                }
            }
            if (sqlex.Number == -1 || sqlex.Number == 1236) // Detection of Disconnection case
            {
                if (cmd != null)
                {
                    newTimeOut = ReconnectAfterLostConnection(ex, cmd.CommandText) ? cmd.CommandTimeout : (int?)null;
                    if (newTimeOut.HasValue)
                        cmd.CommandTimeout = newTimeOut.Value;
                }
                return eNetworkIssue.Interruption;
            }

            if (sqlex.HResult == -2146232060) // this code is also when cable is disconnected, this is why we check on message too !
            {
                if (sqlex.Number == 10054 ||
                    (sqlex.Number == 258 && // "Une connexion a été établie avec le serveur, mais une erreur s'est ensuite produite pendant la négociation préalable à l'ouverture de session. (provider: SSL Provider, error: 0 - Dépassement du délai d’attente.)"
                    !ex.Message.Contains("The server was not found or was not accessible.")))
                    return eNetworkIssue.Disconnection;

                // A network-related or instance-specific error occurred while establishing a connection to SQL Server. The server was not found or was not accessible. Verify that the instance name is correct and that SQL Server is configured to allow remote connections. (provider: Named Pipes Provider, error: 40 - Could not open a connection to SQL Server)
                if (sqlex.Number == 53)
                    return eNetworkIssue.Disconnection;
            }

            return eNetworkIssue.TooBad;
        }

        protected bool ReconnectAfterLostConnection(Exception ex, string sqlQuery)
        {
            var e = new SqlQueryIssueEventArgs(true, ex, sqlQuery);
            OnDisconnect?.Invoke(null, e);
            return !e.Cancel;
        }

        protected bool ReconnectAfterTimeOut(Exception ex, string sqlQuery)
        {
            var e = new SqlQueryIssueEventArgs(true, ex, sqlQuery);
            OnTimeOut?.Invoke(null, e);
            return !e.Cancel;
        }

        public class SqlQueryIssueEventArgs : EventArgs
        {
            public string    Query     { get; protected set; }
            public bool      Cancel    { get; set; }
            public Exception Exception { get; protected set; }

            public SqlQueryIssueEventArgs(bool cancel, Exception exception, string query)
            {
                Query = query;
                Cancel = cancel;
                Exception = exception;
            }
        }

        public event EventHandler<SqlQueryIssueEventArgs> OnDisconnect;
        public event EventHandler<SqlQueryIssueEventArgs> OnTimeOut;


        /// <summary>
        /// This method is just intended for testing purpose!
        /// </summary>
        /// <returns>An instance ready to be thrown</returns>
        public static SqlException CreateSqlException(int number, string message = "Test error message (don't rely on it)")
        { // inspired from http://blog.gauffin.org/2014/08/how-to-create-a-sqlexception/
            var privateInstance = BindingFlags.NonPublic | BindingFlags.Instance;
            var collectionConstructor = typeof(SqlErrorCollection)
                                          .GetConstructor(privateInstance, null, Type.EmptyTypes, null);
            var addMethod = typeof(SqlErrorCollection).GetMethod("Add", privateInstance);
            var errorCollection = (SqlErrorCollection)collectionConstructor.Invoke(null);
            var errorConstructor = typeof(SqlError).GetConstructor(privateInstance, null, SqlErrorConstructorTypeArray, null);
            var error = errorConstructor.Invoke(new object[] { number, (byte)0, (byte)0, "server", "errMsg", "proccedure", 100, (uint)0 });
            addMethod.Invoke(errorCollection, new[] { error });
            var constructor = typeof(SqlException).GetConstructor(privateInstance, null, SqlExceptionConstructorTypeArray, null);
            return (SqlException)constructor.Invoke(new object[] { message, errorCollection, new DataException(), Guid.NewGuid() });
        }
        static readonly Type[] SqlErrorConstructorTypeArray = new[]
              {
                  typeof (int), typeof (byte), typeof (byte), typeof (string), typeof(string), typeof (string),
                  typeof (int), typeof (uint)
              };
        static readonly Type[] SqlExceptionConstructorTypeArray = new[] { typeof(string), typeof(SqlErrorCollection), typeof(Exception), typeof(Guid) };
    }

}
