﻿using System;
using System.IO;


namespace TechnicalTools.Tools
{
    public class TruncatedStreamReadOnly : Stream
    {
        public Stream UnderlyingStream { get; }

        public TruncatedStreamReadOnly(Stream stream, long truncatedLength)
        {
            if (!stream.CanRead)
                throw new ArgumentOutOfRangeException(nameof(stream), "Stream is not readable!");
            if (stream.Length < truncatedLength)
                truncatedLength = stream.Length;
            _immutableRealLength = stream.Length; // For testing purpose
            _availableLength = truncatedLength;
            UnderlyingStream = stream;
        }
        readonly long _immutableRealLength;
        long _availableLength;

        // Implementation of these properties are made this way, but they ARE considered immutable.
        // if they are not immutable it would means stream are mutable in size seek would be not consistentso their value is kind of immutable if underlying streams are too
        // We don't fix their value though in readonly backing field just in case some fools want to do crazy stuff...
        public override bool CanRead => UnderlyingStream.CanRead;
        public override bool CanSeek => UnderlyingStream.CanSeek;
        public override bool CanWrite => UnderlyingStream.CanSeek;
        public override long Length => Math.Min(UnderlyingStream.Length, _availableLength);
        public override void SetLength(long value) { UnderlyingStream.SetLength(value); _availableLength = value; }
        public override long Position
        {
            get { return Math.Min(UnderlyingStream.Position, _availableLength) ; }
            set { UnderlyingStream.Position = Math.Min(_availableLength, value); }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            // offset is seen as a global offset that spread over all other streams
            if (origin == SeekOrigin.Begin)
                offset = Math.Min(_availableLength, offset);
            else if (origin == SeekOrigin.End)
                offset = _availableLength - UnderlyingStream.Length
                             + offset;
            else if (origin == SeekOrigin.Current)
                if (offset >= 0)
                    offset = Math.Min(_availableLength - Position, offset);
                else
                    offset = Math.Max(-Position, offset);
            return UnderlyingStream.Seek(offset, origin);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            count = Math.Min(count, (int)(Length - Position));
            return UnderlyingStream.Read(buffer, offset, count);
        }
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException();
        }

        public override void Flush()
        {
            UnderlyingStream.Flush();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                UnderlyingStream.Dispose();
            base.Dispose(disposing);
        }
    }
}
