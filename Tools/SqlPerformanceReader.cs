﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using TechnicalTools;

namespace TechnicalTools.Tools
{
    // Class from https://msdn.microsoft.com/en-us/library/ms254503(v=vs.110).aspx
    public class SqlPerformanceReader
    {
        readonly List<PerformanceCounter> _perfCounters = new List<PerformanceCounter>();

        public SqlPerformanceReader()
        {
            SetUpPerformanceCounters();
        }

        private enum ADO_Net_Performance_Counters
        {
            NumberOfActiveConnectionPools,
            NumberOfReclaimedConnections,
            HardConnectsPerSecond,
            HardDisconnectsPerSecond,
            NumberOfActiveConnectionPoolGroups,
            NumberOfInactiveConnectionPoolGroups,
            NumberOfInactiveConnectionPools,
            NumberOfNonPooledConnections,
            NumberOfPooledConnections,
            NumberOfStasisConnections,
            // The following performance counters are more expensive to track.
            // Enable ConnectionPoolPerformanceCounterDetail in your config file.
            // in app.Config (inside <configuration>):
            //<system.diagnostics>
            //  <switches>
            //    <add name="ConnectionPoolPerformanceCounterDetail" value="4" />
            //  </switches>
            //</system.diagnostics>
                 SoftConnectsPerSecond,
                 SoftDisconnectsPerSecond,
                 NumberOfActiveConnections,
                 NumberOfFreeConnections
        }
        private static readonly TraceSwitch logSwitch = new TraceSwitch("ConnectionPoolPerformanceCounterDetail", "This is your logLevelSwitch in the config file");

        private void SetUpPerformanceCounters()
        {
            logSwitch.Level = TraceLevel.Verbose;
            bool hasSwitch = true;
            _perfCounters.Clear();
            string instanceName = GetInstanceName();
            Type apc = typeof(ADO_Net_Performance_Counters);
            foreach (var v in Enum.GetValues(apc).Cast<ADO_Net_Performance_Counters>())
                if (hasSwitch || !v.In(ADO_Net_Performance_Counters.SoftConnectsPerSecond,
                                       ADO_Net_Performance_Counters.SoftDisconnectsPerSecond,
                                       ADO_Net_Performance_Counters.NumberOfActiveConnections,
                                       ADO_Net_Performance_Counters.NumberOfFreeConnections))
                _perfCounters.Add(new PerformanceCounter()
                {
                    CategoryName = ".NET Data Provider for SqlServer",
                    CounterName = v.ToString(),
                    InstanceName = instanceName
                });
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern int GetCurrentProcessId();

        string GetInstanceName()
        {
            //This works for Winforms apps.
            string instanceName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name; // TODO : pas mieux avec GetEntryAssembly().Manifest.Name ?

            // For ASP.NET applications your instanceName will be your CurrentDomain's
            // FriendlyName. Replace the line above that sets the instanceName with this:
            // instanceName = AppDomain.CurrentDomain.FriendlyName.ToString().Replace('(','[')
            // .Replace(')',']').Replace('#','_').Replace('/','_').Replace('\\','_');

            string pid = GetCurrentProcessId().ToString();
            instanceName = instanceName + "[" + pid + "]";
            //Console.WriteLine("Instance Name: {0}", instanceName);
            //Console.WriteLine("---------------------------");
            return instanceName;
        }

        public Dictionary<string, float> GetReport()
        {
            var res = new Dictionary<string, float>();
            foreach (var p in _perfCounters)
                res.Add(p.CounterName, p.NextValue());
            return res;
        }
    }
}
