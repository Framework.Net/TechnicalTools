﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using TechnicalTools.Diagnostics;


namespace TechnicalTools.Tools
{
    // TODO : De grande idée d'amelioration (complexe cependant) ici :
    // http://stackoverflow.com/questions/1786985/get-active-references-to-an-object
    public class ObjectTracker
    {
        public static readonly ObjectTracker Instance = new ObjectTracker();

        private ObjectTracker() { } // Singleton

        public void Track<T>(T obj)
            where T : class
        {
            Debug.Assert(obj != null);
            lock (_typesMet)
            {
                ICounter cnt;
                //if (typeof(T).IsAbstract)
                cnt = GetCounterByTypeOrCreate(obj.GetType());
                //else
                //    cnt = GetCounterByTypeOrCreate<T>();

                lock (cnt)
                    cnt.Add(obj);
            }
        }

        /// <summary>
        /// Arrête le suivi d'un objet.
        /// Note importante : Il n'est pas nécéssaire d'appeler cette methode si Track a été appelé.
        /// Le Garbage Collector peut quand même collecter les objets suivis. Leur suivi s'arretera à ce moment là.
        /// </summary>
        /// <param name="obj">Object à suivre</param>
        public void Untrack<T>(T obj)
            where T : class
        {
            Debug.Assert(obj != null);
            lock (_typesMet)
            {
                Counter<T> cnt = GetCounterByTypeOrCreate<T>();
                lock (cnt)
                    cnt.Remove(obj);
            }
        }

        public IEnumerable<ICounterReadOnly> Counters
        {
            get
            {
                lock (_typesMet)
                    return _typesMet.Values.ToList();
            }
        }

        #region Private

        ICounter GetCounterByTypeOrCreate(Type type)
        {
            ICounter cnt;
            if (!_typesMet.TryGetValue(type, out cnt))
            {
                var gType = typeof (Counter<>).MakeGenericType(new[] {type});
                var cons = DefaultObjectFactory.ConstructorFor(gType);
                cnt = (ICounter)cons();
                _typesMet.Add(type, cnt);
            }
            return cnt;
        }

        Counter<T> GetCounterByTypeOrCreate<T>()
            where T : class
        {
            var type = typeof (T);
            ICounter cnt;
            if (!_typesMet.TryGetValue(type, out cnt))
            {
                cnt = new Counter<T>();
                _typesMet.Add(type, cnt);
            }
            return (Counter<T>) cnt;
        }

        static readonly Dictionary<Type, ICounter> _typesMet = new Dictionary<Type, ICounter>();

        public interface ICounterReadOnly
        {
            Type Type { get; }
            uint Count { get; }
            List<object> GetInstances();
            void Clean();
        }

        internal interface ICounter : ICounterReadOnly
        {
            void Add(object obj);
        }

        #endregion Private

        public class Counter<T> : ICounter
            where T : class
        {
            public Type Type
            {
                get { return typeof (T); }
            }

            public uint Count
            {
                get { return (uint)GetInstances().Count; }
            }

            public void Add(T obj)
            {
                Debug.Assert(obj != null);
                lock (_lock)
                {
                    WeakReference wr;
                    if (!_dico.TryGetValue(obj, out wr))
                    {
                        wr = new WeakReference(obj);
                        _dico.Add(obj, wr);
                        _instances.AddLast(wr);
                        //if (_instances.Capacity == _instances.Count)
                        //    Clean();
                    }
                    else
                    {
                        DebugTools.Break("Object already added!");
                    }
                }
            }
            void ICounter.Add(object obj)
            {
                Add((T)obj);
            }
            public void Remove(T obj)
            {
                Debug.Assert(obj != null);
                lock (_lock)
                {
                    WeakReference reference;
                    if (_dico.TryGetValue(obj, out reference))
                    {
                        reference.Target = null;
                        _dico.Remove(obj);
                    }
                }
            }

            /// <summary>
            /// Retourne la liste de tous les objets encore vivants.
            /// Note @Dev : Instances retourne un tableau car cela force la copie de tous les éléments. On evite donc de melanger iterateur (yield return) + lock
            /// On résout le probleme de la même manière que le framework déclenche un event : les subscribers (handler) sont copiés dans une liste temporaire puis on itere dessus.
            /// </summary>
            /// <returns></returns>
            public List<T> GetInstances()
            {
                lock (_lock)
                    return GetCleaningEnumerator<T>().ToList();
            }

            #region Private

            readonly object _lock = new object();
            readonly ConditionalWeakTable<T, WeakReference> _dico = new ConditionalWeakTable<T, WeakReference>();
            readonly LinkedList<WeakReference> _instances = new LinkedList<WeakReference>(); // List(500)


            List<object> ICounterReadOnly.GetInstances()
            {
                lock (_lock)
                    return GetCleaningEnumerator<object>().ToList();
            }

            public void Clean()
            {
                lock (_lock)
                {
                    var node = _instances.First;
                    while (node != null)
                    {
                        var next = node.Next;
                        if (!node.Value.IsAlive)
                            _instances.Remove(node);
                        node = next;
                    }
                }
                //_instances.RemoveAll(weak => !weak.IsAlive);
            }

            /// <summary>
            /// ATTENTION ! Enumerateur privée seulement. Ne pas passer en public.
            /// La valeur enumerable renvoyée doit _TOUJOURS_ être completement parcouru avant qu'une methode public ne retourne.
            /// On peut donc appeler ToList ou ToArray avant de retourner.
            /// </summary>
            /// <returns>Un énumerateur privé qui ne doit jamais être retourné en dehors de la classe</returns>
            IEnumerable<Q> GetCleaningEnumerator<Q>()
                where Q : class
            {
                //// Debug.Assert(_lock must be locked !)

                var node = _instances.First;
                while (node != null)
                {
                    var next = node.Next;
                    var q = node.Value.Target as Q;
                    if (q == null)
                        _instances.Remove(node);
                    else
                        yield return q;
                    node = next;
                }

                //int i = _instances.Count;
                //while (--i > 0)
                //{
                //    var q = _instances[i].Target as Q;
                //    if (q == null)
                //        _instances.RemoveAt(i);
                //    else
                //        yield return q;
                //}
            }

            #endregion Private
        }
    }
}
