﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

using System.Reflection;

using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;


namespace TechnicalTools.Tools
{
    public interface ITypeSizeEstimator
    {
        /// <summary>
        /// Return the _estimated_ size of object type, based on type memory layout and statistics about data
        /// This method does not focus on process size recursively.
        /// </summary>
        /// <returns>Size in bytes</returns>
        int GetMeanInstanceSize(Type type);

        /// <summary>
        /// Return the _estimated_ size contains by a class.
        /// It means the average of all instances of this type, minus the size of all instance zeroified
        /// For example for string it means we should return the average length of all string manipulated by your application
        /// Multiplied by 2 because of unicode
        /// </summary>
        /// <returns>Size in bytes</returns>
        int GetEstimatedSizeOfContent(Type type);

        /// <summary>
        /// Return the _estimated_ default size of an instance of type "type" when all is zero-ified in memory.
        /// It means if type is "string", the method ust return the size taken in memory by "new string()"
        /// </summary>
        /// <returns>Size in bytes</returns>
        int GetEstimatedSizeOfEmptyInstance(Type type);
    }

    public class TypeSizeEstimator : ITypeSizeEstimator
    {
        public int UnknownTypePenaltySizeInBytes { get; set; } = 16;

        protected internal TypeSizeEstimator()
        {
            AddRuleForEmptySize((t, rec) =>
            {
                return typeof(Enum).IsAssignableFrom(t)
                     ? rec(t.GetEnumUnderlyingType())
                     : (int?)null;
            });
            AddRuleForEmptySize((t, rec) =>
            {
                return typeof(IStaticEnum).IsAssignableFrom(t)
                     ? 0 // Assuming the real data is loaded using another field then converted to IStaticEnum (so we dont count twice this)
                     : (int?)null;
            });
            AddRuleForEmptySize((t, rec) =>
            {
                return typeof(MulticastDelegate).IsAssignableFrom(t)
                     ? 0 // Assuming the real data is loaded using another field then converted to IStaticEnum (so we dont count twice this)
                     : (int?)null;
            });
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(TypeSizeEstimator));

        /// <summary> Implements <see cref="ITypeSizeEstimator.GetMeanInstanceSize"/> </summary>
        public virtual int GetMeanInstanceSize(Type type)
        {
            var structSize = GetEstimatedSizeOfEmptyInstance(type);
            var contentSize = GetEstimatedSizeOfContent(type);
            return contentSize + contentSize;
        }


        /// <summary>
        /// Implements <see cref="ITypeSizeEstimator.GetEstimatedSizeOfContent"/> .
        /// This method returns 0 for native value type and <see cref="DefaultInternalSizeInBytes"/> for other types.
        /// </summary>
        public virtual int GetEstimatedSizeOfContent(Type type)
        {
            if (_defaultSizeToStore.TryGetValue(type, out int size))
                return 0;
            return DefaultEstimatedSizeOfContent;
        }
        public int DefaultEstimatedSizeOfContent { get; set; } = 42;

        /// <summary> Implements <see cref="ITypeSizeEstimator.GetEstimatedSizeOfEmptyInstance"/> </summary>
        public virtual int GetEstimatedSizeOfEmptyInstance(Type type)
        {
            if (_defaultSizeInsideByType.TryGetValue(type, out int size))
                return size;
            size = GetEstimatedSizeOfEmptyInstance(type.BaseType);
            var fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (var field in fields)
            {
                var t = field.FieldType.RemoveNullability() ?? field.FieldType;
                if (_defaultSizeToStore.TryGetValue(t, out int s))
                    size += s;
                else
                    lock (_rulesForEmptySize)
                    {
                        int? ruleRes = null;
                        foreach (var rule in _rulesForEmptySize)
                        {
                            ruleRes = rule(t, GetEstimatedSizeOfEmptyInstance);
                            if (ruleRes.HasValue)
                                break;
                        }
                        if (!ruleRes.HasValue)
                        {
                            var metCount = _unknownTypeMet.AddOrUpdate(t, 1, (_, c) => c + 1);
                            if (metCount == 1 && DebugTools.IsForDevelopper)
                                BusEvents.Instance.RaiseBusinessMessage($"Type {t.FullName} is of unknown size, assuming it is {UnknownTypePenaltySizeInBytes} bytes (penalty estimation)", _log);
                            ruleRes = UnknownTypePenaltySizeInBytes; // default penalty value
                        }
                        _defaultSizeInsideByType.TryAdd(t, ruleRes.Value);
                        size += ruleRes.Value;
                    }
            }
            return size;
        }
        readonly ConcurrentDictionary<Type, long> _unknownTypeMet = new ConcurrentDictionary<Type, long>();

        // All Rules are stored in the reversed insertion order,
        // so user overrides behavior by adding a new one
        readonly List<Func<Type, Func<Type, int>, int?>> _rulesForEmptySize = new List<Func<Type, Func<Type, int>, int?>>();
        protected void AddRuleForEmptySize(Func<Type, Func<Type, int>, int?> rule)
        {
            lock (_rulesForEmptySize)
                _rulesForEmptySize.Insert(0, rule);
        }

        readonly ConcurrentDictionary<Type, int> _defaultSizeInsideByType = new Dictionary<Type, int>()
        {
            { typeof(object), 0 },
        }.ToConcurrentDictionary(kvp => kvp.Key, kvp => kvp.Value);
        readonly Dictionary<Type, int> _defaultSizeToStore = new Dictionary<Type, int>()
        {
            { typeof(object), IntPtr.Size },
            { typeof(bool), sizeof(bool) },
            { typeof(byte), sizeof(byte) },
            { typeof(sbyte), sizeof(sbyte) },
            { typeof(short), sizeof(short) },
            { typeof(ushort), sizeof(ushort) },
            { typeof(int), sizeof(int) },
            { typeof(uint), sizeof(uint) },
            { typeof(long), sizeof(long) },
            { typeof(ulong), sizeof(ulong) },
            { typeof(float), sizeof(float) },
            { typeof(double), sizeof(double) },
            { typeof(decimal), sizeof(decimal) },
            { typeof(TimeSpan), sizeof(long) },
            { typeof(DateTime), sizeof(UInt64) },
            { typeof(char), sizeof(char) },
            { typeof(string), IntPtr.Size },
            { typeof(byte[]), IntPtr.Size },
            { typeof(Guid), 16 },
            { typeof(IntPtr), IntPtr.Size },
            { typeof(UIntPtr), UIntPtr.Size },
        };
    }
}
