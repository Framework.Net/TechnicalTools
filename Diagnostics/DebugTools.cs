﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;



namespace TechnicalTools.Diagnostics
{
    /// <summary>
    /// Cette classe met à disposition des outils permettant de debugger plus rapidement.
    /// </summary>
    public static class DebugTools
    {
        //****************************** Configuration ******************************

        // Si vous souhaitez ne plus être ennuyé par les breaks, ajoutez simplement le nom de votre machine (Environment.MachineName) en dur à cette liste
        static readonly List<string> IgnoreBreakOnTheseMachines = new List<string>(); // { "LW-MLABAU" };

        // Autorise les checks couteux en calcul et/ou en temps.
        // A true l'application sera (bcp) plus lente mais effectuera cependant plus de test.
        public static bool EnableCostlyCheck { get; set; }

        public static bool IsDebug
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        public static bool IsForDevelopper
        {
            get
            {
                if (_IsForDevelopper.HasValue)
                    return _IsForDevelopper.Value;
                return Debugger.IsAttached &&
                    (LoginsThatAreDevelopper.Count == 0 || LoginsThatAreDevelopper.Contains(Environment.UserName.ToLowerInvariant()));
            }
            set { _IsForDevelopper = value; } // This setter allow developper to set a forced value to debug at runtime
        }
        static bool? _IsForDevelopper;
        public static void ResetIsForDevelopper()
        {
            _IsForDevelopper = null;
        }

        public static readonly List<string> LoginsThatAreDevelopper = new List<string>();

        public static bool CaptureStackTraceOnDeferredEvents { get; set; }

        // Note : Le mot clef Stop du VB empeche de modifier le code à la volée
        [DebuggerHidden, DebuggerStepThrough]
        [Conditional("DEBUG")]
        public static void Break(string reason = null)
        {
            if (Debugger.IsAttached && // Important ! Sinon Break peut faire crasher en release
                !IgnoreBreakOnTheseMachines.Contains(Environment.MachineName))
                Debugger.Break();
        }

        /// <summary>
        /// Indique à Visual Studio de breaker automatiquement si l'action sous-jacente lève une exception.
        /// Ce code n'est PAS destiné à faire disparaitre les exceptions. Son but n'est que
        /// d'aider les développeurs à trouver rapidement les problèmes éventuels.
        /// Certaines exceptions, à certains endroits, peuvent être silencieusement ignoré en mode debug.
        /// Exemple :
        /// - Initialisation statique => Plante et impossible de revenir dedans sans relancer l'application
        /// - Evenement Load d'une form => l'exception est simplement ignoré !!!
        /// - evenement CustomColumnDisplayText d'une gridView DevExpress, la grille devient toute blanche sans raison
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void BreakOnException(Action action)
        {
#if DEBUG
            try
            {
#endif
                action();
#if DEBUG
            }
            catch
            {
                Break("Unexpected exception found, please correct it!");
                throw; // Assure une transparence maximale de BreakOnException
            }
#endif
        }

        /// <summary>
        /// Indique à Visual Studio de breaker automatiquement si la fonction sous-jacente lève une exception.
        /// Ce code n'est PAS destiné à faire disparaitre les exceptions. Son but n'est que
        /// d'aider les développeurs à trouver rapidement les problèmes éventuels
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T BreakOnException<T>(Func<T> fun)
        {
#if DEBUG
            try
            {
#endif
                return fun();
#if DEBUG
            }
            catch
            {
                Break("Unexpected exception found, please correct it!");
                throw; // Assure une transparence maximale de BreakOnException
            }
#endif
        }

        #region Assert

        [Conditional("DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        public static void Assert(bool cond, string msg = null) // Pour la coherence des assert dans l'appli si on souhaite eviter d'ecrire Debug.Assert
        {
            DateTime dt = DateTime.UtcNow;
            Debug.Assert(cond, msg);
            var ts = DateTime.UtcNow - dt;
            if (!cond && ts.TotalMilliseconds < 1000) // Si le Debug.Assert(cond) n'a pas arrete l'utilisateur au moins une seconde c'est qu'il n'a pas marché;
                Break(); // Si Debug.Assert ne fonctionne pas (ca arrive parfois)!
        }

        [Conditional("DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        public static void Assert<T>(IEnumerable<T> values, Func<T, bool> cond, Func<T, string> msg = null)
        {
            foreach (T value in values)
                Debug.Assert(cond(value), msg(value));
        }

        [Conditional("SUPER_DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        public static void CostlyAssert<T>(IEnumerable<T> values, Func<T, bool> cond, Func<T, string> msg = null)
        {
            if (EnableCostlyCheck)
                foreach (T value in values)
                    Debug.Assert(cond(value), msg(value));
        }

        // Encapsulez les assertions coûteuses dans cette méthode, afin de pouvoir les désactiver
        [Conditional("SUPER_DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        public static void CostlyAssert(Func<bool> action, string msg = null)
        {
            if (EnableCostlyCheck)
                Debug.Assert(action(), msg);
        }
        // Encapsulez les assertions coûteuses dans cette méthode, afin de pouvoir les désactiver
        [Conditional("SUPER_DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        public static void CostlyAssert(bool condition, string msg = null)
        {
            if (EnableCostlyCheck)
                Debug.Assert(condition, msg);
        }

        [Conditional("DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        public static void Should(bool cond, string msg = null)
        {
            if (!cond)
                if (!IgnoreWarningOnTheseMachines.Contains(Environment.MachineName))
                    if (ShouldAssertFail != null)
                        ShouldAssertFail(WarningPrefixOnAssertMessage + " (You can ignore this message) :" + Environment.NewLine + Environment.NewLine + msg);
                    else
                        Break(); // Pas d'event pour prevenir donc on stoppe le developpeur
        }
        public static event Action<string> ShouldAssertFail;
        public static void Should(string msg)
        {
            Should(false, msg);
        }
        #endregion Assert

        // Si vous souhaitez ne plus être ennuyé par les assertions sans gravité, qui avertissent, ajoutez simplement le nom de votre machine (Environment.MachineName) en dur à cette liste
        static readonly List<string> IgnoreWarningOnTheseMachines = new List<string>(); // { "MLA099" };
        public static readonly string WarningPrefixOnAssertMessage = "AVERTISSEMENT";

        //        [Conditional("DEBUG")]
        [Conditional("SUPER_DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        [Obsolete("Use \"Should\" instead")]
        public static void Warning(bool condition, string msg = null)
        {
            Should(condition, msg);
        }
        //        [Conditional("DEBUG")]
        [Conditional("SUPER_DEBUG")]
        [DebuggerHidden, DebuggerStepThrough]
        [Obsolete("Use \"Should\" instead")]
        public static void Warning(string msg = null)
        {
            Should(msg);
        }

        public static Exception UnreachableCodeReachedException()
        {
            const string msg = "Unreachable code reached!";
            Debug.Fail(msg);
            return new InvalidOperationException(msg);
        }

        #region Other tools

        /// <summary>
        /// Au runtime, cette méthode permet de pouvoir brancher un evenement qui nous interesse afin de pouvoir y mettre un point d'arret et debugger.
        /// </summary>
        public static void DebugHandler(object sender, EventArgs e)
        {
            // Nothing
        }

        public static string __METHOD__([CallerMemberName] string callerName = "")
        {
            return callerName;
        }
        public static string __FILEPATH__([CallerFilePath] string filename = "")
        {
            return filename;
        }
        public static int __LINE__([CallerLineNumber] int line = 0)
        {
            return line;
        }
        #endregion Other tools
    }
}


