﻿using System;
using System.Diagnostics;
using System.IO;


namespace TechnicalTools.Diagnostics
{
    public class IssueInfo
    {
        [DebuggerHidden, DebuggerStepThrough]
        internal IssueInfo(StackTrace stack)
        {
            When = DateTime.Now;
            Stack = stack;
        }

        public DateTime When { get; private set; }

        public StackTrace Stack { get; private set; }

        public string CallStack { get { return Stack.ToString(); } }

        public string FilePath { get { return string.IsNullOrWhiteSpace(Stack.GetFrame(0).GetFileName()) ? "" : Path.GetDirectoryName(Stack.GetFrame(0).GetFileName()); } }
        public string FileName { get { return string.IsNullOrWhiteSpace(Stack.GetFrame(0).GetFileName()) ? "" : Path.GetFileName(Stack.GetFrame(0).GetFileName()); } }
        public int Line { get { return Stack.GetFrame(0).GetFileLineNumber(); } }
        public int Column { get { return Stack.GetFrame(0).GetFileColumnNumber(); } }

        public string ClassName
        {
            get
            {
                return Stack.GetFrame(0) == null ? "<NO FRAME>"
                     : Stack.GetFrame(0).GetMethod() == null ? "<NO METHOD>"
                     : Stack.GetFrame(0).GetMethod().DeclaringType == null ? "<NO DECLARING TYPE>"
                     // ReSharper disable PossibleNullReferenceException
                     : Stack.GetFrame(0).GetMethod().DeclaringType.Name;
                // ReSharper restore PossibleNullReferenceException
            }
        }
        public string MethodName { get { return Stack.GetFrame(0).GetMethod().Name; } }


        // Version raccourci de la callstack, en plus intelligent
        public virtual string Info
        {
            get
            {
                return ExceptionManager.Instance.CallStackToSmartString(Stack, false);
            }
        }
    }


}
