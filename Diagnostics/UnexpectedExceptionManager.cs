﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;


namespace TechnicalTools.Diagnostics
{
    public static class UnexpectedExceptionManager
    {
        [Flags]
        public enum eExceptionKind
        {
            Undefined = 0,
            [Description("Unhandled by application :(")]
            Unhandled = 1,
            [Description("Ignored by developper")]
            Ignored = 2,
            [Description("Unhandled by application but ignored anyway")] // For example when a task is finalized, and the Exception state has never been observed. The exception is unhandled but ignored by default
            UnhandledButIgnored = Unhandled | Ignored,
            [Description("Managed by developper")]
            ManagedByDevelopper = 4
        }

        public class ExceptionInfo : IssueInfo
        {
            public eExceptionKind Kind { get; private set; }
            public Exception Exception { get; private set; }
            public string ThreadName { get; private set; }

            public bool ForDevOnly { get; private set; }


            [DebuggerHidden, DebuggerStepThrough]
            internal ExceptionInfo(Exception ex, eExceptionKind kind, string threadName, bool forDevOnly = false)
                : base(new StackTrace(ex, true))
            {
                Exception = ex;
                ThreadName = threadName;
                Kind = kind;
                ForDevOnly = forDevOnly;
            }
            public override string Info
            {
                get
                {
                    return ExceptionManager.Instance.Format(Exception);
                }
            }
        }

        internal static readonly BindingList<ExceptionInfo> _UnexpectedExceptions = new BindingList<ExceptionInfo>();

        // TODO : Utiliser un type "ReadOnlyBindingList"
        /// <summary> Attention, cette liste peut être modifié par un autre thread. </summary>
        public static BindingList<ExceptionInfo> UnexpectedExceptions { get { return _UnexpectedExceptions; } }

        public static event EventHandler<ExceptionInfo> NewUnhandledException;

        static UnexpectedExceptionManager()
        {
            lock (_UnexpectedExceptions)
                _UnexpectedExceptions.ListChanged += (sender, args) =>
                {
                    if (args.ListChangedType == ListChangedType.ItemAdded)
                        OnNewUnhandledException(_UnexpectedExceptions[args.NewIndex]);
                };
        }

        public static void IgnoreThisException(Exception ex)
        {
            Debug.Assert(ex != null);
            IgnoredExceptions.Add(ex);
        }
        public static void DoNotIgnoreThisException(Exception ex)
        {
            Debug.Assert(ex != null);
            var to_remove = IgnoredExceptions.Where(e => ExceptionsSeemEqual(ex, e)).ToList();
            foreach (var e in to_remove)
                IgnoredExceptions.Remove(e);
        }
        static readonly List<Exception> IgnoredExceptions = new List<Exception>();
        static bool ExceptionsSeemEqual(Exception ex1, Exception ex2)
        {
            return ex1.StackTrace == ex2.StackTrace;
        }
        static void OnNewUnhandledException(ExceptionInfo info)
        {
            // ATTENTION : Cas technique tres particulier (lire http://msdn.microsoft.com/en-us/library/system.threading.threadabortexception.aspx)
            // L'exception ThreadAbort catchée est rethrowé __AUTOMATIQUEMENT__, par le framework, à la fin des clause catch ou finally...
            // Il est fortement deconséillé d'utiliser Abort sur les thread,
            // a moins d'etre sur qu'absolument tout le code puisse etre interrompu a n'importe quel moment (possible si le mode lde donne est immutable)
            if (info.Exception is ThreadAbortException)
                return;

            // Erreur detectée ! Inspectez les deux variables ci-dessus
            // En continuant, l'application ne plantera pas mais vous risquez d'etre dans un etat incohérent.
            // En Release l'application aurait très probablement planté !
            if (IgnoredExceptions.Any(e => ExceptionsSeemEqual(info.Exception, e)))
                return;
            if (NewUnhandledException != null && (info.Kind == eExceptionKind.Unhandled || info.Kind == eExceptionKind.UnhandledButIgnored))
                NewUnhandledException(null, info);
        }

    }
}
