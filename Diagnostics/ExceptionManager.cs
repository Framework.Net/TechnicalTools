﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

using TechnicalTools.Model;
using TechnicalTools.UI;


namespace TechnicalTools.Diagnostics
{
    // Initialize it by using: RuntimeHelpers.RunClassConstructor(typeof(ExceptionManager).TypeHandle);
    public static class ExceptionManager
    {
        public static DefaultExceptionManager Instance { get; set; } = new DefaultExceptionManager();
    }

    /// <summary>
    ///  Ce manager permet de gérer tous les cas d'erreur et propose 3 fonctionnalités majeur
    ///  - Gerer les exception ignorable (parce que le développeur sait comment palier au problème ou qu'il a decidé volontairement d'ignorer certaines exceptions),
    ///  - Enrichir une exception en "re-throwant" (TODO)
    ///  - Transformer une exception en texte adapté à l'utilisateur et fournissant des informations pertinentes.
    /// </summary>
    public class DefaultExceptionManager : IDisposable
    {
        public DefaultExceptionManager()
        {
            Init();
        }
        protected virtual void Init()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            System.Threading.Tasks.TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException; // Ca ne marche que quand les taches sont finalisés

            // Force the static constructor to be called
            RuntimeHelpers.RunClassConstructor(typeof(UnexpectedExceptionManager).TypeHandle);
        }

        #region IDisposable Support

        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    AppDomain.CurrentDomain.UnhandledException -= CurrentDomainOnUnhandledException;
                    System.Threading.Tasks.TaskScheduler.UnobservedTaskException -= TaskScheduler_UnobservedTaskException; // Ca ne marche que quand les taches sont finalisés
                }
                _isDisposed = true;
            }
        }
        private bool _isDisposed; // Pour détecter les appels redondants

        #endregion


        #region Reporting sur les exceptions

        // Version raccourcie et plus intelligente de ce qu'affiche Exception.Callstack
        public string CallStackToSmartString(StackTrace stack, bool with_hidden_methods = true)
        {
            var sb = new StringBuilder();
            var frames = stack.GetFrames();
            if (frames == null) // Possible si la source d'exception est ailleurs que dans le framework (exemple mscorlib)
            {
                sb.AppendLine("[ No information about where this error happened!");
                sb.AppendLine("  This often means that origin of the exception is not in a .NET assembly (ex : mscorlib) or a timeout occurs. ]");
            }
            for (int i = 0; i < stack.FrameCount; ++i)
            {
                var frame = stack.GetFrame(i);
                var m = frame.GetMethod();

                if (!with_hidden_methods && m.CustomAttributes.Any(att => att.AttributeType == typeof(DebuggerHiddenAttribute)
                                                                       || att.AttributeType == typeof(DebuggerStepThroughAttribute)))
                    continue;

                sb.Append("at ");
                sb.Append(m.DeclaringType == null ? "???" : m.DeclaringType.Name);
                sb.Append(".");
                sb.Append(m.Name);

                bool is_property_getter = m.IsSpecialName && (m.Name.StartsWith("get_")); // Le compilateur ajoute "get_" et "set_" automatiquement au nom des proprietes
                if (!is_property_getter && !m.GetParameters().Any()) // Les propriétés en VB peuvent être indexés !
                {
                    sb.Append("(");
                    bool first = true;
                    foreach (var param in m.GetParameters())
                    {
                        if (!first)
                            sb.Append(", ");
                        first = false;

                        sb.Append(param.ParameterType.Name);
                        sb.Append(" ");
                        sb.Append(param.Name);
                        if (param.HasDefaultValue)
                        {
                            sb.Append(" = ");
                            sb.Append(param.DefaultValue == null ? "NULL" : param.DefaultValue == DBNull.Value ? "DBNull" : param.DefaultValue.ToString());
                        }
                    }
                    sb.Append(")");
                }

                sb.Append(" in ");
                sb.Append(Path.GetFileName(frame.GetFileName()));
                sb.Append(":");
                sb.Append(frame.GetFileLineNumber());
                sb.Append(":");
                sb.Append(frame.GetFileColumnNumber());

                if (m.ReflectedType != null && m.ReflectedType != m.DeclaringType)
                {
                    sb.Append("  // but object is of type ");
                    sb.Append(m.ReflectedType.Name);
                }
                sb.AppendLine();
            }
            sb.Length = sb.Length - Environment.NewLine.Length;
            return sb.ToString();
        }

        public string Format(Exception original_exception)
        {
            return Format(original_exception, false);
        }
        public string Format(Exception original_exception, bool excludeTechnicalDetails)
        {
            var includeUniqueId = excludeTechnicalDetails && !(original_exception is IUserUnderstandableException);
            return Format(original_exception, excludeTechnicalDetails, includeUniqueId);
        }
        /// <summary>Génère, à partir d'une exception, une chaine de caractère qui comprend le plus d'information comprehensible par l'utilisateur.
        ///          Cette méthode est destinée aux utilisateurs et à la MOA ou aux développeur.
        ///          Le but est de leur fournir le plus d'information exploitable pour qu'il puisse agir par eux même :
        /// Exemple : L'utilisateur a mal saisie une valeur
        ///           L'utilisateur a saisie des valeurs correctes mais elle conduise a une valeur intermediaire, a 0, utilisé comme denominateur dans une division.
        ///           L'utilisateur n'a pas les droits d'effectuer une operation => il va demander à son boss l'autorisation si possible.
        ///           Tout ces messages doivent etre encapsulé dans une exception de type FunctionalException.
        ///           En revanche toutes les autres exceptions seront considéré comme des problèmes techniques.
        /// </summary>
        /// <param name="original_exception">Exception à convertir</param>
        /// <param name="excludeTechnicalDetails">Exclut les détails techniques dans le message</param>
        /// <returns>L'exception sous forme de string avec le plus d'information techniques ou fonctionnelles possible</returns>
        public string Format(Exception original_exception, bool excludeTechnicalDetails, bool includeUniqueId)
        {
            if (original_exception == null) // Si ca arrive en release il faut quand même gérer
            {
                const string NoInfo = "We have currently no information about this issue!";
                Debug.Fail(Tr(NoInfo));
                return Tr(NoInfo);
            }

            // Protège pour permettre d'envoyer un log même si il y a eu un problème au milieu de la génération (cas rare / improbable)
            try
            {
                ExceptionTreeNode root = GetExceptionTreeByRelevance(original_exception);

                return DisplayExceptionTreeNode(root, excludeTechnicalDetails, string.Empty)
                     + (!includeUniqueId ? "" : Environment.NewLine + Environment.NewLine +
                     (original_exception is IUserUnderstandableException
                     ? $"If message is unclear, you can ask about this issue with this ticket Id: {original_exception.GetUniqueBugId()}"
                     : $"A bug report with id {original_exception.GetUniqueBugId()} has been generated."));
            }
            catch (Exception ex)
            {
                DebugTools.Break();
                return original_exception.Message + Environment.NewLine +
                       "[Error building exception error ! :'(" + Environment.NewLine +
                       " Cause : " + ex.Message + "]";
            }
        }


        public class ExceptionTreeNode
        {
            // Pertinence de l'exception vis à vis du public visé lors de la creation de cette classe [0 (valeur par defaut) = sans valeur particulière ]
            public int Relevancy;

            // Exception à afficher (Aucun parcours récursif à faire sur les Innerexception(s), il faut le faire sur Causes et Consequences)
            public Exception Exception { get; set; }

            // Autre exception à afficher au même niveau, elles ont participé , de concert, au même problème sans forcement une relation de cause à effet avec "Exception"
            public List<ExceptionTreeNode> OtherExceptions { get; } = new List<ExceptionTreeNode>();

            // Cause(s) de Exception
            public List<ExceptionTreeNode> Causes { get; } = new List<ExceptionTreeNode>();

            // Conséquences de Exception
            public ExceptionTreeNode Consequence { get; set; }

            public ExceptionTreeNode(Exception ex)
            {
                Exception = ex;
            }
        }

        ExceptionTreeNode ReorganizeHierarchyByRelevance(Exception ex, Func<Exception, int> get_relevancy_score)
        {
            ex = UnwrapException(ex);

            // Parcours de l'arbre des exceptions du bas vers le haut.
            // On fait remonter les exceptions jugé importante.
            // On utilise une classe ExceptionTreeNode qui permet de garder un modèle cohérent et expressive.
            var subnodes = new List<ExceptionTreeNode>();

            if (ex is AggregateException exs)
            {
                subnodes.AddRange(exs.InnerExceptions.Select(e => ReorganizeHierarchyByRelevance(e, get_relevancy_score)));
                // TODO : exs.InnerException == exs.InnerExceptions[0] ?
            }
            else if (ex.InnerException != null)
                subnodes.Add(ReorganizeHierarchyByRelevance(ex.InnerException, get_relevancy_score));

            var node = new ExceptionTreeNode(ex)
            {
                // Par defaut on préserve la hierarchie des exceptions
                Relevancy = get_relevancy_score(ex)
            };
            node.Causes.AddRange(subnodes);

            if (node.Relevancy == 0)
                return node;

            // Si l'exception n'est pas pertinente mais que l'une des exceptions en cause l'est, on la (ou les) fait remonter.
            var subnodes_relevant = subnodes.Where(sn => sn.Relevancy > node.Relevancy).ToList();

            if (subnodes_relevant.Count == 0) // Aucune pertinence trouvé dans les soushierarchie
                return node; // on ne change donc rien

            // Au moins une sous exception pertinente a été trouvé (subnodes_relevant.First())
            // On va la retourner au lieu de l'exception courante (node) qui ne l'est pas.
            var node_relevant = subnodes_relevant.OrderByDescending(n => n.Relevancy).First();

            // La propriete InnerException d'une Exception est la cause de cette derniere.
            // Afin de garder une modelisation cohérente il faut donc inverse ce sens de causalité.
            // On stocke donc l'exception courrante dans un champs Consequence
            var first_node_with_no_consequence = node_relevant;
            while (first_node_with_no_consequence.Consequence != null)
                first_node_with_no_consequence = first_node_with_no_consequence.Consequence;
            first_node_with_no_consequence.Consequence = node;
            node.Causes.Remove(node_relevant);

            // Si il existait d'autre sous exception pertinente, on doit les range au meme niveau que l'exception actuel (pas de favoritisme)
            subnodes_relevant.Remove(node_relevant);
            node_relevant.OtherExceptions.AddRange(subnodes_relevant);

            // On range les autres sous-exceptions non pertinentes sous l'exception courrante (non pertinente aussi)
            node.Causes.AddRange(subnodes.Except(subnodes.Except(subnodes_relevant)));

            return node_relevant;
        }


        /// <summary>
        /// Filtre et réorganise les exceptions dans un format adapté au public visé.
        /// Si for_user_of_application est à false (le public visé est donc les developpeur) :
        ///   L'ordre des exceptions est préservé.
        /// Si for_user_of_application est à true :
        ///   Les exceptions susceptible d'etre comprise par les utilisateurs sont mis en avant.
        ///   Les problèmes purement techniques seront moins visibles.
        /// </summary>
        /// <param name="ex">Exception à réarranger</param>
        /// <returns>Une arbre d'exceptions organisé selon l'ordre de pertinence</returns>
        ExceptionTreeNode GetExceptionTreeByRelevance(Exception ex)
        {
            return ReorganizeHierarchyByRelevance(ex, ExceptionIsUnderstandableByUser);
        }

        /// <summary>
        /// Indique un score de pertinence de l'exception (sans prendre en compte InnerException et équivalente)
        /// Plus le score est haut meilleur est la pertinence de l'information contenu par l'exception.
        /// Les valeurs superieur à 0 indique des exceptions pertinente.
        /// Les valeur inferieur à 0 indique des exceptions non pertinente (ie: à ignorer)
        /// Une valeur a 0 indique de ne pas chercher à gérer la pertinence de ces exceptions et des exceptions internes.
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        int ExceptionIsUnderstandableByUser(Exception ex)
        {
            // Trie les exception de la plus importante (a priori) à la moins importante
            return ex is IBusinessException ? 3
                 : ex is IUserUnderstandableException ? 2
                 : ex is ArithmeticException ? 1
                 : ex is DirectoryNotFoundException ? 1
                 : ex is FileNotFoundException ? 1
                 : ex.GetType() == typeof(Exception) ? 0 // ==>  Exception non typé
                 : ex is ITechnicalException ? -1
                 : ex is MissingMemberException ? -2
                 : ex is TypeInitializationException ? -3
                 : ex is System.Reflection.TargetInvocationException ? -4
                 : -5;// Exception non typé = pire possible car la moins précise
        }


        string DisplayExceptionTreeNode(ExceptionTreeNode node, bool excludeTechnicalDetails = false, string grammar_conjunction = null, bool is_consequence = false)
        {
            Debug.Assert(node != null);
            var sb = new StringBuilder();

            if (grammar_conjunction == null || grammar_conjunction.Length != 0)
                if (node.OtherExceptions.Count == 0)
                    sb.AppendLine(grammar_conjunction ?? Tr("Error:"));
                else
                    sb.AppendLine(grammar_conjunction == null
                                    ? Tr("Several errors occured:")
                                    : grammar_conjunction + Tr(" several errors:"));
            var msg = node.Exception.GetEnrichments()
                                    .Where(e => !excludeTechnicalDetails
                                            || (e.Type & eExceptionEnrichmentType.Technical) == 0)
                                    .Where(e => e.BeforeMessage)
                                    .Select(e => e.Info)
                                    .Concat(node.Exception.Message)
              .Concat(node.Exception.GetEnrichments()
                                    .Where(e => !excludeTechnicalDetails
                                            || (e.Type & eExceptionEnrichmentType.Technical) == 0)
                                    .Where(e => !e.BeforeMessage)
                                    .Select(e => e.Info))
              .Join(Environment.NewLine);

            sb.AppendLine(msg);

            if (!excludeTechnicalDetails)
            {
                sb.AppendLine("  " + CallStackToSmartString(new StackTrace(node.Exception, true))
                               .Replace(Environment.NewLine, Environment.NewLine + "  "));
            }

            const string indent = "    ";

            foreach (var cause_ex in node.Causes)
            {
                string cause = DisplayExceptionTreeNode(cause_ex, excludeTechnicalDetails, is_consequence ? Tr("And also because of") : Tr("Because of"))
                                .Replace(Environment.NewLine, Environment.NewLine + indent);
                sb.AppendLine(cause);
            }

            foreach (var on in node.OtherExceptions)
            {
                string and_also = DisplayExceptionTreeNode(on, excludeTechnicalDetails, Tr("And"));
                sb.AppendLine(and_also);
            }

            if (node.Consequence != null)
            {
                sb.AppendLine("");
                string consequence = DisplayExceptionTreeNode(node.Consequence, excludeTechnicalDetails, node.Consequence.Causes.Count > 0 ? Tr("Might have caused:") : Tr("Has caused:"), true)
                                    .Replace(Environment.NewLine, Environment.NewLine + indent);
                sb.AppendLine(consequence);
            }

            return sb.ToString().Trim();
        }
        public Dictionary<string, string> Translations { get; }= new Dictionary<string, string>();
        string Tr(string text)
        {
            if (Translations.ContainsKey(text))
                return Translations[text];
            return text;
        }
        #endregion Reporting sur les exceptions


        #region Gestion des exceptions non prévu

        /// <summary>
        /// Occurs when an untrapped thread exception is thrown.
        /// DO Not use ApplicationThreadException because of this bug : https://connect.microsoft.com/VisualStudio/feedback/details/783744/application-threadexception-does-not-support-multiple-event-handlers
        /// </summary>
        /// <filterpriority>1</filterpriority>
        public event UnhandledExceptionEventHandler UnhandledException;

        void TaskScheduler_UnobservedTaskException(object sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs e)
        {
            NotifyException(BaseException.TryFindWrappingException(e.Exception), UnexpectedExceptionManager.eExceptionKind.UnhandledButIgnored, null);
            //e.SetObserved(); // To study
        }
        void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject is Exception
                               ? (Exception)e.ExceptionObject
                               : new Exception(string.Format("Unhandled exception on current domain {0}. Cause:", AppDomain.CurrentDomain.FriendlyName) + Environment.NewLine +
                                               e.ExceptionObject.ToString());
            ex = BaseException.TryFindWrappingException(ex);
            NotifyException(ex, UnexpectedExceptionManager.eExceptionKind.Unhandled, null, e.IsTerminating);
        }


        // Pour les autres exceptions qui ne serait pas catché par les deux méthodes ci-dessus
        public void NotifyException(Exception ex, UnexpectedExceptionManager.eExceptionKind kind, string thread_name = null, bool is_terminating = false)
        {
            if (UnhandledException != null)
                IgnoreExceptionAndBreak(() => UnhandledException(null, new UnhandledExceptionEventArgs(ex, is_terminating)));
            thread_name = thread_name ?? System.Threading.Thread.CurrentThread.Name;// ?? Tools.Threading.Thread.CurrentThreadName;
            lock (UnexpectedExceptionManager._UnexpectedExceptions)
                UnexpectedExceptionManager._UnexpectedExceptions.Add(new UnexpectedExceptionManager.ExceptionInfo(ex, kind, thread_name));
        }

        #endregion

        #region Inline exception handling

        /// <summary>
        /// Cette méthode vérifie qu'une action ne doit pas provoquer d'exception.
        /// Elle doit par exemple être utilisé dans les évènements Load des forms car lorsque les handlers des évènements Load déclenchent des exceptions,
        /// celles-ci sont complètement ignorées par le framework .Net
        /// </summary>
        /// <param name="action"></param>
        [DebuggerHidden, DebuggerStepThrough]
        public Exception EnsureNoExceptionButIgnoreIt(Action action)
        {
            return _IgnoreExceptionAndBreak<Exception>(action, true/*, UnexpectedExceptionManager.eExceptionKind.Unhandled*/);
        }
        public TResult EnsureNoExceptionButIgnoreIt<TResult>(TResult default_result, Func<TResult> func)
        {
            TResult result = default_result;
            _IgnoreExceptionAndBreak<Exception>(() => result = func(), true/*, UnexpectedExceptionManager.eExceptionKind.Unhandled*/);
            return result;
        }

        /// <summary>
        /// Cette méthode permet d'executer une action en ignorant toute exception.
        /// Cela permet de protéger le code tout en clarifiant la volonté, explicite, du developpeur à ignorer cette exception.
        /// Cette methode permet en outre de pouvoir mettre en place un traitement des exceptions ignorés (log)
        /// CONSEIL : IgnoreExceptionAndBreak correspond peut être plus à ce que vous cherchez.
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        public Exception IgnoreException(Action action)
        {
            return _IgnoreExceptionAndBreak<Exception>(action, false);
        }

        /// <summary>
        /// Même chose que IgnoreException mais pour uniquement pour ignorer les exception d'un type précis.
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        public Exception IgnoreException<TExceptionType>(Action action)
            where TExceptionType : Exception
        {
            return _IgnoreExceptionAndBreak<TExceptionType>(action, false);
        }

        /// <summary>
        /// Identique à IgnoreException mais cette methode indique votre volonte de faire s'arreter Visual studio en cas d'erreur.
        /// Bien qu'ignorée, l'erreur n'est néanmoins pas censé se produire et vous souhaitez pouvoir investiguer sur sa cause en mode debug.
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        public Exception IgnoreExceptionAndBreak(Action action)
        {
            return _IgnoreExceptionAndBreak<Exception>(action, true);
        }

        /// <summary>
        /// Même chose que IgnoreExceptionAndBreak mais pour uniquement pour ignorer les exception d'un type précis.
        /// </summary>
        [DebuggerHidden, DebuggerStepThrough]
        public Exception IgnoreExceptionAndBreak<TExceptionType>(Action action)
            where TExceptionType : Exception
        {
            return _IgnoreExceptionAndBreak<TExceptionType>(action, true);
        }

        [DebuggerHidden, DebuggerStepThrough]
        Exception _IgnoreExceptionAndBreak<TExceptionType>(Action action, bool with_break/*, UnexpectedExceptionManager.eExceptionKind kind = UnexpectedExceptionManager.eExceptionKind.Ignored*/)
            where TExceptionType : Exception
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                if (with_break)
                {
                    string exMsg = ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;
                    DebugTools.Break(exMsg);
                }

                if (ex is TExceptionType)
                    return ex;

                throw;
            }
            return null;
        }

        #endregion Inline exception handling

        #region Enrichment of exception

        public void OnExceptionEnrichWith(eExceptionEnrichmentType type, string msg, Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex) when (ex.EnrichDiagnosticWithAndReturnFalse(msg, type))
            {
                throw; // Unreachable code!
            }
        }
        public T OnExceptionEnrichWith<T>(eExceptionEnrichmentType type, string msg, Func<T> func)
        {
            try
            {
                return func();
            }
            catch (Exception ex) when (ex.EnrichDiagnosticWithAndReturnFalse(msg, type))
            {
                throw; // Unreachable code!
            }
        }


        #endregion

        #region Tools

        /// <summary>
        /// Allow user to intercept exception and do some transparent handling.
        /// This is often to to add data to exception but not catch it or explicitely treating it.
        /// This method is not recommended for handling logging.
        /// </summary>
        /// <typeparam name="TException">Type of Exception to intercept</typeparam>
        /// <param name="action">action to try</param>
        /// <param name="treatException">transparent handling action to do if exception of the type occurs</param>
        public void InterceptException<TException>(Action action, Action<TException> treatException)
            where TException : Exception
        {
            try
            {
                action();
            }
            catch (TException ex) when (Treat(treatException, ex))
            {
                DebugTools.Break("Should never happen!");
                throw;
            }
        }

        /// <summary>
        /// Same than <see cref="InterceptException{TException}(Action, Action{TException})"/> but for a function instead of action.
        /// </summary>
        public TResult InterceptException<TResult, TException>(Func<TResult> func, Action<TException> treatException)
            where TException : Exception
        {
            try
            {
                return func();
            }
            catch (TException ex) when (Treat(treatException, ex))
            {
                DebugTools.Break("Should never happen!");
                throw;
            }
        }
        bool Treat<T>(Action<T> treat, T data)
        {
            treat(data);
            return false;
        }

        /// <summary>
        /// Unwrap Exception when we are _sure_ there is no useful data in it.
        /// Currently works for System.AggregateException only.
        /// Does not work like <see cref="AggregateException.Flatten"> that may discard some inner AggregateException message.
        /// In our case we unwrap when there is no point to keep an aggregate exception.
        /// </summary>
        /// <returns>Exception with condensed message</returns>
        public Exception UnwrapException(Exception ex)
        {
            string toAdd = "";
            var aex = ex as AggregateException;
            // Check the type of exception is exactly the default type.
            // Otherwise we may risk to discard some useful/ important debug data
            while (ex.GetType() == typeof(AggregateException) &&
                   aex.InnerExceptions.Count == 1 &&
                   aex.Data.Count == 0)
            {
                if (!string.IsNullOrWhiteSpace(aex.Message))
                    toAdd += aex.Message + Environment.NewLine + Environment.NewLine;
                ex = ex.InnerException;
                Debug.Assert(ex != null);
                aex = ex as AggregateException;
            }
            if (toAdd.Length > 0)
                ex.EnrichDiagnosticWith(toAdd, eExceptionEnrichmentType.UserUnderstandable, true);
            return ex;
        }

        #endregion
    }
}
