﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;


namespace TechnicalTools.Diagnostics
{
    // Initialize it by using: RuntimeHelpers.RunClassConstructor(typeof(AssertionManager).TypeHandle);
    public static class AssertionManager
    {
        public static DefaultAssertionManager Instance { get; set; } = new DefaultAssertionManager();
    }
    public class DefaultAssertionManager
    {
        // Si vous souhaitez ne plus jamais être ennuyé par les boites de dialogue qui pop quand les assertions echouent,
        // ajoutez simplement le nom de votre machine (Environment.MachineName) en dur à cette liste.
        //
        // Vous pouvez aussi desactiver les popup au runtime en passant par la fenêtre du menu "FILE" => "Debug Diagnostics" (onglet Assertions)
        // pour désactiver / (re)activer les popups. L'onglet montre toutes les assertions qui ont echoué independanment du fait que les popups soient activés ou non.
        public readonly List<string> MachineNameOnWhichFailedAssertionPopupIsDisabled = new List<string>(); // {  "Nom de votre machine" };

        public readonly List<Tuple<string, string, int>> AssertionsDisabledForTheseLines = new List<Tuple<string, string, int>>(); // file_name / method_name / line_number
        public bool IsAssertionDisabledFor(StackTrace stack)
        {
            var frame = stack.GetFrame(1);
            var key = (frame == null) ? Tuple.Create(string.Empty, string.Empty, 0) : Tuple.Create(frame.GetFileName(), frame.GetMethod().Name, frame.GetFileLineNumber());
            return AssertionsDisabledForTheseLines.Contains(key);
        }

        public class FailedAssertionInfo : IssueInfo
        {
            public string Message         { get; private set; }
            public string DetailedMessage { get; private set; }

            [DebuggerHidden, DebuggerStepThrough]
            internal FailedAssertionInfo(string msg, string detailedMsg, StackTrace stack)
                : base(stack)
            {
                Message = msg;
                DetailedMessage = detailedMsg;
            }
        }

        public BindingList<FailedAssertionInfo> FailedAssertions = new BindingList<FailedAssertionInfo>();


        public DefaultAssertionManager()
        {
            foreach (TraceListener listener in Trace.Listeners)
                OriginalListeners.Add(listener);
            Trace.Listeners.Clear();
            Trace.Listeners.Add(new TraceListenerInterceptor(this));
            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;
        }

        private void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            AppDomain.CurrentDomain.ProcessExit -= CurrentDomain_ProcessExit;
            var listeners = Trace.Listeners.OfType<TraceListenerInterceptor>().ToList();
            foreach (var listener in listeners)
                listener.Enable = false;
        }

        #region IDisposable Support

        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    var listeners = Trace.Listeners.OfType<TraceListenerInterceptor>().ToList();
                    foreach (var listener in listeners)
                    {
                        listener.Dispose();
                        Trace.Listeners.Remove(listener);
                    }
                    foreach (var olistener in OriginalListeners)
                        Trace.Listeners.Add(olistener);
                    OriginalListeners.Clear();
                }
                disposedValue = true;
            }
        }
        private bool disposedValue; // Pour détecter les appels redondants

        #endregion


        readonly List<TraceListener> OriginalListeners = new List<TraceListener>();

        // msg, detailsMsg, isError (false is for warning)
        public enum eFailedAssertionAnswer
        {
            Abort,
            Retry,
            Ignore
        }
        public Func<string, string, StackTrace, bool, eFailedAssertionAnswer> DisplayFailedAssertion = null;
        public bool StoreFailedAssertions = true;

        class TraceListenerInterceptor : TraceListener
        {
            public DefaultAssertionManager Manager { get; }

            public TraceListenerInterceptor(DefaultAssertionManager manager)
            {
                Manager = manager;
                DebugTools.ShouldAssertFail += DebugTools_ShouldAssertFail;
            }
            internal bool Enable { get; set; } = true;

            protected override void Dispose(bool disposing)
            {
                if (!_disposedValue)
                {
                    if (disposing)
                    {
                        DebugTools.ShouldAssertFail -= DebugTools_ShouldAssertFail;
                    }
                    _disposedValue = true;
                }
            }
            bool _disposedValue = false; // Pour détecter les appels redondants

            private void DebugTools_ShouldAssertFail(string msg)
            {
                _Fail(msg, null, true);
            }

            [DebuggerHidden, DebuggerStepThrough]
            [MethodImpl(MethodImplOptions.NoInlining)]
            public override void Fail(string msg, string detailedMsg)
            {
                _Fail(msg, detailedMsg, false);
            }
            [DebuggerHidden, DebuggerStepThrough]
            [MethodImpl(MethodImplOptions.NoInlining)]
            public void _Fail(string msg, string detailedMsg, bool isAssertOnlyShould)
            {
                if (!DebugTools.IsForDevelopper ||
                     Manager.MachineNameOnWhichFailedAssertionPopupIsDisabled.Contains(Environment.MachineName))
                    return;

                // En cas d'assertion qui echoue, on ne l'affiche (via la gestion par defaut) que si le debugger est attaché.
                // Cela signifie que c'est un developpeur qui execute l'application
                // Dans le cas contraire  (un utilisateur non developpeur executant la version de debug.. ex : le Pole Produit, la MOA, pendant une démo...),
                // on stocke les assertions mais on ne les affiche pas afin de ne pas polluer le comportement des logiciels de test ou de la présentation.
                // Les assertions sont stockées dans la propriété "FailedAssertions" et peuvent être affiché manuellement dans une fenêtre de debug
                StackTrace stack;
                int skipped_frames = 0;
                do
                    stack = new StackTrace(++skipped_frames, true);
                while (stack.ToString().StartsWith("   at System.Diagnostics"));

                // Et dans tous les cas on stocke le problème
                if (Enable && Manager.StoreFailedAssertions)
                    Manager.FailedAssertions.Add(new FailedAssertionInfo(msg, detailedMsg, stack));

                if (Enable && Manager.IsAssertionDisabledFor(stack))
                    return;

                if (isAssertOnlyShould)
                {
                    detailedMsg = null;
                    msg += Environment.NewLine + Environment.NewLine + "This message is JUST a warning (not a real assert)! You can continue your work";
                }

                if (Manager.DisplayFailedAssertion == null || !Enable)
                    foreach (TraceListener listener in Manager.OriginalListeners)
                        listener.Fail(msg, detailedMsg);
                else
                {
                    var isError = !msg.StartsWith(DebugTools.WarningPrefixOnAssertMessage);
#if SUPER_DEBUG
                    // A cause de http://stackoverflow.com/questions/18044707/debug-assert-has-unexpected-side-effects-searching-for-alternatives
                    DebugTools.Break();
#endif
                    var answer = Manager.DisplayFailedAssertion(msg, detailedMsg, stack, isError);
                    if (answer == eFailedAssertionAnswer.Abort)
                        Environment.Exit(-1); // a better way would be Application.Exist because it closes all application's forms properly
                    else if (answer == eFailedAssertionAnswer.Retry)
                        DebugTools.Break();
                }
            }


            [DebuggerHidden, DebuggerStepThrough]
            public override void Write(string message)
            {
                foreach (TraceListener listener in Manager.OriginalListeners)
                    listener.Write(message);
            }

            [DebuggerHidden, DebuggerStepThrough]
            public override void WriteLine(string message)
            {
                foreach (TraceListener listener in Manager.OriginalListeners)
                    listener.WriteLine(message);
            }
        }
    }
    public class FailedAssertionInfo : IssueInfo
    {
        public string Message { get; private set; }
        public string DetailedMessage { get; private set; }

        [DebuggerHidden, DebuggerStepThrough]
        internal FailedAssertionInfo(string msg, string detailedMsg, StackTrace stack)
            : base(stack)
        {
            Message = msg;
            DetailedMessage = detailedMsg;
        }
    }
}
