﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

using TechnicalTools.Model;


namespace TechnicalTools.Diagnostics
{
    public enum eContractOrigin
    {
        /// <summary> Contract between two IT members or teams that belong to the same company </summary>
        TechnicalInternal,
        /// <summary> Contract between two IT members or teams that does NOT belong to the same company </summary>
        TechnicalExternal,
        /// <summary> Contract between two business members or business teams that belong to the same company </summary>
        BusinessInternal,
        /// <summary> Contract between two business members or business teams that does NOT belong to the same company </summary>
        BusinessExternal,
    }
    public class FactAuthor
    {
        public string Firstname { get; }
        public string Lastname  { get; }

        public FactAuthor(string firstname, string lastname)
        {
            if (string.IsNullOrWhiteSpace(firstname))
                throw new ArgumentException("Cannot be empty!", nameof(firstname));
            if (string.IsNullOrWhiteSpace(lastname))
                throw new ArgumentException("Cannot be empty!", nameof(lastname));
            Firstname = firstname;
            Lastname = lastname;
        }
    }
    public class ContractFact
    {
        public string          UniqueRef   { get; }
        public eContractOrigin Origin      { get; }
        public string          Description { get; }
        /// <summary>
        /// when origin is business : Persons that give us their "truth" about the business fact
        /// when origin is technical / IT : Persons that take the _decision_ that thing should work like this
        /// </summary>
        public FactAuthor[]    Authors     { get; }

        public ContractFact(eContractOrigin origin, string description, FactAuthor[] authors, string uniqueRef)
        {
            if (string.IsNullOrWhiteSpace(uniqueRef))
                throw new ArgumentException("Cannot be empty!", nameof(uniqueRef));
            Origin = origin;
            UniqueRef = uniqueRef.Trim();
            Description = description.Trim();
            Authors = authors?.Where(a => a != null).ToArray() ?? Array_Extensions<FactAuthor>.Empty;
            if (Authors.Length == 0)
                if (origin == eContractOrigin.BusinessExternal ||
                    origin == eContractOrigin.TechnicalExternal)
                    throw new ArgumentException("External/Shared contract must have authors provided!");
        }
        static readonly ConcurrentDictionary<string, ContractFact> _AllFacts = new ConcurrentDictionary<string, ContractFact>();
        public ContractFact(eContractOrigin origin, string description, FactAuthor[] authors = null, [CallerFilePath] string fromFile = null, [CallerLineNumber] int fromLine = 0)
            : this(origin, description, authors, Path.GetFileName(fromFile) + ":" + fromLine.ToStringInvariant())
        {
            if (origin == eContractOrigin.BusinessExternal ||
                origin == eContractOrigin.TechnicalExternal)
                throw new ArgumentException("External/Shared contract must have an explicitly provided reference! Use other constructor");
        }

        public static List<ContractFact> GetAllFacts { get { return _AllFacts.Values.ToList(); } }

        /// <summary>
        /// Throw a well formated and typed exception including issueMessageThatShouldBeBlank if issueMessageThatShouldBeBlank is not blank.
        /// Use case:
        /// <code>
        ///   ContractFact someBusinessPeopleSayMeAFieldIsUnique = ...
        ///   someBusinessPeopleSayMeAFieldIsUnique.EnsureIsRespected(values.Distinct(v => v.Field).Count &lt;= 1)
        ///                                        .EnrichDiagnosticWith("We are screwed!, do this do that to fix thing, and prey a little", eExceptionEnrichmentType.Technical)
        ///                                        .ThrowIfNotNull()
        /// </code>
        /// <returns></returns>
        [DebuggerHidden, DebuggerStepThrough]
        public Exception EnsureIsRespected(string issueMessageThatShouldBeBlank)
        {
            if (!string.IsNullOrWhiteSpace(issueMessageThatShouldBeBlank))
                return BuildException(issueMessageThatShouldBeBlank.Trim());
            return null;
        }
        [DebuggerHidden, DebuggerStepThrough]
        public Exception EnsureIsRespected(bool cond)
        {
            if (!cond)
                throw BuildException(null);
            return null;
        }
        Exception BuildException(string additionalData)
        {
            var msg = "A contract fact is not respected!" + Environment.NewLine +
                      "Contract's Reference: " + UniqueRef + Environment.NewLine +
                      "Contract's Description: " + Description +
                      (Authors.Length == 0 ? "" : Environment.NewLine +
                        "contact(s) who may help: " + Authors.Select(a => a.Firstname + " " + a.Lastname).Join()) +
                      (additionalData == null ? "" : Environment.NewLine +
                        "Additional information: " + additionalData);
            if (Origin == eContractOrigin.BusinessExternal ||
                Origin == eContractOrigin.BusinessInternal)
                return new BusinessException(msg, null);
            else
                return new TechnicalException(msg, null);
        }
    }
}
