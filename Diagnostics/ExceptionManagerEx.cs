﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.UI;


namespace TechnicalToolsClassic.Diagnostics
{
    /// <summary>
    ///  Ce manager permet de gérer tous les cas d'erreur et propose 3 fonctionnalités majeur
    ///  - Gerer les exception ignorable (parce que le développeur sait comment palier au problème ou qu'il a decidé volontairement d'ignorer certaines exceptions),
    ///  - Enrichir une exception en "re-throwant" (TODO)
    ///  - Transformer une exception en texte adapté à l'utilisateur et fournissant des informations pertinentes.
    /// </summary>
    public class DefaultExceptionManager : TechnicalTools.Diagnostics.DefaultExceptionManager
    {
        protected override void Init()
        {
            Application.ThreadException += OnApplicationThreadException;
            // Sans ce test les controls utilisant des User Control qui hérite de Devexpress (XtraUserControl) ne fonctionne pas dans le designer de VS
            // Les classes nécéssitant ce test sont celles dont le nom contient "RepositoryItem", voir https://documentation.devexpress.com/#WindowsForms/CustomDocument4716
            if (!DesignTimeHelper.IsInDesignMode)
            {
                try
                {
                    Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                }
                catch
                {
                    DebugTools.Break("To make this exception disappeared, please write this line of code in the beginning of Program.cs :" + Environment.NewLine +
                                     "ExceptionManager.Initialize();");
                }
            }
            base.Init();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                Application.ThreadException -= OnApplicationThreadException;
            base.Dispose(disposing);
        }

        // TODO : Tester le cas d'un traitement long  qui déclenche 50 exceptions (division par zero sur plusieur element business ou client independant de l'entreprise)
        //        => l'utilisateur devra fermer 50 fenêtres => pas genial (todo : deplacer ce commentaire la ou on affiche la form)
        // Donc à faire : Ne pas les afficher en modal,
        //                Détecter si une fenêtre est programmée pour être affiché et ajouter l'erreur suplementaire a celle ci le cas echeant
        //                Pour cela, s'inspirer de la classe PostponingExecuter
        // Lire TOUS ces liens avant de faire :
        //     http://msdn.microsoft.com/en-us/library/system.windows.forms.application.threadexception.aspx
        //     http://msdn.microsoft.com/en-us/library/system.appdomain.unhandledexception.aspx
        //     http://msdn.microsoft.com/en-us/library/system.windows.forms.application.setunhandledexceptionmode%28v=vs.110%29.aspx
        //     pour voir si on a pas loupé des features intéressantes : http://stackoverflow.com/questions/2014562/whats-the-difference-between-application-threadexception-and-appdomain-currentd
        //     pour anticiper (mutli thread des traitement) : http://msdn.microsoft.com/en-us/library/dd997415(v=vs.110).aspx
        //     Cas de test : les binding qui foirent :) http://stackoverflow.com/questions/24702330/exception-in-bound-propertys-set-method-not-caught-by-application-threadexcepti
        void OnApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            NotifyException(BaseException.TryFindWrappingException(e.Exception), UnexpectedExceptionManager.eExceptionKind.Unhandled, null);
        }
    }
}
