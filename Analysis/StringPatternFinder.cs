﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;


namespace TechnicalTools.Analysis
{
    public class StringPatternFinder<TItem>
        where TItem : class
    {
        readonly Func<TItem, string> _getStrToAnalyse;

        public StringPatternFinder(Func<TItem, string> getStrToAnalyse)
        {
            Debug.Assert(getStrToAnalyse != null);
            _getStrToAnalyse = getStrToAnalyse;
        }

        readonly ConditionalWeakTable<TItem, StringPattern> _patternByObjects = new ConditionalWeakTable<TItem, StringPattern>();
        readonly ConcurrentDictionary<string, StringPattern> _patternByPatterns = new ConcurrentDictionary<string, StringPattern>();

        public class StringPattern
        {
            public string Pattern { get; internal set; }
            public int    Id      { get; internal set; }
        }

        public StringPattern GetPattern(TItem owner)
        {
            if (owner == null)
                throw new ArgumentNullException(nameof(owner));
            return _patternByObjects.GetValue(owner, o => BuildPattern(_getStrToAnalyse(o)));
        }
        public void UpdatePattern(TItem owner)
        {
            if (owner == null)
                throw new ArgumentNullException(nameof(owner));
            _patternByObjects.Remove(owner);
        }

        StringPattern BuildPattern(string str)
        {
            var pattern = _patternByPatterns.GetOrAdd(ProcessPattern(str), patternStr => new StringPattern()
            {
                Pattern = patternStr,
                Id = Interlocked.Increment(ref _seedId)
            });
            return pattern;
        }
        int _seedId;


        string ProcessPattern(string request)
        {
            string str = request;
            foreach (var pattern in _patterns)
            {
                Match m = pattern.Match(str);
                if (m.Success)
                //while ((m = pattern.re.Match(str)).Success)
                {
                    int g = m.Groups.Count - 1;
                    foreach (var grp in m.Groups.Cast<Group>().Skip(1).Reverse())
                    {
                        int c = grp.Captures.Count;
                        foreach (Capture capture in grp.Captures.Cast<Capture>().Reverse())
                        {
                            string id = "¤";
                            id += string.Join("_", new[] { (m.Groups.Count == 2 ? null : g.ToString()),
                                                               grp.Captures.Count == 1 ? null : c.ToString() }.Where(s => s != null));
                            str = str.Remove(capture.Index) + id + str.Substring(capture.Index + capture.Length);
                            --c;
                        }
                        --g;
                    }
                }
            }
            return str;
        }


        readonly List<Regex> _patterns = new List<Regex>()
            {
                new Regex("((?<number>[0-9]+)[^0-9]*)+", reOpts)
            };
        static readonly RegexOptions reOpts = RegexOptions.Compiled | RegexOptions.ExplicitCapture;
    }
}
