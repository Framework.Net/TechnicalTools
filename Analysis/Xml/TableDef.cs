﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;


namespace TechnicalTools.Analysis
{
    public class TableDef
    {
        public string           Schema   { get; protected internal set; }
        public string           Name     { get; protected internal set; }
        public List<ColumnDef>  Columns  { get; protected internal set; } = new List<ColumnDef>();
        public List<TableDef>   Children { get; protected internal set; } = new List<TableDef>();

        public IEnumerable<ColumnDef> RegularColumns { get { return Columns.Where(c => !c.IsPK && c.FkTo == null); } }
        public ColumnDef Pk                          { get { return Columns.SingleOrDefault(c => c.IsPK); } }
        public ColumnDef Fk                          { get { return Columns.SingleOrDefault(c => c.FkTo != null); } }

        #region Metadata part
        /// <summary> Original node that is mapped on this table def </summary>
        public XElement RelatedNode { get; protected internal set; }
        /// <summary> Indicate if parent of RelatedNode can have lultiple child of same type</summary>
        public bool     HasSibling  { get { return SiblingNode != null; } }
        public XElement SiblingNode { get; protected internal set; }
        //public Func<string, string, string> GenerateCSharpCode { get; protected internal set; }
        #endregion

        public void SetSchemaRecursive(string schema)
        {
            Schema = schema;
            foreach (var child in Children)
                child.SetSchemaRecursive(schema);
        }
        public string ToSqlScript(bool withChildren, bool onlyRegularColumns = false)
        {
            var res = "CREATE TABLE "
                    + "[".AsPrefixForIfNotBlank(Schema).IfNotBlankAddSuffix("].")
                    + "[" + Name + "]" + Environment.NewLine;
            res += "(" + Environment.NewLine;
            res += (onlyRegularColumns ? RegularColumns : Columns)
                    .Select(col => "    " + col.ToSqlScript())
                    .Join("," + Environment.NewLine);
            res += Environment.NewLine;
            res += ")";

            if (withChildren)
            {
                res += Children.Count > 0 ? Environment.NewLine + Environment.NewLine + Environment.NewLine : "";
                res += Children.Select(child => child.ToSqlScript(withChildren, onlyRegularColumns)).Join(Environment.NewLine + Environment.NewLine);
            }
            return res;
        }
        public override string ToString() { return ToSqlScript(false, true); }
    }
}
