﻿using System;
using System.Linq;
using System.Xml.Linq;


namespace TechnicalTools.Analysis
{
    public class ColumnDef
    {
        public TableDef     Owner                   { get; protected internal set; }
        public string       Name                    { get; protected internal set; }
        public string       SqlType                 { get; protected internal set; }
        public bool         Nullable                { get; protected internal set; }
        public bool         IsPK                    { get; protected internal set; }
        public bool         IsAutoIncrementable     { get; protected internal set; }
        public ColumnDef    FkTo                    { get; protected internal set; }

        #region Metadata part
        /// <summary> Original node that is mapped on this table def </summary>
        public XObject  RelatedNode { get; protected internal set; }
        /// <summary> Indicate if parent of RelatedNode can have lultiple child of same type</summary>
        public bool     HasSibling  { get { return SiblingNode != null; } }
        public XObject  SiblingNode { get; protected internal set; }
        //public Func<string, string, string> GenerateCode { get; protected internal set; }
        #endregion

        public string ToSqlScript()
        {
            var res = "";
            res += "[" + Name + "]";
            res += " " + SqlType;
            res += " " + (Nullable ? "" : "NOT ") + "NULL";
            if (IsPK)
                res += "  PRIMARY KEY";
            if (IsAutoIncrementable)
                res += "  IDENTITY(1,1)";
            if (FkTo != null)
                res += "  FOREIGN KEY REFERENCES " + "[".AsPrefixForIfNotBlank(FkTo.Owner.Schema).IfNotBlankAddSuffix("].")
                                                    + "[" + FkTo.Owner.Name + "]"
                                                    + "(" + FkTo.Owner.Columns.Single(col => col.IsPK).Name + ")";
            return res;
        }
        public override string ToString() { return ToSqlScript(); }
    }
}
