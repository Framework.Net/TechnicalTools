﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;


namespace TechnicalTools.Analysis
{
    public partial class XmlSkeletonFinder
    {
        readonly Dictionary<Tuple<XElement, XElement>, decimal> _pathProba = new Dictionary<Tuple<XElement, XElement>, decimal>();
        readonly Dictionary<XElement, int> _parentCounts = new Dictionary<XElement, int>();
        readonly Dictionary<XElement, XElement> _seconds = new Dictionary<XElement, XElement>();
        readonly List<Tuple<XElement, XElement>> _addNamespaces = new List<Tuple<XElement, XElement>>();

        public XDocument BuildSkeletonFromFiles(IReadOnlyCollection<string> files, IProgress<string> pr = null)
        {
            return BuildSkeletonFromText(files.Select(f => File.ReadAllText(f)), files.Count, pr);
        }
        public XDocument BuildSkeletonFromText(IEnumerable<string> xmlSimilarPieces, int fileCount, IProgress<string> pr = null)
        {
            pr = pr ?? new Progress<string>();
            _pathProba.Clear();
            _parentCounts.Clear();
            _seconds.Clear();
            _addNamespaces.Clear();

            var nRootMerged = new XElement("VIRTUAL_ROOT");
            var xmlDocMerged = new XDocument(nRootMerged);

            var subpr = pr.WrapReportWith("Building model...");
            bool fileRootNodeNameAreDifferent = ReadAllNodes(xmlSimilarPieces, nRootMerged.Name.LocalName,
                                                            (pieceIndex, virtualRoot) =>
                                                            {
                                                                subpr.Report("Reading file " + pieceIndex + " / " + fileCount);
                                                                MapSkeleton(virtualRoot, nRootMerged);
                                                            });

            pr.Report("Finalizing skeleton");
            FinalizingSkeleton();
            pr.Report("Cleaning");
            var warnComment = CleanSkeletonAndWarn(nRootMerged, fileRootNodeNameAreDifferent);
            var res = ReparseAndTransferData(xmlDocMerged, nRootMerged, fileRootNodeNameAreDifferent, warnComment);
            return res;
        }



        public static bool ReadAllNodes(IEnumerable<string> xmlSimilarPieces, string virtualNodeName, Action<int, XElement> treat)
        {
            string lastRootNodeName = null;
            var fileRootNodeNameAreDifferent = false;
            int pieceIndex = -1;
            foreach (var text in xmlSimilarPieces)
            {
                ++pieceIndex;
                var matches = reXmlPrologDeclaration.Matches(text).Cast<Match>().ToList();
                // rare case :
                if (matches.Count == 0)
                    matches.Add(Match.Empty);
                if (matches.Count == 1 && matches[0].Index > 0 && string.IsNullOrWhiteSpace(text.Remove(matches[0].Index)))
                    matches.Insert(0, null);
                foreach (var mm in matches.Zip(matches.Skip(1).Concat((Match)null), (from, end) => new { from, end }))
                {
                    var start = mm.from?.Index + mm.from?.Length ?? 0;
                    var textpart = (mm.from.Value ?? "")
                                 + "<" + virtualNodeName + ">"// Create a virtual node so we align node with mapping. Plus, this allow to handle xml file with multiple root node
                                 + text.Substring(start, (mm.end?.Index ?? text.Length) - start)
                                 + "</" + virtualNodeName + ">";
                    using (var sr = new StringReader(textpart))
                    {
                        var doc = XDocument.Load(sr);

                        if (!fileRootNodeNameAreDifferent)
                        {
                            var grps = doc.Root.Elements().GroupBy(elt => elt.Name.LocalName);
                            fileRootNodeNameAreDifferent = grps.Count() > 1;
                            if (!fileRootNodeNameAreDifferent && grps.Count() == 1)
                            {
                                if (lastRootNodeName == null)
                                    lastRootNodeName = grps.First().Key;
                                else
                                    fileRootNodeNameAreDifferent = grps.First().Key != lastRootNodeName;
                            }
                        }
                        treat(pieceIndex, doc.Root);
                    }
                }
            }
            return fileRootNodeNameAreDifferent;
        }
        static readonly Regex reXmlPrologDeclaration = new Regex("<[?]xml [^>]*>", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        void MapSkeleton(XElement curParent, XElement mapParent)
        {
            _parentCounts[mapParent] = (_parentCounts.TryGetValueStruct(mapParent) ?? 0) + 1;

            var metNames = new HashSet<string>();
            foreach (var node in curParent.Nodes())
            {
                if (node.NodeType == XmlNodeType.Text)
                {
                    var mapText = mapParent.Nodes().OfType<XText>().SingleOrDefault();
                    if (mapText == null)
                        mapParent.Add(new XText((node as XText).Value));
                    else if (mapText.Value.Length < (node as XText).Value.Length)
                        mapText.Value = (node as XText).Value;
                }
                else if (node.NodeType == XmlNodeType.Element)
                {
                    var curNode = (XElement)node;
                    var mapNode = MapXElement(curParent, mapParent, curNode);
                    if (!metNames.Contains(curNode.Name.LocalName))
                    {
                        var key = Tuple.Create(mapParent, mapNode);
                        _pathProba[key] = (_pathProba.TryGetValueStruct(key) ?? 0) + 1;
                        metNames.Add(curNode.Name.LocalName);
                    }
                    MapSkeleton(curNode, mapNode);
                }
                else
                    Debug.Assert(node.NodeType == XmlNodeType.Comment);
            }
        }

        XElement MapXElement(XElement curParent, XElement mapParent, XElement curNode)
        {
            var index = Math.Min(curNode.IndexForThisName(), 1);
            var mappedNodes = mapParent.XPathSelectElements(curNode.Name.LocalName);
            var mapNode = mappedNodes.ElementAtOrDefault(index);
            if (mapNode == null)
            {
                if (index == 0)
                {
                    mapParent.Add(mapNode = new XElement(curNode.Name.LocalName));
                    // if original node is empty we add a blank text so it will be serialized as <foo></foo> instead of <foo />
                    // Because "<foo />" has the special meaning of "foo can exist multiple times"
                    // While "<foo></foo>" has the meaning of "foo may contains some text"
                    if (curNode.Nodes().OfType<XElement>().IsEmpty() &&
                        curNode.Nodes().OfType<XText>().All(n => string.IsNullOrWhiteSpace(n.Value)))
                        mapNode.Add(new XText(""));
                }
                else
                {
                    mapNode = mappedNodes.ElementAt(0);
                    var second = new XElement(curNode.Name.LocalName);
                    mapNode.AddAfterSelf(second);
                    Debug.Assert(mapNode != null);
                    _seconds.Add(mapNode, second);
                }
            }
            else if (index != 0)
                mapNode = mappedNodes.ElementAt(0);


            foreach (var curAtt in curNode.Attributes())
            {
                var mapAtt = mapNode.Attribute(curAtt.Name);
                if (mapAtt == null && !curAtt.IsNamespaceDeclaration)
                {
                    mapAtt = new XAttribute(curAtt);
                    Debug.Assert(mapAtt.Name == curAtt.Name);
                    mapNode.Add(mapAtt);
                }
            }
            if (curNode.Attributes().Any(att => att.IsNamespaceDeclaration))
                // We delay this so XPathSelectElements call above works
                _addNamespaces.Add(Tuple.Create(curNode, mapNode));
            return mapNode;
        }


        protected virtual void FinalizingSkeleton()
        {
            foreach (var kvp in _pathProba)
            {
                var parentCount = _parentCounts[kvp.Key.Item1];
                var probability = kvp.Value == parentCount ? 100
                                : Math.Max(0.01m, Math.Min(99.99m, Math.Round(kvp.Value * 100 / parentCount, 2)));
                var proba = new ProbabilityAnnotation()
                {
                    ProbabilityOfAppearance = probability,
                    ParentNodeCount = parentCount
                };
                kvp.Key.Item2.AddAnnotation(proba);
                Debug.Assert(kvp.Key.Item2.Annotation<ProbabilityAnnotation>() == proba);
                kvp.Key.Item2.AddBeforeSelf(new XComment("Probability: " + proba.ProbabilityOfAppearance.ToString("0.00'%'") + " parent node count:" + proba.ParentNodeCount));
            }

            var namespaceNameInComment = new Dictionary<XElement, List<XNamespace>>();
            foreach (var nsNodes in _addNamespaces)
            {
                var xNode = nsNodes.Item1;
                var map = nsNodes.Item2;
                foreach (var att in xNode.Attributes())
                    if (att.IsNamespaceDeclaration)
                    {
                        var mapAtt = map.Attribute(att.Name);
                        var ns = XNamespace.Get(att.Value);
                        if (mapAtt == null)
                        {
                            if (map.Name.Namespace == "")
                                foreach (var node in map.EnumerateAllSubElements())
                                    node.Name = ns + node.Name.LocalName;
                            mapAtt = new XAttribute(att);
                            Debug.Assert(mapAtt.Name == att.Name);
                            map.Add(mapAtt);
                        }
                        else if (map.Name.Namespace != ns)
                        {
                            if (!namespaceNameInComment.ContainsKey(map))
                                namespaceNameInComment.Add(map, new List<XNamespace>());
                            if (!namespaceNameInComment[map].Contains(ns))
                            {
                                namespaceNameInComment[map].Add(ns);
                                map.AddBeforeSelf(new XComment("there is also xmlns=" + ns));
                            }
                        }
                    }
            }

        }
        public class ProbabilityAnnotation
        {
            /// <summary>
            /// Indicate the probability of appearance of at least one of name where this annotation is attached
            /// when its parent node is met.
            /// </summary>
            public decimal ProbabilityOfAppearance { get; internal set; }
            /// <summary>
            /// Indicate the number of parent node on which this probabilty is based.
            /// </summary>
            public int ParentNodeCount { get; internal set; }
        }

        protected virtual XComment CleanSkeletonAndWarn(XElement nRootMerged, bool fileRootNodeNameAreDifferent)
        {
            XComment comment = null;
            // To do after all edits on tree
            // because when we remove "node" from tree using Remove() the "parent" property of
            // its children is not updated so AddBeforeSelf method does not work.
            if (fileRootNodeNameAreDifferent)
            {
                comment = new XComment("Some file contains different root nodes:/ !" + Environment.NewLine +
                                       "This is non standard xml file (only one root node is allowed)" + Environment.NewLine +
                                       "This node has been added to make this file compliant with XML");
                nRootMerged.AddBeforeSelf(comment);
            }
            else
            {
                Debug.Assert(nRootMerged.Elements().Count() <= 2);
                if (nRootMerged.Elements().Count() == 2)
                {
                    var first = nRootMerged.Elements().First();
                    var last = nRootMerged.Elements().Last();
                    Debug.Assert(first.Name == last.Name);
                    Debug.Assert(last.Nodes().IsEmpty());
                    last.Remove();
                    comment = new XComment("Some files contain multiple root node \"" + last.Name.LocalName + "\"!" + Environment.NewLine +
                                           "Which makes these files non standard XML document (only one root node is allowed)");
                    nRootMerged.Nodes().First().AddBeforeSelf(comment);
                }
                // we use Nodes and not Elements() to take any comment nodes...
                var children = nRootMerged.Nodes();
                var doc = nRootMerged.Document;
                nRootMerged.Remove();
                doc.Add(children);
            }
            return comment;
        }

        protected virtual XDocument ReparseAndTransferData(XDocument xmlDocMerged, XElement nRootMerged, bool fileRootNodeNameAreDifferent, XComment warnComment)
        {
            // Reparse to add metadata (line number etc) for any "View" layer
            var res = XDocument.Parse(xmlDocMerged.ToStringWithProlog(), LoadOptions.SetLineInfo);
            int count;
            // we dont use docMergedNodes = xmlDocMerged.DescendantNodes()
            // because node instances returned are not the same (reference) than the one when we call nRootMerged.DescendantNodes() !!!
            // So we process docMergedNodes like this...
            IEnumerable<XNode> docMergedNodes;
            if (fileRootNodeNameAreDifferent)
            {
                docMergedNodes = warnComment == null ? Enumerable.Empty<XNode>() : new XNode[] { warnComment };
                docMergedNodes = docMergedNodes.Concat(nRootMerged.DescendantNodesAndSelf());
            }
            else
                docMergedNodes = nRootMerged.DescendantNodes();
            var map = new Dictionary<XNode, XNode>();
            using (var e1 = docMergedNodes.GetEnumerator())
            using (var e2 = res.DescendantNodes().GetEnumerator())
                while (2 == (count = (e1.MoveNext() ? 1 : 0) + (e2.MoveNext() ? 1 : 0)))
                {
                    if (e1.Current is XText && (e1.Current as XText).Value == "")
                    {
                        map.Add(e1.Current, null);
                        count += (e1.MoveNext() ? 1 : 0) - 1;
                        Debug.Assert(count == 2);
                    }
                    map.Add(e1.Current, e2.Current);
                    Debug.Assert((e1.Current as XElement)?.Name == (e2.Current as XElement)?.Name);
                    var proba = e1.Current.Annotation<ProbabilityAnnotation>();
                    if (proba != null)
                        e2.Current.AddAnnotation(proba);
                }
            foreach (var kvp in map)
                if (kvp.Value == null)
                {
                    var parent = map[kvp.Key.Parent] as XElement;
                    Debug.Assert(parent != null && !parent.Nodes().Any());
                    parent.Add(new XText(""));
                }
            Debug.Assert(count != 1);
            return res;
        }
    }
}
