﻿using System;
using System.Collections.Concurrent;


namespace TechnicalTools.Logs
{
    public class LogManager
    {
        // Must be filled by program. This can be an specialized instance inherited from current class...
        public static LogManager Default { get; set; } = UI.DesignTimeHelper.IsInDesignMode ? new LogManager() : null;

        protected virtual ILogger CreateLogger(string name)
        {
            return new Logger(this, name);
        }
        public virtual ILogger CreateLogger(Type type)
        {
            return new Logger(this, type);
        }

        public ILogger GetLogger(Type type)
        {
            return _loggerByType.GetOrAdd(type, t => CreateLogger(t));
        }
        readonly ConcurrentDictionary<Type, ILogger> _loggerByType = new ConcurrentDictionary<Type, ILogger>();

        public ILogger GetLogger(string name)
        {
            return _loggerByName.GetOrAdd(name, n => CreateLogger(n));
        }
        readonly ConcurrentDictionary<string, ILogger> _loggerByName = new ConcurrentDictionary<string, ILogger>();

        public event Action<ILogger, ILog> Log;

        internal void RaiseLog(ILogger logger, ILog log) { Log?.Invoke(logger, log); }
    }
}
