﻿using System;
using System.Globalization;
using TechnicalTools.Annotations.Design;


namespace TechnicalTools.Logs
{
    public interface ILog
    {
                  DateTime  TimestampUTC   { get; set; }
                  Level     Level          { get; set; }
                  int       ThreadId       { get; set; }
                  int?      ParentThreadId { get; set; }
                  string    Hostname       { get; set; }
                  string    Type           { get; set; }
        [NotNull] string    Message        { get; set; }
        /// <summary> Comma separated tags </summary>
                  string    Tags           { get; set; }
    }

    public static class ILog_Extensions
    {
        public static string ToFormatedMessage(this ILog log, bool withApplicationInfo = true, string separator = " | ")
        {
            return (withApplicationInfo ? log.Hostname : string.Empty).IfNotBlankAddSuffix(separator)
                  + log.TimestampUTC.ToString("yyyy/MM/dd HH:mm:ss.fff") + separator
                  + log.Level.Name + separator
                  + (log.ParentThreadId == null ? null : log.ParentThreadId + "-") + log.ThreadId.ToString(CultureInfo.InvariantCulture) + separator
                  + log.Type + separator
                  + log.Message
                  + " #".AsPrefixForIfNotBlank(log.Tags == null ? string.Empty : log.Tags.Split(',').Join(" #"));
        }
    }
}
