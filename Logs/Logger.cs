using System;
using System.Collections.Generic;
using System.Linq;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;


namespace TechnicalTools.Logs
{
    public class Logger : ILogger
    {
        public LogManager Manager { get; private set; }
        public string     Name    { get; private set; }
        public Type       Type    { get; private set; }

        protected internal Logger(LogManager manager, string name)
        {
            Manager = manager;
            Name = name;
        }

        protected internal Logger(LogManager manager, Type type, string name = null)
        {
            Manager = manager;
            Type = type;
            Name = name ?? type.ToSmartString();
        }

        protected virtual ILog CreateLog()
        {
            return new Log();
        }

        protected virtual ILog BuildLog(string message, Exception ex, Level level, IEnumerable<string> tags)
        {
            var log = CreateLog();

            log.TimestampUTC   = DateTime.UtcNow;
            log.Level          = level;
            log.ThreadId       = System.Threading.Thread.CurrentThread.ManagedThreadId;
            //log.ParentThreadId = null; // will be handled in futur
            log.Hostname       = Environment.MachineName;
            log.Type           = Name;
            log.Message        = message ?? string.Empty;
            if (ex != null)
            {
                if (!string.IsNullOrEmpty(message))
                    log.Message += Environment.NewLine;
                log.Message += (ex is IUserUnderstandableException ? TicketIdLogLabel : BugIdLogLabel) + ex.GetUniqueBugId() + Environment.NewLine;
                log.Message += ExceptionManager.Instance.Format(ex, false, false);
            }

            log.Tags = ExceptionTypeToTag(ex) ?? string.Empty;
            var others = tags == null
                       ? string.Empty
                       // no space after comma to speed up log treatement later (no trim() to call)
                       : string.Join(",", tags);
            if (others.Length > 0)
            {
                if (log.Tags.Length > 0)
                {
                    // no space after comma to speed up log treatement later (no trim() to call)
                    log.Tags += ',';
                }
                log.Tags += others;
            }
            return log;
        }
        public static string TicketIdLogLabel { get; } = "Ticket Id:";
        public static string BugIdLogLabel    { get; } = "Bug Id:";

        string ExceptionTypeToTag(Exception ex)
        {
            if (ex == null)
                return null;
            if (ex is ISilentBusinessException)
                return LogTags.DummySilenced;
            if (ex is IBusinessException)
                return LogTags.BusinessCheck;
            if (ex is IUserUnderstandableException)
                return LogTags.TechnicalError;
            // Detected by a developper working in our company or working on the open-source framework!
            if (ex is ITechnicalException)
                return LogTags.DetectedBug;
            // Detected by a developper working in our company or working on the open-source framework!
            // But his exception class should implement ITechnicalException or IUserUnderstandableException, so we are not sure
            if (ex is IBaseException)
                return LogTags.PotentialDetectedBug;
            // all other arer unexpected error
            return LogTags.UnknownBug;
        }


        public virtual void Debug(string message, Exception ex = null, params string[] tags) { Manager.RaiseLog(this, BuildLog(message,   ex, Level.Debug, tags)); }
        public virtual void Debug(string message,                      params string[] tags) { Manager.RaiseLog(this, BuildLog(message, null, Level.Debug, tags)); }
        public virtual void Info (string message, Exception ex = null, params string[] tags) { Manager.RaiseLog(this, BuildLog(message,   ex, Level.Info,  tags)); }
        public virtual void Info (string message,                      params string[] tags) { Manager.RaiseLog(this, BuildLog(message, null, Level.Info,  tags)); }
        public virtual void Warn (string message, Exception ex = null, params string[] tags) { Manager.RaiseLog(this, BuildLog(message,   ex, Level.Warn,  tags)); }
        public virtual void Warn (string message,                      params string[] tags) { Manager.RaiseLog(this, BuildLog(message, null, Level.Warn,  tags)); }
        public virtual void Error(string message, Exception ex = null, params string[] tags) { Manager.RaiseLog(this, BuildLog(message,   ex, Level.Error, tags)); }
        public virtual void Error(                Exception ex       , params string[] tags) { Manager.RaiseLog(this, BuildLog(null,      ex, Level.Error, tags)); }
        public virtual void Error(string message,                      params string[] tags) { Manager.RaiseLog(this, BuildLog(message, null, Level.Error, tags)); }
        public virtual void Fatal(string message, Exception ex = null, params string[] tags) { Manager.RaiseLog(this, BuildLog(message,   ex, Level.Fatal, tags)); }
        public virtual void Fatal(string message,                      params string[] tags) { Manager.RaiseLog(this, BuildLog(message, null, Level.Fatal, tags)); }

        public virtual void Audit(string message, Exception ex = null, params string[] tags) { Manager.RaiseLog(this, BuildLog(message,   ex, Level.Notice, tags)); }
        public virtual void Audit(string message,                      params string[] tags) { Manager.RaiseLog(this, BuildLog(message, null, Level.Notice, tags)); }
    }
}
