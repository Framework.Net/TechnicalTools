﻿using System;

using TechnicalTools.Tools;


namespace TechnicalTools.Logs
{
    public abstract class DeferredLogHandler<TLog> : ThreadDeferredObjectHandler<TLog>
        where TLog:  class, ILog
    {
        public DeferredLogHandler(LogManager manager, object mainOwner, params ITinyService[] dependencies)
            : base(mainOwner, dependencies)
        {
            LogException = false; // we won't log about ... log handling
            _manager = manager;
            _manager.Log += OnAnyLog;
        }
        readonly LogManager _manager;

        public override bool Release(object owner)
        {
            bool removed = base.Release(owner);
            if (IsStopping)
                _manager.Log -= OnAnyLog;
            return removed;
        }

        protected virtual void OnAnyLog(ILogger logger, ILog log)
        {
            if (!Enabled)
                return;
            ScheduleHandle((TLog)log);
        }
    }
}
